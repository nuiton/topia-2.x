.. -
.. * #%L
.. * ToPIA
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 2004 - 2014 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

=======
Accueil
=======

Presentation
------------

ToPIA stands for Tools for Portable and Independent Architecture. It is a
technical platform abstraction framework. This documentation is not up-to-date
and in French. We are in the process to update this documentation, we will
translate it in English at the same time.

Présentation
------------

ToPIA, pour Tools for Portable and Independant Architecture, est un framework 
d'abstraction des plateformes techniques. 

Constitution
------------

Il est actuellement composé d'un module principal :

  * `ToPIA-persistence`_ : pour la gestion de la persistance sur hibernate

Depuis la version 2.3.3, le module **ToPIA-soa** n'est plus maintenu.

Services
--------

Depuis la version 2.2.0, ToPIA-service a été intégré au projet ToPIA, chaque service devient un module :

  * `ToPIA-service-migration`_ : service de migration de bases sans perte de données.
  * `ToPIA-service-replication`_ : service de replication de données entre 2 bases.
  * `ToPIA-service-security`_ : service de sécurité.

Depuis la version 2.3.3, certains services ne sont plus maintenus (car ils sont à
l'heure actuelle des prototypes et ne sont pas utilisables ad hoc).

  * **ToPIA-service-history** : service d'historisation de données en base.
  * **ToPIA-service-index** : service d'indexation de données.

.. _ToPIA-persistence: ./topia-persistence
.. _ToPIA-service-migration: ./topia-service-migration
.. _ToPIA-service-replication: ./topia-service-replication
.. _ToPIA-service-security: ./topia-service-security
