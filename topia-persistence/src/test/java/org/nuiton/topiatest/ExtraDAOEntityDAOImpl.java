/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.topiatest;

import org.nuiton.topia.TopiaException;

/**
 * Created: 26 mai 2010
 *
 * @author fdesbois &lt;fdesbois@codelutin.com&gt;
 * @version $Id$
 */
public class ExtraDAOEntityDAOImpl<E extends ExtraDAOEntity> extends ExtraDAOEntityDAOAbstract<E> {

    @Override
    public void extra() throws TopiaException {
    }
}
