/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.topiatest.persistence;

import org.nuiton.topia.TopiaException;
import org.nuiton.topia.persistence.EntityVisitor;
import org.nuiton.topia.persistence.TopiaEntityAbstract;

/**
 * Created: 11 mai 2010
 *
 * @author fdesbois &lt;fdesbois@codelutin.com&gt;
 * @version $Id$
 */
public abstract class Entity1Abstract extends TopiaEntityAbstract
        implements Entity1 {

    protected String attr1;

    protected String attr2;

    @Override
    public String getAttr1() {
        fireOnPreRead(ATTR_1, attr1);
        String result = attr1;
        fireOnPostRead(ATTR_1, attr1);
        return result;
    }

    @Override
    public void setAttr1(String attr1) {
        String _oldValue = this.attr1;
        fireOnPreWrite(ATTR_1, _oldValue, attr1);
        this.attr1 = attr1;
        fireOnPostWrite(ATTR_1, _oldValue, attr1);
    }

    @Override
    public String getAttr2() {
        fireOnPreRead(ATTR_2, attr2);
        String result = attr2;
        fireOnPostRead(ATTR_2, attr2);
        return result;
    }

    @Override
    public void setAttr2(String attr2) {
        String _oldValue = this.attr2;
        fireOnPreWrite(ATTR_2, _oldValue, attr2);
        this.attr2 = attr2;
        fireOnPostWrite(ATTR_2, _oldValue, attr2);
    }

    @Override
    public void accept(EntityVisitor visitor) throws TopiaException {
        visitor.start(this);
        visitor.visit(this, ATTR_1, String.class, attr1);
        visitor.visit(this, ATTR_2, String.class, attr2);
        visitor.end(this);
    }
}
