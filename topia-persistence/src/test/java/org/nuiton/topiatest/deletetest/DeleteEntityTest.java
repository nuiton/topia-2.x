/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */


/**
 * DeleteEntityTest.java
 *
 * Created: 4 juin 2009
 *
 * @author Florian Desbois &lt;fdesbois@codelutin.com&gt;
 * @version $Revision$
 *
 * Mise a jour: $Date$
 * par : $Author$
 */

package org.nuiton.topiatest.deletetest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Rule;
import org.junit.Test;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaDatabase;
import org.nuiton.topia.TopiaException;
import org.nuiton.topia.TopiaTestDAOHelper;
import org.nuiton.topia.generator.DAOAbstractTransformer;
import org.nuiton.topiatest.Personne;
import org.nuiton.topiatest.PersonneDAO;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * Deleting tests with DAO and Entities generated with ToPIA (diagram
 * delete-test in topiatest.zargo). Different case of deleting, with inheritance
 * or NMultiplicity relationship between two entities, or both. Initiate from an
 * issue with DAOAbstractGenerator delete method generation. Tests with H2
 * Database. Configuration in src/test/resources/TopiaContextImpl.properties
 */
public class DeleteEntityTest {

    private static final Log log = LogFactory.getLog(DeleteEntityTest.class);

    @Rule
    public final TopiaDatabase db = new TopiaDatabase();

    /**
     * Test for deleting entities with inheritance. Delete from the DAO linked
     * with the subclass entity and from the DAO linked with the superclass
     * entity. In the test model, the two entities have NMultiplicity
     * relationship without association class entity.
     *
     * @throws TopiaException if any exception while manipulating db
     */
    @Test
    public void testDeleteEntityWithInheritance() throws TopiaException {
        log.debug("START TEST : testDeleteEntityWithInheritance");

        TopiaContext transaction = db.beginTransaction();

        log.debug("DAO : PersonneDAO");
        PersonneDAO dao = TopiaTestDAOHelper.getPersonneDAO(transaction);

        log.debug("CREATE PERSONNE : Bob Marley");
        Personne personne = dao.create(Personne.PROPERTY_NAME, "Bob Marley");
        transaction.commitTransaction();
        String idPersonne = personne.getTopiaId();
        assertNotNull(idPersonne);
        log.debug("ENTITY PERSONNE SAVED !");

        log.debug("DELETE PERSONNE");
        dao.delete(personne);
        transaction.commitTransaction();
        Personne res = dao.findByTopiaId(idPersonne);
        assertNull(res);
        log.debug("ENTITY PERSONNE DELETED !");

        log.debug("CREATE PERSONNE : Ziggy Marley");
        Personne personne2 = dao.create(Personne.PROPERTY_NAME, "Ziggy Marley");
        transaction.commitTransaction();
        String idPersonne2 = personne2.getTopiaId();
        assertNotNull(idPersonne2);
        log.debug("ENTITY PERSONNE SAVED !");

        log.debug("DAO parent (abstract) : PartyDAO");
        Party2DAO dao2 = TopiaTestDAOHelper.getParty2DAO(transaction);

        log.debug("DELETE PERSONNE with PartyDAO");
        dao2.delete(personne2);
        transaction.commitTransaction();
        Party2 res2 = dao2.findByTopiaId(idPersonne2);
        assertNull(res2);
        log.debug("ENTITY PERSONNE DELETED !");


    }

    /**
     * Test for deleting entities with NMultiplicity relation without
     * association class entity. Test DAO generation for deleting references
     * between two entities with NMultiplicity relation. In the test model, the
     * two entities have both inheritance.
     *
     * @throws TopiaException if any exception while manipulating db
     * @see DAOAbstractTransformer
     */
    @Test
    public void testDeleteEntityWithManyToManyRelation() throws TopiaException {
        log.debug("START TEST : testDeleteEntityWithManyToManyRelation");

        TopiaContext transaction = db.beginTransaction();

        PersonneDAO dao = TopiaTestDAOHelper.getPersonneDAO(transaction);

        log.debug("CREATE PERSONNE : Bob Marley");
        Personne personne = dao.create(Personne.PROPERTY_NAME, "Bob Marley");
        transaction.commitTransaction();
        String idPersonne = personne.getTopiaId();
        assertNotNull(idPersonne);
        log.debug("ENTITY PERSONNE SAVED !");

        Contact2DAO contactDAO = TopiaTestDAOHelper.getContact2DAO(transaction);

        log.debug("CREATE CONTACT : jaja@codelutin.com");
        Contact2 contact = contactDAO.create(Contact2.PROPERTY_CONTACT_VALUE, "jaja@codelutin.com");
        transaction.commitTransaction();
        String idContact = contact.getTopiaId();
        assertNotNull(idContact);
        log.debug("ENTITY CONTACT SAVED !");

        log.debug("ADD CONTACT TO PERSONNE");
        personne.addContacts(contact);
        transaction.commitTransaction();
        assertEquals(1, personne.getContacts().size());
        log.debug("CONTACT ADDED !");

        log.debug("DELETE PERSONNE");
        dao.delete(personne);
        transaction.commitTransaction();
        Personne res = dao.findByTopiaId(idPersonne);
        assertNull(res);
        log.debug("ENTITY PERSONNE DELETED !");

        assertEquals(0, contact.getParty2().size());

        log.debug("DELETE CONTACT");
        contactDAO.delete(contact);
        transaction.commitTransaction();
        Contact2 res2 = contactDAO.findByTopiaId(idContact);
        assertNull(res2);
        log.debug("ENTITY PERSONNE DELETED !");

    }

}
