/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.topiatest.deletetest;


import java.util.Set;
import java.util.TreeSet;
import org.nuiton.topia.TopiaException;
import org.nuiton.topiatest.Company;
import org.nuiton.topiatest.Employe;

/**
 *
 * @author desbois
 */
public class Contact2DAOImpl<E extends Contact2> extends Contact2DAOAbstract<E> {

    @Override
    public Set<Contact2> findAllByCompany(Company company) throws TopiaException {
        Set<Contact2> contacts = new TreeSet<Contact2>();
        for (Employe e : company.getEmploye()) {
            contacts.addAll(e.getContacts());
        }
        return contacts;
    }
  
}
