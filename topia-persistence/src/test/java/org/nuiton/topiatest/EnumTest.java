/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.topiatest;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaDatabase;
import org.nuiton.topia.TopiaException;
import org.nuiton.topia.TopiaTestDAOHelper;

/**
 * Test the support of possibility to have an attribute of type enumeration
 * in a entity
 */
public class EnumTest {

    @Rule
    public final TopiaDatabase db = new TopiaDatabase();

    /**
     * Create an entity having two field of type enumeration. One is stored
     * using ordinal, the other using the name.
     * 
     * The test check that values are stored, and that find methods works
     *
     * @throws TopiaException if any exception with db
     */
    @Test
    public void storeEntityWithEnumValue() throws TopiaException {
        TopiaContext transaction = db.beginTransaction();

        PersonneDAO dao = TopiaTestDAOHelper.getPersonneDAO(transaction);
        Personne personne = new PersonneImpl();
        personne.setGender(Gender.FEMALE);
        personne.setOtherGender(Gender.MALE);
        dao.create(personne);
        String topiaId = personne.getTopiaId();
        transaction.commitTransaction();
        transaction.closeContext();

        transaction = db.beginTransaction();
        dao = TopiaTestDAOHelper.getPersonneDAO(transaction);
        dao.findByTopiaId(topiaId);
        Assert.assertEquals(Gender.FEMALE, personne.getGender());
        Assert.assertEquals(Gender.MALE, personne.getOtherGender());

        Assert.assertNotNull(dao.findByGender(Gender.FEMALE));
        Assert.assertNotNull(dao.findByOtherGender(Gender.MALE));
        transaction.closeContext();
    }
}
