/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.topia.framework;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaContextFactory;
import org.nuiton.topia.TopiaDatabase;
import org.nuiton.topia.TopiaTestDAOHelper;
import org.nuiton.topia.test.entities.Person;
import org.nuiton.topia.test.entities.PersonDAO;
import org.nuiton.topia.test.entities.Pet;
import org.nuiton.topia.test.entities.PetDAO;

import java.io.File;
import java.util.Properties;

/**
 * To test replication sugin TopiaContext.
 *
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @since 2.6.8
 */
public class TopiaContextReplicateTest {

    @Rule
    public final TopiaDatabase dbSource =
            new TopiaDatabase() {

                @Override
                protected void onDbConfigurationCreate(Properties configuration, File testDir, String dbPath) {
                    configuration.setProperty(
                            TopiaContextFactory.CONFIG_URL, "jdbc:h2:file:" + dbPath + "-source");

                }
            };

    @Rule
    public final TopiaDatabase dbTarget =
            new TopiaDatabase() {

                @Override
                protected void onDbConfigurationCreate(Properties configuration, File testDir, String dbPath) {
                    configuration.setProperty(
                            TopiaContextFactory.CONFIG_URL, "jdbc:h2:file:" + dbPath + "-target");

                }
            };

    @Test
    public void replicateEntity() throws Exception {
//
//        Properties configSource = TestHelper.initTopiaContextConfiguration(
//                testBasedir,
//                "/TopiaContextImpl.properties",
//                "replicateSource");
//
//        Properties configTarget = TestHelper.initTopiaContextConfiguration(
//                testBasedir,
//                "/TopiaContextImpl.properties",
//                "replicateTarget");
//

        TopiaContext contextSource = dbSource.getRootCtxt();
        TopiaContext contextTarget = dbTarget.getRootCtxt();

//        try {
//            contextSource = TopiaContextFactory.getContext(configSource);
//            contextTarget = TopiaContextFactory.getContext(configTarget);

        TopiaContext txSource;
        TopiaContext txTarget;
        PersonDAO daoSource, daoTarget;
        PetDAO petDAOSource, petDAOTarget;
        Person personSource, personTarget;
        Pet petSource, petTarget;

        txSource = contextSource.beginTransaction();
        daoSource = TopiaTestDAOHelper.getPersonDAO(txSource);
        petDAOSource = TopiaTestDAOHelper.getPetDAO(txSource);

        personSource = daoSource.create(Person.PROPERTY_FIRSTNAME, " firstName",
                                        Person.PROPERTY_NAME, " name"
        );

        petSource = petDAOSource.create(Pet.PROPERTY_NAME, "name",
                                        Pet.PROPERTY_TYPE, "type",
                                        Pet.PROPERTY_PERSON, personSource
        );

        personSource.addPet(petSource);

        txSource.commitTransaction();

        daoSource = TopiaTestDAOHelper.getPersonDAO(txSource);

        personSource = daoSource.findByTopiaId(personSource.getTopiaId());
        Assert.assertNotNull(personSource);

        petSource = petDAOSource.findByTopiaId(petSource.getTopiaId());
        Assert.assertNotNull(petSource);
        Assert.assertEquals(1, personSource.sizePet());
        Assert.assertEquals(petSource, personSource.getPet().iterator().next());

        txTarget = contextTarget.beginTransaction();

        txSource.replicateEntity(txTarget, petSource);
        txSource.replicateEntity(txTarget, personSource);

        txTarget.commitTransaction();

        daoTarget = TopiaTestDAOHelper.getPersonDAO(txTarget);
        petDAOTarget = TopiaTestDAOHelper.getPetDAO(txTarget);

        personTarget = daoTarget.findByTopiaId(personSource.getTopiaId());
        Assert.assertNotNull(personTarget);
        Assert.assertEquals(personSource, personTarget);
        Assert.assertEquals(1, personTarget.sizePet());

        petTarget = petDAOTarget.findByTopiaId(petSource.getTopiaId());
        Assert.assertNotNull(petTarget);
        Assert.assertEquals(petSource, petTarget);

        Assert.assertEquals(petTarget, personTarget.getPet().iterator().next());


//        } finally {
//            closeDb(contextSource);
//            closeDb(contextTarget);
//        }

    }
}
