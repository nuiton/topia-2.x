/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.topia.framework;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaContextFactory;
import org.nuiton.topia.TopiaDatabase;
import org.nuiton.topia.TopiaException;
import org.nuiton.topia.TopiaTestDAOHelper;
import org.nuiton.topia.test.entities.Person;
import org.nuiton.topia.test.entities.PersonDAO;
import org.nuiton.topiatest.Personne;

import java.io.File;
import java.util.Locale;
import java.util.Properties;

import static org.junit.Assert.assertNotNull;

/**
 * To test the {@link TopiaConnectionProvider} and make sure all connections
 * are done from here...
 *
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @since 2.5.3
 */
public class TopiaConnectionProviderTest {

//    private static final Log log =
//            LogFactory.getLog(TopiaConnectionProviderTest.class);

    public static final String TEST_URL = "testURL";

    @Rule
    public final TopiaDatabase db =
            new TopiaDatabase("/TopiaConnectionProviderHardcoded.properties") {

                @Override
                protected void onDbConfigurationCreate(Properties configuration,
                                                       File testdir,
                                                       String dbPath) {

                    Assert.assertFalse(testdir.exists());

                    String dbPathFake = new File(testdir, "fake" + File.separator + "db").getAbsolutePath();

                    Assert.assertFalse(new File(dbPathFake).getParentFile().exists());

                    configuration.setProperty("dbPath", dbPath);
                    configuration.setProperty("dbPathFake", dbPathFake);

                    // give the path where connection provider will create db
                    configuration.setProperty(TEST_URL,
                                              "jdbc:h2:file:" + dbPath);

                    // give a fake db path (we will make sure it is never create after hibernate usage).
                    configuration.setProperty(TopiaContextFactory.CONFIG_URL,
                                              "jdbc:h2:file:" + dbPathFake);
                }
            };

    @Test
    public void testWithHardcoded() throws Exception {

//        Properties dbProperties = TestHelper.loadHibernateConfiguration(
//                "/TopiaConnectionProviderHardcoded.properties");
//
//        File directory = new File(TestHelper.getDbName(testBasedir, "testWithHardcoded"));

        String dbPath = (String) db.getDbConfiguration().get("dbPath");
        String dbPathFake = (String) db.getDbConfiguration().get("dbPathFake");

//        new File(directory, "real" + File.separator + "db").getAbsolutePath();
//        Assert.assertFalse(new File(dbPath).getParentFile().exists());

//        String dbPathFake = new File(directory, "fake" + File.separator + "db").getAbsolutePath();

//        Assert.assertFalse(new File(dbPathFake).getParentFile().exists());

//        // give the path where connection provider will create db
//        dbProperties.setProperty(TEST_URL, "jdbc:h2:file:" + dbPath);
//
//        // give a fake db path (we will make sure it is never create after hibernate usage).
//        dbProperties.setProperty(Environment.URL, "jdbc:h2:file:" + dbPathFake);
//
//        root = TopiaContextFactory.getContext(dbProperties);

        Locale.setDefault(Locale.FRANCE);

        doStuffOnDb();

        // the db file must have been created
        Assert.assertTrue(new File(dbPath).getParentFile().exists());

        // make sure the fake db path was never used
        Assert.assertFalse(new File(dbPathFake).getParentFile().exists());
    }

    private void doStuffOnDb() throws TopiaException {
        TopiaContext transaction = db.beginTransaction();

        try {
            PersonDAO dao = TopiaTestDAOHelper.getPersonDAO(transaction);

            Person personne = dao.create(Personne.PROPERTY_NAME, "Jack Bauer");
            transaction.commitTransaction();
            String idPersonne = personne.getTopiaId();
            assertNotNull(idPersonne);

            transaction.commitTransaction();
        } finally {
            transaction.closeContext();
        }
    }
}
