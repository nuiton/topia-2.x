/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.topia.framework;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Test;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topiatest.QueriedEntity;

/** @author fdesbois */
public class TopiaQueryTest {

    private static final Log log = LogFactory.getLog(TopiaQueryTest.class);

    @Test
    public void testAdd() {

        // Test with one paramValue
        String value = "topia";
        TopiaQuery query = new TopiaQuery(QueriedEntity.class);
        query.addEquals(QueriedEntity.PROPERTY_TEST_ADD, value);
        Assert.assertEquals(
                "FROM org.nuiton.topiatest.QueriedEntity " +
                "WHERE testAdd = :testAdd",
                query.fullQuery());

        // Test with null paramValue
        //String nullValue = null;
        query = new TopiaQuery(QueriedEntity.class);
        query.addEquals(QueriedEntity.PROPERTY_TEST_ADD, new Object[]{null});
        Assert.assertEquals(
                "FROM org.nuiton.topiatest.QueriedEntity " +
                "WHERE testAdd IS NULL",
                query.fullQuery());

        // Test with two paramValues
        String value2 = "eugene";
        query = new TopiaQuery(QueriedEntity.class);
        query.addEquals(QueriedEntity.PROPERTY_TEST_ADD, value, value2);
        Assert.assertEquals(
                "FROM org.nuiton.topiatest.QueriedEntity " +
                "WHERE testAdd IN (:testAdd1, :testAdd2)",
                query.fullQuery());

        // Test with two paramValues + null
        query = new TopiaQuery(QueriedEntity.class);
        query.addEquals(QueriedEntity.PROPERTY_TEST_ADD, value, value2, null);
        Assert.assertEquals(
                "FROM org.nuiton.topiatest.QueriedEntity " +
                "WHERE testAdd IN (:testAdd1, :testAdd2) OR testAdd IS NULL",
                query.fullQuery());
    }

    @Test
    public void testAddSubQuery() {

        // Test 1 : Subquery with two params with different values
        TopiaQuery query = new TopiaQuery(QueriedEntity.class).
                addEquals(QueriedEntity.PROPERTY_TEST_ADD, "value1");
        // Exist 2 params
        Assert.assertEquals(2, query.getParams().size());

        TopiaQuery subquery = new TopiaQuery(QueriedEntity.class).
                addEquals(QueriedEntity.PROPERTY_TEST_ADD, "value2");

        query.addSubQuery("Q1 = (?)", subquery);
        log.debug(query);
        // Add other params from subquery
        Assert.assertEquals(4, query.getParams().size());


        // Test 2 : Subquery with two params with different values
        // one of them is null
        query = new TopiaQuery(QueriedEntity.class).
                addWhere(QueriedEntity.PROPERTY_TEST_ADD, TopiaQuery.Op.EQ, null);
        // Exist 0 param (null value)
        Assert.assertEquals(0, query.getParams().size());

        subquery = new TopiaQuery(QueriedEntity.class).
                addEquals(QueriedEntity.PROPERTY_TEST_ADD, "value1");

        query.addSubQuery("Q1 = (?)", subquery);
        log.debug(query);
        // Add 2 params from subquery
        Assert.assertEquals(2, query.getParams().size());


        // Test 3 : Subquery with two params with same value
        query = new TopiaQuery(QueriedEntity.class, "Q1").
                addEquals(QueriedEntity.PROPERTY_TEST_ADD, "value1");
        // Exist 2 params
        Assert.assertEquals(2, query.getParams().size());

        subquery = new TopiaQuery(QueriedEntity.class, "Q2").
                addEquals(QueriedEntity.PROPERTY_TEST_ADD, "value1");

        query.addSubQuery("Q1 = (?)", subquery);
        log.debug(query);
        // Still 2 params
        Assert.assertEquals(2, query.getParams().size());

        // non-regression test, when parameters of main query is not empty
        // and sub-query use different parameters from the one used in the
        // main query, parameters needed by the subquery are lost in the
        // resulting query

        // Test 4 : add a subquery with its own parameters
        query = new TopiaQuery(QueriedEntity.class, "Q1").
                addEquals(QueriedEntity.TOPIA_ID, "ID1");

        subquery = new TopiaQuery(QueriedEntity.class, "Q2").
                addEquals(QueriedEntity.PROPERTY_TEST_ADD, "value");

        query.addSubQuery(QueriedEntity.PROPERTY_TEST_ADD + " IN (?)", subquery);

        log.debug(query);
        Assert.assertEquals(4, query.getParams().size());
    }

    /** Test of addFilter method, of class TopiaQuery. */
    @Test
    public void testAddFilter() {
        log.info("testAddFilter");

        EntityFilter filter = new TopiaFilter();
        filter.setStartIndex(1);
        filter.setEndIndex(40);
        filter.setOrderBy(QueriedEntity.PROPERTY_TEST_ADD);

        TopiaQuery query = new TopiaQuery(QueriedEntity.class).addFilter(filter);

        log.debug("Query : " + query);

        Assert.assertEquals("FROM " + QueriedEntity.class.getName() +
                            " ORDER BY " + QueriedEntity.PROPERTY_TEST_ADD,
                            query.fullQuery());

        filter.setOrderBy(null);

        query = new TopiaQuery(QueriedEntity.class).addFilter(filter);

        log.debug("Query : " + query);

        Assert.assertEquals("FROM " + QueriedEntity.class.getName() +
                            " ORDER BY " + TopiaEntity.TOPIA_CREATE_DATE + " DESC",
                            query.fullQuery());

    }

    @Test
    // cf http://nuiton.org/issues/1745
    public void testWhereWithFunction() {

        TopiaQuery query = new TopiaQuery(QueriedEntity.class);
        query.addWhere("lower(name)", TopiaQuery.Op.LIKE, "%azerty%");

        log.debug("Query : " + query);

        Assert.assertEquals("FROM " + QueriedEntity.class.getName() +
                            " WHERE lower(name) LIKE :lower_name_",
                            query.fullQuery());
    }

}
