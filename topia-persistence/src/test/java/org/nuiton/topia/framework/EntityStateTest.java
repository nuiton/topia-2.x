/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.topia.framework;

import org.junit.Assert;
import org.junit.Test;

/**
 * EntityStateTest.java
 * 
 * Created: 22 nov. 06 12:15:11
 *
 * @author poussin &lt;poussin@codelutin.com&gt;
 * @version $Revision$
 *          
 *          Last update: $Date$
 *          by : $Author$
 */
public class EntityStateTest {

    /**
     * Test les changements d'etat de {@link EntityState}.
     *
     * @throws Exception
     */
    @Test
    public void testState() throws Exception {
        EntityState state = new EntityState();

        state.addLoad();
        Assert.assertTrue(state.isLoad());
        Assert.assertFalse(state.isRead());
        Assert.assertFalse(state.isCreate());
        Assert.assertFalse(state.isUpdate());
        Assert.assertFalse(state.isDelete());

        state.addRead();
        Assert.assertTrue(state.isLoad());
        Assert.assertTrue(state.isRead());
        Assert.assertFalse(state.isCreate());
        Assert.assertFalse(state.isUpdate());
        Assert.assertFalse(state.isDelete());

        //state.addRead();
        state.addCreate();
        Assert.assertTrue(state.isLoad());
        Assert.assertTrue(state.isRead());
        Assert.assertTrue(state.isCreate());
        Assert.assertFalse(state.isUpdate());
        Assert.assertFalse(state.isDelete());

        state.addUpdate();
        Assert.assertTrue(state.isLoad());
        Assert.assertTrue(state.isRead());
        Assert.assertTrue(state.isCreate());
        Assert.assertTrue(state.isUpdate());
        Assert.assertFalse(state.isDelete());

        state.addDelete();
        Assert.assertTrue(state.isLoad());
        Assert.assertTrue(state.isRead());
        Assert.assertTrue(state.isCreate());
        Assert.assertTrue(state.isUpdate());
        Assert.assertTrue(state.isDelete());

        state = new EntityState();
        state.addDelete();
        Assert.assertFalse(state.isRead());
        Assert.assertFalse(state.isCreate());
        Assert.assertFalse(state.isUpdate());
        Assert.assertTrue(state.isDelete());
    }
}
