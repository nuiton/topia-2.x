/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.topia.framework;

import java.io.File;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.h2.Driver;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.dialect.H2Dialect;
import org.hibernate.mapping.PersistentClass;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.nuiton.i18n.I18n;
import org.nuiton.topia.TopiaContextFactory;
import org.nuiton.topia.TopiaDatabase;
import org.nuiton.topia.TopiaNotFoundException;
import org.nuiton.topiatest.persistence.Entity1;
import org.nuiton.topiatest.persistence.Entity1Impl;
import org.nuiton.topiatest.service.FakeService;
import org.nuiton.topiatest.service.TestService;

/**
 * Created: 10 mai 2010
 *
 * @author fdesbois &lt;fdesbois@codelutin.com&gt;
 * @version $Id$
 */
public class TopiaContextImplTest {

    private static final Log log =
            LogFactory.getLog(TopiaContextImplTest.class);

    protected Properties properties = new Properties();

    static File testBasedir;

    @BeforeClass
    public static void setUpClass() throws Exception {

        testBasedir = TopiaDatabase.getTestSpecificDirectory(TopiaContextImplTest.class, "dummy");
        I18n.init(null, Locale.FRENCH);
    }

    @Before
    public void setUp() throws Exception {
        properties.clear();
        properties.setProperty("hibernate.dialect", H2Dialect.class.getName());
        properties.setProperty("hibernate.connection.username", "sa");
        properties.setProperty("hibernate.connection.password", "");
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testLoadServices() throws Exception {
        if (log.isDebugEnabled()) {
            log.debug("## testLoadServices");
        }

        /** PREPARE DATA **/
        properties.setProperty("topia.service.test",
                               TestService.class.getName());

        TopiaContextImpl context = new TopiaContextImpl();

        /** EXEC METHOD **/
        if (log.isInfoEnabled()) {
            log.info("test 1 : load a simple TestService from properties");
        }
        Map<String, TopiaService> results = context.loadServices(properties);
        Assert.assertEquals(1, results.size());
        Assert.assertTrue(results.containsKey("test"));
        TopiaService service = results.get("test");
        Assert.assertEquals(TestService.class, service.getClass());

        if (log.isInfoEnabled()) {
            log.info("test 2 : load with wrong key : will display a WARN");
        }
        properties.clear();

        properties.setProperty("topia.service.fake",
                               TestService.class.getName());

        results = context.loadServices(properties);
        Assert.assertEquals(0, results.size());
        Assert.assertFalse(results.containsKey("fake"));

        if (log.isInfoEnabled()) {
            log.info("test 3 : load with fake service name : will display an ERROR");
        }
        properties.clear();

        properties.setProperty("topia.service.test", "FAKE");

        results = context.loadServices(properties);
        Assert.assertEquals(0, results.size());
        Assert.assertFalse(results.containsKey("test"));
    }

    @Test
    public void testGetServices() throws Exception {
        if (log.isDebugEnabled()) {
            log.debug("## testGetServices");
        }

        /** PREPARE DATA **/
        properties.setProperty("topia.service.test",
                               TestService.class.getName());

        // Calling the constructor with properties will load the services
        TopiaContextImpl context = new TopiaContextImpl(properties);

        // Instantiate a child context and set its parent
        TopiaContextImpl child = new TopiaContextImpl(context);

        /** EXEC METHOD **/
        if (log.isInfoEnabled()) {
            log.info("test 1 : with child context");
        }
        Map<String, TopiaService> test1 = child.getServices();
        Assert.assertEquals(1, test1.size());
        Assert.assertTrue(test1.containsKey("test"));

        if (log.isInfoEnabled()) {
            log.info("test 2 : test serviceEnabled method");
        }
        boolean test2 = child.serviceEnabled("test");
        Assert.assertTrue(test2);

        if (log.isInfoEnabled()) {
            log.info("test 3 : test getService method");
        }
        TopiaService test3 = child.getService("test");
        Assert.assertEquals(TestService.class, test3.getClass());

        if (log.isInfoEnabled()) {
            log.info("test 4 : test serviceEnabled from class TestService");
        }
        boolean test4 = child.serviceEnabled(TestService.class);
        Assert.assertTrue(test4);

        if (log.isInfoEnabled()) {
            log.info("test 5 : test getService from class TestService");
        }
        TestService test5 = child.getService(TestService.class);
        Assert.assertNotNull(test5);

        if (log.isInfoEnabled()) {
            log.info("test 6 : test serviceEnabled error with class FakeService");
        }
        // FakeService doesn't contains property SERVICE_NAME used by
        // serviceEnabled method
        // Even it's properly loaded the serviceEnabled method will return false
        properties.clear();
        properties.setProperty("topia.service.fake",
                               FakeService.class.getName());
        properties.setProperty("hibernate.dialect", H2Dialect.class.getName());
        properties.setProperty("hibernate.connection.username", "sa");
        properties.setProperty("hibernate.connection.password", "");
        TopiaContextImpl otherContext = new TopiaContextImpl(properties);

        boolean test6 = otherContext.serviceEnabled(FakeService.class);
        Assert.assertFalse(test6);

        if (log.isInfoEnabled()) {
            log.info("test 7 : test getService with error TopiaNotFoundException" +
                     " : service not loaded");
        }
        // TestService is not loaded in otherContext
        try {
            TestService test7 = otherContext.getService(TestService.class);
            Assert.fail();
        } catch (TopiaNotFoundException eee) {
            //log.error(eee.getClass().getSimpleName() + " : " + eee.getMessage());
            //Assert.assertEquals(TopiaNotFoundException.class, eee.getClass());
        } catch (Exception e) {
            Assert.fail();
        }
    }

    @Test
    public void testContextHierarchy() throws Exception {
        if (log.isDebugEnabled()) {
            log.debug("## testContextHierarchy");
        }

        /** PREPARE DATA **/
        TopiaContextImpl context = new TopiaContextImpl(properties);

        /** EXEC METHODS **/
        if (log.isInfoEnabled()) {
            log.info("test 1 : constructor with parent context");
        }
        TopiaContextImpl test1 = new TopiaContextImpl(context);
        Assert.assertEquals(context, test1.parentContext);

        if (log.isInfoEnabled()) {
            log.info("test 2 : addChildContext");
        }
        TopiaContextImpl test2 = new TopiaContextImpl(properties);
        TopiaContextImpl child2 = new TopiaContextImpl();
        test2.addChildContext(child2);
        Assert.assertEquals(1, test2.childContext.size());

        if (log.isInfoEnabled()) {
            log.info("test 3 : removeChildContext");
        }
        TopiaContextImpl test3 = new TopiaContextImpl(properties);
        TopiaContextImpl child3 = new TopiaContextImpl(test3);
        test3.childContext.add(child3);
        test3.removeChildContext(child3);
        Assert.assertEquals(0, test3.childContext.size());

        // No remove if context is closed
        test3.childContext.add(child3);
        test3.closed = true;
        test3.removeChildContext(child3);
        Assert.assertEquals(1, test3.childContext.size());

        if (log.isInfoEnabled()) {
            log.info("test 4 : getRootContext");
        }
        TopiaContextImplementor test4 = child3.getRootContext();
        Assert.assertEquals(test3, test4);

        // Note : existing test is already done for concurrency problem on
        // getChildContext(). Go to : http://www.nuiton.org/repositories/browse/sandbox/testTopiaPostgresError/trunk
    }
//
//    @Test
//    public void testCreateSchema() throws Exception {
//    }
//
//    @Test
//    public void testShowCreateSchema() throws Exception {
//    }
//
//    @Test
//    public void testUpdateSchema() throws Exception {
//    }
//
//    @Test
//    public void testGetHibernate() throws Exception {
//    }
//

    @Test
    public void testGetHibernateFactory() throws Exception {
        if (log.isDebugEnabled()) {
            log.debug("## testGetHibernateFactory");
        }

        /** PREPARE DATA **/
        TopiaContextImpl context = new TopiaContextImpl();
        context.services = new HashMap<String, TopiaService>();

        String basedir = System.getenv("basedir");
        if (basedir == null) {

            // says basedir is where we start tests.
            basedir = new File("").getAbsolutePath();
        }

        if (log.isDebugEnabled()) {
            log.debug("baseDir : " + basedir);
        }
        File persistenceDir = new File(basedir,
                                       "target" + File.separator +
                                       "test-classes" + File.separator +
                                       "org" + File.separator +
                                       "nuiton" + File.separator +
                                       "topiatest" + File.separator +
                                       "persistence");
        if (log.isDebugEnabled()) {
            log.debug("persistenceDir : " + persistenceDir);
        }
        File resourcesDir = new File(basedir,
                                     "target" + File.separator +
                                     "test-classes");

        /** EXEC METHOD **/
        if (log.isInfoEnabled()) {
            log.info("test 1 : load mappings from directory");
        }

        properties.setProperty(TopiaContextFactory.CONFIG_PERSISTENCE_DIRECTORIES,
                               persistenceDir.getAbsolutePath());
        context.config = properties;

        Metadata test1 = context.getMetadata();
        PersistentClass persistentClass = test1.getEntityBinding(Entity1Impl.class.getName());
        Assert.assertNotNull(persistentClass);
        Assert.assertEquals(Entity1.class, persistentClass.getProxyInterface());

//        for (Iterator<RootClass> it = test1.getClassMappings(); it.hasNext();) {
//            RootClass o = it.next();
//            log.debug("entity : " + o.getEntityName());
//        }

        if (log.isInfoEnabled()) {
            log.info("test 2 : load mappings for all entities");
        }
        //reset from previous test
        context = new TopiaContextImpl();
        context.services = new HashMap<String, TopiaService>();
        properties.clear();
        properties.setProperty("hibernate.dialect", H2Dialect.class.getName());
        properties.setProperty("hibernate.connection.username", "sa");
        properties.setProperty("hibernate.connection.password", "");

        // use property TOPIA_PERSISTENCE_CLASSES
        properties.setProperty(TopiaContextFactory.CONFIG_PERSISTENCE_CLASSES,
                               Entity1Impl.class.getName());
        context.config = properties;

        Metadata test2 = context.getMetadata();
        persistentClass = test2.getEntityBinding(Entity1Impl.class.getName());
        Assert.assertNotNull(persistentClass);
        Assert.assertEquals(Entity1.class, persistentClass.getProxyInterface());

        if (log.isInfoEnabled()) {
            log.info("test 3 : add properties from file");
        }
        //reset from previous test
        context = new TopiaContextImpl();
        context.services = new HashMap<String, TopiaService>();
        properties.clear();

        // use property TOPIA_PERSISTENCE_PROPERTIES_FILE to add default
        // properties from file
        properties.setProperty(TopiaContextFactory.CONFIG_PERSISTENCE_PROPERTIES_FILE,
                               resourcesDir + File.separator + "TopiaContextImpl.properties");
        context.config = properties;

        /* Ca fonctionne, mais c'est plus facilement testable :(
        StandardServiceRegistry serviceRegistry = (StandardServiceRegistry)context.getServiceRegistry();
        Map<String, String> test3 = serviceRegistry.getSettings();
        Assert.assertEquals(Driver.class.getName(),
                test3.get("hibernate.connection.driver_class"));*/

        // Note : maybe add a test to load classes from services
    }

//    @Test
//    public void replicateEntity() throws Exception {
//
//        Properties configSource = TestHelper.initTopiaContextConfiguration(
//                testBasedir,
//                "/TopiaContextImpl.properties",
//                "replicateSource");
//
//        Properties configTarget = TestHelper.initTopiaContextConfiguration(
//                testBasedir,
//                "/TopiaContextImpl.properties",
//                "replicateTarget");
//
//
//        TopiaContext contextSource = null;
//        TopiaContext contextTarget = null;
//
//        try {
//            contextSource = TopiaContextFactory.getContext(configSource);
//            contextTarget = TopiaContextFactory.getContext(configTarget);
//
//            TopiaContext txSource;
//            TopiaContext txTarget;
//            PersonDAO daoSource, daoTarget;
//            PetDAO petDAOSource, petDAOTarget;
//            Person personSource, personTarget;
//            Pet petSource, petTarget;
//
//            txSource = contextSource.beginTransaction();
//            daoSource = TopiaTestDAOHelper.getPersonDAO(txSource);
//            petDAOSource = TopiaTestDAOHelper.getPetDAO(txSource);
//
//            personSource = daoSource.create(Person.PROPERTY_FIRSTNAME, " firstName",
//                                            Person.PROPERTY_NAME, " name"
//            );
//
//            petSource = petDAOSource.create(Pet.PROPERTY_NAME, "name",
//                                            Pet.PROPERTY_TYPE, "type",
//                                            Pet.PROPERTY_PERSON, personSource
//            );
//
//            personSource.addPet(petSource);
//
//            txSource.commitTransaction();
//
//            daoSource = TopiaTestDAOHelper.getPersonDAO(txSource);
//
//            personSource = daoSource.findByTopiaId(personSource.getTopiaId());
//            Assert.assertNotNull(personSource);
//
//            petSource = petDAOSource.findByTopiaId(petSource.getTopiaId());
//            Assert.assertNotNull(petSource);
//            Assert.assertEquals(1, personSource.sizePet());
//            Assert.assertEquals(petSource, personSource.getPet().iterator().next());
//
//            txTarget = contextTarget.beginTransaction();
//
//            txSource.replicateEntity(txTarget, petSource);
//            txSource.replicateEntity(txTarget, personSource);
//
//            txTarget.commitTransaction();
//
//            daoTarget = TopiaTestDAOHelper.getPersonDAO(txTarget);
//            petDAOTarget = TopiaTestDAOHelper.getPetDAO(txTarget);
//
//            personTarget = daoTarget.findByTopiaId(personSource.getTopiaId());
//            Assert.assertNotNull(personTarget);
//            Assert.assertEquals(personSource, personTarget);
//            Assert.assertEquals(1, personTarget.sizePet());
//
//            petTarget = petDAOTarget.findByTopiaId(petSource.getTopiaId());
//            Assert.assertNotNull(petTarget);
//            Assert.assertEquals(petSource, petTarget);
//
//            Assert.assertEquals(petTarget, personTarget.getPet().iterator().next());
//
//
//        } finally {
//            closeDb(contextSource);
//            closeDb(contextTarget);
//        }
//
//    }
//
//    protected static void closeDb(TopiaContext contextSource) {
//        if (contextSource != null && !contextSource.isClosed())
//            try {
//                contextSource.clear(false);
//            } catch (TopiaException e) {
//                if (log.isErrorEnabled()) {
//                    log.error("Could not close db " + contextSource, e);
//                }
//            }
//    }


//
//    @Test
//    public void testGetHibernateConfiguration() throws Exception {
//    }
//
//    @Test
//    public void testGetDAO() throws Exception {
//    }
//
//    @Test
//    public void testBeginTransaction() throws Exception {
//    }
//
//    @Test
//    public void testCommitTransaction() throws Exception {
//    }
//
//    @Test
//    public void testRollbackTransaction() throws Exception {
//    }
//
//    @Test
//    public void testCloseContext() throws Exception {
//    }
//
//    @Test
//    public void testIsClosed() throws Exception {
//    }
//
//    @Test
//    public void testFindByTopiaId() throws Exception {
//    }
//
//    @Test
//    public void testFind() throws Exception {
//    }
//
//    @Test
//    public void testFind2() throws Exception {
//    }
//
//    @Test
//    public void testExecute() throws Exception {
//    }
//
//    @Test
//    public void testAdd() throws Exception {
//    }
//
//    @Test
//    public void testImportXML() throws Exception {
//    }
//
//    @Test
//    public void testExportXML() throws Exception {
//    }
//
//
//    @Test
//    public void testReplicateEntity() throws Exception {
//    }
//
//    @Test
//    public void testReplicateEntities() throws Exception {
//    }
//
//    @Test
//    public void testGetFiresSupport() throws Exception {
//    }
//
//    @Test
//    public void testBackup() throws Exception {
//    }
//
//    @Test
//    public void testRestore() throws Exception {
//    }
//
//    @Test
//    public void testClear() throws Exception {
//    }
//
//    @Test
//    public void testGetPersistenceClasses() throws Exception {
//    }
//
//    @Test
//    public void testIsSchemaExist() throws Exception {
//    }
}
