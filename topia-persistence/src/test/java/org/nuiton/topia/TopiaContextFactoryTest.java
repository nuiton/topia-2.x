/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.topia;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.dialect.H2Dialect;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.nuiton.topia.framework.TopiaContextImpl;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

/**
 * Created: 8 mai 2010
 *
 * @author fdesbois &lt;fdesbois@codelutin.com&gt;
 * @version $Id$
 */
public class TopiaContextFactoryTest {

    private static final Log log =
            LogFactory.getLog(TopiaContextFactoryTest.class);

    protected static File testBasedir;

    protected Properties properties;

    @BeforeClass
    public static void init() throws IOException {

        testBasedir = TopiaDatabase.getTestSpecificDirectory(TopiaContextFactoryTest.class, "dummy");

    }

    @Before
    public void setUp() throws Exception {
        properties = new Properties();
        properties.setProperty("prop1", "value1");
        properties.setProperty("prop2", "value2");
        properties.setProperty("hibernate.dialect", H2Dialect.class.getName());
        properties.setProperty("hibernate.connection.username", "sa");
        properties.setProperty("hibernate.connection.password", "");
        TopiaContextFactory.contextCache.clear();
    }

    @Test
    public void testGetContextOpened() throws Exception {
        log.debug("## testGetContextOpened");

        /** PREPARE DATA **/
        String databaseName = "h2data-testGetContextByPropertie";
        File dbDirectory = new File(testBasedir, databaseName);
        String url = "jdbc:h2:file:" + dbDirectory;
        properties.setProperty("hibernate.connection.url", url);

        TopiaContextImpl test = new TopiaContextImpl(properties);
        TopiaContextFactory.contextCache.put(properties, test);

        /** EXEC METHOD **/
        List<String> result = TopiaContextFactory.getContextOpened();
        Assert.assertEquals(1, result.size());
        Assert.assertEquals(url, result.get(0));
    }

    @Test
    public void testRemoveContext() throws Exception {
        log.debug("## testRemoveContext");

        /** PREPARE DATA **/
        TopiaContextImpl test = new TopiaContextImpl(properties);
        TopiaContextFactory.contextCache.put(properties, test);

        /** EXEC METHOD **/
        TopiaContextFactory.removeContext(test);
        Assert.assertEquals(0, TopiaContextFactory.contextCache.size());
    }

    //@Test

    public void testGetContext() throws Exception {
        // TODO-fdesbois-20100508 : only used TopiaUtil.getProperties, need tests for this method
    }

    @Test
    public void testGetContextByProperties() throws Exception {
        log.debug("## testGetContextByProperties");

        /** PREPARE DATA **/
        Properties propertiesParent = new Properties(properties);
        propertiesParent.setProperty("prop3", "value3");

        Properties propertiesAll = new Properties();
        propertiesAll.setProperty("prop1", "value1");
        propertiesAll.setProperty("prop2", "value2");
        propertiesAll.setProperty("prop3", "value3");
        propertiesAll.setProperty("hibernate.connection.username", "sa");
        propertiesAll.setProperty("hibernate.connection.password", "");
        propertiesAll.setProperty("hibernate.dialect", H2Dialect.class.getName());

        /** EXEC METHOD **/

        log.info("test 0 : add null properties");
        try {
            TopiaContextFactory.getContext(null);
        } catch (Exception eee) {
            Assert.assertEquals(NullPointerException.class, eee.getClass());
        }

        log.info("test 1 : add new properties, will instantiate a new" +
                 " TopiaContext");
        TopiaContext test1 = TopiaContextFactory.getContext(propertiesParent);
        Assert.assertNotNull(test1);
        Assert.assertEquals(1, TopiaContextFactory.contextCache.size());

        log.info("test 2 : with same properties, will retrieve existing" +
                 " TopiaContext");
        TopiaContext test2 = TopiaContextFactory.getContext(propertiesParent);
        Assert.assertEquals(test1, test2);
        Assert.assertEquals(1, TopiaContextFactory.contextCache.size());

        log.info("test 3 : use other properties, will instantiate a different" +
                 "TopiaContext");
        TopiaContext test3 = TopiaContextFactory.getContext(properties);
        log.debug("cache size : " + TopiaContextFactory.contextCache.size());
        log.debug("result : " + test1);
        log.debug("result3 : " + test3);
        Assert.assertNotSame(test1, test3);
        Assert.assertEquals(2, TopiaContextFactory.contextCache.size());

        log.info("test 4 : use other properties but equivalent to existing " +
                 "TopiaContext");
        // Test flating of properties
        TopiaContext test4 = TopiaContextFactory.getContext(propertiesAll);
        Assert.assertEquals(test1, test4);
        Assert.assertEquals(2, TopiaContextFactory.contextCache.size());

        log.info("test5a : reinstantiate new TopiaContext after one is closed.");
        // TEST
        // Strange behavior the closed flag of context stay true if
        // hibernateFactory is not loaded from real properties
//        test1.closeContext();
//        Assert.assertTrue(test1.isClosed());

        // Add properties for Hibernate to have real opened topiaContext
        String databaseName = "h2data-testGetContextByPropertie";
        File f = new File(testBasedir, databaseName);

        properties.setProperty("hibernate.dialect", H2Dialect.class.getName());
        properties.setProperty("hibernate.connection.username", "sa");
        properties.setProperty("hibernate.connection.password", "");
        properties.setProperty("hibernate.connection.driver_class", "org.h2.Driver");
        properties.setProperty("hibernate.connection.url",
                               "jdbc:h2:file:" + f.getAbsolutePath());


        TopiaContext test5 = TopiaContextFactory.getContext(properties);
        Assert.assertNotSame(test1, test5);
        Assert.assertEquals(3, TopiaContextFactory.contextCache.size());

        log.info("test5b : beginTransaction to properly close the context");
        test5.beginTransaction();

        test5.closeContext();

        TopiaContext result = TopiaContextFactory.getContext(properties);
        Assert.assertNotSame(test5, result);
        Assert.assertEquals(3, TopiaContextFactory.contextCache.size());
    }
}
