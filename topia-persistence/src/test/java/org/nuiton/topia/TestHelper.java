/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.topia;

import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.cfg.Environment;
import org.junit.Ignore;
import org.nuiton.util.FileUtil;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Helper for all topia tests.
 *
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @since 2.5
 */
@Ignore
// this is not a test :)
public class TestHelper {

    private static final Log log = LogFactory.getLog(TestHelper.class);

    protected static File testBasedir;

    protected static File targetdir;

    protected static File dirDatabase;

    public static final String DEFAULT_CONFIGURATION_LOCATION = "/TopiaContextImpl.properties";

    public static File getTestWorkdir() {
        if (testBasedir == null) {
            String base = System.getProperty("java.io.tmpdir");
            if (base == null || base.isEmpty()) {
                base = new File("").getAbsolutePath();
            }
            testBasedir = new File(base);
            log.info("basedir for test " + testBasedir);
        }
        return testBasedir;
    }

    public static File getTestBasedir(Class<?> testClass) throws IOException {
        File dir = getTestWorkdir();
        File result = new File(dir, testClass.getName());
        if (result.exists()) {

            // when calling this method (always in a BeforeClass method), we wants
            // to clean the directory, this is a new build
            FileUtils.deleteDirectory(result);
        }

        // always create the directory
        FileUtil.createDirectoryIfNecessary(result);
        return result;
    }

    public static TopiaContext initTopiaContext(File testDirectory,
                                                String dbname)
            throws IOException, TopiaNotFoundException {


        TopiaContext topiaContext = initTopiaContext(
                testDirectory,
                DEFAULT_CONFIGURATION_LOCATION,
                dbname
        );
        return topiaContext;
    }

    public static TopiaContext initTopiaContext(File testDirectory,
                                                String dbPropertiesPath,
                                                String dbname)
            throws IOException, TopiaNotFoundException {

        Properties configuration = initTopiaContextConfiguration(
                testDirectory,
                dbPropertiesPath,
                dbname);
        return TopiaContextFactory.getContext(configuration);
    }

    public static Properties initTopiaContextConfiguration(File testDirectory,
                                                           String dbPropertiesPath,
                                                           String dbname)
            throws IOException {

        Properties configuration = loadHibernateConfiguration(dbPropertiesPath);

        // make sure we always use a different directory

        String dbPath = getDbName(testDirectory, dbname);

        if (log.isInfoEnabled()) {
            log.info("dbPath = " + dbPath);
        }
        configuration.setProperty(
                Environment.URL,
//                "hibernate.connection.url",
                "jdbc:h2:file:" + dbPath);

        return configuration;
    }

    public static Properties loadHibernateConfiguration(String dbPropertiesPath) throws IOException {
        InputStream stream = TestHelper.class.getResourceAsStream(dbPropertiesPath);

        Properties configuration = new Properties();

        configuration.load(stream);
        configuration.setProperty(
                TopiaContextFactory.CONFIG_PERSISTENCE_CLASSES,
                TopiaTestDAOHelper.getImplementationClassesAsString());
        return configuration;
    }

    public static String getDbName(File testDirectory, String dbname) {
        return new File(testDirectory,
                        dbname + '_' + System.nanoTime()).getAbsolutePath();
    }

    public static Properties initTopiaContextConfiguration(File testDirectory,
                                                           String dbname)
            throws IOException {

        return initTopiaContextConfiguration(
                testDirectory,
                DEFAULT_CONFIGURATION_LOCATION,
                dbname
        );
    }
}
