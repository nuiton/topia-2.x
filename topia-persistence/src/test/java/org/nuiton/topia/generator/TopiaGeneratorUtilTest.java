/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.topia.generator;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/** @author tchemit &lt;chemit@codelutin.com&gt; */
public class TopiaGeneratorUtilTest {

    @Deprecated
    @Test
    public void testConvertVariableNameToConstantName() {

        String variableName = "abc";
        String expResult = "ABC";
        String result = TopiaGeneratorUtil.convertVariableNameToConstantName(variableName);
        assertEquals(expResult, result);

        variableName = "ABC";
        expResult = "ABC";
        result = TopiaGeneratorUtil.convertVariableNameToConstantName(variableName);
        assertEquals(expResult, result);

        variableName = "abC";
        expResult = "AB_C";
        result = TopiaGeneratorUtil.convertVariableNameToConstantName(variableName);
        assertEquals(expResult, result);

        variableName = "AbC";
        expResult = "AB_C";
        result = TopiaGeneratorUtil.convertVariableNameToConstantName(variableName);
        assertEquals(expResult, result);

        variableName = "AbC";
        expResult = "AB_C";
        result = TopiaGeneratorUtil.convertVariableNameToConstantName(variableName);
        assertEquals(expResult, result);
    }
}
