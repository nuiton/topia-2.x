/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.topia.generator;

import org.junit.Assert;
import org.junit.Test;
import org.nuiton.eugene.java.JavaBuilder;
import org.nuiton.eugene.models.object.xml.ObjectModelClassImpl;

import java.util.HashMap;

/**
 * Created: 24 juin 2010
 *
 * @author fdesbois &lt;fdesbois@codelutin.com&gt;
 * @version $Id$
 */
public class QueryHelperTransformerTest {

    @Test
    public void testCreateAliasConstant() {
        QueryHelperTransformer transformer = new QueryHelperTransformer();
        transformer.aliases = new HashMap<String, String>();
        transformer.helperClass = new ObjectModelClassImpl();
        transformer.setBuilder(new JavaBuilder("TopiaTest"));

        String entityName = "Department";
        transformer.createAliasConstant(entityName);
        Assert.assertEquals(1, transformer.aliases.size());
        Assert.assertTrue(transformer.aliases.containsKey("D"));

        entityName = "Depot";
        transformer.createAliasConstant(entityName);
        Assert.assertEquals(2, transformer.aliases.size());
        Assert.assertTrue(transformer.aliases.containsKey("DE"));

        entityName = "Deposite";
        transformer.createAliasConstant(entityName);
        Assert.assertEquals(3, transformer.aliases.size());
        Assert.assertTrue(transformer.aliases.containsKey("DEP"));

        entityName = "Dep";
        transformer.createAliasConstant(entityName);
        Assert.assertEquals(4, transformer.aliases.size());
    }
}
