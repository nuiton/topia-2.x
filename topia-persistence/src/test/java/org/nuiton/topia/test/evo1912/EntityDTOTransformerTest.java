/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.topia.test.evo1912;

import org.junit.Assert;
import org.junit.Test;
import org.nuiton.topiatest.CompanyDTO;

/** @author ymartel &lt;martel@codelutin.com&gt; */
public class EntityDTOTransformerTest {

    @Test
    public void testEvo1912() throws Exception {
        // simply test the getter/setter on id property : with the tagValue, they should be generated
        CompanyDTO companyDTO = new CompanyDTO();
        String originalId = companyDTO.getTopiaId();
        Assert.assertNull(originalId);
        String wantedId = "mycompany";
        companyDTO.setTopiaId(wantedId);
        String updatedId = companyDTO.getTopiaId();
        Assert.assertEquals(wantedId, updatedId);
    }

}
