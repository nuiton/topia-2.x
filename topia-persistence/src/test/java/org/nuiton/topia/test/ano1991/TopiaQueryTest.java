/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.topia.test.ano1991;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaDatabase;
import org.nuiton.topia.TopiaException;
import org.nuiton.topia.TopiaTestDAOHelper;
import org.nuiton.topia.framework.TopiaQuery;
import org.nuiton.topia.test.entities.Person;
import org.nuiton.topia.test.entities.PersonDAO;
import org.nuiton.topia.test.entities.Pet;
import org.nuiton.topia.test.entities.PetDAO;

import java.util.Arrays;
import java.util.List;

/**
 * Prevent regression of http://nuiton.org/issues/1991.
 *
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @since 2.6.8
 */
@Deprecated
public class TopiaQueryTest {

    @Rule
    public final TopiaDatabase db = new TopiaDatabase();

    @Test
    public void testInOperatorForIds() throws TopiaException {

        TopiaContext context = db.beginTransaction();

        PersonDAO personDAO = TopiaTestDAOHelper.getPersonDAO(context);

        // appel 1 find all
        Person toto = personDAO.create(Person.PROPERTY_NAME, "toto");
        Person titi = personDAO.create(Person.PROPERTY_NAME, "titi");

        TopiaQuery query;

        query = personDAO.createQuery().addWhere(
                Person.PROPERTY_NAME, TopiaQuery.Op.IN, new String[]{"toto"}
        );

        List<Person> result;
        result = personDAO.findAllByQuery(query);
        Assert.assertNotNull(result);
        Assert.assertEquals(1, result.size());
        Assert.assertEquals(toto, result.get(0));

        query = personDAO.createQuery().addWhere(
                Person.PROPERTY_NAME, TopiaQuery.Op.NOT_IN, new String[]{"toto"}
        );

        result = personDAO.findAllByQuery(query);
        Assert.assertNotNull(result);
        Assert.assertEquals(1, result.size());
        Assert.assertEquals(titi, result.get(0));
    }

    @Test
    public void testInOperatorForEntities() throws TopiaException {

        TopiaContext context = db.beginTransaction();

        PetDAO petDAO = TopiaTestDAOHelper.getPetDAO(context);
        PersonDAO personDAO = TopiaTestDAOHelper.getPersonDAO(context);

        Pet bernard = petDAO.create(Pet.PROPERTY_NAME, "bernard");
        Pet bianca = petDAO.create(Pet.PROPERTY_NAME, "bianca");
        Pet minou = petDAO.create(Pet.PROPERTY_NAME, "minou");
        Person toto = personDAO.create(Person.PROPERTY_NAME, "toto");
        toto.addPet(bernard);

        Person titi = personDAO.create(Person.PROPERTY_NAME, "titi");
        titi.addPet(bianca);

        Person tutu = personDAO.create(Person.PROPERTY_NAME, "tutu");
        tutu.addPet(minou);

        TopiaQuery query;

        query = petDAO.createQuery("p").addWhere(
                "p." + Pet.PROPERTY_PERSON, TopiaQuery.Op.IN, Arrays.asList(toto, titi)
        );

        List<Pet> result;
        result = petDAO.findAllByQuery(query);
        Assert.assertNotNull(result);
        Assert.assertEquals(2, result.size());
        Assert.assertTrue(result.contains(bernard));
        Assert.assertTrue(result.contains(bianca));

        query = petDAO.createQuery("p").addWhere(
                "p." + Pet.PROPERTY_PERSON, TopiaQuery.Op.NOT_IN, Arrays.asList(toto, titi)
        );

        result = petDAO.findAllByQuery(query);
        Assert.assertNotNull(result);
        Assert.assertEquals(1, result.size());
        Assert.assertTrue(result.contains(minou));
    }

}
