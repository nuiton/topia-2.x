/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.topia.persistence.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Test;
import org.nuiton.topia.TopiaTestDAOHelper.TopiaTestEntityEnum;
import org.nuiton.topia.test.entities.Person;
import org.nuiton.topia.test.entities.Pet;
import org.nuiton.topia.test.entities.Race;

/**
 * Test the {@link TopiaEntityRefTester} on
 * 
 * <ul> <li>{@link Pet}</li> <li>{@link Race}</li> <li>{@link Person}</li>
 * </ul>
 *
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @since 2.3.1
 */
public class TopiaEntityRefTesterTest extends TopiaEntityRefTester<TopiaTestEntityEnum> {

    /** Logger */
    private static final Log log =
            LogFactory.getLog(TopiaEntityRefTesterTest.class);

    private static final String PET1 = "pet1";

    private static final String PET2 = "pet2";

    private static final String RACE1 = "race1";

    private static final String PERSON1 = "person1";

    @Override
    protected TopiaTestEntityEnum[] getContracts0() {
        return new TopiaTestEntityEnum[]{
                TopiaTestEntityEnum.Pet,
                TopiaTestEntityEnum.Person,
                TopiaTestEntityEnum.Race,
        };
    }

    @Test
    public void testNoReferences() throws Exception {

        Pet pet = newEntity(TopiaTestEntityEnum.Pet, PET1);

        detectReferences(pet, 0);

        Race race = newEntity(TopiaTestEntityEnum.Race, RACE1);

        detectReferences(race, 0);

        Person person = newEntity(TopiaTestEntityEnum.Person, PERSON1);

        detectReferences(person, 0);

    }

    @Test
    public void testReferences() throws Exception {

        Pet pet = newEntity(TopiaTestEntityEnum.Pet, PET1);
        Race race = newEntity(TopiaTestEntityEnum.Race, RACE1);
        pet.setRace(race);
        Person person = newEntity(TopiaTestEntityEnum.Person, PERSON1);

        detectReferences(pet, 1, RACE1);

        nextEntry();
        assertCurrentEntry(race, 1);
        assertNextEntityRef(pet, Pet.PROPERTY_RACE, pet, race);

        pet.setPerson(person);

        detectReferences(pet, 2, RACE1, PERSON1);

        nextEntry();
        assertCurrentEntry(person, 1);
        assertNextEntityRef(pet, Pet.PROPERTY_PERSON, pet, person);

        nextEntry();
        assertCurrentEntry(race, 1);
        assertNextEntityRef(pet, Pet.PROPERTY_RACE, pet, race);

        person.addPet(pet);

        detectReferences(person, 1, PET1);

        nextEntry();
        assertCurrentEntry(pet, 1);
        assertNextAssociationEntityRef(person, Person.PROPERTY_PET, PET1, person, pet);

        Pet pet2 = newEntity(TopiaTestEntityEnum.Pet, PET2);

        person.addPet(pet2);

        detectReferences(person, 3, PET1, PET2, RACE1);

        nextEntry();
        assertCurrentEntry(pet, 1);
        assertNextAssociationEntityRef(person, Person.PROPERTY_PET, PET1, person, pet);

        nextEntry();
        assertCurrentEntry(pet2, 1);
        assertNextAssociationEntityRef(person, Person.PROPERTY_PET, PET2, person, pet2);

        nextEntry();
        assertCurrentEntry(race, 1);
        assertNextEntityRef(pet, Pet.PROPERTY_RACE, person, pet, race);

        pet2.setRace(race);

        detectReferences(person, 3, PET1, PET2, RACE1);

        nextEntry();
        assertCurrentEntry(pet, 1);
        assertNextAssociationEntityRef(person, Person.PROPERTY_PET, PET1, person, pet);

        nextEntry();
        assertCurrentEntry(pet2, 1);
        assertNextAssociationEntityRef(person, Person.PROPERTY_PET, PET2, person, pet2);

        nextEntry();
        assertCurrentEntry(race, 2);
        assertNextEntityRef(pet, Pet.PROPERTY_RACE, person, pet, race);
        assertNextEntityRef(pet2, Pet.PROPERTY_RACE, person, pet2, race);

    }
}
