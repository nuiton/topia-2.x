/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.topia.persistence;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Ignore;

/**
 * Visitor pour export xml.
 *
 * @author chatellier
 * @version $Revision$
 *          
 *          Last update : $Date$
 *          By : $Author$
 */
@Ignore
public class ExportXMLVisitor implements EntityVisitor {

    /** log. */
    private static Log log = LogFactory.getLog(ExportXMLVisitor.class);

    protected StringBuffer buffer;

    public ExportXMLVisitor() {
        buffer = new StringBuffer();
    }

    @Override
    public void start(TopiaEntity e) {
        if (log.isDebugEnabled()) {
            log.debug("start : " + e);
        }

        buffer.append("<").append(e.getClass().getName());
        buffer.append(" topiaId=\"").append(e.getTopiaId()).append("\"");
        buffer.append(" topiaCreateDate=\"").append(e.getTopiaCreateDate()).append("\"");
        buffer.append(" topiaVersion=\"").append(e.getTopiaVersion()).append("\"");
        buffer.append(">\n");
    }

    @Override
    public void visit(TopiaEntity e, String name, Class<?> type, Object value) {
        if (log.isDebugEnabled()) {
            log.debug("visit : " + e);
        }

        buffer.append("<").append(type.getName()).append(">").append(value).append("</").append(type.getName()).append(">\n");
    }

    @Override
    public void visit(TopiaEntity e, String name, Class<?> collectionType, Class<?> type,
                      Object value) {
    }

    @Override
    public void visit(TopiaEntity e, String name, Class<?> collectionType, Class<?> type, int index, Object value) {
        visit(e, name, type, value);
    }

    @Override
    public void end(TopiaEntity e) {
        if (log.isDebugEnabled()) {
            log.debug("end : " + e);
        }

        buffer.append("</").append(e.getClass().getName()).append(">\n");
    }

    @Override
    public String toString() {
        String content = buffer.toString();
        return content;
    }

    @Override
    public void clear() {
        // do nothing
    }


}
