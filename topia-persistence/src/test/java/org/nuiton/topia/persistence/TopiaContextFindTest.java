/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.topia.persistence;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaDatabase;
import org.nuiton.topia.TopiaException;
import org.nuiton.topia.TopiaTestDAOHelper;
import org.nuiton.topiatest.Address;
import org.nuiton.topiatest.AddressDAO;
import org.nuiton.topiatest.Gender;
import org.nuiton.topiatest.Personne;
import org.nuiton.topiatest.PersonneDAO;

import java.util.List;

/**
 * Tests the TopiaContext#find|findAll|findUnique methods
 *
 * @author Arnaud Thimel &lt;thimel@codelutin.com&gt;
 */
public class TopiaContextFindTest {

    @Rule
    public final TopiaDatabase db = new TopiaDatabase();

    protected TopiaContext context;
    protected AddressDAO addressDAO;
    protected PersonneDAO personneDAO;
    protected Address address;

    @Before
    public void createCompanies() throws TopiaException {
        context = db.beginTransaction();
        personneDAO = TopiaTestDAOHelper.getPersonneDAO(context);
        addressDAO = TopiaTestDAOHelper.getAddressDAO(context);

        address = addressDAO.create(
                Address.PROPERTY_ADRESS, "17 rue de la Pote Gellée, 44200 NANTES");

        personneDAO.create(
                Personne.PROPERTY_NAME, "Arnaud",
                Personne.PROPERTY_GENDER, Gender.MALE);
        personneDAO.create(
                Personne.PROPERTY_NAME, "Charlotte",
                Personne.PROPERTY_GENDER, Gender.FEMALE);
        personneDAO.create(
                Personne.PROPERTY_NAME, "Hortense",
                Personne.PROPERTY_GENDER, Gender.FEMALE);
        context.commitTransaction();
    }

    @Test
    public void testFindDAO() throws TopiaException {
        Assert.assertEquals(3, personneDAO.count());

        Assert.assertEquals(2, personneDAO.findAllByGender(Gender.FEMALE).size());
        Assert.assertNotNull(personneDAO.findByGender(Gender.FEMALE));
        Assert.assertNotNull(personneDAO.findByGender(Gender.MALE));
        Assert.assertNull(personneDAO.findByGender(null));

        Assert.assertEquals(0, personneDAO.findAllByName("nobody").size());
    }

    @Test
    public void testFindAll() throws TopiaException {
        Assert.assertEquals(3, personneDAO.count());

        String query = "from " + Personne.class.getName() +
                " where " + Personne.PROPERTY_GENDER + "=:g";

        List females = context.findAll(query, "g", Gender.FEMALE);
        Assert.assertEquals(2, females.size());

        List males = context.findAll(query, "g", Gender.MALE);
        Assert.assertEquals(1, males.size());

        List all = context.findAll("from " + Personne.class.getName());
        Assert.assertEquals(3, all.size());

        List none = context.findAll("from " + Personne.class.getName() +
                " where " + Personne.PROPERTY_NAME + "=:pax", "pax", "nobody");
        Assert.assertEquals(0, none.size());
    }

    @Test
    public void testFind() throws TopiaException {
        Assert.assertEquals(3, personneDAO.count());

        String query = "from " + Personne.class.getName() +
                " where " + Personne.PROPERTY_GENDER + "=:g";

        List females = context.find(query, 0, 100, "g", Gender.FEMALE);
        Assert.assertEquals(2, females.size());

        females = context.find(query, 0, 0, "g", Gender.FEMALE);
        Assert.assertEquals(1, females.size());
        Personne charlotte = (Personne)females.get(0);

        females = context.find(query, 1, 1, "g", Gender.FEMALE);
        Assert.assertEquals(1, females.size());
        Personne hortense = (Personne)females.get(0);

        Assert.assertFalse(hortense.equals(charlotte));

        // endIndex = -1 not supported in ToPIA 2.6, wait for 3.0
//        females = context.find(query, 0, -1, "g", Gender.FEMALE);
//        Assert.assertEquals(2, females.size());
    }

    @Test
    public void testFindUnique() throws TopiaException {
        Assert.assertEquals(3, personneDAO.count());

        String query = "from " + Personne.class.getName() +
                " where " + Personne.PROPERTY_GENDER + "=:g";

        Object male = context.findUnique(query, "g", Gender.MALE);
        Assert.assertNotNull(male);

        Object none = context.findUnique("from " + Personne.class.getName() +
                " where " + Personne.PROPERTY_NAME + "=:pax", "pax", "nobody");
        Assert.assertNull(none);
    }

    @Test(expected = TopiaException.class)
    public void testFindUniqueOutOfBounds() throws TopiaException {
        Assert.assertEquals(3, personneDAO.count());

        String query = "from " + Personne.class.getName() +
                " where " + Personne.PROPERTY_GENDER + "=:g";

        Object female = context.findUnique(query, "g", Gender.FEMALE);
        Assert.assertNotNull(female);
    }

}
