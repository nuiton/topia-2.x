/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.topia.persistence;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.PropertyValueException;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaDatabase;
import org.nuiton.topia.TopiaException;
import org.nuiton.topia.TopiaTestDAOHelper;
import org.nuiton.topiatest.NaturalizedEntity;
import org.nuiton.topiatest.NaturalizedEntityDAO;

/**
 * NaturalIdTest
 * 
 * Created: 18 févr. 2010
 *
 * @author fdesbois
 * @version $Revision$
 *          
 *          Mise a jour: $Date$
 *          par : $Author$
 */
public class NaturalIdTest {

    private static final Log log = LogFactory.getLog(NaturalIdTest.class);

    @Rule
    public final TopiaDatabase db = new TopiaDatabase();

    @Test
    public void testCreateSucessfull() throws Exception {
        log.debug("Test naturalId : create succesfull");
        TopiaContext transaction = db.beginTransaction();

        NaturalizedEntityDAO dao =
                TopiaTestDAOHelper.getNaturalizedEntityDAO(transaction);

        // No exception will be thrown with the two properties
        dao.createByNaturalId(5, "str");
        transaction.commitTransaction();

        // No exception will only the need property
        dao.createByNotNull(3);
        transaction.commitTransaction();

        // No exception will only the need property
        dao.create(NaturalizedEntity.PROPERTY_NATURAL_ID_NOT_NULL, 3);
        transaction.commitTransaction();
    }

    @Test
    public void testCreateFailed() throws Exception {
        log.debug("Test naturalId : create failed");
        TopiaContext transaction = db.beginTransaction();

        NaturalizedEntityDAO dao =
                TopiaTestDAOHelper.getNaturalizedEntityDAO(transaction);

        // Exception will be throw
        try {
            dao.create();
            transaction.commitTransaction();

            // Note : this is possible to create an empty entity if the type
            // is primitive like 'int' which have a default value of '0'
        } catch (PropertyValueException eee) {
            Assert.assertEquals("naturalIdNotNull", eee.getPropertyName());
        }
    }

    @Test
    public void testUpdateFailed() throws Exception {
        log.debug("Test naturalId : update failed");

        TopiaContext transaction = db.beginTransaction();

        NaturalizedEntityDAO dao =
                TopiaTestDAOHelper.getNaturalizedEntityDAO(transaction);

        NaturalizedEntity entity =
                dao.createByNaturalId(5, "str");
        transaction.commitTransaction();

        // Exception will be throw : not allowed to modify a naturalId property
        try {
            entity.setNaturalIdNotNull(8);
            transaction.commitTransaction();
        } catch (TopiaException eee) {
            Assert.assertEquals(javax.persistence.PersistenceException.class, eee.getCause().getClass());
        }
    }

    @Test
    public void testFindByNaturalId() throws Exception {
        log.debug("Test naturalId : findByNaturalId");
        TopiaContext transaction = db.beginTransaction();

        NaturalizedEntityDAO dao =
                TopiaTestDAOHelper.getNaturalizedEntityDAO(transaction);

        NaturalizedEntity entity =
                dao.createByNaturalId(5, "str");
        transaction.commitTransaction();

        NaturalizedEntity result = dao.findByNaturalId(5, "str");

        Assert.assertEquals(entity, result);
    }

    @Test
    public void testExistNaturalId() throws Exception {
        log.debug("Test naturalId : existNaturalId");
        TopiaContext transaction = db.beginTransaction();

        NaturalizedEntityDAO dao =
                TopiaTestDAOHelper.getNaturalizedEntityDAO(transaction);

        dao.createByNaturalId(5, "str");
        transaction.commitTransaction();

        boolean result = dao.existByNaturalId(5, "str");

        Assert.assertTrue(result);

        // not find with only one correct property
        result = dao.existByNaturalId(8, "str");

        Assert.assertFalse(result);
    }

    @Test
    public void testNaturalIdAreGeneralized() throws Exception {

        // test that natural ids are generalized
        String[] generalizedNaturalizedNaturalIds = TopiaTestDAOHelper.TopiaTestEntityEnum.GeneralizedNaturalizedEntity.getNaturalIds();
        String[] naturalizedNaturalIds = TopiaTestDAOHelper.TopiaTestEntityEnum.NaturalizedEntity.getNaturalIds();
        Assert.assertArrayEquals(generalizedNaturalizedNaturalIds, naturalizedNaturalIds);
    }

    @Test
    public void testNotNullsAreGeneralized() throws Exception {

        // test that not nulls are generalized
        String[] generalizedNaturalizedNotNulls = TopiaTestDAOHelper.TopiaTestEntityEnum.GeneralizedNaturalizedEntity.getNotNulls();
        String[] naturalizedNotNulls = TopiaTestDAOHelper.TopiaTestEntityEnum.NaturalizedEntity.getNotNulls();
        Assert.assertArrayEquals(generalizedNaturalizedNotNulls, naturalizedNotNulls);
    }
}
