/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.topia.persistence.util;

import org.junit.After;
import org.junit.Assert;
import org.nuiton.topia.TopiaException;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaEntityEnum;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.SortedMap;

import static java.util.Map.Entry;

/**
 * A abstract class to help testing {@link TopiaEntityRef} as detectes types, or
 * detects or references.
 * 
 * An example of use if given in the test {@link TopiaEntityRefTesterTest}.
 *
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @since 2.3.1
 */
public abstract class TopiaEntityRefTester<T extends TopiaEntityEnum> {

    protected SortedMap<TopiaEntity, List<TopiaEntityRef>> detected;

    protected Iterator<Entry<TopiaEntity, List<TopiaEntityRef>>> itr;

    protected Entry<TopiaEntity, List<TopiaEntityRef>> entry;

    protected List<TopiaEntityRef> refs;

    protected T[] contracts;

    protected int index;

    protected T[] getContracts() {
        if (contracts == null) {
            contracts = getContracts0();
        }
        return contracts;
    }

    /** @return all the {@link TopiaEntityEnum} to be used */
    protected abstract T[] getContracts0();

    @After
    public void after() {
        itr = null;
        entry = null;
        refs = null;
        detected = null;
        index = 0;
    }

    /**
     * Creates a new entity with the given {@code topiaId} (will use the {@link
     * TopiaEntityEnum#getImplementation()} class).
     *
     * @param constant the constant defining the entity
     * @param topiaId  the topia to assign
     * @param <T>      the type of entity
     * @return the new entity
     * @throws IllegalAccessException if can no access entity constructor
     * @throws InstantiationException if can no instanciate the entity
     */
    protected <T extends TopiaEntity> T newEntity(
            TopiaEntityEnum constant, String topiaId) throws
            IllegalAccessException,
            InstantiationException {
        Class<? extends TopiaEntity> impl = constant.getImplementation();
        TopiaEntity topiaEntity = impl.newInstance();
        topiaEntity.setTopiaId(topiaId);
        return (T) topiaEntity;
    }

    /**
     * Obtain the reference of an association for a given entity.
     * 
     * Example, to obtain the Pet 'pudding' on a Person :
     * <pre>pet[@topiaId='pudding']</pre>
     * invoke
     * <pre>getAssociationRef('pet',pet);</pre>
     *
     * @param associationName the name of the association
     * @param e               the required entity
     * @return the reference of the association
     */
    protected String getAssociationRef(String associationName, TopiaEntity e) {
        return getAssociationRef(associationName, e.getTopiaId());
    }

    /**
     * Obtain the reference of an association for a given id.
     * 
     * Example to obtain the Pet 'pudding' on a Person :
     * <pre>pet[@topiaId='pudding']</pre>
     * invoke
     * <pre>getAssociationRef('pet','pudding');</pre>
     *
     * @param associationName the name of the association
     * @param e               the id
     * @return the reference of the association
     */
    protected String getAssociationRef(String associationName, String e) {
        String s = String.format(
                TopiaEntityHelper.ASSOCIATION_PATTERN,
                associationName,
                e
        );
        return s;
    }

    /**
     * Obtain the next entry from the iterator.
     * 
     * As a side-effect, it will increment the state {@link #index}.
     */
    protected void nextEntry() {
        if (!itr.hasNext()) {
            throw new IllegalStateException("no more entry to get...");
        }
        entry = itr.next();
        index = 0;
    }

    /**
     * Detects the references from the given {@code entity} which have their
     * topiaId in the given list of {@code ids}.
     * 
     * As a side-effect, it will update the states {@code detected} and set the
     * iterator {@code itr} to the first position on detected entries.
     *
     * @param entity the entity to seek
     * @param nb     the required number of entries
     * @param ids    the ids to seek
     * @throws TopiaException if any pb while visiting entities
     */
    protected void detectReferences(
            TopiaEntity entity, int nb, String... ids) throws TopiaException {
        detected = TopiaEntityHelper.detectReferences(
                getContracts(), ids, entity);
        assertDetected(nb);

    }

    /**
     * Detects the references from the given {@code entity} which have their
     * topiaId in the given list of {@code ids}.
     * 
     * As a side-effect, it will update the states {@code detected} and set the
     * iterator {@code itr} to the first position on detected entries.
     *
     * @param entity the entity to seek
     * @param nb     the required number of entries
     * @param ids    the ids to seek
     * @throws TopiaException if any pb while visiting entities
     */
    protected void detectReferences(
            Collection<? extends TopiaEntity> entity,
            int nb, String... ids) throws TopiaException {
        detected = TopiaEntityHelper.detectReferences(
                getContracts(), ids, entity);
        assertDetected(nb);
    }

    /**
     * Detects the type of entities fro the given {@code data}.
     *
     * @param expected the array of expected types
     * @param data     the data to seek
     * @throws TopiaException if any pb while visiting data
     */
    protected void detectTypes(Class<?>[] expected,
                               TopiaEntity... data) throws TopiaException {
        Set<String> fqns = new HashSet<String>(expected.length);
        Set<Class<? extends TopiaEntity>> actual = null;
        for (Class<?> c : expected) {
            fqns.add(c.getName());
        }
        try {
            actual = TopiaEntityHelper.detectTypes(getContracts(), data);
            Assert.assertEquals(expected.length, actual.size());
            for (Class<? extends TopiaEntity> c : actual) {
                Assert.assertTrue(fqns.contains(c.getName()));
            }
        } finally {
            fqns.clear();
            if (actual != null) {
                actual.clear();
            }
        }
    }

    /**
     * Asserts if the given entry definition is equals to the next entry
     * references on internal state state {@code entry}.
     *
     * @param invoker         the invoker of the reference
     * @param invokerProperty the access path of the reference
     * @param expected        the expected topia entities path to access
     *                        reference target
     */
    protected void assertNextEntityRef(TopiaEntity invoker,
                                       String invokerProperty,
                                       TopiaEntity... expected) {
        assertEntityRef(index, invoker, invokerProperty, expected);
        index++;
    }

    /**
     * Asserts if the given entry definition (of an association) is equals to
     * the next entry reference on internal state {@code entry}.
     *
     * @param invoker     the invoker of the reference
     * @param association the association name
     * @param id          the id of the association ( see {@link
     *                    #getAssociationRef(String, TopiaEntity)}
     * @param expected    the expected topia entities path to access reference
     *                    target
     */
    protected void assertNextAssociationEntityRef(TopiaEntity invoker,
                                                  String association,
                                                  String id,
                                                  TopiaEntity... expected) {
        String invokerProperty = getAssociationRef(association, id);
        assertNextEntityRef(invoker, invokerProperty, expected);
    }

    /**
     * Asserts if the given entry definition is equals to the reference entry at
     * position {@code index} on internal state {@code entry}.
     *
     * @param index           the index of the reference to test in {@code
     *                        entry}
     * @param invoker         the invoker of the reference
     * @param invokerProperty the path of the reference
     * @param expected        th expected topia entities path to access
     *                        reference target
     */
    protected void assertEntityRef(int index,
                                   TopiaEntity invoker,
                                   String invokerProperty,
                                   TopiaEntity... expected) {
        Assert.assertNotNull(refs);
        Assert.assertTrue("no index [" + index + "] in refs of size " +
                          refs.size(), index < refs.size());
        TopiaEntityRef actual = refs.get(index);
        Assert.assertEquals(actual.getInvoker(), invoker);
        Assert.assertEquals(actual.getInvokerProperty(), invokerProperty);

        TopiaEntity[] path = actual.getPath();
        Assert.assertEquals(expected.length, path.length);
        for (int i = 0; i < expected.length; i++) {
            Assert.assertEquals(expected[i].getTopiaId(), path[i].getTopiaId());
        }
    }

    /**
     * Asserts if the given {@code expected} entity is equals to the source of
     * the internal state {@code entry}.
     *
     * @param expected the required source of the entry
     * @param nbPath   the expected number of references of the given entry
     */
    protected void assertCurrentEntry(TopiaEntity expected,
                                      int nbPath) {
        assertEntry(expected, nbPath, entry);
    }

    /**
     * Asserts if the given {@code entry} has a good source and a good number of
     * references.
     *
     * @param expected the expected entry source
     * @param nbPath   the expected number of reference of the entry
     * @param entry    the entry to test
     */
    private void assertEntry(TopiaEntity expected,
                             int nbPath,
                             Entry<TopiaEntity, List<TopiaEntityRef>> entry) {
        Assert.assertNotNull(entry);
        Assert.assertEquals(expected, entry.getKey());
        refs = entry.getValue();
        Assert.assertEquals(nbPath, refs.size());
    }

    /**
     * Asserts that the number of detected entries (store in internal state
     * {@code detected}) is ok.
     * 
     * As a side-effect, it will reset the internal state {@code itr} on the
     * first entry of the {@code detected} list.
     *
     * @param size the expected number of detected entries
     */
    protected void assertDetected(int size) {
        Assert.assertNotNull(detected);
        Assert.assertEquals(size, detected.size());
        itr = detected.entrySet().iterator();
    }
}
