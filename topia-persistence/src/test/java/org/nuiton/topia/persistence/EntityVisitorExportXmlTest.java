/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.topia.persistence;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaDatabase;
import org.nuiton.topia.TopiaException;
import org.nuiton.topia.TopiaTestDAOHelper;
import org.nuiton.topiatest.Address;
import org.nuiton.topiatest.AddressDAO;
import org.nuiton.topiatest.Company;
import org.nuiton.topiatest.CompanyDAO;
import org.nuiton.topiatest.Department;
import org.nuiton.topiatest.DepartmentDAO;
import org.nuiton.topiatest.Employe;
import org.nuiton.topiatest.EmployeDAO;

/**
 * Test de visitor.
 *
 * @author chatellier
 * @version $Revision$
 *          
 *          Last update : $Date$
 *          By : $Author$
 */
public class EntityVisitorExportXmlTest {

    private static final Log log =
            LogFactory.getLog(EntityVisitorExportXmlTest.class);

    @Rule
    public final TopiaDatabase db = new TopiaDatabase();

    /**
     * Prepare test.
     * 
     * Add all tests commons data
     *
     * @throws TopiaException if could not create datas
     */
    @Before
    public void setUp() throws TopiaException {

        TopiaContext newContext = db.beginTransaction();
        try {
            // company
            CompanyDAO companyDAO = TopiaTestDAOHelper.getCompanyDAO(newContext);
            Company clCompany = companyDAO.create(Company.PROPERTY_NAME, "CodeLutin");

            // employe
            EmployeDAO employeDAO = TopiaTestDAOHelper.getEmployeDAO(newContext);
            Employe empl1 = employeDAO.create(Employe.PROPERTY_NAME, "boss", Employe.PROPERTY_SALARY, 30000);

            AddressDAO adressDAO = TopiaTestDAOHelper.getAddressDAO(newContext);
            Address addr1 = adressDAO.create(Address.PROPERTY_CITY, "Nantes", Address.PROPERTY_ADRESS, "12 Avenue Jules Vernes");
            empl1.setAddress(addr1);

            Employe empl2 = employeDAO.create(Employe.PROPERTY_NAME, "boss2", Employe.PROPERTY_SALARY, 29000);
            Address addr2 = adressDAO.create(Address.PROPERTY_CITY, "Nantes", Address.PROPERTY_ADRESS, "12 Avenue Jules Vernes");
            empl2.setAddress(addr2);

            // departement
            DepartmentDAO departmentDAO = TopiaTestDAOHelper.getDepartmentDAO(newContext);
            Department depComm = departmentDAO.create(Department.PROPERTY_NAME, "Commercial");
            depComm.setLeader(empl1);

            Department depDev = departmentDAO.create(Department.PROPERTY_NAME, "Dev");
            depDev.setLeader(empl2);
            clCompany.addDepartment(depComm);
            clCompany.addDepartment(depDev);

            newContext.commitTransaction();
        } finally {

            newContext.closeContext();
        }
    }


    /**
     * Test l'export XML via un visiteur.
     * 
     * Parcourt en profondeur.
     *
     * @throws TopiaException
     */
    @Test
    public void testExportXMLDepth() throws TopiaException {

        TopiaContext context = db.beginTransaction();

        CompanyDAO companyDAO = TopiaTestDAOHelper.getCompanyDAO(context);
        Company clCompany = companyDAO.findByName("CodeLutin");

        EntityVisitor delegateVisitor = new ExportXMLVisitor();
        EntityVisitor visitor = new DepthEntityVisitor(delegateVisitor);
        clCompany.accept(visitor);

        if (log.isInfoEnabled()) {
            log.info("Export XML = \n" + delegateVisitor.toString());
        }
    }
}
