/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.topia.persistence.util;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.nuiton.topia.TopiaTestDAOHelper;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaEntityEnum;
import org.nuiton.topiatest.Company;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;
import org.nuiton.util.beans.BinderModelBuilder;

public class TopiaEntityBinderTest {

    static TopiaEntityEnum[] contracts;

    @BeforeClass
    public static void setUpClass() throws Exception {
        contracts = TopiaTestDAOHelper.getContracts();
    }

    public static class CompanyDTO {

        protected String name;

        protected int siret;

        protected String id;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getSiret() {
            return siret;
        }

        public void setSiret(int siret) {
            this.siret = siret;
        }

        public String getTopiaId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }

    @Before
    public void setUp() {
        BinderFactory.clear();
    }

    @AfterClass
    public static void afterClass() {
        BinderFactory.clear();
    }

    @Test
    public void testBinder() {

        BinderModelBuilder<Company, CompanyDTO> builder = BinderModelBuilder.newEmptyBuilder(Company.class, CompanyDTO.class);
        builder.addSimpleProperties(Company.PROPERTY_NAME, Company.PROPERTY_SIRET);
        builder.addProperties(TopiaEntity.TOPIA_ID, "id");
        BinderFactory.registerBinderModel(builder);

        Binder<Company, CompanyDTO> binder =
                BinderFactory.newBinder(Company.class, CompanyDTO.class);
        Assert.assertNotNull(binder);
    }
}
