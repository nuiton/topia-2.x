/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.topia.persistence;

import com.google.common.collect.Lists;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaDatabase;
import org.nuiton.topia.TopiaException;
import org.nuiton.topia.TopiaTestDAOHelper;
import org.nuiton.topia.test.entities.Person;
import org.nuiton.topia.test.entities.PersonDAO;

import java.util.List;

/**
 * Test on {@link TopiaDAO}.
 * 
 * Last update : $Date$
 * By : $Author$
 *
 * @author chatellier
 * @version $Revision$
 */
public class TopiaDAOTest {

    @Rule
    public final TopiaDatabase db = new TopiaDatabase();

    protected TopiaContext context;

    protected PersonDAO dao;

    @Before
    public void setup() throws TopiaException {

        context = db.beginTransaction();

        dao = TopiaTestDAOHelper.getPersonDAO(context);
    }

    /**
     * Test de creer une entité et de verifier qu'elle est
     * présente dans la persistence au sein de la transaction.
     *
     * @throws Exception if any exception while test
     */
    @Test
    public void testCreateAndFindInTransaction() throws Exception {

        // appel 1 find all
        createPerson("toto");
        List<Person> allPerson = dao.findAll();
        Assert.assertEquals(1, allPerson.size());
        context.commitTransaction();

        // recherce la personne créée dans la même transaction
        Person person2 = createPerson("titi");
        allPerson = dao.findAll();
        Assert.assertEquals(2, allPerson.size());
        Assert.assertThat(allPerson, CoreMatchers.hasItem(person2));

        context.rollbackTransaction();

        // meme test apres roolback
        Person person3 = createPerson("tata");
        allPerson = dao.findAll();
        Assert.assertEquals(2, allPerson.size());
        Assert.assertThat(allPerson, CoreMatchers.hasItem(person3));

        context.commitTransaction();
    }

    @Test
    public void findAllLazyByQuery() throws TopiaException {

        Assert.assertEquals(dao.count(), 0);

        createPersons(101);

        Iterable<Person> allByLazy = dao.findAllLazyByQuery(
                100,
                "FROM " + dao.getTopiaEntityEnum().getImplementationFQN() + " ORDER BY id");

        List<Person> actual = Lists.newArrayList();

        for (Person person : allByLazy) {
            actual.add(person);
        }
        Assert.assertEquals(dao.count(), actual.size());

        allByLazy = dao.findAllLazyByQuery(
                54,
                "FROM " + dao.getTopiaEntityEnum().getImplementationFQN() + " ORDER BY id");

        actual = Lists.newArrayList();

        for (Person person : allByLazy) {
            actual.add(person);
        }
        Assert.assertEquals(dao.count(), actual.size());

        allByLazy = dao.findAllLazyByQuery(
                49,
                "FROM " + dao.getTopiaEntityEnum().getImplementationFQN() + " ORDER BY id");

        actual = Lists.newArrayList();

        for (Person person : allByLazy) {
            actual.add(person);
        }
        Assert.assertEquals(dao.count(), actual.size());

        allByLazy = dao.findAllLazyByQuery(
                101,
                "FROM " + dao.getTopiaEntityEnum().getImplementationFQN() + " ORDER BY id");

        actual = Lists.newArrayList();

        for (Person person : allByLazy) {
            actual.add(person);
        }
        Assert.assertEquals(dao.count(), actual.size());

        allByLazy = dao.findAllLazyByQuery(
                102,
                "FROM " + dao.getTopiaEntityEnum().getImplementationFQN() + " ORDER BY id");

        actual = Lists.newArrayList();

        for (Person person : allByLazy) {
            actual.add(person);
        }
        Assert.assertEquals(dao.count(), actual.size());
    }

    @Test
    public void iterateOnTopiaDAO() throws TopiaException {

        createPersons(1999);

        List<Person> excepted = dao.findAll();

        List<Person> actual = Lists.newArrayList();

        for (Person person : dao) {
            Assert.assertThat(excepted, CoreMatchers.hasItem(person));
            actual.add(person);
        }
        Assert.assertEquals(excepted.size(), actual.size());

        dao.setBatchSize(54);

        actual = Lists.newArrayList();

        for (Person person : dao) {
            Assert.assertThat(excepted, CoreMatchers.hasItem(person));
            actual.add(person);
        }
        Assert.assertEquals(excepted.size(), actual.size());

    }

    protected void createPersons(int number) throws TopiaException {
        for (int i = 0; i < number; i++) {
            createPerson("toto" + i);
        }

        context.commitTransaction();
    }

    protected Person createPerson(String name) throws TopiaException {
        return dao.create(Person.PROPERTY_NAME, name);
    }
}
