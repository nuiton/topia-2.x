/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.topia;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import java.io.File;
import java.io.InputStream;
import java.util.Properties;

/**
 * Put this class as a Rule in test to obtain a new isolated db for each test.
 * 
 * Here is a simple example of usage :
 * <pre>
 * public class MyTest {
 *
 *   \@Rule
 *   public final TopiaDatabase db = new TopiaDatabase();
 *
 *   \@Test
 *   public void testMethod() throws TopiaException {
 *
 *       TopiaContext tx = db.beginTransaction();
 *       ...
 * }
 * </pre>
 * The db created will be unique for each test method (and for each build also).
 * 
 * You don't need to close any transaction, it will be done for you and the end
 * of each method test.
 *
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @since 2.6.8
 */
public class TopiaDatabase extends TestWatcher {

    /** Logger. */
    private static final Log log = LogFactory.getLog(TopiaDatabase.class);

    /** A time-stamp, allow to make multiple build and keep the tests data. */
    public static final String TIMESTAMP = String.valueOf(System.nanoTime());

    private File testBasedir;

    private Properties dbConfiguration;

    private TopiaContext rootCtxt;

    private final String configurationPath;

    public TopiaDatabase() {
        this(TestHelper.DEFAULT_CONFIGURATION_LOCATION);
    }

    public TopiaDatabase(String configurationPath) {
        this.configurationPath = configurationPath;
    }

    @Override
    protected void starting(Description description) {

        // get test directory
        testBasedir = getTestSpecificDirectory(
                description.getTestClass(),
                description.getMethodName());

        if (log.isDebugEnabled()) {
            log.debug("testBasedir = " + testBasedir);
        }

        // create the root context
        try {

            dbConfiguration = new Properties();
            InputStream stream =
                    getClass().getResourceAsStream(configurationPath);

            try {
                dbConfiguration.load(stream);
            } finally {
                stream.close();
            }
            dbConfiguration.setProperty(
                    TopiaContextFactory.CONFIG_PERSISTENCE_CLASSES,
                    TopiaTestDAOHelper.getImplementationClassesAsString());

            // make sure we always use a different directory

            String dbPath = new File(testBasedir, "db").getAbsolutePath();

            if (log.isDebugEnabled()) {
                log.debug("dbPath = " + dbPath);
            }
            dbConfiguration.setProperty(
                    TopiaContextFactory.CONFIG_URL, "jdbc:h2:file:" + dbPath);

            onDbConfigurationCreate(dbConfiguration, testBasedir, dbPath);

            rootCtxt = TopiaContextFactory.getContext(dbConfiguration);
        } catch (Exception e) {
            throw new IllegalStateException(
                    "Could not start db at " + testBasedir, e);
        }
    }

    @Override
    public void finished(Description description) {

        if (rootCtxt != null && !rootCtxt.isClosed()) {
            try {
                rootCtxt.closeContext();
            } catch (TopiaException e) {
                if (log.isErrorEnabled()) {
                    log.error("Could not close topia root context", e);
                }
            }
        }
        rootCtxt = null;
        dbConfiguration = null;
    }

    public File getTestBasedir() {
        return testBasedir;
    }

    public TopiaContext getRootCtxt() {
        return rootCtxt;
    }

    public Properties getDbConfiguration() {
        return dbConfiguration;
    }

    public TopiaContext beginTransaction() throws TopiaException {
        return rootCtxt.beginTransaction();
    }

    protected void onDbConfigurationCreate(Properties configuration,
                                           File testDir,
                                           String dbPath) {

    }

    public static File getTestSpecificDirectory(Class<?> testClassName, String methodName) {
        // Trying to look for the temporary folder to store data for the test
        String tempDirPath = System.getProperty("java.io.tmpdir");
        if (tempDirPath == null) {
            // can this really occur ?
            tempDirPath = "";
            if (log.isWarnEnabled()) {
                log.warn("'\"java.io.tmpdir\" not defined");
            }
        }
        File tempDirFile = new File(tempDirPath);

        // create the directory to store database data
        String dataBasePath = testClassName.getName()
                              + File.separator // a directory with the test class name
                              + methodName// a sub-directory with the method name
                              + '_'
                              + TIMESTAMP; // and a timestamp
        File databaseFile = new File(tempDirFile, dataBasePath);
        return databaseFile;
    }
}

