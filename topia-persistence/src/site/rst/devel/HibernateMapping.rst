.. -
.. * #%L
.. * ToPIA :: Persistence
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 2004 - 2015 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

=================
Mapping hibernate
=================

Ce document décrit les choix de mapping faits en fonction
du diagramme de classe UML.

JDBC
====

Généralité
----------

- Tous les objets utilisent le versionnement dans un champs version::
  
        <version name="topiaVersion" type="long"/>
  
- On utilise les méthodes d'accès pour accêder aux propriétés

Héritage
--------

- Seules les classes concrêtes ont un mapping (au travers de l'interface + impl)
- Chaque classe à un fichier de mapping séparé.

On utilisera union-subclass

Identifiant
-----------

Lors de la description de la classe, on peut indiquer de ne pas générer de clé
technique. Dans ce cas, la clé métier est utilisée (tagvalue: technicalKey=false).
Par défaut une clé technique est utilisée et est de la forme uuid.hex::

  <id name="topiaId" column="topiaId" type="string">
    <generator class="uuid.hex"/>
  </id>

La description de la clé métier est faite par un tagvalue sur la classe
(tagvalue: key=prop1,prop2,prop3). Si une clé technique est utilisée, une 
contrainte d'unicité est tout de même faite sur la clé métier.

La clé métier sert aussi pour le toString, equals, hashCode.

Si l'id d'une entité est composé de plusieurs champs, ces champs doivent-être
rassemblés dans une classe indépendante de l'entité. Par contre le mapping
assemble l'entity et son id dans la même table::

  <composite-id name="#field" class="#class">
    <key-property name="#field1" column="#col1"/>
    <key-property name="#field2" column="#col2"/>
    <key-property name="#field3" column="#col3"/>
  </composite>
  
Si l'id est un objet simple il peut être directement mis dans l'entité.

Relation 1-0
------------



Relation 1-1
------------

Relation 0-N
------------

Relation 1-N
------------

Relation N-N
------------

Classe d'association
--------------------

Composition
-----------

Le composant peut changer de propriétaire (set méthode) mais le 
propriétaire perd en même temps le lien vers son composé.

Aggregation
-----------

Si une classe est aggrégée avec une autre, alors elle suit la vie de l'entité
à laquelle elle est aggrégée (cascade delete, update)

Elle ne peut pas être affectée a une autre entité. Pas de set sur cette classe
vers l'autre classe.

XML
===

Toutes les propriétés sont des éléments sauf la clé technique qui est un
attribut.
