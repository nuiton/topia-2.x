.. -
.. * #%L
.. * ToPIA :: Persistence
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 2004 - 2014 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

===================
TopiaContextFactory
===================

Le topia context est créé en faisant la demande sur le *TopiaContextFactory*.
On peut passer en paramètre au *TopiaContextFactory* un object *Property* qui
permet de configurer le context. Si aucun fichier est passé en paramètre, un fichier de
propriété **TopiaContextImpl.properties** est recherché dans le classpath.

Fichier de configuration
========================

Le fichier de configuration est un fichier lisible par un objet *Property*.
Il contient différentes informations :
- liste des entités à gérer
- option de connexion à la base de données
- les services à activer

Liste des entités à gérer
-------------------------

Il est possible de définir les entités soit directement en indiquant un
répertoire contenant les mappings hibernate::

  topia.persistence.directories=<path>

soit en indiquant une liste de classe séparé par des virgule::

  topia.persistence.classes=<list de classe>

Le mieux est d'utiliser la liste de classe qui est non spécifique à une
persistence ou à une plateforme.

Option de connexion à la base de données
----------------------------------------

Hibernate
~~~~~~~~~

Les options pour hibernate peuvent être directement dans le fichier de configuration principal ou dans un fichier de configuration secondaire. Dans ce cas il faut indiquer dans le fichier de configuration principal le chemin de ce fichier::

  topia.persistence.properties.file=<filesystem or classpath path>

Les options hibernates sont les options hibernate classique :

- hibernate.connection.username
- hibernate.connection.password
- hibernate.dialect
- hibernate.connection.driver_class
- hibernate.connection.url

Les services à activer
----------------------

Il est possible d'indiquer les services à activer grâce aux options::

  topia.service.<service name>=<class>
  topia.service.security=org.nuiton.topia.security.TopiaSecurityServiceImpl
  topia.service.index=org.nuiton.topia.index.LuceneIndexer
  topia.service.history=org.nuiton.topia.history.TopiaHistoryServiceImpl

<service name> doit correspondre au nom de service que <class> retourne, sinon il est désactivé.

Les services disponibles actuellement sont:

- Service d'indexation full text
- Service d'historisation des modifications des entités
- Service de sécurité
- Service d'upgrade automatique des données hibernates

TopiaContext
============

Le *TopiaContext* permet de récupérer les services, d'ouvrir des transactions
en récupérant des nouveaux *TopiaContext* fils, de s'enregistrer en tant que
listener pour recevoir les notifications de modification sur les entités.

Sur les *TopiaContext* fils, on peut récupérer des DAO qui permettent de
créer, modifier, rechercher les entités.

Entité
======

Normalement **Topia** est fait pour pouvoir générer n'importe quel type de POJO
et les rendre persistants. Mais il est plus simple d'utiliser la génération
de code fournit avec **Topia** pour générer les entités à partir d'un
diagramme UML. Dans ce cas toutes les entités héritent de *TopiaEntity* qui
contient des méthodes pour s'enregistrer sur les modifications des attributs
ou pouvoir lever un droit de veto sur une modification. Sont aussi
disponible les méthodes:

- getTopiaId
- getTopiaVersion
- getTopiaCreationDate
- getTopiaContext
- getComposite: Retourne tous les objets qui seront effacé si cet objet est effacé
- getAggregate: Retourne les objets en lien avec celui-ci

Lorsque l'on utilise la génération on peut implanter une classe qui hérite
du *Abstract* généré et qui se finisse par Impl. Par exemple si dans notre
modèle nous avons la classe *Toto* il sera généré une interface *Toto* et
une classe abstraite *TotoAbstract*, il faudra alors implanter *TotoImpl*::

  extends TotoAbstract extends TopiaEntityAbstract implements Toto
  extends TopiaEntityAbstract implements *TopiaEntity

Cette classe impl permet d'ajouter des méthodes métiers à l'entité ou des
méthode d'accès différent sur les attributs.

DAO
===

Il n'y a plus qu'un seul type de persistence possible pour les DAO, à savoir Hibernate (depuis la 2.2.0). Il est possible d'étendre un DAO
pour lui ajouter différentes méthodes plus complexes ou plus spécifiques que celles proposées par défaut :

- findAll
- findAllByProperties(Map<String, Object> properties)
- findAllByProperties(String param, Object property, ...)
- findByProperty(String property)
- tous les find pour chaque property existante de l'entité
- ...

Pour spécifier d'autres méthodes, il suffit d'utiliser directement le modèle UML qui sert à la génération des entités.
Un stéréotype <<dao>> doit être précisé sur une méthode d'entité pour indiquer qu'elle doit se situer dans un DAO.
Le fichier *DAOImpl* associé ne sera plus généré et devra être créé par le développeur en héritant du *DAOAbstract*.
(voir FAQ pour un exemple sur l'extension d'un DAO).

Note sur les signatures de méthodes
  Par commodité, les méthodes retournant une liste d'entités commencent par findAllBy tandis que celles retournant une seule entité
  commencent par findBy.

TopiaService
============

Pour implanter un TopiaService il faut absolument créer une interface qui
hériter de l'interface *TopiaService* et mettre dans cette interface un
attribut static qui définit le nom du service::

  public static final String SERVICE_NAME = "monservice";

Un service peut avoir besoin de nouvelles entités pour fonctionner. Pour cela
il faut retourner la liste des entités grâce à la méthode
**getPersistenceClasses**. 

Après instanciation du service par le **TopiaContext** celui-ci est
initialisé grâce à la méthode **init(TopiaContextImplementor)**. Il peut
alors se mettre listener sur le context ou les DAO par exemple.
