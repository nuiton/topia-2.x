.. -
.. * #%L
.. * ToPIA :: Persistence
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 2004 - 2014 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

===========================
Isolation des TopiaContexts
===========================


Remarque: les requètes ne sont pas bonnes, mais sont là pour donner l'idée
générale.

Pour mettre en place l'isolation entre les différents TopiaContexts créés par
beginTransaction, il faut ajouter un nouvelle propriété sur les
TopiaEntity: TopiaContextId. Cette propriété prend la valeur de l'id du
TopiaContext qui a créé ou chargé l'objet. On voit donc que plusieurs objets
ayant le même id peuvent exister, le TopiaContextId doit donc faire partie
de la clé de l'objet. Il faut aussi ajouter un champs TopiaDeleted pour
savoir si un objet a été effacé ou non.

Dans le mapping hibernate pour chaque class d'entité, on a un filtre:
where TopiaContextId == :id || (TopiaContextId > 0 && TopiaContextId <= :id)

Ce filtre n'est pas suffisant car on a toutes les versions commités de
l'objet jusqu'a :id, il faut un autre filtre apres les recherches pour
n'avoir que les dernieres versions des entites et supprimer dans ce groupe
les entitées marquées et effacées.
where TopiaContextId = max(TopiaContextId) && isDeleted=false group by topiaId

Lors d'un beginTransaction le nouveau TopiaContext active ce filtre pour son
propre id.

De cette manière un TopiaContext ne voit que les objets qu'il a créé et/ou
modifié, ou les objets plus ancien que lui.

Cet Id est: - System.nanoTime() et donc toujours négatif.

Lors d'un commit d'un TopiaContext, on passe tous les TopiaContextId de
toutes les entités qui sont égales a l'id du TopiaContext en sa version
positive: update <table> set TopiaContextId = -:id where TopiaContextId = :id
De plus le TopiaContext renouvelle son id et modifie tous les filtres pour
qu'il aie la vision d'objets créés par d'autre TopiaContext avant son commit.

Lors d'un rollback il suffit de faire le renouvellement de l'id et la mise à
jour des filtres.

Sous-transaction
================

La gestion des TopiaContextId pourrait offrir les sous-transactions, si
dans le filtre on ajoutait comme condition, au lieu de TopiaContextId == :id,
TopiaContextId in (:idList) ou idList serait la liste des ids du
TopiaContext courant ainsi que des TopiaContext parents.

Historisation
=============

La gestion des TopiaContextId permet d'offrir l'historisation de tous les
objets. Car tous les objets commités sont conservés avec un TopiaContextId
différent pour chaque version.

Implantation
============

- Mettre en place les champs supplémentaires: TopiaContextId, TopiaDeleted
- Modifier la clé primaire pour ajouter TopiaContextId
- Ajouter les filtres dans les mappings
- Ajouter un id sur les TopiaContext
- Modifier le comportement de DAO.delete pour qu'il marque juste l'objet
  comme effacé sans l'effacer TopiaDeleted=true
- Pour toutes les recherches ajouter un filtre pour ne retourner que les
  derniere version non deleted
