.. -
.. * #%L
.. * ToPIA :: Persistence
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 2004 - 2014 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

======================
Gestion des évènements
======================

On peut se mettre listener sur deux sortes d'évènements : les TopiaEntityEvent et
les TopiaVetoableEntityEvent. La difference entre les deux se situe sur le moment de
l'appel.


TopiaVetoableEntityEvent
------------------------

Les TopiaVetoableEntityEvent sont appelés avant l'action, ce qui 
permet de l'interdire en levant une exception (par exemple pour gérer la
sécurité).

Les TopiaVetoableEntityEvent contiennent la classe à laquelle se rapporte l'event
et si possible l'identifiant de l'objet qui sera impacté. Cela n'est pas
toujours le cas; par exemple lors de la creation, l'identifiant n'existe pas forcément
et donc ne peut-etre donné.

Propagation
~~~~~~~~~~~

Les TopiaVetoableEntityEvent sont aussi levés pour tous les contexts pères
du context qui a produit l'event et ceci juste apres avoir prévenu les
listeners du context courant.

TopiaEntityEvent
----------------

Les TopiaEntityEvent sont appelés après l'action pour prévenir du changement.

Les TopiaEntityEvent contiennent l'entity à laquelle se rapporte l'event

Propagation
~~~~~~~~~~~

Les TopiaEntityEvent sont propagé au context pere lors du commit du context.

Si un rollback est fait, alors ce sont les listeners du context eux-même qui
sont prévenus du changement.

Implantation
============

Les DAO sont responsables de l'appel des méthodes fire sur le context qui les
a créés lors de l'appel sur ceux-ci des methodes create, update, delete, find
(pour le chargement).

Le cas Hibernate
----------------

Dans le DAO hibernate le context est listener des différents évènements levés
par la session. Mais ces évènements ne sont levés que lors du commit. Dans Topia
on souhaite avoir directement un évènement lors d'un create, update ou delete
sur le DAO. Le DAO hibernate lève donc des évènements lorsque l'on appelle
ces méthodes.

On a aussi des évènements lors du commit. Il est donc possible qu'on aie, 
par exemple, le même évènement Update envoyé deux fois au listener si on fait
un update sur le DAO suivi du commit sur le context.

On conserve tout de meme le mécanisme d'évènement levé au moment du commit par
hibernate car il est beaucoup plus puissant. Il permet aussi d'être prévenu de
modifications apportées aux constituants d'une entité et non pas seulement des
évènements portants sur l'entité elle-même comme c'est le cas dans le mecanisme
implanté dans les DAO.
