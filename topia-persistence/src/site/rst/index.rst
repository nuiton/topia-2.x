.. -
.. * #%L
.. * ToPIA :: Persistence
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 2004 - 2014 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

=====
ToPIA
=====

.. contents::

Présentation
------------

ToPIA, pour Tools for Portable and Independant Architecture, est un framework 
d'abstraction des plateformes techniques. 

C'est à dire ?
~~~~~~~~~~~~~~
 
Le cycle de développement en Y traditionnel peut être représenté de la manière
suivante :

Les deux branches, la branche fonctionnelle, porteuse de la solution logique, et
la branche technique, qui définit la plateforme technique sur laquelle la 
solution sera implantée, se rejoignent en amont de la conception détaillée et du
codage. Cette projection peut être accompagnée de génération de code, mais il 
reste toujours une certaine quantité de code écrite à la main.

Cette séparation des responsabilités et ce processsus donne pleine satisfaction 
lors du développement initial d'un logiciel. La vie d'un logiciel commence 
toutefois au moment de sa mise en production et les évolutions qui y seront 
apportées sont de deux natures :
  - Évolutions fonctionnelles : Ajout de fonctionnalités, correction de bugs, ...
  - Evolutions techniques : Changement de bases de données, changement de techno
    sur les UI, nouveaux accès (SOAP ?), ...

Si les évolutions fonctionnelles sont prises en compte par la nature itérative 
des processus de développement, les évolutions techniques sont elles difficiles.

Comment passer d'hibernate à JDO, de Swing au client léger, des EJB aux 
conteneurs légers ? En supprimant les dépendances de votre code sur les briques 
techniques, en codant sur une plateforme technique abstraite, interfaces et 
facades au travers desquels vous pourrez manipuler les différentes solutions 
techniques que vous retiendrez. Hibernate et JDO ne sont que deux API d'accès 
aux données; Swing et client léger, des UI; les EJB et les conteneurs légers, 
des logiques métiers déportées...

Et ToPIA alors ?
~~~~~~~~~~~~~~~~
 
ToPIA propose une telle plateforme. Vous codez en regard d'une API qui abstrait 
la distribution (1-tiers ?, 2-tiers ?, 3-tiers ?, n-tiers ? quels protocoles 
d'accès ?), la persistence (quelles bases bien sûr, mais également quel 
framework ?, quelle API de requètage (EJB-QL, JDO-QL, ... ?), quel méchanisme 
transactionnel ?), quelle UI ? Puis vous projettez votre application sur la 
palteforme de votre choix.

Fonctionnement classique
------------------------

Le framework ToPIA permet dans un projet de générer la partie métier de l'application, 
c'est à dire les classes JAVA ainsi que le mapping Hibernate pour la persistance. 
Cela représente un travail considérable en moins pour le développeur et une 
flexibilité importante dans les évolutions futures de la parties métier.

L'utilisation du framework ToPIA se découpe en plusieurs phases, elles sont 
itératives :
  1. Création du diagramme de classe sur ArgoUML_ par exemple
  2. Exportation du modèle en XMI
  3. Génération des fichiers JAVA et Hibernate par Eugene_
  4. Implentation des méthodes

ToPIA-service
-------------

Le framework ToPIA peut être complèté par une multitude de services. Il existe 
actuellement trois :

  - `service pour la sécurité`_ : permet la gestion des authentification et des 
    autorisations. Il repose sur le mécanisme de JAAS.
  - `service pour la gestion des mises à jour`_ : permet de mettre à jour une version
    de base à partir d'un modèle versionné.
  - `service de réplication`_ : permet de répliquer des données de base à base.
  - ``service pour la gestion d'un historique`` : permet de conserver l'ensemble des 
    actions réalisées sur la base de données (**plus maintenu depuis la 2.3.3**).
  - ``service pour la recherche`` : permet de rechercher un mot clé parmis les entités
    de l'application. Il repose sur la librairie lucene d'apache (**plus maintenu depuis la 2.3.3**).

.. _service pour la sécurité: ../topia-service-security
.. _service pour la gestion des mises à jour: ../topia-service-migration
.. _service de réplication: ../topia-service-replication
.. _Eugene: http://maven-site.nuiton.org/eugene/
.. _ArgoUML: http://argouml.tigris.org/
