.. -
.. * #%L
.. * ToPIA :: Persistence
.. * 
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 2004 - 2010 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

======================
Génération des modèles
======================


Le module "topia-persistence" de topia est capable de
générer la persistence à partir d'un modèle UML.

Nous allons détailler ici comme inclure la génération dans le
cycle de compilation maven.


Architecture maven
------------------

+---------------------------------+-----------------------------------------+
| src/main/xmi/mymodel.zargo      | exemple de modele UML avec argoUML      |
+---------------------------------+-----------------------------------------+
| src/main/xmi/mymodel.properties | fichier de propriétés attaché au modele |
+---------------------------------|-----------------------------------------+


Configuration du pom.xml
------------------------

Pour générer les sources dans la phase "generate-sources" de maven, et utiliser
les sources générées, et faut configurer le pom.

Plugin eugene
=============

::

<plugin>
    <groupId>org.nuiton.eugene</groupId>
    <artifactId>eugene-maven-plugin</artifactId>
    <version>${eugeneVersion}</version>
    <executions>
        <execution>
            <id>Generator</id>
            <phase>generate-sources</phase>
            <configuration>
                <inputs>zargo</inputs>
                <fullPackagePath>org.company.package</fullPackagePath>
                <extractedPackages>org.company.package</extractedPackages>
                <templates>org.nuiton.topia.generator.TopiaMetaGenerator</templates>
                <defaultPackage>org.company.package</defaultPackage>
            </configuration>
            <goals>
                <goal>generate</goal>
            </goals>
        </execution>
    </executions>
    <dependencies>
        <dependency>
            <groupId>org.nuiton.topia</groupId>
            <artifactId>topia-persistence</artifactId>
            <version>${project.version}</version>
        </dependency>
    </dependencies>
</plugin>

Pour plus d'information à propos d'eugene, merci de consulter le site :
http://maven-site.nuiton.org/eugene/

Dépendances du projet
=====================

Il faut enfin ajouter "topia-persistence" en dépendance du projet :

::

<dependency>
    <groupId>org.nuiton.topia</groupId>
    <artifactId>topia-persistence</artifactId>
    <version>${project.version}</version>
</dependency>
