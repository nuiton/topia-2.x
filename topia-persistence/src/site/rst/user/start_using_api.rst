.. -
.. * #%L
.. * ToPIA :: Persistence
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 2004 - 2014 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

======================
Commencer à développer
======================

Nous pouvons maintenant ouvrir le projet dans un IDE et commencer à développer.

Configurer une base de données embarquée
========================================

Pour assurer la persitance de nos entités, il faut faire appel à
un SGBD supporté par Hibernate. Pour cela, un peu de configuration JDBC est
nécessaire. Dans le fichier ``src/main/resources/library-config.properties``, on
définit

::

    # Pour démarrer, on utilisera une base H2
    hibernate.connection.url=jdbc:h2:file:target/db/h2data

    # Configuration JDBC
    hibernate.dialect=org.hibernate.dialect.H2Dialect
    hibernate.connection.username=sa
    hibernate.connection.password=
    hibernate.connection.driver_class=org.h2.Driver

    # On demande à Hibernate de créer toutes les tables nécessaires
    # au besoin
    hibernate.hbm2ddl.auto=update

    # Un peu de config pour savoir ce que fait hibernate, cette directive
    # affiche toutes les requêtes effectuées sur la base dans la console
    hibernate.show_sql=true

Sans oublier les dépendances nécessaires à l'exécution ::

    <dependencies>
        ...

        <!-- Implémentation pour les logger -->
        <dependency>
          <groupId>org.slf4j</groupId>
          <artifactId>slf4j-log4j12</artifactId>
          <version>${sl4jVersion}</version>
          <scope>runtime</scope>
        </dependency>

        <!-- Driver pour la base de données H2 -->
        <dependency>
          <groupId>com.h2database</groupId>
          <artifactId>h2</artifactId>
          <version>${h2Version}</version>
        </dependency>

    </dependencies>


Développer un premier service
=============================

Gestion des transactions
------------------------

>>> Properties conf = new Properties();
URL url = Resource.getURL("TopiaContextImpl.properties");
conf.load(new FileInputStream(new File(url.toURI())));

Permet de charger un fichier contenant les informations de connexion à la base
et de configuration de Topia.

>>> TopiaContext rootContext = TopiaContextFactory.getContext(conf);

Récupère le context de départ pour la création de sous context pour la gestion
des entités.

>>> TopiaContext transaction = rootContext.beginTransaction();

Création d'un sous context pour la gestion des entités.

>>> transaction.commitTransaction();

Validation de l'état du context, les modifications des entités sont reportées
dans la base de données.

>>> transaction.rollBackTransaction();

Invalidation de l'état du context, les modifications des entités ne sont pas
reportées dans la base de données.

>>> transaction.closeContext();

Relâche la connexion JDBC d'un sous context et de ses sous contexts sans
validation de l'état des contexts.

>>> rootContext.closeContext();

Relâche l'ensemble des ressources (cache, pools, ...) et relâche la connexion
JDBC des sous contexts avec validation de l'état des contexts.

Manipulation des entités
------------------------

>>> PersonDAO personDAO = <nom modele>DAOHelper.getPersonDAO(transaction);

Récupération d'un DAO pour accéder à une entité.

>>> Person person = personDAO.findByTopiaId(id);

Les méthodes find permettent de récupérer une ou plusieures entités selon différents
critères.

>>> person.setName("tutu");

Manipulation de l'entité par les getters et setters.

>>> Person person = personDAO.create();

Création d'une entité. Si vous utilisez l'instanciation classique java avec le
new, l'entité n'est pas persistée.

>>> personDAO.update(person);

Modification d'un entité.

>>> personDAO.delete(person);

Suppression d'une entité.

Configuration I18n
------------------

ToPIA contient une internationnalisation des messages utilisateurs (logs).
Il est préférable d'initialiser I18n pour accélérer la recherche des messages.

Au démarrage de l'application ::

    I18nInitializer i18nInitializer = new DefaultI18nInitializer("library-bundle");
    I18n.init(i18nInitializer, new Locale("fr", "FR"));

Configuration maven ::

    <!-- Plugin i18n pour gérer le bundle de messages -->
    <plugin>
      <groupId>org.nuiton.i18n</groupId>
      <artifactId>i18n-maven-plugin</artifactId>
      <version>${nuitonI18nVersion}</version>
      <executions>
        <!-- Cette exécution permet de rassembler tous les messages i18n
             dans un seul 'properties bundle' nommé library-bundle
        -->
        <execution>
          <id>make-bundle</id>
          <configuration>
            <bundleOutputName>library-bundle</bundleOutputName>
          </configuration>
          <goals>
            <goal>bundle</goal>
          </goals>
        </execution>
      </executions>
    </plugin>

Vous pourrez également utiliser `nuiton-i18n`_ pour l'internationnalisation de
votre application.

.. _nuiton-i18n: http://maven-site.nuiton.org/i18n/

Exemple complet
---------------

Un exemple complet dans StartTest_.

.. _StartTest: http://svn.nuiton.org/svn/topia/trunk/topia-tutorial/src/test/java/org/nuiton/topia/tutorial/library/StartTest.java
