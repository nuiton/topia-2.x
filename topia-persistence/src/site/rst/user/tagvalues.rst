.. -
.. * #%L
.. * ToPIA :: Persistence
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 2004 - 2014 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

Tag values
==========

Topia templates are customizable using tag values.

Here are current tag values defined in topia. They can be defined in source
model or model property file.

Topia tag values
----------------

+--------------------------+-----------+----------------------------------------------------+
|  Name                    | Category  | Description                                        |
+==========================+===========+====================================================+
| orderBy                  | Hibernate | Define order-by attribute in hibernate mapping for |
|                          |           | a specific relation                                |
+--------------------------+-----------+----------------------------------------------------+
| dbName                   | Hibernate | Database table name                                |
+--------------------------+-----------+----------------------------------------------------+
| dbSchema                 | Hibernate | Database schema                                    |
+--------------------------+-----------+----------------------------------------------------+
| length                   | Hibernate | Attribute length in database                       |
+--------------------------+-----------+----------------------------------------------------+
| annotation               | all       | Add annotation on attribute                        |
+--------------------------+-----------+----------------------------------------------------+
| copyright                | all       | Generated files header copyright                   |
+--------------------------+-----------+----------------------------------------------------+
| access                   | hibernate | Field access type                                  |
+--------------------------+-----------+----------------------------------------------------+
| i18n                     | entity    | Generate i18n ???                                  |
+--------------------------+-----------+----------------------------------------------------+
| naturalId                | hibernate | Generate hibernate naturalId                       |
+--------------------------+-----------+----------------------------------------------------+
| naturalIdMutable         | hibernate | Tell hibernate if a natural id key is mutable      |
+--------------------------+-----------+----------------------------------------------------+
| inverse                  | hibernate | To configure with part of a N-N relation is inverse|
+--------------------------+-----------+----------------------------------------------------+
| lazy                     | hibernate | Hibernate multiple attribute fetch property        |
|                          |           | (default to ''hibernate default value'')           |
+--------------------------+-----------+----------------------------------------------------+
| notNull                  | hibernate | Hibernate attribute not null property              |
|                          |           | (default to ''hibernate default value'')           |
+--------------------------+-----------+----------------------------------------------------+
| hibernateProxyInterface  | hibernate | To not generate proxy information in               |
|                          |           | hibernate mappings.                                |
+--------------------------+-----------+----------------------------------------------------+
| useEnumerationName       | hibernate | To make an entity attribute of type enum be stored |
|                          |           | in DB with its name (instead of ordinal, which is  |
|                          |           | the default in Hibernate)                          |
+--------------------------+-----------+----------------------------------------------------+

Eugene tag values
-----------------

Eugene contains also his tag values. See `eugene documentation`_ for more détails.

.. _eugene documentation: http://maven-site.nuiton.org/eugene/
