/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.topia.event;

import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.framework.EntityState;
import org.nuiton.topia.persistence.TopiaEntity;

import java.util.EventObject;
import java.util.IdentityHashMap;
import java.util.Map;
import java.util.Set;

/**
 * Event fires for {@link TopiaTransactionListener}.
 *
 * @author poussin &lt;poussin@codelutin.com&gt;
 * @version $Id$
 * @see TopiaTransactionListener
 */
public class TopiaTransactionEvent extends EventObject {

    private static final long serialVersionUID = 1L;

    private Map<TopiaEntity, EntityState> entities = new IdentityHashMap<>();

    public TopiaTransactionEvent(TopiaContext source) {
        super(source);
    }

    public TopiaTransactionEvent(TopiaContext source,
                                 Map<TopiaEntity, EntityState> entities) {
        this(source);
        this.entities.putAll(entities);
    }

    public Set<TopiaEntity> getEntities() {
        return entities.keySet();
    }

    public boolean isLoad(TopiaEntity entity) {
        EntityState state = entities.get(entity);
        return state != null && state.isLoad();
    }

    public boolean isRead(TopiaEntity entity) {
        EntityState state = entities.get(entity);
        return state != null && state.isRead();
    }

    public boolean isCreate(TopiaEntity entity) {
        EntityState state = entities.get(entity);
        return state != null && state.isCreate();
    }

    public boolean isUpdate(TopiaEntity entity) {
        EntityState state = entities.get(entity);
        return state != null && state.isUpdate();
    }

    public boolean isDelete(TopiaEntity entity) {
        EntityState state = entities.get(entity);
        return state != null && state.isDelete();
    }

    public boolean isModification(TopiaEntity entity) {
        EntityState state = entities.get(entity);
        return state != null
               && (state.isCreate() || state.isUpdate() || state.isDelete());
    }

    @Override
    public TopiaContext getSource() {
        return (TopiaContext) super.getSource();
    }

    /**
     * @return the source context that fires the event
     * @deprecated since 2.3.4, prefer the overriden {@link #getSource()}.
     */
    @Deprecated
    public TopiaContext getTopiaContext() {
        return getSource();
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        if (entities != null) {
            entities.clear();
            entities = null;
        }
    }
}
