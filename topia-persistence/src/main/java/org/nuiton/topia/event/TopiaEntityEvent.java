/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.topia.event;

import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.persistence.TopiaEntity;

import java.util.EventObject;

/**
 * TODO-fdesbois-20100507 : Need javadoc.
 *
 * @author jruchaud &lt;jruchaud@codelutin.com&gt;
 * @version $Id$
 */
public class TopiaEntityEvent extends EventObject {

    private static final long serialVersionUID = 1L;

    private TopiaEntity entity;

    private Object[] state;

    /** Old state in case of update event. */
    private Object[] oldState;

    /** Dirty properties in case of update event. */
    private int[] dirtyProperties;

    public TopiaEntityEvent(Object source, TopiaEntity entity, Object[] state) {
        super(source);
        this.entity = entity;
        this.state = state;
    }

    public TopiaEntityEvent(Object source, TopiaEntity entity, Object[] state, Object[] oldState) {
        this(source, entity, state);
        this.oldState = oldState;
    }

    public TopiaEntityEvent(Object source, TopiaEntity entity, Object[] state, Object[] oldState, int[] dirtyProperties) {
        this(source, entity, state, oldState);
        this.dirtyProperties = dirtyProperties;
    }

    public TopiaEntity getEntity() {
        return entity;
    }

    @Override
    public TopiaContext getSource() {
        return (TopiaContext) super.getSource();
    }

    public Object[] getState() {
        return state;
    }

    public Object[] getOldState() {
        return oldState;
    }

    public int[] getDirtyProperties() {
        return dirtyProperties;
    }
}
