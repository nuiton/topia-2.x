/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.topia;

/**
 * TODO-FD20100507 : Need javadoc.
 *
 * Created: 23 déc. 2005 23:04:28
 *
 * @author poussin &lt;poussin@codelutin.com&gt;
 * @version $Id$
 */
public class TopiaNotFoundException extends TopiaException {

    /** Version UID */
    private static final long serialVersionUID = -8206486077608923797L;

    /**
     * Default constructor.
     */
    public TopiaNotFoundException() {
    }

    /**
     * Constructor with {@code message}.
     *
     * @param message exception message
     */
    public TopiaNotFoundException(String message) {
        super(message);
    }

    /**
     * Constructor for a wrapped TopiaNotFoundException over a {@code cause}
     * with a {@code message}.
     *
     * @param message exception message
     * @param cause exception cause
     */
    public TopiaNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Constructor for a wrapped TopiaNotFoundException over a {@code cause}.
     *
     * @param cause exception cause
     */
    public TopiaNotFoundException(Throwable cause) {
        super(cause);
    }
}
