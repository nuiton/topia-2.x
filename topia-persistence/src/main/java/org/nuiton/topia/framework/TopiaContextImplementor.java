/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2015 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.topia.framework;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.service.ServiceRegistry;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaException;
import org.nuiton.topia.TopiaNotFoundException;
import org.nuiton.topia.persistence.TopiaDAO;
import org.nuiton.topia.persistence.TopiaEntity;

/**
 * Technical contract of a {@link TopiaContext}.
 *
 * Any implementation of the {@link TopiaContext} should also implements this
 * contract.  
 *
 * @author poussin &lt;poussin@codelutin.com&gt;
 * @version $Id$
 */
//TODO-fdesbois-20100507 : Need more javadoc.
public interface TopiaContextImplementor extends TopiaContext {

    /**
     * Retrieve a thread-safe copy of children context set.
     *
     * @return Returns the childContext.
     */
    Set<TopiaContextImplementor> getChildContext();

    /** @return Returns the parentContext. */
    TopiaContextImplementor getParentContext();

    TopiaContextImplementor getRootContext();

    /** @return Returns the config. */
    Properties getConfig();

    /** Service registry bootstrap. */
    ServiceRegistry getServiceRegistry();

    /** Proprietes de mapping de la base de données. */
    Metadata getMetadata();

    /**
     * @return Returns the hibernate.
     * @throws TopiaException si aucune transaction n'est ouverte
     */
    Session getHibernate() throws TopiaException;

    /**
     * @return Returns the hibernateFactory.
     * @throws TopiaNotFoundException
     */
    SessionFactory getHibernateFactory() throws TopiaNotFoundException;

    /**
     * Tells to the context if it has to use a flush mode before each query.
     *
     * By default, we use a flush mode, but in some case it costs to much doing 
     * this, that's why you can desactivate it setting the value to {@code false}.
     *
     * @param useFlushMode the new value to set
     * @since 2.5
     */
    void setUseFlushMode(boolean useFlushMode);

    /**
     * Detect if the table is created on storage for a given persistant class.
     *
     * @param clazz the researched class
     * @return Returns the hibernate.
     * @throws TopiaException si aucune transaction n'est ouverte
     */
    boolean isSchemaExist(Class<?> clazz) throws TopiaException;

    /**
     * Get DAO for specified class. If Specialized DAO exists then it returned
     * otherwize TopiaDAO&lt;entityClass&gt; is returned
     *
     * @param <E> type of entity
     * @param entityClass type of entity
     * @return the required dao
     * @throws TopiaException if any error
     */
    <E extends TopiaEntity> TopiaDAO<E> getDAO(Class<E> entityClass)
            throws TopiaException;

    /**
     * Get DAO for specified class. If Specialized DAO exists then it returned
     * otherwize TopiaDAO&lt;entityClass&gt; is returned
     *
     * @param <E> type of entity
     * @param entityClass type of entity
     * @param daoClass the concrete dao class to use
     * @return the required dao
     * @throws TopiaException if any error
     */
    <E extends TopiaEntity, D extends TopiaDAO<E>> D getDAO(Class<E> entityClass,Class<D> daoClass)
            throws TopiaException;

    TopiaFiresSupport getFiresSupport();

    void removeChildContext(TopiaContextImplementor child);

    Map<String, TopiaService> getServices();

    /**
     * @return a collection of {@link TopiaService}
     * @deprecated since 2.3.4 : useless method, use {@link #getServices()} instead
     */
    @Deprecated
    Collection<TopiaService> getAllServices();

    List<Class<?>> getPersistenceClasses();
} //TopiaContextImplementor

