/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2015 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.topia.framework;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.FlushMode;
import org.hibernate.HibernateException;
import org.hibernate.ReplicationMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataBuilder;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.engine.jdbc.connections.spi.ConnectionProvider;
import org.hibernate.event.service.spi.EventListenerRegistry;
import org.hibernate.event.spi.EventType;
import org.hibernate.jdbc.Work;
import org.hibernate.metamodel.spi.MetamodelImplementor;
import org.hibernate.persister.entity.EntityPersister;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.spi.Stoppable;
import org.hibernate.tool.hbm2ddl.SchemaExport;
import org.hibernate.tool.hbm2ddl.SchemaUpdate;
import org.hibernate.tool.schema.TargetType;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaContextFactory;
import org.nuiton.topia.TopiaException;
import org.nuiton.topia.TopiaNotFoundException;
import org.nuiton.topia.event.TopiaContextListener;
import org.nuiton.topia.event.TopiaEntitiesVetoable;
import org.nuiton.topia.event.TopiaEntityListener;
import org.nuiton.topia.event.TopiaEntityVetoable;
import org.nuiton.topia.event.TopiaTransactionListener;
import org.nuiton.topia.event.TopiaTransactionVetoable;
import org.nuiton.topia.persistence.TopiaDAO;
import org.nuiton.topia.persistence.TopiaDAOImpl;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaId;

import java.beans.PropertyChangeListener;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.zip.GZIPInputStream;

import static org.nuiton.i18n.I18n.t;

/**
 * Le TopiaContextImpl est le point d'entre pour acceder aux donnees. Il est
 * configurer par un fichier de propriete
 * 
 * List des proprietes disponible <dl> <dt> topia.persistence.properties.file
 * <dd> le fichier de propriété a utiliser pour configurer hibernate
 * 
 * <dt> topia.persistence.directories <dd> la liste des repertoires contenant
 * les mappings hibernates (.hbm.xml) la liste de repertoire est separer par des
 * virgules ','
 * 
 * <dt> topia.persistence.classes <dd> la liste des classes que doit géré
 * hibernate. On peut tres bien utiliser topia.persistence.directories pour un
 * ensemble d'entié du meme repertoire et topia.persistence.classes pour
 * d'autres classes </dl>
 * 
 * TopiaContextImpl.java
 * 
 * Created: 23 déc. 2005 16:58:50
 *
 * @author poussin &lt;poussin@codelutin.com&gt;
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @author fdesbois &lt;desbois@codelutin.com&gt;
 * @version $Id$
 */
//TODO-fdesbois-20100507 : Need translation of javadoc.
public class TopiaContextImpl implements TopiaContextImplementor {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    private static final Log log = LogFactory.getLog(TopiaContextImpl.class);

    /** @deprecated since 2.5.4 use directly {@link TopiaContextFactory#CONFIG_PERSISTENCE_DIRECTORIES}*/
    @Deprecated
    public static final String TOPIA_PERSISTENCE_DIRECTORIES =
            TopiaContextFactory.CONFIG_PERSISTENCE_DIRECTORIES;

    /** @deprecated since 2.5.4 use directly {@link TopiaContextFactory#CONFIG_PERSISTENCE_CLASSES}*/
    @Deprecated
    public static final String TOPIA_PERSISTENCE_CLASSES =
            TopiaContextFactory.CONFIG_PERSISTENCE_CLASSES;

    /** @deprecated since 2.5.4 use directly {@link TopiaContextFactory#CONFIG_PERSISTENCE_PROPERTIES_FILE}*/
    @Deprecated
    public static final String TOPIA_PERSISTENCE_PROPERTIES_FILE =
            TopiaContextFactory.CONFIG_PERSISTENCE_PROPERTIES_FILE;

    /** Le pere de ce context, les contexts initaux n'ont pas de context pere */
    protected TopiaContextImplementor parentContext;

    /** Service registry bootstrap. */
    protected ServiceRegistry serviceRegistry;

    /** Proprietes de mapping de la base de données. */
    protected Metadata metadata;

    /**
     * la factory permettant de recuperer la session hibernate. Seul les
     * TopiaContextImpl initiaux contiennent un hibernateFactory
     */
    protected SessionFactory hibernateFactory;

    /** La session utilisé par le TopiaContextImpl */
    protected Session hibernate;

    /** Indique si le contexte a ete ferme */
    protected boolean closed;

    /**
     * This flag permits to use (or not) the flush mode when doing queries.
     *
     * The normal usage is to says yes (that's why the default value is
     * {@code true}), in that case whebn doing queries (says in method
     * {@link #findAll(String, Object...)} or {@link #find(String, int, int, Object...)})
     * it will use the flush mode {@link FlushMode#AUTO}).
     *
     * But sometimes, when doing a lot of queries (for some imports for example),
     * we do NOT want the session to be flushed each time we do a find, then you
     * can set this flag to {@code false} using the method {@link #setUseFlushMode(boolean)}
     * @since 2.5
     */
    protected boolean useFlushMode = true;

    /** Propriete de configuration */
    protected Properties config;

    /** cache des DAO deja chargé pour ce context */
    protected Map<Class<? extends TopiaEntity>,
            TopiaDAO<? extends TopiaEntity>> daoCache =
            new HashMap<Class<? extends TopiaEntity>,
                    TopiaDAO<? extends TopiaEntity>>();

    /**
     * Set of child context created with {@link #beginTransaction()}. We are
     * listener on these context. A WeakHashMap is used to remove old context
     * automically when it's not used anymore. The {@link #finalize} method will
     * be executed when Garbage collector is called when reference is removed.
     * The set is synchronized in case of using multi-threading.
     *
     * @see Collections#synchronizedSet(Set)
     * @see Collections#newSetFromMap(Map)
     */
    protected final Set<TopiaContextImplementor> childContext =
            Collections.synchronizedSet(
                    Collections.newSetFromMap(
                            new WeakHashMap<TopiaContextImplementor, Boolean>()));

    /** key: service name; value: service instance */
    protected Map<String, TopiaService> services;

    protected TopiaFiresSupport firesSupport = new TopiaFiresSupport();

    /** Liste des classes perssitance */
    protected List<Class<?>> persistenceClasses = new ArrayList<Class<?>>();

    /** Default constructor, useful for tests. */
    protected TopiaContextImpl() {
    }

    /**
     * Constructor used by {@link TopiaContextFactory} to initialize rootContext
     * using {@code config}.
     *
     * @param config for the new root context
     * @throws TopiaNotFoundException if one of persistent class from
     *                                configuration is not found
     */
    public TopiaContextImpl(Properties config) throws TopiaNotFoundException {
        this.config = config;
        services = loadServices(config);
        preInitServices(services);
        getMetadata(); // force mapping loading
        postInitServices(services);
    }

    protected String getProperExceptionMessage(Throwable eee) {
        return eee.getClass().getSimpleName() + " : " +
                eee.getMessage();
    }

    /* -------------------- SERVICES MANAGMENT -------------------------------*/

    protected Map<String, TopiaService> loadServices(Properties config) {
        Map<String, TopiaService> result = new HashMap<String, TopiaService>();
        // recherche des services present dans la config
        for (Enumeration<?> e = config.propertyNames(); e.hasMoreElements();) {
            String key = (String) e.nextElement();
            if (key.matches("^topia\\.service\\.\\w+$")) {
                String classService = config.getProperty(key);
                try {
                    Class<?> forName = Class.forName(classService);
                    Object newInstance = forName.getConstructor().newInstance();
                    TopiaService service = (TopiaService) newInstance;
                    if (key.equals("topia.service." + service.getServiceName())) {
                        result.put(service.getServiceName(), service);
                        log.info(t("topia.persistence.service.loaded",
                                key, classService));
                    } else {
                        log.warn(t("topia.persistence.warn.service.not.loaded",
                                key, service.getServiceName()));
                    }
                } catch (Throwable eee) {
                    String message =
                           t("topia.persistence.error.service.unknown",
                                    key, classService);
                    if (log.isDebugEnabled()) {
                        log.debug(message, eee);
                    } else if (log.isErrorEnabled()) {
                        log.error(message);
                    }
                }
            }
        }
        return result;
    }

    protected void preInitServices(Map<String, TopiaService> services) {
        for (TopiaService service : services.values()) {
            if (!service.preInit(this)) {
                log.warn(t("topia.persistence.warn.service.not.preInit",
                        service.getServiceName()));
            }
        }
    }

    protected void postInitServices(Map<String, TopiaService> services) {
        for (TopiaService service : services.values()) {
            if (!service.postInit(this)) {
                log.warn(t("topia.persistence.warn.service.not.postInit",
                        service.getServiceName()));
            }
        }
    }

    protected TopiaService getService(String name) {
        TopiaService result = getServices().get(name);
        return result;
    }

    protected boolean serviceEnabled(String name) {
        boolean result = getServices().containsKey(name);
        return result;
    }

    /**
     * Retrieve service name using SERVICE_NAME static field on service
     * interface.
     *
     * @param interfaceService class of the service
     * @param <E>              type of the service that extends {@link
     *                         TopiaService}
     * @return the service name
     * @throws IllegalAccessException if field SERVICE_NAME can't be accessed
     * @throws NoSuchFieldException   if no field SERVICE_NAME is defined
     */
    protected <E extends TopiaService> String getServiceName(
            Class<E> interfaceService)
            throws IllegalAccessException, NoSuchFieldException {
        Field f = interfaceService.getField("SERVICE_NAME");
        String name = (String) f.get(null);
        return name;
    }

    @Override
    public Map<String, TopiaService> getServices() {
        TopiaContextImplementor parent = getParentContext();

        Map<String, TopiaService> result;
        if (parent != null) {
            result = parent.getServices();
        } else {
            result = services;
        }
        return result;
    }

    /**
     * Take one service, this service must be valid service interface with
     * public static final SERVICE_NAME declaration.
     *
     * @param <E> type of the service that extends {@link TopiaService}
     * @throws TopiaNotFoundException if an error appears or service not found.
     * @see #getServiceName(Class)
     */
    @Override
    public <E extends TopiaService> E getService(Class<E> interfaceService)
            throws TopiaNotFoundException {
        E result;
        try {
            String name = getServiceName(interfaceService);
            result = (E) getService(name);
        } catch (Exception eee) {
            throw new TopiaNotFoundException(
                   t("topia.persistence.error.service.not.retreaved",
                            interfaceService, getProperExceptionMessage(eee)),
                    eee);
        }
        if (result == null) {
            throw new TopiaNotFoundException(
                   t("topia.persistence.error.service.not.found",
                            interfaceService));
        }
        return result;
    }

    @Override
    public <E extends TopiaService> boolean serviceEnabled(
            Class<E> interfaceService) {
        boolean result = false;
        try {
            String name = getServiceName(interfaceService);
            result = serviceEnabled(name);
        } catch (Exception eee) {
            String message = t("topia.persistence.warn.service.not.found",
                    interfaceService, getProperExceptionMessage(eee));
            if (log.isDebugEnabled()) {
                log.debug(message, eee);
            } else if (log.isWarnEnabled()) {
                log.warn(message);
            }
        }
        return result;
    }

    @Override
    public Collection<TopiaService> getAllServices() {
        Collection<TopiaService> result = getServices().values();
        return result;
    }

    /* -------------------- CONTEXT HIERARCHY MANAGMENT ----------------------*/

    /**
     * Constructor used by {@link #beginTransaction()} to instantiate child from
     * {@code parentContext}.
     *
     * @param parentContext context parent of the new TopiaContext child
     */
    protected TopiaContextImpl(TopiaContextImplementor parentContext) {
        this.parentContext = parentContext;
    }

    @Override
    public Set<TopiaContextImplementor> getChildContext() {
        // fdesbois-20100421 : Ano #546
        // Copy the childContext into a new set
        Set<TopiaContextImplementor> values;
        // Synchronize copy to be thread-safe during iteration
        synchronized (childContext) {
            values = new HashSet<TopiaContextImplementor>(childContext);
        }
        return values;
    }

    protected void addChildContext(TopiaContextImplementor child) {
        childContext.add(child);
    }

    @Override
    public void removeChildContext(TopiaContextImplementor child) {
        // Remove child only if this context is not already closed.
        if (!closed) {
            childContext.remove(child);
        }
    }

    @Override
    public TopiaContextImplementor getParentContext() {
        return parentContext;
    }

    @Override
    public TopiaContextImplementor getRootContext() {
        TopiaContextImplementor result = this;
        if (getParentContext() != null) {
            result = getParentContext().getRootContext();
        }
        return result;
    }

    @Override
    public Properties getConfig() {
        if (config == null && getParentContext() != null) {
            config = getParentContext().getConfig();
        }
        return config;
    }

    /**
     * Change the value of flag {@link #useFlushMode}.
     * 
     * @param useFlushMode the new value to set
     * @see #useFlushMode
     * @since 2.5
     */
    public void setUseFlushMode(boolean useFlushMode) {
        this.useFlushMode = useFlushMode;
    }

    /* -------------------- HIBERNATE MANAGMENT  -----------------------------*/

    @Override
    public void createSchema() throws TopiaException {
        try {
            EnumSet<TargetType> targetTypes = EnumSet.of(TargetType.DATABASE);
            if (log.isDebugEnabled()) {
                targetTypes = EnumSet.of(TargetType.DATABASE, TargetType.STDOUT);
            }
            getFiresSupport().firePreCreateSchema(this);
            new SchemaExport().createOnly(targetTypes, getMetadata());
            getFiresSupport().firePostCreateSchema(this);
        } catch (HibernateException eee) {
            throw new TopiaException(
                   t("topia.persistence.error.create.schema",
                            eee.getMessage()), eee);
        }
    }
    
    @Override
    public void updateSchema() throws TopiaException {
        try {
            EnumSet<TargetType> targetTypes = EnumSet.of(TargetType.DATABASE);
            if (log.isDebugEnabled()) {
                targetTypes = EnumSet.of(TargetType.DATABASE, TargetType.STDOUT);
            }
            getFiresSupport().firePreUpdateSchema(this);
            new SchemaUpdate().execute(targetTypes, getMetadata());
            getFiresSupport().firePostUpdateSchema(this);
        } catch (HibernateException eee) {
            throw new TopiaException(
                   t("topia.persistence.error.update.schema",
                            eee.getMessage()), eee);
        }
    }

    @Override
    public void showCreateSchema() throws TopiaException {
        try {
            new SchemaExport().createOnly(EnumSet.of(TargetType.STDOUT), getMetadata());
        } catch (HibernateException eee) {
            throw new TopiaException(
                   t("topia.persistence.error.create.schema",
                            eee.getMessage()), eee);
        }
    }

    @Override
    public Session getHibernate() throws TopiaException {
        if (hibernate == null) {
            throw new TopiaException(
                   t("topia.persistence.error.no.hibernate.session"));
        }
        return hibernate;
    }

    @Override
    public SessionFactory getHibernateFactory() throws TopiaNotFoundException {
        if (hibernateFactory == null) {
            if (getParentContext() != null) {
                hibernateFactory = getParentContext().getHibernateFactory();
            } else {
                        
                // build session factory
                hibernateFactory = getMetadata().getSessionFactoryBuilder().build();

                // add event listener service
                EventListenerRegistry eventListenerRegistry = TopiaUtil.getHibernateService(hibernateFactory, EventListenerRegistry.class);

                TopiaFiresSupport.TopiaHibernateEvent listener =
                        new TopiaFiresSupport.TopiaHibernateEvent(this);
                eventListenerRegistry.appendListeners(EventType.PRE_INSERT, listener);
                eventListenerRegistry.appendListeners(EventType.PRE_LOAD, listener);
                eventListenerRegistry.appendListeners(EventType.PRE_UPDATE, listener);
                eventListenerRegistry.appendListeners(EventType.PRE_DELETE, listener);
                eventListenerRegistry.appendListeners(EventType.POST_INSERT, listener);
                eventListenerRegistry.appendListeners(EventType.POST_LOAD, listener);
                eventListenerRegistry.appendListeners(EventType.POST_UPDATE, listener);
                eventListenerRegistry.appendListeners(EventType.POST_DELETE, listener);
            }
            
        }
        return hibernateFactory;
    }

    @Override
    public Metadata getMetadata() {
        // add mappings
        if (metadata == null) {
            if (getParentContext() != null) {
                metadata = getParentContext().getMetadata();
            } else {
                MetadataSources sources = new MetadataSources(getServiceRegistry());
    
                // ajout des repertoires contenant les mappings hibernate
                String[] dirs = getConfig().getProperty(
                        TopiaContextFactory.CONFIG_PERSISTENCE_DIRECTORIES, "").split(",");
                for (String dir : dirs) {
                    dir = dir.trim();
                    if (StringUtils.isNotEmpty(dir)) {
                        if (log.isDebugEnabled()) {
                            log.debug("Load persistence from dir : " + dir);
                        }
                        sources.addDirectory(new File(dir));
                    }
                }
    
                // ajout des classes dites persistentes
                Set<String> hibernatePersistanceClassNames = new HashSet<String>();
                for (TopiaService service : getServices().values()) {
                    Class<?>[] classes = service.getPersistenceClasses();
    
                    // certains service n'ont pas de classe persistantes
                    if (classes != null) {
                        for (Class<?> clazz : classes) {
                            hibernatePersistanceClassNames.add(clazz.getName());
                        }
                    }
                }
                
                String listPersistenceClasses = getConfig().getProperty(
                        TopiaContextFactory.CONFIG_PERSISTENCE_CLASSES, "");
    
                String[] classes = listPersistenceClasses.split(",");
                for (String classname : classes) {
                    classname = classname.trim();
                    if (StringUtils.isNotEmpty(classname)) {
                        if (log.isDebugEnabled()) {
                            log.debug("Load persistent class : " + classname);
                        }
                        
                        hibernatePersistanceClassNames.add(classname);
                    }
                }
    
                // Add persistance classes in metadata
                for (String hibernatePersistanceClassName : hibernatePersistanceClassNames) {
                    String hbmXmlFile = hibernatePersistanceClassName.replace( '.', '/' ) + ".hbm.xml";
                    sources.addResource(hbmXmlFile);
                }
                
                MetadataBuilder metadataBuilder = sources.getMetadataBuilder();
                metadata = metadataBuilder.build();
            }
        }

        return metadata;
    }

    @Override
    public ServiceRegistry getServiceRegistry() {
        if (serviceRegistry == null) {
            if (getParentContext() != null) {
                serviceRegistry = getParentContext().getServiceRegistry();
            } else {
                StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder();
    
                Properties prop = new Properties();
                prop.putAll(getConfig());
        
                // Strange behavior, all properties are already loaded from
                // constructor. Difficult to use this behavior, need to have
                // TOPIA_PERSISTENCE_PROPERTIES_FILE in config.
                Properties propertiesFromClasspath =
                        TopiaUtil.getProperties(getConfig().
                                getProperty(TopiaContextFactory.CONFIG_PERSISTENCE_PROPERTIES_FILE));
        
                if (!propertiesFromClasspath.isEmpty()) {
                    if (log.isDebugEnabled()) {
                        log.debug("Load properties from file : " +
                                propertiesFromClasspath);
                    }
                    prop.putAll(propertiesFromClasspath);
                }
    
                builder.applySettings(prop);
                serviceRegistry = builder.build();
            }
        }
        return serviceRegistry;
    }

    /* -------------------- CHILD CONTEXT AND DAOS --------------------------*/

    @SuppressWarnings({"unchecked"})
    @Override
    public <E extends TopiaEntity> TopiaDAO<E> getDAO(Class<E> entityClass)
            throws TopiaException {
        if (entityClass == null) {
            throw new IllegalArgumentException(
                   t("topia.persistence.error.null.param",
                            "entityClass", "getDAO"));
        }
        if (equals(getRootContext())) {
            throw new TopiaException(
                   t("topia.persistence.error.rootContext.access"));
        }
        MetamodelImplementor metamodel = (MetamodelImplementor)getHibernateFactory().getMetamodel();
        Map<String, EntityPersister> entityPersisterMap = metamodel.entityPersisters();
        if (entityPersisterMap.get(entityClass) == null &&
                entityPersisterMap.get(entityClass.getName() + "Impl") == null &&
                entityPersisterMap.get(entityClass.getName() + "Abstract") == null) {

            log.info(t("topia.persistence.supported.classes.for.context", entityPersisterMap.keySet()));
            throw new TopiaException(
                   t("topia.persistence.error.unsupported.class",
                            entityClass.getName()));
        }

        TopiaDAO<E> result = (TopiaDAO<E>) daoCache.get(entityClass);
        if (result == null) {

            // looking for specialized DAO
            // normalement il en existe un car il est généré automatiquement
            // si on utilise la génération
            String daoClassname = entityClass.getName() + "DAO";
            try {
                Class<TopiaDAO<E>> daoClass =
                        (Class<TopiaDAO<E>>) Class.forName(daoClassname);
                TopiaDAO<E> spe = daoClass.getConstructor().newInstance();
                result = spe;
            } catch (Exception eee) {
                log.warn("specialized DAO " + daoClassname +
                        " not found, use default TopiaDAOHibernate");
                result = new TopiaDAOImpl<E>();
            }

            result.init(this, entityClass);
            daoCache.put(entityClass, result);
        }
        return result;
    }

    @SuppressWarnings({"unchecked"})
    @Override
    public <E extends TopiaEntity, D extends TopiaDAO<E>> D getDAO(Class<E> entityClass,
                                                                   Class<D> daoClass) throws TopiaException {
        return (D) getDAO(entityClass);
    }

    @Override
    public TopiaContext beginTransaction() throws TopiaException {
        checkClosed(t("topia.persistence.error.context.is.closed"));
        TopiaContextImpl result = new TopiaContextImpl(this);

        SessionFactory factory = getHibernateFactory();
        result.hibernate = factory.openSession();

        // new TopiaInterceptor(result));
        // on ne synchronise jamais les données avec la base tant que
        // l'utilisateur n'a pas fait de commit du context
        result.hibernate.setHibernateFlushMode(FlushMode.MANUAL);
        
        // tchemit 2010-12-06 propagates the value of the flag
        result.useFlushMode = useFlushMode;

        // 20060926 poussin ajouter pour voir si ca regle les problemes de 
        // deadlock h2. Conclusion, il faut bien ouvrir une transaction 
        // maintenant, sinon lorsque l'on fait des acces a la base, une 
        // transaction par defaut est utilisé mais elle n'est jamais vraiment 
        // fermé ce qui pose des problemes de lock sur les tables.
        try {
            result.hibernate.beginTransaction();
        } catch (Exception eee) {

            // on a pas pu ouvrir la transaction, on faut donc tout fermer
            // et declancher une exception
            try {
                result.hibernate.close();
            } catch (HibernateException e1) {
                if (log.isErrorEnabled()) {
                    log.error("Could not close hibernate session", e1);
                }
            }

            throw new TopiaException(
                   t("topia.persistence.error.open.transaction.failed",
                           eee.getMessage()),
                    eee);
        }

        // 20081217 : add child AFTER hibernate session is opened
        addChildContext(result);

        // fire event
        getFiresSupport().fireOnBeginTransaction(result);
        return result;
    }

    @Override
    public void commitTransaction() throws TopiaException {
        if (equals(getRootContext())) {
            throw new TopiaException(t("topia.persistence.error.unsupported.operation.on.root.context",
                    "commit"));
        }
        checkClosed(t("topia.persistence.error.unsupported.operation.on.closed.context",
                "commit"));

        try {
//            for (TopiaDAO<? extends TopiaEntity> dao : daoCache.values()) {
//                // TODO-fdesbois-20100507 : need to be removed for 2.5 version
//                dao.commitTransaction();
//            }
            Transaction tx = hibernate.getTransaction();
            //            Transaction tx = hibernate.beginTransaction();
            hibernate.flush();
            tx.commit();

            getFiresSupport().fireOnPostCommit(this);
            TopiaContextImplementor parent = getParentContext();
            if (parent != null) {
                parent.getFiresSupport().fireOnPostCommit(this);
            }

            hibernate.beginTransaction();

            // it's seem necessary to change session after commit
            // NON, NON, NON, il ne faut surtout pas le faire, ca pose plein de 
            // probleme 
            //            hibernate = getHibernateFactory().openSession();
            //            hibernate.setFlushMode(FlushMode.NEVER);
        } catch (Exception eee) {
            throw new TopiaException(t("topia.persistence.error.on.commit",
                    eee.getMessage()), eee);
        }
    }

    @Override
    public void rollbackTransaction() throws TopiaException {
        if (equals(getRootContext())) {
            throw new TopiaException(t("topia.persistence.error.unsupported.operation.on.root.context",
                    "rollback"));
        }
        checkClosed(t("topia.persistence.error.unsupported.operation.on.closed.context",
                "rollback"));
        try {
//            for (TopiaDAO<? extends TopiaEntity> dao : daoCache.values()) {
//                // TODO-fdesbois-20100507 : need to be removed for 2.5 version
//                dao.rollbackTransaction();
//            }
            Transaction tx = hibernate.getTransaction();
            //            Transaction tx = hibernate.beginTransaction();
            hibernate.clear();
            tx.rollback();
            hibernate.close();
            // it's very important to change the session after rollback
            // otherwize there are many error during next Entity's modification
            hibernate = getHibernateFactory().openSession();
            hibernate.setHibernateFlushMode(FlushMode.MANUAL);

            hibernate.beginTransaction();

            getFiresSupport().fireOnPostRollback(this);
            TopiaContextImplementor parent = getParentContext();
            if (parent != null) {
                parent.getFiresSupport().fireOnPostRollback(this);
            }

        } catch (HibernateException eee) {
            throw new TopiaException(
                   t("topia.persistence.error.on.rollback",
                            eee.getMessage()), eee);
        }
    }

    @Override
    public void closeContext() throws TopiaException {
        // Throw exception if context is already closed
        checkClosed(t("topia.persistence.error.context.already.closed"));

        // FD-20100421 : Ano #546 : no need to copy childContext, the
        // {@link #getChildContext()} provides a thread-safe copy to iterate
        // on it.
//        TopiaContextImplementor[] children = childContext.toArray(
//                new TopiaContextImplementor[childContext.size()]);

        // Remove all children context
        for (TopiaContextImplementor child : getChildContext()) {
            // Avoid to have exception from checkClosed method on child
            if (!child.isClosed()) {
                child.closeContext();
            }
        }

        // on se desenregistre du context pere et on ferme les connexions si 
        // on est pas le root context
        if (!equals(getRootContext())) {
            closed = true;
            hibernate.close();
            getParentContext().removeChildContext(this);
        } else {

            // XXX AThimel 03/04/14 This is just a security, I'm not sure it is useful anymore
            // close connection provider if possible (http://nuiton.org/issues/2757)
            ServiceRegistry serviceRegistry = getServiceRegistry();
            ConnectionProvider service = serviceRegistry.getService(ConnectionProvider.class);
            if (service != null && service instanceof Stoppable) {
                Stoppable stoppable = (Stoppable) service;
                stoppable.stop();
            }

            if (hibernateFactory != null) {
                
                ConnectionProvider service2 = TopiaUtil.getHibernateService(hibernateFactory, ConnectionProvider.class);
                if (service2 != null && service2 instanceof Stoppable) {
                    Stoppable stoppable = (Stoppable) service2;
                    stoppable.stop();
                }

                hibernateFactory.close();
                closed = true;
                TopiaContextFactory.removeContext(this);
                log.debug("TopiaContext removed");
            }
        }
    }

    /**
     * Pour le context root on ferme tous les fils, et la factory hibernate.
     */
    @Override
    protected void finalize() throws Throwable {
//        if (hibernateFactory != null) {
//            closeContext();
//            hibernateFactory.close();
//            closed = true;
//            log.debug("TopiaContext finalized");
//        }
        if (!closed && log.isErrorEnabled()) {
            log.error("TopiaContext "+this+" was not closed!");
        }
        super.finalize();
    }

    @Override
    public boolean isClosed() {
        return closed;
    }

    @Override
    public void executeSQL(String sqlScript) throws TopiaException {
        SQLWork sqlWork = new SQLWork(sqlScript);
        try {
            getHibernate().doWork(sqlWork);
        } catch (HibernateException e) {
            throw new TopiaException("Could not execute sql code", e);
        }
    }

    protected void checkClosed(String message) throws TopiaException {
        if (closed) {
            throw new TopiaException(message);
        }
    }

    /* -------------------- GLOBAL OPERATIONS ON SCHEMA ----------------------*/


    @SuppressWarnings({"unchecked"})
    @Override
    public TopiaEntity findByTopiaId(String id) throws TopiaException {
        checkClosed(t("topia.persistence.error.unsupported.operation.on.closed.context",
                "findById"));

        Class<TopiaEntity> entityClass = TopiaId.getClassName(id);
        TopiaDAO<TopiaEntity> dao = getDAO(entityClass);
        TopiaEntity result = dao.findByTopiaId(id);
        return result;
    }

    @Override
    public List<?> findByQuery(TopiaQuery query) throws TopiaException {
        return query.execute(this);
    }

    @SuppressWarnings({"unchecked"})
    @Override
    public TopiaQuery createQuery(Class<?> entityClass, String alias) {
        return new TopiaQuery((Class<? extends TopiaEntity>)entityClass, alias);
    }

    @Override
    public List<?> findAll(String hql, Object... args) throws TopiaException {
        checkClosed(t("topia.persistence.error.unsupported.operation.on.closed.context",
                "findAll"));

        try {
            Query query = getHibernate().createQuery(hql);
            for (int j = 0; j < args.length; j += 2) {
                String name = (String) args[j];
                Object value = args[j + 1];
                if (value.getClass().isArray()) {
                    query.setParameterList(name, (Object[]) value);
                } else if (value instanceof Collection<?>) {
                    query.setParameterList(name, (Collection<?>) value);
                } else {
                    query.setParameter(name, value);
                }
            }
            // tchemit 2010-11-30 reproduce the same behaviour than before with the dao legacy
            if (useFlushMode) {
                query.setHibernateFlushMode(FlushMode.AUTO);
            }
            List result = query.list();
            result = firesSupport.fireEntitiesLoad(this, result);
            return result;
        } catch (HibernateException eee) {
            throw new TopiaException(t("topia.persistence.error.on.query",
                    hql, eee.getMessage()), eee);
        }
    }

    @Override
    public List<?> find(String hql, int startIndex, int endIndex, Object... args)
            throws TopiaException {
        checkClosed(t("topia.persistence.error.unsupported.operation.on.closed.context",
                "find"));

        try {
            Query query = getHibernate().createQuery(hql);
            for (int j = 0; j < args.length; j += 2) {
                String name = (String) args[j];
                Object value = args[j + 1];
                if (value.getClass().isArray()) {
                    query.setParameterList(name, (Object[]) value);
                } else if (value instanceof Collection<?>) {
                    query.setParameterList(name, (Collection<?>) value);
                } else {
                    query.setParameter(name, value);
                }
            }
            query.setFirstResult(startIndex);
            query.setMaxResults(endIndex - startIndex + 1);
            // tchemit 2010-11-30 reproduce the same behaviour than before with the dao legacy
            if (useFlushMode) {
                query.setHibernateFlushMode(FlushMode.AUTO);
            }
            List result = query.list();
            result = firesSupport.fireEntitiesLoad(this, result);
            return result;
        } catch (HibernateException eee) {
            throw new TopiaException(t("topia.persistence.error.on.query",
                    hql, eee.getMessage()), eee);
        }
    }

    @Override
    public Object findUnique(String hql, Object... paramNamesAndValues)
            throws TopiaException {
        checkClosed(t("topia.persistence.error.unsupported.operation.on.closed.context",
                "findUnique"));

        List<?> results = find(hql, 0, 1, paramNamesAndValues);

        // If there is more than 1 result, throw an exception
        if (results.size() > 1) {
            String message = String.format(
                    "Query '%s' returns more than 1 unique result", hql);
            throw new TopiaException(message);
        }

        // otherwise return the first one, or null
        Object result = null;
        if (!results.isEmpty()) {
            result = results.get(0);
        }
        return result;
    }

    /**
     * Execute HQL operation on data (Update, Delete)
     *
     * @param hql  HQL query
     * @param args arguments for query
     * @return The number of entities updated or deleted.
     * @throws TopiaException
     */
    @Override
    public int execute(String hql, Object... args) throws TopiaException {
        checkClosed(t("topia.persistence.error.unsupported.operation.on.closed.context",
                "find"));

        try {
            Query query = getHibernate().createQuery(hql);
            for (int j = 0; j < args.length; j += 2) {
                query.setParameter((String) args[j], args[j + 1]);
            }
            int result = query.executeUpdate();
            return result;
        } catch (HibernateException eee) {
            throw new TopiaException(t("topia.persistence.error.on.query",
                    hql, eee.getMessage()), eee);
        }
    }

    @Override
    public void add(TopiaEntity e) throws TopiaException {
        checkClosed(t("topia.persistence.error.unsupported.operation.on.closed.context",
                "add"));

        String id = e.getTopiaId();
        Class<TopiaEntity> entityClass = TopiaId.getClassName(id);
        TopiaDAO<TopiaEntity> dao = getDAO(entityClass);
        dao.update(e);
    }

    @Override
    public void replicate(TopiaContext dstCtxt, Object... entityAndCondition)
            throws TopiaException, IllegalArgumentException {
        checkClosed(t("topia.persistence.error.unsupported.operation.on.closed.context",
                "replicate"));

        TopiaContextImpl dstContextImpl = (TopiaContextImpl) dstCtxt;
        dstContextImpl.checkClosed(
               t("topia.persistence.error.unsupported.operation.on.closed.context",
                        "replicate"));

        if (getRootContext().equals(dstContextImpl.getRootContext())) {
            throw new IllegalArgumentException(
                   t("topia.persistence.error.replicate.on.same.context"));
        }

        String[] queries = buildQueries(entityAndCondition);
        try {
            for (String query : queries) {
                if (log.isDebugEnabled()) {
                    log.debug("acquire entities " + query);
                }
                // acquire data to replicate
                List<?> entities = findAll(query);
                replicate0(dstContextImpl, entities.toArray());
                if (log.isDebugEnabled()) {
                    log.debug("replication of entities " + query +
                            " was sucessfully done.");
                }
            }
        } catch (HibernateException eee) {
            throw new TopiaException(t("topia.persistence.error.on.replicate",
                    eee.getMessage()), eee);
        }
    }

    @Override
    public <T extends TopiaEntity> void replicateEntity(TopiaContext dstCtxt,
                                                        T entity)
            throws TopiaException, IllegalArgumentException {
        checkClosed(t("topia.persistence.error.unsupported.operation.on.closed.context",
                "replicateEntity"));

        TopiaContextImpl dstContextImpl = (TopiaContextImpl) dstCtxt;
        dstContextImpl.checkClosed(t("topia.persistence.error.unsupported.operation.on.closed.context",
                "replicateEntity"));

        if (getRootContext().equals(dstContextImpl.getRootContext())) {
            throw new IllegalArgumentException(t(
                    "topia.persistence.error.replicate.on.same.context"));
        }
        replicate0(dstContextImpl, entity);
    }

    @Override
    public <T extends TopiaEntity> void replicateEntities(TopiaContext dstCtxt,
                                                          List<T> entities)
            throws TopiaException, IllegalArgumentException {
        checkClosed(t("topia.persistence.error.unsupported.operation.on.closed.context",
                "replicateEntities"));

        TopiaContextImpl dstContextImpl = (TopiaContextImpl) dstCtxt;
        dstContextImpl.checkClosed(t("topia.persistence.error.unsupported.operation.on.closed.context",
                "replicateEntities"));

        if (getRootContext().equals(dstContextImpl.getRootContext())) {
            throw new IllegalArgumentException(t("topia.persistence.error.replicate.on.same.context"));
        }
        replicate0(dstContextImpl, entities.toArray());
    }

    @Override
    public TopiaFiresSupport getFiresSupport() {
        return firesSupport;
    }

    /**
     * Backup database in gzip compressed file.
     *
     * <b>Note: </b> Only works for h2 database.
     *
     * @param file     file to write backup
     * @param compress if true then use gzip to compress file
     * @see TopiaContext#backup(File,boolean)
     */
    @Override
    public void backup(File file, boolean compress) throws TopiaException {
        checkClosed(t("topia.persistence.error.unsupported.operation.on.closed.context",
                "backup"));
        try {
            String options = "";
            if (compress) {
                options += " COMPRESSION GZIP";
            }

            NativeQuery query = getHibernate().createNativeQuery(
                    "SCRIPT TO '" + file.getAbsolutePath() + "'" + options);
            query.list();

        } catch (Exception eee) {
            throw new TopiaException(t(
                    "topia.persistence.error.on.backup",
                    eee.getMessage()), eee);
        }
    }

    /**
     * Read database from gzip compressed file
     * 
     * Only work for h2 database
     *
     * @see TopiaContext#restore(File)
     */
    @Override
    public void restore(File file) throws TopiaException {
        // send event
        getFiresSupport().firePreRestoreSchema(this);
        checkClosed(t("topia.persistence.error.unsupported.operation.on.closed.context",
                "restore"));

        String sql = null;
        String options = "";
        try {
            // decompresse file in temporary file
            InputStream in = new BufferedInputStream(new FileInputStream(file));
            try {
            in.mark(2);

                // read header to see if is compressed file
                int b = in.read();
                // redundant cast : int magic = ((int) in.read() << 8) | b;
                int magic = in.read() << 8 | b;
                in.reset();

                if (magic == GZIPInputStream.GZIP_MAGIC) {
                    options += " COMPRESSION GZIP";
                }
            } finally {

                in.close();
            }

            NativeQuery query = getHibernate().createNativeQuery(
                    "RUNSCRIPT FROM '" + file.getAbsolutePath() + "'" + options);
            
            query.executeUpdate();

            // send event AFTER restore
            getFiresSupport().firePostRestoreSchema(this);
        } catch (Exception eee) {
            throw new TopiaException(t(
                    "topia.persistence.error.on.restore",
                    sql, eee.getMessage()), eee);
        }
    }

    /**
     * Only h2 supported for now
     *
     * @see TopiaContext#clear(boolean)
     */
    @Override
    public void clear(boolean dropDatabase) throws TopiaException {
        try {
            TopiaContextImpl root = (TopiaContextImpl) getRootContext();
            TopiaContextImpl tx = (TopiaContextImpl) root.beginTransaction();

            String sql = "DROP ALL OBJECTS";
            if (dropDatabase) {
                sql += " DELETE FILES";
            }
            Query query = tx.getHibernate().createNativeQuery(sql);
            query.executeUpdate();
            tx.closeContext();

            // Do not invoke finalize here, we are not garbage collector...
            // duplicate then the previous code of the finalize method
            root.closeContext();
            root.hibernateFactory.close();
            root.closed = true;
//            root.finalize();
        } catch (Throwable eee) {
            throw new TopiaException(
                   t("topia.persistence.error.on.clear", eee.getMessage()), eee);
        }
    }

    /**
     * Clear hibernate cache to free memory.
     * 
     * see http://docs.jboss.org/hibernate/orm/3.5/reference/en-US/html/transactions.html#transactions-basics-issues
     */
    @Override
    public void clearCache() throws TopiaException {
        getHibernate().clear();
    }

    @Override
    public List<Class<?>> getPersistenceClasses() {
        return persistenceClasses;
    }

    @Override
    public boolean isSchemaExist(Class<?> clazz)
            throws TopiaException {
        checkClosed(t("topia.persistence.error.unsupported.operation.on.closed.context",
                "replicateEntity"));
        boolean result = TopiaUtil.isSchemaExist(this, clazz.getName());
        return result;
    }

    /* Listeners adders */

    @Override
    public void addTopiaEntityListener(TopiaEntityListener listener) {
        getFiresSupport().addTopiaEntityListener(listener);
    }

    @Override
    public void addTopiaEntityListener(
            Class<? extends TopiaEntity> entityClass,
            TopiaEntityListener listener) {
        getFiresSupport().addTopiaEntityListener(entityClass, listener);
    }

    @Override
    public void addTopiaEntityVetoable(TopiaEntityVetoable vetoable) {
        getFiresSupport().addTopiaEntityVetoable(TopiaEntity.class, vetoable);
    }

    @Override
    public void addTopiaEntityVetoable(
            Class<? extends TopiaEntity> entityClass,
            TopiaEntityVetoable vetoable) {
        getFiresSupport().addTopiaEntityVetoable(entityClass, vetoable);
    }

    @Override
    public void addTopiaTransactionListener(TopiaTransactionListener listener) {
        getFiresSupport().addTopiaTransactionListener(listener);
    }

    @Override
    public void addTopiaTransactionVetoable(TopiaTransactionVetoable vetoable) {
        getFiresSupport().addTopiaTransactionVetoable(vetoable);
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        getFiresSupport().addPropertyChangeListener(listener);
    }

    @Override
    public void addTopiaContextListener(TopiaContextListener listener) {
        getFiresSupport().addTopiaContextListener(listener);
    }

    /* Listeners removers */

    @Override
    public void removeTopiaEntityListener(TopiaEntityListener listener) {
        getFiresSupport().removeTopiaEntityListener(TopiaEntity.class,
                listener);
    }

    @Override
    public void removeTopiaEntityListener(
            Class<? extends TopiaEntity> entityClass,
            TopiaEntityListener listener) {
        getFiresSupport().removeTopiaEntityListener(entityClass, listener);
    }

    @Override
    public void removeTopiaEntityVetoable(TopiaEntityVetoable vetoable) {
        getFiresSupport().removeTopiaEntityVetoable(TopiaEntity.class,
                vetoable);
    }

    @Override
    public void removeTopiaEntityVetoable(
            Class<? extends TopiaEntity> entityClass,
            TopiaEntityVetoable vetoable) {
        getFiresSupport().removeTopiaEntityVetoable(entityClass, vetoable);
    }

    @Override
    public void removeTopiaTransactionListener(
            TopiaTransactionListener listener) {
        getFiresSupport().removeTopiaTransactionListener(listener);
    }

    @Override
    public void removeTopiaTransactionVetoable(
            TopiaTransactionVetoable vetoable) {
        getFiresSupport().removeTopiaTransactionVetoable(vetoable);
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        getFiresSupport().removePropertyChangeListener(listener);
    }

    @Override
    public void removeTopiaContextListener(TopiaContextListener listener) {
        getFiresSupport().removeTopiaContextListener(listener);
    }

    @Override
    public void addTopiaEntitiesVetoable(TopiaEntitiesVetoable vetoable) {
        getFiresSupport().addTopiaEntitiesVetoable(vetoable);
    }

    @Override
    public void removeTopiaEntitiesVetoable(TopiaEntitiesVetoable vetoable) {
        getFiresSupport().removeTopiaEntitiesVetoable(vetoable);
    }

    /**
     * Build the list of queries from the given parameter
     * <code>entityAndCondition</code>.
     * 
     * If no parameter is given, then build the queries for all entities is db,
     * with no condition.
     *
     * @param entityAndCondition the list of tuples (Class,String)
     * @return the list of queries.
     * @throws TopiaException           if any pb of db while getting entities
     *                                  classes.
     * @throws IllegalArgumentException if any pb with the given parameter
     *                                  (mainly ClassCastException).
     */
    protected String[] buildQueries(Object... entityAndCondition)
            throws TopiaException, IllegalArgumentException {
        Class<?> entityClass;
        String condition;

        // si entityAndcondition est vide alors il faut le remplir
        // avec toutes les entités du mapping (class, null)
        if (entityAndCondition.length == 0) {
            Map<?,?> classMetadata = getHibernateFactory().getAllClassMetadata();
            entityAndCondition = new Object[classMetadata.size() * 2];
            int i = 0;
            for (Object className : classMetadata.keySet()) {
                try {
                    entityAndCondition[i++] = Class.forName((String) className);
                } catch (ClassNotFoundException e) {
                    // should never happen!
                    throw new TopiaException(
                            "class cast exception for entity " + className);
                }
                entityAndCondition[i++] = null;

            }
        }

        // prepare queries to perform beofre opening any transaction
        if (entityAndCondition.length % 2 != 0) {
            throw new IllegalArgumentException(
                    "entityAndCondition must be a couple of (Class, String)");
        }
        String queries[] = new String[entityAndCondition.length / 2];
        for (int i = 0; i < entityAndCondition.length;) {
            try {
                entityClass = (Class<?>) entityAndCondition[i++];
                condition = (String) entityAndCondition[i++];
                String query = "from " + entityClass.getName();
                if (condition != null && !condition.isEmpty()) {
                    query += " where " + condition;
                }
                queries[(i - 1) / 2] = query;
            } catch (ClassCastException e) {
                if (i % 2 == 0) {
                    throw new IllegalArgumentException(
                            "Others arguement must be String not " +
                                    entityAndCondition[i - 1], e);
                } else {
                    throw new IllegalArgumentException(
                            "Others arguement must be Class not " +
                                    entityAndCondition[i - 1], e);
                }
            }
        }
        return queries;
    }

    protected void replicate0(TopiaContextImpl dstContextImpl,
                              Object... entities) throws TopiaException {
        try {
            for (Object entity : entities) {
                // dettach entity to source session, to make possible copy of
                // collection without a hibernate exception (list opened in
                // two session...)
                getHibernate().evict(entity);
                dstContextImpl.getHibernate().replicate(entity,
                        ReplicationMode.EXCEPTION);
            }

        } catch (HibernateException eee) {
            throw new TopiaException(t("topia.persistence.error.on.replicate",
                    eee.getMessage()), eee);
        }
    }

    public static class SQLWork implements Work {
        private final String script;

        public SQLWork(String script) {
            this.script = script;
        }

        @Override
        public void execute(Connection connection) throws SQLException {
            PreparedStatement sta = connection.prepareStatement(script);
            try {
                sta.execute();
            } finally {
                sta.close();
            }
        }
    }
} //TopiaContextImpl

