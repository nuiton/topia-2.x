/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.topia.framework;

/**
 * Used to implement a service for Topia. You have to provide a static property
 * called SERVICE_NAME that identify the service :
 * 
 * <ul>
 * <li>public static final String SERVICE_NAME = "monservice";</li>
 * </ul>
 * 
 * The value of this attribute need to be returned when using
 * {@link #getServiceName()} method.
 *
 * @author poussin &lt;poussin@codelutin.com&gt;
 * @version $Id$
 */
public interface TopiaService {

    /**
     * Return the name of the service, this name need to match with
     * configuration file, for example for index service, we need to have :
     * "topia.service.index" and this method will returned "index".
     *
     * @return the service name
     */
    String getServiceName();

    /**
     * Retrieve entities of this service needed for persistence.
     *
     * @return List of entities full qualified name separated by a comma
     */
    Class<?>[] getPersistenceClasses();

    /**
     * Initiliaze the service before create the {@code context}.
     *
     * @param context TopiaContextImplementor
     * @return true if service need to be activated or not
     */
    boolean preInit(TopiaContextImplementor context);

    /**
     * Initiliaze the service after create the {@code context}.
     *
     * @param context TopiaContextImplementor
     * @return true if service need to be activated or not
     */
    boolean postInit(TopiaContextImplementor context);

}
