/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.topia.framework;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.TopiaNotFoundException;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaId;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

/**
 * Filter
 * 
 * Created: 23 avr. 2010
 *
 * @author fdesbois
 * @since 2.0
 */
public class TopiaFilter implements EntityFilter {

    private static final Log log = LogFactory.getLog(TopiaFilter.class);

    protected Integer startIndex;

    protected Integer endIndex;

    protected String orderBy;

    protected String referenceId;

    protected String referenceProperty;

    private PropertyChangeSupport propertyChangeSupport =
            new PropertyChangeSupport(this);

    @Override
    public Integer getStartIndex() {
        return startIndex;
    }

    @Override
    public void setStartIndex(Integer startIndex) {
        Integer oldStartIndex = this.startIndex;
        this.startIndex = startIndex;
        propertyChangeSupport.firePropertyChange(PROPERTY_START_INDEX,
                oldStartIndex, startIndex);
    }

    @Override
    public String getOrderBy() {
        return orderBy;
    }

    @Override
    public void setOrderBy(String orderBy) {
        String oldOrderBy = this.orderBy;
        this.orderBy = orderBy;
        propertyChangeSupport.firePropertyChange(PROPERTY_ORDER_BY,
                oldOrderBy, orderBy);
    }

    @Override
    public Integer getEndIndex() {
        return endIndex;
    }

    @Override
    public void setEndIndex(Integer endIndex) {
        Integer oldEndIndex = this.endIndex;
        this.endIndex = endIndex;
        propertyChangeSupport.firePropertyChange(PROPERTY_END_INDEX,
                oldEndIndex, endIndex);
    }

    @Override
    public String getReferenceId() {
        return referenceId;
    }

    @Override
    public void setReferenceId(String referenceId) {
        String oldReferenceId = this.referenceId;
        this.referenceId = referenceId;
        propertyChangeSupport.firePropertyChange(PROPERTY_REFERENCE_ID,
                oldReferenceId, referenceId);
    }

    @Override
    public void setReference(Object entity) throws IllegalArgumentException {

        if (! (entity instanceof TopiaEntity)) {
            throw new IllegalArgumentException("Can't set reference of type '" +
                entity.getClass().getName() + "' need a TopiaEntity");
        }

        setReferenceId(((TopiaEntity)entity).getTopiaId());
    }

    @Override
    public boolean hasReference() {
        return StringUtils.isNotEmpty(referenceId);
    }

    @Override
    public String getReferenceProperty() {
        return referenceProperty;
    }

    @Override
    public void setReferenceProperty(String referenceProperty) {
        String oldReferenceProperty = this.referenceProperty;
        this.referenceProperty = referenceProperty;
        propertyChangeSupport.firePropertyChange(PROPERTY_REFERENCE_PROPERTY,
                oldReferenceProperty, referenceProperty);
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.addPropertyChangeListener(listener);
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.removePropertyChangeListener(listener);
    }

    /**
     * Test if the {@code entityClass} is corresponding to the current reference
     * in the filter. Will return false if no reference is set in the filter.
     *
     * @param entityClass Class reference to test
     * @return true if the classReference is corresponding, false otherwise
     * @see #checkReference(Class, boolean)
     */
    @Override
    public boolean isClassReference(Class<?> entityClass) {

        boolean result = false;

        if (hasReference()) {
            try {
                Class<?> referenceClass = TopiaId.getClassName(referenceId);
                if (referenceClass.isAssignableFrom(entityClass)) {
                    result = true;
                }
            } catch (TopiaNotFoundException eee) {
                if (log.isWarnEnabled()) {
                    log.warn("ReferenceId '" + referenceId + "' is not a" +
                            " compatible topiaId : " + eee.getMessage());
                }
            }
        }
        return result;
    }

    /**
     * Use to check if {@code reference} class is supported by the current
     * filter reference. The reference can be not {@code mandatory}. Exceptions
     * are thrown if the check failed. If you prefer to have a boolean instead
     * of exceptions, you can use {@link #isClassReference(Class)}.
     *
     * @param reference Class reference to check
     * @param mandatory If the existence of the reference is mandatory
     * @throws IllegalArgumentException for errors on check
     * @see #hasReference()
     * @see #isClassReference(Class)
     */
    @Override
    public void checkReference(Class<?> reference, boolean mandatory)
            throws IllegalArgumentException {

        if (log.isTraceEnabled()) {
            log.trace("referenceClass to check : " + reference.getName());
            log.trace("mandatory : " + mandatory);
            log.trace("filter hasReference : " + hasReference());
            log.trace("filter isClassReference : " + isClassReference(reference));
        }

        if (mandatory && !hasReference()) {
            throw new IllegalArgumentException("The filter reference" +
                    " of type '" + reference.getSimpleName() + "' is mandatory !");
        }

        if (hasReference() &&
                !isClassReference(reference)) {
            throw new IllegalArgumentException("Reference filtered need to be" +
                    " a '" + reference.getSimpleName() + "' (referenceId = " +
                    referenceId + ")");
        }

    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder("TopiaFilter{").
                append("referenceProperty = '").append(referenceProperty).
                append("', startIndex = ").append(startIndex).
                append(", endIndex = ").append(endIndex).
                append(", orderBy = '").append(orderBy).
                append("' , referenceId = '").append(referenceId).
                append("'}");
        return builder.toString();
    }
}
