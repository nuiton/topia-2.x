/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.topia.framework;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaException;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.util.StringUtil;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Query HQL managment to simplify usage of {@link TopiaContext#findAll(String,
 * Object...) }.
 * 
 * TODO-FD20091224 JUnit Tests
 * 
 * <pre>
 * This class is used to construct a HQL query and then execute it from a
 * TopiaContext. The TopiaQuery is linked to a TopiaEntity which
 * is the main element manipulated in the query. There is two parts in using
 * this class :
 * - construction of the query, using add, addFrom, addOrder, addSelect,
 *   addGroup, ...
 * - execution of the query, using executeToEntityList, executeToEntity,
 *   executeToInteger, ...
 *
 * Construction
 * ============
 *
 * This class make easier the way to construct a HQL query.
 *
 * Example 1 :
 * -----------
 *
 * SQL :
 *       "SELECT * FROM PersonImpl WHERE firstName LIKE 'M%' AND year &gt; 1980"
 *
 * HQL using {@link TopiaContext#findAll(String, Object...) } :
 *       TopiaContext context = rootContext.beginTransaction();
 *       context.find("FROM " + Person.class.getName() + " WHERE firstName LIKE
 * :firstName AND year &gt; :year",
 *              "firstName", "M%", year, 1980);
 *
 * TopiaQuery :
 *       TopiaQuery query = TopiaQuery.createQuery(Person.class).add(
 *           Person.FIRST_NAME, Op.LIKE, "M%").add(Person.YEAR, Op.GT, 1980);
 *
 * But the real advantage is when you have some parameters to test before
 * adding
 * them to the query. With the older method, it was tidious to construct
 * and add parameters to finally use the find method from TopiaContext.
 *
 * Example 2 :
 * -----------
 *
 * HQL using {@link TopiaContext#findAll(String, Object...) } :
 *       TopiaContext context = rootContext.beginTransaction();
 *
 *       String query = "FROM " + Person.class.getName();
 *       List&lt;Object&gt; params = new ArrayList&lt;&gt;();
 *       String separator = " WHERE ";
 *       // company parameter can be null
 *       if (company != null) {
 *           query += separator + "company = :company";
 *           params.add("company");
 *           params.add(company);
 *           separator = " AND ";
 *       }
 *
 *       // contact paramater can be null
 *       if (contact != null) {
 *           query += separator + "contact = :contact";
 *           params.add("contact");
 *           params.add(contact);
 *           separator = " AND ";
 *       }
 *
 *       context.findAll(query, params.toArray());
 *
 * Here we have only two non obligatory params, but imagine if we must have
 * almost 6 or 7 parameters like this !
 *
 * TopiaQuery :
 *       TopiaQuery query = TopiaQuery.createQuery(Person.class);
 *
 *       if (company != null) {
 *           query.add(Person.COMPANY, company);
 *       }
 *
 *       if (contact != null) {
 *           query.add(Person.CONTACT, contact);
 *       }
 *
 * Many ways to create the same query :
 * ------------------------------------
 *
 * You can use multiple different manners to create a query, it depends on the
 * complexicity. More complex is the query, more easier is to construct it.
 *
 * HQL : "FROM PersonImpl AS P WHERE (P.company IS NULL OR P.company =
 * :company)
 * AND P.firstName LIKE :firstName"
 *
 * Using TopiaQuery and an Alias (these different queries are equivalent) :
 * query = TopiaQuery.createQuery(Person.class, "P");
 * 1- query.add("(P.company IS NULL OR P.company = :company") AND P.firstName
 * LIKE :firstName")
 *         .addParam("company", company).addParam("firstName",firstName + "%");
 * 2- query.add("P.company IS NULL OR P.company = :company")
 *         .add("P.firstName LIKE :firstName").addParam("company", company)
 *         .addParam("firstName",firstName + "%");
 * 3- query.add("P.company IS NULL OR P.company = :company")
 *         .add("P.firstName", Op.LIKE, firstName + "%")
 *         .addParam("company", company);
 * 4- query.addNullOr("P.company", Op.EQ, company).
 *          add("P.firstName", Op.LIKE, firstName + "%");
 *
 * You can use TopiaQuery to create a subquery in an other TopiaQuery, you have
 * to use the method {@link #fullQuery() } to get the full query in HQL and
 * give
 * it as a string in the other TopiaQuery.
 *
 * Execution
 * =========
 *
 * After construction, you can execute the query in different ways.
 *
 * Default method :
 * ----------------
 *
 * - execute : as the same result as
 * {@link TopiaContext#findAll(String, Object...) }
 *
 * Depends on entity type ;
 * ------------------------
 *
 * - executeToEntity : only one result, the first one
 * - executeToEntityList : all results returned in a List
 * - executeToEntityMap : all results returned in a Map with key defined by
 * user
 * or topiaId by default
 *
 * For aggregate :
 * ---------------
 *
 * These methods have in argument the SELECT to execute the query. The previous
 * SELECT (if defined) will not be deleted, but temporarly not used.
 *
 * - executeToInteger : for example for "SUM", "COUNT"
 * - executeToString : for example for "MAX"
 * - executeCount : directly a "count(*)"
 * - executeToObject : for other type of possible result (Long, Boolean,
 * Double,
 * ...)
 *
 * Property loading
 * ================
 *
 * When using Hibernate, some times, Entities linked to the main one will be
 * lazy initialized, but you want them directly when the query will be executed
 * to avoid problems when closing context. You can use the method
 * {@link #addLoad(String...) } to tell the TopiaQuery to load some
 * properties when executing the query. After that, you don't need to call them
 * for loading them in Hibernate.
 *
 * The syntax is the same as a property in HQL query using delegation :
 * "person.company" where person and company are entities.
 *
 * Note : loading only available on collection or entities but not property
 * on a collection of entities which must be made manually.
 *
 * For a Contact which is linked to a person (entity) and the person linked to
 * company (entity) you can add to a TopiaQuery&lt;Contact&gt; :
 *       query.addLoad("person.company")
 *
 * For a list of addresses (entity) in the contact you can do :
 *       query.addLoad("addresses")
 *
 * But it's not possible to do for example with meeting (entity) linked to the
 * contact and responsible (entity) linked to a meeting :
 *       query.addLoad("meetings.responsible")
 *
 * </pre>
 * 
 * Created: 21 déc. 2009
 *
 * @author fdesbois
 * @version $Revision$
 * @since 2.3.0
 * @deprecated since 2.6.12, {@link TopiaQuery} will be removed in version 3.0
 */
@Deprecated
public class TopiaQuery {

    private static final Log log = LogFactory.getLog(TopiaQuery.class);

    public static final String FROM_SEPARATOR_DEFAULT = ",";

    public static final String FROM_SEPARATOR_JOIN = "JOIN";

    public static final String FROM_SEPARATOR_LEFT_JOIN = "LEFT JOIN";

    /** Params for HQL query. */
    protected List<Object> params;

//    /** Select part of the query * */
//    protected StringBuilder select;

    /**
     * To keep SELECT part of the query filled by user.
     *
     * @since 2.6.7
     */
    protected List<String> userSelects;

    /**
     * To keep GROUP BY part of the query filled by user.
     *
     * @since 2.6.7
     */
    protected List<String> groupBys;

    /**
     * To keep ORDER BY part of the query filled by user.
     *
     * @since 2.6.7
     */
    protected List<String> orderBys;

    /**
     * To keep WHERE part of the query filled by user.
     *
     * @since 2.6.7
     */
    protected List<String> wheres;

    protected boolean distinct;

    //TODO tchemit-2012-02-01 Remove this (see Evol #1931)

    /** From part of the query. */
    protected StringBuilder from;

    //TODO tchemit-2012-02-01 Remove this (see Evol #1931)
//    /** Where part of the query. */
//    protected StringBuilder where;

//    /** Order By part of the query. */
//    protected StringBuilder orderBy;

//    /** Group By part of the query. */
//    protected StringBuilder groupBy;

    protected Integer startIndex;

    protected Integer endIndex;

//    /** Used to determine if parentheses are needed for Where statement. */
//    protected boolean parentheses;

    protected List<String> propertiesToLoad;

    protected String mainAlias;

    /** Enum to simmplify using operation in query. */
    public enum Op {

        /** EQUALS. */
        EQ("="),
        /** GREATER THAN. */
        GT(">"),
        /** GREATER OR EQUALS. */
        GE(">="),
        /** LIKE for String manipulation. */
        LIKE("LIKE"),
        /** LESS THAN. */
        LT("<"),
        /** LESS OR EQUALS. */
        LE("<="),
        /** IS NOT NULL. */
        NOT_NULL("IS NOT NULL"),
        /** IS NULL. */
        NULL("IS NULL"),
        /** NOT EQUAL. */
        NEQ("!="),
        /** IN. */
        IN("IN"),
        /** NOT IN. */
        NOT_IN("NOT IN");

        protected String value;

        /**
         * Constructor of the Op Enum.
         *
         * @param value corresponding to the String for the query
         */
        Op(String value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return value;
        }
    }

    public TopiaQuery() {
//        parentheses = true;
    }

    /**
     * Create a TopiaQuery based on the {@code entityClass}. The from statement
     * is automatically set.
     *
     * @param mainEntityClass used as from part of the query
     */
    public TopiaQuery(Class<? extends TopiaEntity> mainEntityClass) {
        this();
        setFrom(mainEntityClass);
    }

    /**
     * Create a TopiaQuery based on the {@code entityClass}. The from statement
     * is automatically set, the select statement must be necessary in some
     * case, the query will manage this case using the mainAlias by default.
     *
     * @param mainEntityClass used as from part of the query
     * @param alias           for the mainEntityClass
     */
    public TopiaQuery(Class<? extends TopiaEntity> mainEntityClass,
                      String alias) {
        this();
        setFrom(mainEntityClass, alias);
    }

    /**
     * Set the mainEntity in the from part of the query.
     *
     * @param mainEntityClass type of the mainEntity
     * @return the TopiaQuery
     */
    public TopiaQuery setFrom(Class<? extends TopiaEntity> mainEntityClass) {
        setFrom(mainEntityClass, null);
        return this;
    }

    /**
     * Set the mainEntity in the from part of the query and use an alias for
     * this mainEntity.
     *
     * @param mainEntityClass type of the mainEntity
     * @param alias           for the entity in the query
     * @return the TopiaQuery
     */
    public TopiaQuery setFrom(Class<? extends TopiaEntity> mainEntityClass,
                              String alias) {
        from = new StringBuilder(" FROM ").append(mainEntityClass.getName());
        mainAlias = alias;
        if (StringUtils.isNotEmpty(mainAlias)) {
            from.append(' ').append(alias);
        }
        return this;
    }

    /**
     * Add an element to the from in the query. Used to add some other data in
     * the query. The default separator used is the ", ".
     *
     * @param str the element to add
     * @return the TopiaQuery
     * @see #addFrom(Class, String)
     * @see #addJoin(String, String, boolean)
     * @see #addLeftJoin(String, String, boolean)
     * @deprecated since 2.3.4 use correct addFrom or addJoin or addLeftJoin
     */
    @Deprecated
    public TopiaQuery addFrom(String str) {
        return addFrom(FROM_SEPARATOR_DEFAULT, str, null);
    }

    /**
     * Add an element to the from in the query. Used to add some other data in
     * the query or for join as specific {@code separator}.
     *
     * @param property  the property to add
     * @param alias     alias of the property to add in form part of the query
     * @param separator The separator to use before adding the element (if null
     *                  the {@link #FROM_SEPARATOR_DEFAULT} will be used).
     * @return the TopiaQuery
     * @since 2.3.4
     */
    protected TopiaQuery addFrom(String separator, String property, String alias) {
        if (!separator.equals(FROM_SEPARATOR_DEFAULT)) {
            from.append(' ');
        }
        from.append(separator).append(' ').append(property);

        if (alias != null) {
            from.append(' ').append(alias);
        }
        return this;
    }

    /**
     * Add a inner join {@code property} to the query with {@code alias}.
     * The join is done in From statement as : FROM Contact C JOIN C.boat B. The
     * added part is 'JOIN C.boat B' using addJoin("C.boat", "B", false). The order
     * of calling {@link #addFrom(Class, String)} or this method is very important.
     * The first element in the FROM is always the main entity of the query.
     *
     * @param property Property name to use as a Join
     * @param alias    Alias of the property in the query
     * @param fetch    Add FETCH keyword to load the property in result (avoid
     *                 lazy initialization)
     * @return the TopiaQuery
     * @since 2.3.4
     */
    public TopiaQuery addJoin(String property, String alias, boolean fetch) {
        return addFromJoin(FROM_SEPARATOR_JOIN, property, alias, fetch);
    }


    /**
     * Add a left join {@code property} to the query with {@code alias}.
     * The join is done in From statement as : FROM Contact C LEFT JOIN C.boat B.
     * The added part is 'LEFT JOIN C.boat B' using addJoin("C.boat", "B", false).
     * The order of calling {@link #addFrom(Class, String)} or this method is
     * very important. The first element in the FROM is always the main entity
     * of the query.
     *
     * @param property Property name to use as a Join
     * @param alias    Alias of the property in the query
     * @param fetch    Add FETCH keyword to load the property in result (avoid
     *                 lazy initialization)
     * @return the TopiaQuery
     * @since 2.3.4
     */
    public TopiaQuery addLeftJoin(String property, String alias, boolean fetch) {
        return addFromJoin(FROM_SEPARATOR_LEFT_JOIN, property, alias, fetch);
    }

    protected TopiaQuery addFromJoin(String separator, String property,
                                     String alias, boolean fetch) {
        String sep = separator;
        if (fetch) {
            sep = new StringBuilder(separator).append(" FETCH").toString();
        }
        return addFrom(sep, property, alias);
    }

    /**
     * Add an other entity type to the from in the query.
     *
     * @param entityClass different from the mainEntity
     * @return the TopiaQuery
     */
    public TopiaQuery addFrom(Class<? extends TopiaEntity> entityClass) {
        return addFrom(entityClass, null);
    }

    /**
     * Add an other entity type to the from in the query with an alias.
     *
     * @param entityClass different from the mainEntity
     * @param alias       of the entity in the query
     * @return the TopiaQuery
     */
    public TopiaQuery addFrom(Class<? extends TopiaEntity> entityClass,
                              String alias) {
        return addFrom(FROM_SEPARATOR_DEFAULT, entityClass.getName(), alias);
    }

    /**
     * Get the full query.
     *
     * @return a String corresponding to the full query.
     */
    public String fullQuery() {
        StringBuilder result = new StringBuilder();
        StringBuilder selectStatement = new StringBuilder("SELECT ");
        if (distinct) {
            selectStatement.append("DISTINCT ");
        }
        if (CollectionUtils.isNotEmpty(userSelects)) {

            result.append(selectStatement);
            result.append(StringUtils.join(userSelects, ','));
        } else if (StringUtils.contains(from.toString(), ',') &&
                   StringUtils.isNotEmpty(mainAlias)) {
            // Set default select if there is more than one table in from
            // part and main alias is defined
            // Note : maybe a problem with using new {@link #addFrom(String, String) with other than ',' separator
            result.append(selectStatement).append(mainAlias);
        }

//        if (select != null) {
//            result.append(selectStatement).append(select);
//            // Set default select if there is more than one table in from
//            // part and main alias is defined
//            // Note : maybe a problem with using new {@link #addFrom(String, String) with other than ',' separator
//        } else if (StringUtils.contains(from.toString(), ',') &&
//                StringUtils.isNotEmpty(mainAlias)) {
//            result.append(selectStatement).append(mainAlias);
//        }
        result.append(from);

        if (CollectionUtils.isNotEmpty(wheres)) {
            result.append(" WHERE ");
            boolean first = true;
            boolean moreThanOne = wheres.size() > 1;
            for (String where : wheres) {
                if (!first) {
                    result.append(" AND ");
                }
                if (moreThanOne) {
                    result.append('(');
                }
                result.append(where);
                if (moreThanOne) {
                    result.append(')');
                }
                first = false;
            }
        }
        if (CollectionUtils.isNotEmpty(groupBys)) {
            result.append(" GROUP BY ").append(StringUtils.join(groupBys, ','));
        }
        if (CollectionUtils.isNotEmpty(orderBys)) {
            result.append(" ORDER BY ").append(StringUtils.join(orderBys, ','));
        }
//        if (where != null) {
//            result.append(where);
//        }
//        if (groupBy != null) {
//            result.append(groupBy);
//        }
//        if (orderBy != null) {
//            result.append(orderBy);
//        }
        return StringUtils.trim(result.toString());
    }

    /**
     * Add a HQL parameter to the Query.
     *
     * @param id         identification of the param in the query
     * @param paramValue value of the param
     * @return the TopiaQuery
     */
    public TopiaQuery addParam(String id, Object paramValue) {
        getParams().add(id);
        getParams().add(paramValue);
        return this;
    }

    /**
     * Add muliple paramaters to the Query. The key of each param will be tested
     * if not already exist in the existing params list and will be renamed in
     * this case.
     *
     * @param params a list of HQL params with key and value in order.
     * @return the TopiaQuery
     * @see TopiaQuery#getValueName(String)
     */
    public TopiaQuery addParams(List<Object> params) {
        for (int i = 0; i < params.size(); i += 2) {
            String paramName = (String) params.get(i);
            addParam(getValueName(paramName), params.get(i + 1));
        }
        return this;
    }

    public List<Object> getParams() {
        if (params == null) {
            params = new ArrayList<Object>();
        }
        return params;
    }

    /**
     * Return the mainAlias set from constructor.
     *
     * @return a String or null if no alias is set
     */
    public String getMainAlias() {
        return mainAlias;
    }

    /**
     * Add a property to load when query is executed. Used to avoid
     * LazyInitializationException for property needed after closing context.
     * The property is a string like those in HQL query.
     * <pre>
     * Exemples :
     * - "person.company" (Property TopiaEntity person linked to the result
     *   entity in query and company linked to person)
     *   --&gt; calling myEntity.getPerson().getCompany();
     * - "partyRoles" (Property Collection partyRoles linked to the result
     *   entity in query)
     *   --&gt; calling myEntity.getPartyRoles().size();
     * </pre>
     *
     * @param properties List of properties to load
     * @return the TopiaQuery
     */
    public TopiaQuery addLoad(String... properties) {
        getPropertiesToLoad().addAll(Arrays.asList(properties));
        return this;
    }

    /**
     * Used to load properties during query execution using FETCH keyword. This
     * keyword is used in a JOIN, so the alias is needed to identify properties
     * to load.
     * 
     * Also an empty SELECT statement will be defined to retrieve the correct
     * entity depends on the mainEntity type in the query. Carefull using
     * addFetch, hibernate doesn't support more than 3 or 4 join. In this case,
     * you can use {@link #addLoad(String...)} or load manually the entities
     * wanted.
     *
     * @param properties Properties to load during query execution
     * @return the TopiaQuery
     */
    public TopiaQuery addFetch(String... properties) {

        // Note : creating alias is not very efficient if other parameters is needed
        // Maybe the solution is to throw an exception if no mainAlias is defined

        // Check mainAlias, necessary to use join fetch
        boolean needAlias = false;
        if (StringUtils.isEmpty(mainAlias)) {
            mainAlias = RandomStringUtils.randomAlphabetic(4);
            from.append(' ').append(mainAlias);
            needAlias = true;
        }

        // Init select for single result
//        if (select == null) {
//            setSelect(mainAlias);
//        }
        if (userSelects == null) {
            setSelect(mainAlias);
        }

        for (String current : properties) {
            // Add missing alias if needed
            String property = needAlias ?
                              getProperty(mainAlias, current) : current;

            // Split property on .
            String[] parts = property.split("\\.");

            // First alias need to be a property in query
            String alias = parts[0];

            for (int i = 1; i < parts.length; i++) {
                // Construct property with current alias
                String propertyToJoin = getProperty(alias, parts[i]);

                // If next occurence exists create a new alias
                if ((i + 1) < parts.length) {
                    alias = RandomStringUtils.randomAlphabetic(4);
                } else {
                    // loop will stop, last alias is null
                    alias = null;
                }

                // Add the property in left join with fetch to true
                addLeftJoin(propertyToJoin, alias, true);
            }
        }
        return this;
    }

    protected List<String> getPropertiesToLoad() {
        if (propertiesToLoad == null) {
            propertiesToLoad = new ArrayList<String>();
        }
        return propertiesToLoad;
    }

    /**
     * @param where Where statement to add
     * @return TopiaQuery
     * @deprecated since 2.3.4, use {@link #addWhere(String)} instead
     */
    @Deprecated
    public TopiaQuery add(String where) {
        return addWhere(where);
    }

    /**
     * Add a where element to the Query. Could be anything. Parentheses are
     * added automatically (even if there are not needed).
     *
     * @param where element to add
     * @return the TopiaQuery
     * @since 2.3.4
     */
    public TopiaQuery addWhere(String where) {
        if (StringUtils.isNotEmpty(where)) {
            if (wheres == null) {
                wheres = new ArrayList<String>();
            }
            wheres.add(where);
//            // Reinitialize parentheses boolean for next add call
//            parentheses = true;
        }
//        if (this.where == null) {
//            this.where = new StringBuilder(" WHERE ");
//        } else {
//            this.where.append(" AND ");
//        }
//        if (parentheses) {
//            this.where.append('(');
//        }
//        this.where.append(where);
//        if (parentheses) {
//            this.where.append(')');
//        }
//        // Reinitialize parentheses boolean for next add call
//        parentheses = true;
        return this;
    }

    /**
     * @param paramName  name of the parameter to add
     * @param constraint constraint to use
     * @param paramValue value of this parameter
     * @return TopiaQuery
     * @deprecated since 2.3.4, use {@link #addWhere(String, Op, Object)} instead
     */
    @Deprecated
    public TopiaQuery add(String paramName, Op constraint, Object paramValue) {
        return addWhere(paramName, constraint, paramValue);
    }

    /**
     * Add an element to the query. The parameter will be automatically added.
     * The {@code operator} is needed to determine what type of operation it is.
     * Ex : add("boat", Op.EQ, boat) means -&gt; boat = :boat. Also if the paramValue
     * is Null, the paramName will be added to the query with the constraint
     * null (IS NULL).
     * 
     * TODO-fdesbois-2010-05-26 : maybe manage more than one paramValue with Object... Depends on operator
     *
     * @param paramName  the name of the parameter in the query (attribute of
     *                   the entity)
     * @param operator   the operation concerned
     * @param paramValue the value of the parameter (an other entity, a String,
     *                   ...)
     * @return the TopiaQuery
     * @since 2.3.4
     */
    public TopiaQuery addWhere(String paramName, Op operator, Object paramValue) {
        StringBuilder result = new StringBuilder(paramName).append(' ');
        if (log.isTraceEnabled()) {
            log.trace("paramValue = " + paramValue);
        }
        if (paramValue == null) {
            result.append(Op.NULL);
        } else {
            String valueName = getValueName(paramName);
            if (Op.IN == operator || Op.NOT_IN == operator) {

                result.append(operator).append(" (:").append(valueName).append(')');
            } else {
                result.append(operator).append(" :").append(valueName);
            }
            addParam(valueName, paramValue);
        }
//        parentheses = false;
        return addWhere(result.toString());

    }

    protected String getValueName(String paramName) {
        int dot = paramName.lastIndexOf('.');
        String valueName = paramName;
        if (dot != -1) {
            valueName = paramName.substring(dot + 1);
        }

        // If the paramName contains a function, escape parenthesis
        valueName = valueName.replace('(', '_');
        valueName = valueName.replace(')', '_');

        if (getParams().contains(valueName)) {
            valueName = valueName + "_" +
                        RandomStringUtils.randomAlphanumeric(4);
        }
        return valueName;
    }

    /**
     * @param paramName  name of the parameter to add
     * @param paramValue value of this parameter
     * @return TopiaQuery
     * @since 2.3.1
     * @deprecated since 2.3.4, use {@link #addEquals(String, Object...)} instead
     */
    @Deprecated
    public TopiaQuery add(String paramName, Object... paramValue) {
        return addEquals(paramName, paramValue);
    }

    /**
     * Add an element to the query. The parameter will be automatically added.
     * The default constrainst operation is Op.EQ for EQUALS. Ex : add("boat",
     * boat) means -&gt; boat = :boat. If you add more than one values, the
     * statement IN will be used. You can also have a null value in the {@code
     * paramValue} list (except if it's the only one value, it's ambiguous).
     * Note : this method do nothing if the {@code paramValue} is not defined.
     * You can also set {@code paramValue} to null if you want the {@code
     * paramName} to be null in the query.
     *
     * @param paramName  name of the parameter in the query
     * @param paramValue values of the parameter
     * @return the TopiaQuery
     * @see TopiaQuery#addWhere(String, Op, Object)
     * @since 2.3.4
     */
    public TopiaQuery addEquals(String paramName, Object... paramValue) {
        if (paramValue == null) {
            return addWhere(paramName, Op.EQ, null);
        }
        int length = paramValue.length;
        // Do nothing if there is no value defined
        if (length == 0) {
            return this;
        }
        // Only one paramValue
        if (length == 1) {
            if (log.isTraceEnabled()) {
                log.trace("Only one value " + Arrays.toString(paramValue));
            }
            return addWhere(paramName, Op.EQ, paramValue[0]);
        }
        // Multiple values is defined
        StringBuilder values = new StringBuilder();
        int count = 1;
        // Used if one of the value is null
        boolean addNull = false;
        for (Object value : paramValue) {
            if (value != null) {
                // Add the valueName to the values list for IN statement
                String valueName = getValueName(paramName + count);
                if (count != 1) {
                    values.append(", ");
                }
                values.append(':').append(valueName);
                addParam(valueName, value);
                count++;
            } else {
                addNull = true;
            }
        }
        // Create buffer for IN statement with values
        StringBuilder buffer = new StringBuilder();
        buffer.append(paramName).append(" IN (").append(values).append(")");
        // Add the OR statement for null value if needed
        if (addNull) {
            buffer.append(" OR ").
                    append(paramName).append(' ').append(Op.NULL.toString());
        } else {
//            // no parentheses needed in this case (no OR statement)
//            parentheses = false;
        }
        return addWhere(buffer.toString());
    }

    /**
     * @param properties map of the properties to add
     * @return TopiaQuery
     * @deprecated since 2.3.4 use {@link #addEquals(Map)}
     */
    @Deprecated
    public TopiaQuery add(Map<String, Object> properties) {
        return addEquals(properties);
    }

    /**
     * Add a map of properties to the where clause of the query. Each property
     * will be added to the query with Op.EQ operation, the key in the map is
     * the property name, and the value is the value of the parameter in the
     * query.
     *
     * @param properties to add to the query
     * @return the TopiaQuery
     * @since 2.3.4
     */
    public TopiaQuery addEquals(Map<String, Object> properties) {
        for (String key : properties.keySet()) {
            addEquals(key, properties.get(key));
        }
        return this;
    }

    /**
     * Add an element to the query with the constraint Not null.
     *
     * @param paramName name of the parameter in the query
     * @return the TopiaQuery
     */
    public TopiaQuery addNotNull(String paramName) {
        StringBuilder result =
                new StringBuilder(paramName).append(' ').append(Op.NOT_NULL);
//        parentheses = false;
        addWhere(result.toString());
//        parentheses = true;
        return this;
    }

    /**
     * Add an element to the query. The nullity is tested or a constraint is
     * added for that element. Ex : addNullOr("begin", Op.GT, new Date()) means
     * begin IS NULL OR begin &gt; :begin (where :begin = new Date()).
     *
     * @param paramName  the name of the parameter in the query (attribute of
     *                   the entity)
     * @param constraint the operation concerned by the or
     * @param paramValue the value of the parameter (an other entity, a String,
     *                   ...)
     * @return the TopiaQuery
     */
    public TopiaQuery addNullOr(String paramName, Op constraint,
                                Object paramValue) {
        String valueName = getValueName(paramName);
        StringBuilder result =
                new StringBuilder(paramName).append(' ').append(Op.NULL).
                        append(" OR ").append(paramName).append(constraint).
                        append(" :").append(valueName);
        addParam(valueName, paramValue);
        return addWhere(result.toString());
    }

    /**
     * Add an element to the query with the constraint null.
     *
     * @param paramName name of the parameter in the query
     * @return the TopiaQuery
     */
    public TopiaQuery addNull(String paramName) {
        addWhere(paramName, Op.EQ, null);
        return this;
    }

    /**
     * Add an element with BETWEEN operation. The {@code paramName} will be
     * found between {@code value1} and {@code value2}. Useful for date
     * manipulations.
     *
     * @param paramName The name of the parameter in the query (entity property)
     * @param value1    First value
     * @param value2    Second value
     * @return the TopiaQuery
     */
    public TopiaQuery addBetween(String paramName, Object value1, Object value2) {

        String valueName = getValueName(paramName);
        String valueName1 = new StringBuilder(valueName).append('1').toString();
        String valueName2 = new StringBuilder(valueName).append('2').toString();

        addParam(valueName1, value1);
        addParam(valueName2, value2);

        StringBuilder builder =
                new StringBuilder(paramName).
                        append(" BETWEEN ").
                        append(':').append(valueName1).
                        append(" AND ").
                        append(':').append(valueName2);

//        parentheses = false;
        addWhere(builder.toString());
//        parentheses = true;
        return this;
    }


    /**
     * Add link constraint between two properties. {@code elementProperty} is in
     * elements of {@code containerProperty} which is a collection with same type
     * than {@code elementProperty}. (HQL : elementProperty IN elements
     * (containerProperty))
     *
     * @param elementProperty   contains in containerProperty collection
     * @param containerProperty collection which contains elementProperty
     * @return the TopiaQuery
     * @since 2.3.4
     */
    public TopiaQuery addInElements(String elementProperty, String containerProperty) {
        StringBuilder builder = new StringBuilder(elementProperty).
                append(" IN elements(").append(containerProperty).append(')');
//        parentheses = false;
        addWhere(builder.toString());
//        parentheses = true;
        return this;
    }

    /**
     * Method used to add a subquery in an existing query. The params will be
     * automatically checked and copied from the subquery to the current one.
     * This method is used to inject {@code subquery} in WHERE part of the
     * query. The {@code queryPart} is the element in the query to bind with the
     * {@code subquery}. The ? character is used to inject the subquery into the
     * {@code queryPart}. Ex :
     * <pre>
     * // Add a SUB_ELMT = (subquery) into the query
     * query.addSubQuery("SUB_ELMT = (?)", subquery, false);
     * </pre>
     *
     * @param queryPart part of the query where subquery need to be injected
     * @param subquery  existing topiaQuery as subquery
     * @return the TopiaQuery
     * @see TopiaQuery#getValueName(String)
     * @since 2.3.4
     */
    public TopiaQuery addSubQuery(String queryPart,
                                  TopiaQuery subquery) {

        List<Object> subqueryParams = subquery.getParams();
        String subqueryString = subquery.fullQuery();

        // If no params is still defined, use those from subquery.
        if (CollectionUtils.isEmpty(params)) {
            addParams(subqueryParams);
        } else {
            for (int i = 0; i < subqueryParams.size(); i += 2) {

                String paramName = (String) subqueryParams.get(i);
                Object paramValue = subqueryParams.get(i + 1);

                // Check existence of paramName
                int index = params.indexOf(paramName);

                if (index == -1) { // if not defined, param is only used
                    // in sub-query, so add it in the whole query
                    addParam(paramName, paramValue);
                } else { // If already defined

                    Object existingValue = params.get(index + 1);

                    // Only change paramName in not equals case
                    if (!ObjectUtils.equals(existingValue, paramValue)) {
                        String newParamName = getValueName(paramName);
                        // Replace old paramName in subquery
                        subqueryString =
                                subqueryString.replace(":" + paramName,
                                                       ":" + newParamName);

                        // Add the param to the current query
                        addParam(newParamName, paramValue);
                    }
                }
            }
        }

        // Replace ? injection by the subquery
        String result = queryPart.replace("?", subqueryString);
        return addWhere(result);
    }

    /**
     * Add an element to the select in the query. Depends on the result wanted
     * in execute methods. The main entity will be automatically added only if
     * an alias is initialize from constructor. If you want only this select
     * element, use {@link #setSelect(String...) } method instead.
     *
     * @param select element to add
     * @return the TopiaQuery
     */
    public TopiaQuery addSelect(String... select) {

//        if (mainAlias != null &&
//            CollectionUtils.isNotEmpty(userSelects) &&
//            userSelects.contains(mainAlias)) {
//            // if select is the mainAlias, do nothing
//            return this;
//        }
//        String str = convertStringArray(select);
//        // if select is the mainAlias, do nothing
//        if (mainAlias != null && str.equals(mainAlias)) {
//            return this;
//        }

        if (userSelects == null) {

            userSelects = new ArrayList<String>();

            if (mainAlias != null) {

                // if mainAlias is not null, add it before adding the select in argument
                userSelects.add(mainAlias);
            }
        }

        userSelects.addAll(Arrays.asList(select));

//        // if select is not null, add the new element to the select
//        if (this.select != null) {
//            this.select.append(", ");
//            // if mainAlias is not null, add it before adding the select in argument
//        } else if (mainAlias != null) {
//            this.select = new StringBuilder(mainAlias).append(", ");
//        } else {
//            this.select = new StringBuilder();
//        }
//        this.select.append(convertStringArray(select));
        return this;
    }

    /**
     * Set the select in the query. Depends on the result wanted in execute
     * methods.
     *
     * @param select element to set
     * @return the TopiaQuery
     */
    public TopiaQuery setSelect(String... select) {
        userSelects = new ArrayList<String>(Arrays.asList(select));
//        this.select = new StringBuilder(convertStringArray(select));
        return this;
    }

    /**
     * Add the distinct key word in the query. The result will not have multiple
     * same values.
     *
     * @return the TopiaQuery
     */
    public TopiaQuery addDistinct() {
        distinct = true;
        return this;
    }

    /**
     * Add an element to the order in the query. Used to add some parameters to
     * order by.
     *
     * @param order element to add
     * @return the TopiaQuery
     */
    public TopiaQuery addOrder(String... order) {
        if (orderBys == null) {
            orderBys = new ArrayList<String>();
        }
        Collections.addAll(orderBys, order);
//        if (orderBy == null) {
//            orderBy = new StringBuilder(" ORDER BY ");
//        } else {
//            orderBy.append(", ");
//        }
//        orderBy.append(convertStringArray(order));
        return this;
    }

    public TopiaQuery addOrderDesc(String order) {
        return addOrder(order + " DESC");
    }

    /**
     * Add an element to the group of the query. Used to add some paramters to
     * group by.
     *
     * @param group element to add
     * @return the TopiaQuery
     */
    public TopiaQuery addGroup(String... group) {
        if (groupBys == null) {
            groupBys = new ArrayList<String>();
        }
        Collections.addAll(groupBys, group);
//        if (groupBy == null) {
//            groupBy = new StringBuilder(" GROUP BY ");
//        } else {
//            groupBy.append(", ");
//        }
//        groupBy.append(convertStringArray(group));
        return this;
    }

//    /**
//     * Helper method for array type. Each value will be separated by a comma.
//     * TODO-fdesbois-2010-05-25 : replace this algo by StringUtil.join()
//     *
//     * @param array of String
//     * @return a String with values of the array separated by a comma
//     */
//    protected String convertStringArray(String... array) {
//        StringBuilder result = new StringBuilder();
//        for (String value : array) {
//            result.append(", ").append(value);
//        }
//        String str = "";
//        if (result.length() > 0) {
//            str = result.substring(2);
//        }
//        return str;
//    }

    /**
     * Limit the result of the query with startIndex and endIndex.
     *
     * @param start first index to get from the results
     * @param end   last index to get from the results
     * @return the TopiaQuery
     */
    public TopiaQuery setLimit(int start, int end) {
        startIndex = start;
        endIndex = end;
        return this;
    }

    /**
     * Remove limits previously set
     *
     * @return the TopiaQuery
     */
    public TopiaQuery resetLimit() {
        startIndex = null;
        endIndex = null;
        return this;
    }

    /**
     * Set the max results wanted for the query.
     *
     * @param max the number of elements wanted
     * @return the TopiaQuery
     */
    public TopiaQuery setMaxResults(int max) {
        return setLimit(0, max - 1);
    }

    /**
     * Add a {@code filter} to the query that contains limit indexes,
     * orderBy condition and referenceId if needed. The referenceProperty is
     * necessary to use the referenceId of the {@code filter}. The filter will
     * be applied on the main entity in the query (using the mainAlias if
     * necessary).
     * 
     * Note : the default orderBy is the topiaCreateDate ordered desc (the most
     * recent in first)
     *
     * @param filter Filter to apply on the query
     * @return the TopiaQuery
     * @throws IllegalArgumentException if referenceId is defined but no
     *                                  referenceProperty was set
     * @see #addFilter(EntityFilter, String)
     */
    public TopiaQuery addFilter(EntityFilter filter)
            throws IllegalArgumentException {
        return addFilter(filter, null);
    }

    /**
     * Add a {@code filter} to the query that contains limit indexes,
     * orderBy condition and referenceId if needed. In some case it's necessary
     * to specify explicitely the {@code propertyToFilter} in complex queries.
     * The referenceProperty need to be specifie in {@code filter} to have a
     * correspondance between the referenceId and it's property in the query. By
     * default, the {@code propertyToFilter} is the mainAlias of the query.
     * 
     * Note : the default orderBy is the topiaCreateDate ordered desc (the most
     * recent in first)
     *
     * @param filter           Filter to apply on the query
     * @param propertyToFilter Explicit property to filter
     * @return the TopiaQuery
     * @throws IllegalArgumentException if referenceId is defined but no
     *                                  referenceProperty was set
     */
    public TopiaQuery addFilter(EntityFilter filter,
                                String propertyToFilter)
            throws IllegalArgumentException {

        if (propertyToFilter == null) {
            propertyToFilter = mainAlias;
        }

        Integer startIndex = filter.getStartIndex();
        Integer endIndex = filter.getEndIndex();
        String orderBy = filter.getOrderBy();
        String referenceId = filter.getReferenceId();
        String referenceProperty = filter.getReferenceProperty();

        if (log.isDebugEnabled()) {
            log.debug("Filter added to the query : " + filter);
        }

        // Add limits. Only startIndex do nothing.
        // startIndex + endIndex provides the limit
        if (startIndex != null && endIndex != null) {
            setLimit(startIndex, endIndex);

            // endIndex only provides the maxResults wanted
        } else if (endIndex != null) {
            setMaxResults(endIndex);
        }

        // Add order to the main entity in the query, splitted by comma
        if (orderBy != null) {
            List<String> order = new ArrayList<String>();
            for (String elmt : orderBy.split(",")) {
                String property =
                        TopiaQuery.getProperty(propertyToFilter, elmt.trim());
                order.add(property);
            }
            addOrder(order.toArray(new String[order.size()]));

            // Default order by creation date
        } else {
            addOrderDesc(getPropertyCreateDate(propertyToFilter));
        }

        if (filter.hasReference()) {
            if (referenceProperty == null) {
                throw new IllegalArgumentException("Reference property need" +
                                                   " to be defined in filter to use referenceId = " +
                                                   referenceId);
            }
            addEquals(getPropertyId(referenceProperty), referenceId);
        }

        return this;
    }

    /**
     * Simple execution of the query. This method use directly the find method
     * in TopiaContext interface.
     *
     * @param transaction the TopiaContext to use for execution
     * @return a List of results
     * @throws TopiaException for error on query execution
     * @see TopiaContext#findAll(String, Object...)
     */
    public List execute(TopiaContext transaction) throws TopiaException {
        String query = fullQuery();
        if (log.isDebugEnabled()) {
            log.debug(this);
        }
        List result;
        if (startIndex != null && endIndex != null) {
            result = transaction.find(query, startIndex, endIndex,
                                      getParams().toArray());
        } else {
            result = transaction.findAll(query, getParams().toArray());
        }
        return result;
    }

    /**
     * Execute the query and get a List of entity. Some properties will be
     * loaded if they are prealably set using ${@link #addLoad(String...) }.
     *
     * @param <E>         entity type
     * @param transaction the TopiaContext to use for execution
     * @param entityClass used to check return type of execution results
     * @return a List of TopiaEntity corresponding to the entityClass in
     *         argument
     * @throws TopiaException     for error on query execution
     * @throws ClassCastException if entityClass doesn't match to results
     */
    public <E extends TopiaEntity> List<E> executeToEntityList(
            TopiaContext transaction, Class<E> entityClass)
            throws TopiaException, ClassCastException {
        List res = execute(transaction);
        if (log.isTraceEnabled()) {
            log.trace("Properties to load : " + getPropertiesToLoad());
        }
        List<E> results = new ArrayList<E>();
        for (Object o : res) {
            if (o == null) {
                continue;
            }
            if (o instanceof Object[]) {
                // If it's an array, we want only the first element wich is the
                // entity wanted
                // We know that the array have at least one element
                o = ((Object[]) o)[0];
            }
            if (!entityClass.isAssignableFrom(o.getClass())) {
                throw new ClassCastException(o.getClass().getName() +
                                             " can't be cast to " + entityClass.getName() +
                                             " o : " + o);
            }
            E entity = (E) o;
            // Check distinct constraint for complex query where o is firstly an
            // Object[] (potentially distinct results with existing entity to add)
            if (!(distinct && results.contains(entity))) {
                if (!getPropertiesToLoad().isEmpty()) {
                    loadProperties(entity);
                }
                results.add(entity);
            }
        }
        return results;
    }

    /**
     * Execute the query and get a Map of entity with key type in argument. Some
     * properties will be loaded if they are prealably set using ${@link
     * #addLoad(String...) }.
     *
     * @param <E>         entity type
     * @param <K>         the type of the map key
     * @param transaction the TopiaContext to use for execution
     * @param entityClass needed to execute the query
     * @param keyName     the property name of the key in the entity
     * @param keyClass    the key class for the result map
     * @return a Map with the key type defined and the entity in value
     * @throws TopiaException     for error on query execution
     * @throws ClassCastException if entityClass doesn't match to results
     */
    public <E extends TopiaEntity, K> Map<K, E> executeToEntityMap(
            TopiaContext transaction, Class<E> entityClass,
            String keyName, Class<K> keyClass)
            throws TopiaException, ClassCastException {

        // Use LinkedHashMap to keep insert order from list results which
        // can be ordered
        Map<K, E> results = new LinkedHashMap<K, E>();
        List<E> list = executeToEntityList(transaction, entityClass);
        for (E elmt : list) {
            Object value = loadProperty(elmt, keyName);
            if (value != null && !keyClass.isAssignableFrom(value.getClass())) {
                throw new ClassCastException(value.getClass().getName() +
                                             " can't be cast to " + keyClass.getName());
            }
            results.put((K) value, elmt);
        }
        return results;
    }

    /**
     * Execute the query and get a Map of entity with topiaId in key. Some
     * properties will be loaded if they are prealably set using ${@link
     * #addLoad(String...) }.
     *
     * @param <E>         entity type
     * @param transaction the TopiaContext to use for execution
     * @param entityClass used to check return type of execution results
     * @return a Map with the key type defined and the entity in value
     * @throws TopiaException     for error on query execution
     * @throws ClassCastException if entityClass doesn't match to results
     */
    public <E extends TopiaEntity> Map<String, E> executeToEntityMap(
            TopiaContext transaction, Class<E> entityClass)
            throws TopiaException, ClassCastException {
        return executeToEntityMap(transaction, entityClass,
                                  TopiaEntity.TOPIA_ID, String.class);
    }

    /**
     * Execute the query and get the first result entity. Some properties will
     * be loaded if they are prealably set using ${@link #addLoad(String...) }.
     *
     * @param <E>         entity type
     * @param transaction the TopiaContext to use for execution
     * @param entityClass used to check return type of execution results
     * @return a TopiaEntity corresponding  to the entityClass in argument
     * @throws TopiaException     for error on query execution
     * @throws ClassCastException if entityClass doesn't match to results
     */
    public <E extends TopiaEntity> E executeToEntity(TopiaContext transaction,
                                                     Class<E> entityClass)
            throws TopiaException, ClassCastException {
        setMaxResults(1);
        List<E> results = executeToEntityList(transaction, entityClass);
        resetLimit();
        return !results.isEmpty() ? results.get(0) : null;
    }

    /**
     * Execute the query and get an Object for result. The select is overriden
     * to get only the right value for return.
     *
     * @param transaction the TopiaContext to use for execution
     * @param select      the Select overriden
     * @return an Object
     * @throws TopiaException for error on query execution
     */
    public Object executeToObject(TopiaContext transaction, String select)
            throws TopiaException {
        List<String> oldValue = this.userSelects;
//        StringBuilder oldValue = this.select;
        if (!StringUtils.isEmpty(select)) {
            setSelect(select);
        }
        Object result = null;
        setMaxResults(1);
        List results = execute(transaction);
        if (!results.isEmpty()) {
            result = results.get(0);
        }
//        this.select = oldValue;
        userSelects = oldValue;
        resetLimit();
        return result;
    }

    /**
     * Execute the query and get an Integer for result. Used only for query with
     * aggration select which return a Long : COUNT, SUM ... The select is
     * overriden to get only the right value for return.
     *
     * @param transaction the TopiaContext to use for execution
     * @param select      the Select overriden (ex : SUM(myParam))
     * @return an Integer
     * @throws TopiaException for error on query execution
     */
    public int executeToInteger(TopiaContext transaction, String select)
            throws TopiaException {
        Long res = (Long) executeToObject(transaction, select);
        return res != null ? res.intValue() : 0;
    }

    /**
     * Execute the query and get a String for result. Used for query with MAX,
     * ... The select is overriden to get only the right value for return.
     *
     * @param transaction the TopiaContext to use for execution
     * @param select      the Select overriden (ex : MAX(myParam))
     * @return a String
     * @throws TopiaException for error on query execution
     */
    public String executeToString(TopiaContext transaction, String select)
            throws TopiaException {
        Object res = executeToObject(transaction, select);
        return res != null ? (String) res : "";
    }

    /**
     * Execute a simple count on the query, i.e. the number of results get from
     * the query. The order is not considered to count the elements and will be
     * temporarly disabled. The distinct constraint will be manage if necessary
     * :
     * <pre>
     * COUNT(DISTINCT mainAlias)
     * </pre>
     *
     * @param transaction the TopiaContext to use for execution
     * @return an int corresponding to the number of result in the query
     * @throws TopiaException for error on query execution
     */
    public int executeCount(TopiaContext transaction) throws TopiaException {
        List<String> oldOrderBys = orderBys;
        orderBys = null;
//        StringBuilder oldOrder = orderBy;
//        orderBy = null;
        StringBuilder count = new StringBuilder("COUNT(");
        // Ano #560 : manage distinct case when the alias is set (otherwise
        // no need the distinct keyword because the entity type in query is
        // unique)

        //Note Ano #1930 tchemit  2012-02-01 this code does not work if a setSelect was done

//        if (distinct && StringUtils.isNotEmpty(mainAlias) &&
//            isUserSelectEqualsMainAlias()) {
//            count.append("DISTINCT ").append(mainAlias);
//            // When distinct is not set, use <pre>*</pre>
//        } else {
//            count.append('*');
//        }

        String mainSelect = null;

        if (distinct) {

            if (CollectionUtils.isNotEmpty(userSelects)) {

                if (userSelects.size() > 1) {

                    // can not count this on multiple select...
                    throw new TopiaException(
                            "To count, can not have more than one select, but found " +
                            userSelects);
                }
                mainSelect = userSelects.get(0);
            } else if (StringUtils.isNotEmpty(mainAlias)) {

                // use main alias
                mainSelect = mainAlias;
            }
        }

        if (mainSelect != null) {
            count.append("DISTINCT ").append(mainSelect);
        } else {
            count.append('*');
        }
        count.append(')');
        int result = executeToInteger(transaction, count.toString());
        orderBys = oldOrderBys;
//        orderBy = oldOrder;
        return result;
    }

    protected boolean isUserSelectEqualsMainAlias() {
        boolean result = !CollectionUtils.isEmpty(userSelects) &&
                         userSelects.size() == 1 &&
                         userSelects.get(0).equals(mainAlias);
        return result;
    }

    /**
     * Load all properties for the entity.
     *
     * @param entity used to load properties
     * @throws TopiaException for error on query execution
     */
    protected void loadProperties(TopiaEntity entity)
            throws TopiaException {
        for (String prop : getPropertiesToLoad()) {
            if (log.isTraceEnabled()) {
                log.trace("load property " + prop + " ...");
            }
            List<String> str = Arrays.asList(prop.split("\\."));
            Iterator<String> it = str.iterator();
            TopiaEntity currEntity = entity;
            while (it.hasNext()) {
                String s = it.next();
                if (mainAlias != null && s.equals(mainAlias)) {
                    if (log.isTraceEnabled()) {
                        log.trace("Skip alias : " + mainAlias);
                    }
                    continue;
                }
                if (log.isTraceEnabled()) {
                    log.trace("Current entity : " +
                              currEntity.getClass().getSimpleName());
                    log.trace("Current loading : " + s);
                }
                if (it.hasNext()) {
                    currEntity = loadEntityProperty(currEntity, s);
                } else {
                    loadProperty(currEntity, s);
                }
            }
        }
    }

    /**
     * Load a property of type TopiaEntity from an other entity.
     *
     * @param <T>      type of the entity extends TopiaEntity
     * @param entity   used to load the property
     * @param property name of the property in the entity
     * @return a TopiaEntity corresponding to the property loaded
     * @throws TopiaException for error on query execution
     */
    protected <T extends TopiaEntity> TopiaEntity loadEntityProperty(T entity,
                                                                     String property)
            throws TopiaException {
        return (TopiaEntity) loadProperty(entity, property);
    }

    /**
     * Load a property from an entity.
     *
     * @param <T>      type of the entity extends TopiaEntity
     * @param entity   used to load the property
     * @param property name of the property in the entity
     * @return an Object corresponding to the property loaded
     * @throws TopiaException for error loading property (encapsulate
     *                        IllegalACessException, InvocationTargetException,
     *                        NoSuchMethodException)
     */
    protected <T extends TopiaEntity> Object loadProperty(T entity,
                                                          String property)
            throws TopiaException {
        try {
            Object res = PropertyUtils.getProperty(entity, property);
            if (log.isDebugEnabled()) {
                log.debug("load property '" + property + "' for '" +
                          entity.getClass().getSimpleName() + "'");
            }
            if (res != null && Collection.class.isAssignableFrom(res.getClass())) {
                Collection<?> list = (Collection<?>) res;
                list.size();
            }
            return res;
        } catch (IllegalAccessException eee) {
            throw new TopiaException("Illegal access on property " +
                                     property + " from entity " +
                                     entity.getClass().getName(), eee);
        } catch (InvocationTargetException eee) {
            throw new TopiaException("Invocation error on entity " +
                                     entity.getClass().getName() + " for property " +
                                     property, eee);
        } catch (NoSuchMethodException eee) {
            throw new TopiaException("Getter method does not exist for" +
                                     " property " + property + " from entity " +
                                     entity.getClass().getName(), eee);
        }
    }

    /**
     * This method is used to concat properties from entities. Ex in HQL you can
     * have boat.shipOwner.name, these properties are defined as constants in
     * each entity associated (SHIP_OWNER in Boat entity, NAME in ShipOwner
     * entity) so you just have to call this method as :
     * <pre>
     * getProperty("boat", Boat.SHIP_OWNER, ShipOwner.NAME);
     * // will return boat.shipOwner.name
     * </pre>
     * 
     * It's better to use constants instead of directly the string chain to
     * avoid problems on changing property name in model. Furthermore it's
     * better to use this method instead of doing :
     * <pre>
     * "boat." + Boat.SHIP_OWNER + "." + ShipOwner.NAME
     * </pre>
     *
     * @param entityProperty to concat
     * @return the string chain with properties separated with a dot
     */
    public static String getProperty(String... entityProperty) {
        List<String> list = Arrays.asList(entityProperty);
        String result = StringUtil.join(list, ".", false);
        return result;
    }

    public String getPropertyId(String alias) {
        return getProperty(alias, TopiaEntity.TOPIA_ID);
    }

    public String getPropertyCreateDate(String alias) {
        return getProperty(alias, TopiaEntity.TOPIA_CREATE_DATE);
    }

    public String getPropertyVersion(String alias) {
        return getProperty(alias, TopiaEntity.TOPIA_VERSION);
    }

    @Override
    protected void finalize() throws Throwable {
        // Clean StringBuilder statements
//        select = null;
        from = null;
//        where = null;
//        orderBy = null;
//        groupBy = null;
        super.finalize();
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder(fullQuery()).
                append("; (PARAMS : ").
                append(getParams()).
                append("); (LIMIT : ").
                append(startIndex).
                append(", ").
                append(endIndex).
                append(')');
        return result.toString();
    }

}
