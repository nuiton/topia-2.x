/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.topia.framework;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.beans.VetoableChangeSupport;
import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Session;
import org.hibernate.event.spi.PostDeleteEvent;
import org.hibernate.event.spi.PostDeleteEventListener;
import org.hibernate.event.spi.PostInsertEvent;
import org.hibernate.event.spi.PostInsertEventListener;
import org.hibernate.event.spi.PostLoadEvent;
import org.hibernate.event.spi.PostLoadEventListener;
import org.hibernate.event.spi.PostUpdateEvent;
import org.hibernate.event.spi.PostUpdateEventListener;
import org.hibernate.event.spi.PreDeleteEvent;
import org.hibernate.event.spi.PreDeleteEventListener;
import org.hibernate.event.spi.PreInsertEvent;
import org.hibernate.event.spi.PreInsertEventListener;
import org.hibernate.event.spi.PreLoadEvent;
import org.hibernate.event.spi.PreLoadEventListener;
import org.hibernate.event.spi.PreUpdateEvent;
import org.hibernate.event.spi.PreUpdateEventListener;
import org.hibernate.persister.entity.EntityPersister;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaException;
import org.nuiton.topia.TopiaVetoException;
import org.nuiton.topia.event.TopiaContextEvent;
import org.nuiton.topia.event.TopiaContextListener;
import org.nuiton.topia.event.TopiaEntitiesEvent;
import org.nuiton.topia.event.TopiaEntitiesVetoable;
import org.nuiton.topia.event.TopiaEntityEvent;
import org.nuiton.topia.event.TopiaEntityListener;
import org.nuiton.topia.event.TopiaEntityVetoable;
import org.nuiton.topia.event.TopiaTransactionEvent;
import org.nuiton.topia.event.TopiaTransactionListener;
import org.nuiton.topia.event.TopiaTransactionVetoable;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaEntityAbstract;
import org.nuiton.util.CategorisedListenerSet;
import org.nuiton.util.ListenerSet;

/**
 * TODO-fdesbois-20100507 : Need translation of javadoc.
 * 
 * Contient l'ensemble de la partie listener et vetoable c'est à dire la
 * gestion, les fires, ...
 *
 * @author jruchaud &lt;jruchaud@codelutin.com&gt;
 * @version $Id$
 */
public class TopiaFiresSupport {

    static private Log log = LogFactory.getLog(TopiaFiresSupport.class);

    /** used to fire read event */
    final static Object NO_CHANGE = new Object();

    /** used to collect entity modification during transaction */
    protected Map<TopiaEntity, EntityState> transactionEntities =
            new IdentityHashMap<>();

    protected Set<PropertyChangeListener> propertyChangeListeners =
            new HashSet<PropertyChangeListener>();

    /* Pour la transaction */

    protected ListenerSet<TopiaTransactionListener> transactionListeners =
            new ListenerSet<TopiaTransactionListener>();

    protected ListenerSet<TopiaTransactionVetoable> transactionVetoables =
            new ListenerSet<TopiaTransactionVetoable>();

    /* Pour les entités */

    protected CategorisedListenerSet<TopiaEntityListener> entityListeners =
            new CategorisedListenerSet<TopiaEntityListener>();

    protected CategorisedListenerSet<TopiaEntityVetoable> entityVetoables =
            new CategorisedListenerSet<TopiaEntityVetoable>();

    /* Pour les listes d'entités */

    protected ListenerSet<TopiaEntitiesVetoable> entitiesVetoables =
            new ListenerSet<TopiaEntitiesVetoable>();

    /* Pour les actions du topia context */

    protected ListenerSet<TopiaContextListener> topiaContextListeners =
            new ListenerSet<TopiaContextListener>();

    /**
     * used to register objects loaded during transaction.
     *
     * @param entity the loaded entity
     */
    public void warnOnLoadEntity(TopiaEntity entity) {
        if (log.isDebugEnabled()) {
            log.debug("warnOnReadEntity");
        }
        EntityState state = transactionEntities.get(entity);
        if (state == null) {
            state = new EntityState();
            transactionEntities.put(entity, state);
        }
        state.addLoad();
    }

    /**
     * used to register objects created during transaction.
     *
     * @param entity the created entity
     */
    public void warnOnCreateEntity(TopiaEntity entity) {
        if (log.isDebugEnabled()) {
            log.debug("warnOnCreateEntity");
        }
        EntityState state = transactionEntities.get(entity);
        if (state == null) {
            state = new EntityState();
            transactionEntities.put(entity, state);
        }
        state.addCreate();
    }

    /**
     * used to register objects loaded during transaction.
     *
     * @param entity the read entity
     */
    public void warnOnReadEntity(TopiaEntity entity) {
        if (log.isDebugEnabled()) {
            log.debug("warnOnReadEntity");
        }
        EntityState state = transactionEntities.get(entity);
        if (state == null) {
            state = new EntityState();
            transactionEntities.put(entity, state);
        }
        state.addRead();
    }

    /**
     * used to register objects modified during transaction.
     *
     * @param entity the updated entity
     */
    public void warnOnUpdateEntity(TopiaEntity entity) {
        if (log.isDebugEnabled()) {
            log.debug("warnOnUpdateEntity");
        }

        EntityState state = transactionEntities.get(entity);
        if (state == null) {
            state = new EntityState();
            transactionEntities.put(entity, state);
        }
        state.addUpdate();
    }

    /**
     * used to register objects deleted during transaction.
     *
     * @param entity the deleted entity
     */
    public void warnOnDeleteEntity(TopiaEntity entity) {
        if (log.isDebugEnabled()) {
            log.debug("warnOnDeleteEntity");
        }
        EntityState state = transactionEntities.get(entity);
        if (state == null) {
            state = new EntityState();
            transactionEntities.put(entity, state);
        }
        state.addDelete();
    }

    protected boolean isNotEmpty(ListenerSet<?> set) {
        return set.size() > 0;
    }

    protected boolean isNotEmpty(CategorisedListenerSet<?> set, Class<?> category) {
        return set.iterator(category).hasNext();
    }

    /* Fires sur les transactions */

    public void fireOnBeginTransaction(TopiaContextImplementor context) {
        if (log.isDebugEnabled()) {
            log.debug("fireOnBeginTransaction");
        }
        if (isNotEmpty(transactionVetoables)) {
            TopiaTransactionEvent e = new TopiaTransactionEvent(context);
            for (TopiaTransactionVetoable listener : transactionVetoables) {
                try {
                    listener.beginTransaction(e);
                } catch (Exception eee) {
                    throw new TopiaVetoException(eee);
                }
            }
        }
    }

    public void fireOnPostCommit(TopiaContextImplementor context) {
        if (log.isDebugEnabled()) {
            log.debug("fireOnPostCommit");
        }
        if (isNotEmpty(transactionListeners)) {
            TopiaTransactionEvent e = new TopiaTransactionEvent(context,
                    transactionEntities);
            for (TopiaTransactionListener listener : transactionListeners) {
                try {
                    listener.commit(e);
                } catch (Exception eee) {
                    if (log.isErrorEnabled()) {
                        log.error("Can't fireOnPostCommit", eee);
                    }
                }
            }
        }
        transactionEntities.clear();
    }

    public void fireOnPostRollback(TopiaContextImplementor context) {
        if (log.isDebugEnabled()) {
            log.debug("fireOnPostRollback");
        }
        if (isNotEmpty(transactionListeners)) {
            TopiaTransactionEvent e = new TopiaTransactionEvent(context,
                    transactionEntities);
            for (TopiaTransactionListener listener : transactionListeners) {
                try {
                    listener.rollback(e);
                } catch (Exception eee) {
                    if (log.isErrorEnabled()) {
                        log.error("Can't fireOnPostRollback", eee);
                    }
                }
            }
        }
        transactionEntities.clear();
    }

    /* Fires sur les entités */

    public void fireOnPreCreate(TopiaContextImplementor context,
                                TopiaEntity entity, Object[] state) {
        if (log.isDebugEnabled()) {
            log.debug("fireOnPreCreate");
        }
        if (isNotEmpty(entityVetoables, entity.getClass())) {
            TopiaEntityEvent event = new TopiaEntityEvent(context, entity, state);
            for (Iterator<TopiaEntityVetoable> l = entityVetoables.iterator(entity
                    .getClass()); l.hasNext(); ) {
                try {
                    l.next().create(event);
                } catch (Exception eee) {
                    throw new TopiaVetoException(eee);
                }
            }
        }
    }

    public void fireOnPostCreate(TopiaContextImplementor context,
                                 TopiaEntity entity, Object[] state) {
        if (log.isDebugEnabled()) {
            log.debug("fireOnPostCreate");
        }
        warnOnCreateEntity(entity);
        if (isNotEmpty(entityListeners, entity.getClass())) {
            TopiaEntityEvent event = new TopiaEntityEvent(context, entity, state);
            for (Iterator<TopiaEntityListener> l = entityListeners.iterator(entity
                    .getClass()); l.hasNext(); ) {
                try {
                    l.next().create(event);
                } catch (Exception eee) {
                    if (log.isErrorEnabled()) {
                        log.error("Can't fireOnPostCreate for entity: " + entity,
                                eee);
                    }
                }
            }
        }
    }

    public void fireOnPreLoad(TopiaContextImplementor context,
                              TopiaEntity entity, Object[] state) {
        if (log.isDebugEnabled()) {
            log.debug("fireOnPreLoad");
        }
        if (isNotEmpty(entityVetoables, entity.getClass())) {
            TopiaEntityEvent event = new TopiaEntityEvent(context, entity, state);
            for (Iterator<TopiaEntityVetoable> l = entityVetoables.iterator(entity
                    .getClass()); l.hasNext(); ) {
                try {
                    l.next().load(event);
                } catch (Exception eee) {
                    throw new TopiaVetoException(eee);
                }
            }
        }
    }

    public void fireOnPostLoad(TopiaContextImplementor context,
                               TopiaEntity entity, Object[] state) {
        if (log.isDebugEnabled()) {
            log.debug("fireOnPostLoad");
        }
        warnOnLoadEntity(entity);
        if (isNotEmpty(entityListeners, entity.getClass())) {
            TopiaEntityEvent event = new TopiaEntityEvent(context, entity, state);
            for (Iterator<TopiaEntityListener> l = entityListeners.iterator(entity
                    .getClass()); l.hasNext(); ) {
                try {
                    l.next().load(event);
                } catch (Exception eee) {
                    if (log.isErrorEnabled()) {
                        log
                                .error(
                                        "Can't fireOnPostLoad for entity: "
                                                + entity, eee);
                    }
                }
            }
        }
    }

    public void fireOnPreUpdate(TopiaContextImplementor context,
                                TopiaEntity entity, Object[] state, Object[] oldState) {
        if (log.isDebugEnabled()) {
            log.debug("fireOnPreUpdate");
        }
        if (isNotEmpty(entityVetoables, entity.getClass())) {
            TopiaEntityEvent event = new TopiaEntityEvent(context, entity, state, oldState);
            for (Iterator<TopiaEntityVetoable> l = entityVetoables.iterator(entity
                    .getClass()); l.hasNext(); ) {
                try {
                    l.next().update(event);
                } catch (Exception eee) {
                    throw new TopiaVetoException(eee);
                }
            }
        }
    }

    public void fireOnPostUpdate(TopiaContextImplementor context,
                                 TopiaEntity entity, Object[] state, Object[] oldState, int[] dirtyProperties) {
        if (log.isDebugEnabled()) {
            log.debug("fireOnPostUpdate");
        }
        warnOnUpdateEntity(entity);
        if (isNotEmpty(entityListeners, entity.getClass())) {
            TopiaEntityEvent event = new TopiaEntityEvent(context, entity, state, oldState, dirtyProperties);
            for (Iterator<TopiaEntityListener> l = entityListeners.iterator(entity
                    .getClass()); l.hasNext(); ) {
                try {
                    l.next().update(event);
                } catch (Exception eee) {
                    if (log.isErrorEnabled()) {
                        log.error("Can't fireOnPostUpdate for entity: " + entity,
                                eee);
                    }
                }
            }
        }
    }

    public void fireOnPreDelete(TopiaContextImplementor context,
                                TopiaEntity entity, Object[] state) {
        if (log.isDebugEnabled()) {
            log.debug("fireOnPreDelete");
        }
        if (isNotEmpty(entityVetoables, entity.getClass())) {
            TopiaEntityEvent event = new TopiaEntityEvent(context, entity, state);
            for (Iterator<TopiaEntityVetoable> l = entityVetoables.iterator(entity
                    .getClass()); l.hasNext(); ) {
                try {
                    l.next().delete(event);
                } catch (Exception eee) {
                    throw new TopiaVetoException(eee);
                }
            }
        }
    }

    public void fireOnPostDelete(TopiaContextImplementor context,
                                 TopiaEntity entity, Object[] state) {
        if (log.isDebugEnabled()) {
            log.debug("fireOnPostDelete");
        }
        warnOnDeleteEntity(entity);
        if (isNotEmpty(entityListeners, entity.getClass())) {
            TopiaEntityEvent event = new TopiaEntityEvent(context, entity, state);
            for (Iterator<TopiaEntityListener> l = entityListeners.iterator(entity
                    .getClass()); l.hasNext(); ) {
                try {
                    l.next().delete(event);
                } catch (Exception eee) {
                    if (log.isErrorEnabled()) {
                        log.error("Can't fireOnPostDelete for entity: " + entity,
                                eee);
                    }
                }
            }
        }
    }

    /* Fires sur les propriétés */

    public void fireOnPreRead(VetoableChangeSupport vetoables,
                              TopiaEntity entity,
                              String propertyName,
                              Object value) {

        if (log.isDebugEnabled()) {
            log.debug("fireOnPreRead");
        }
        try {
            vetoables.fireVetoableChange(propertyName, value, NO_CHANGE);
        } catch (Exception eee) {
            throw new TopiaVetoException(eee);
        }
    }

    public void fireOnPostRead(PropertyChangeSupport listeners,
                               TopiaEntity entity, String propertyName,
                               Object value) {

        if (log.isDebugEnabled()) {
            log.debug("fireOnPostRead");
        }
        warnOnReadEntity(entity);
        try {
            listeners.firePropertyChange(propertyName, value, NO_CHANGE);
        } catch (Exception eee) {
            if (log.isErrorEnabled()) {
                log.error("Can't fireOnPostRead", eee);
            }
        }

    }

    public void fireOnPostRead(PropertyChangeSupport listeners,
                               TopiaEntity entity,
                               String propertyName,
                               int index,
                               Object value) {

        if (log.isDebugEnabled()) {
            log.debug("fireOnPostRead");
        }
        warnOnReadEntity(entity);
        try {
            listeners.fireIndexedPropertyChange(propertyName, index, value,
                    NO_CHANGE);
        } catch (Exception eee) {
            if (log.isErrorEnabled()) {
                log.error("Can't fireOnPostRead", eee);
            }
        }

    }

    public void fireOnPreWrite(VetoableChangeSupport vetoables,
                               TopiaEntity entity,
                               String propertyName,
                               Object oldValue,
                               Object newValue) {

        if (log.isDebugEnabled()) {
            log.debug("fireOnPreWrite");
        }
        try {
            vetoables.fireVetoableChange(propertyName, oldValue, newValue);
        } catch (Exception eee) {
            throw new TopiaVetoException(eee);
        }
    }

    public void fireOnPostWrite(PropertyChangeSupport listeners,
                                TopiaEntity entity,
                                String propertyName,
                                Object oldValue,
                                Object newValue) {

        if (log.isDebugEnabled()) {
            log.debug("fireOnPostWrite");
        }
        warnOnUpdateEntity(entity);
        if (propertyChangeListeners.size() > 0) {
            PropertyChangeEvent e = new PropertyChangeEvent(entity,
                    propertyName, oldValue, newValue);
            for (PropertyChangeListener l : propertyChangeListeners) {
                try {
                    l.propertyChange(e);
                } catch (Exception eee) {
                    if (log.isErrorEnabled()) {
                        log.error("Can't fire property change for: "
                                + propertyName, eee);
                    }
                }
            }
        }
        try {
            listeners.firePropertyChange(propertyName, oldValue, newValue);
        } catch (Exception eee) {
            if (log.isErrorEnabled()) {
                log.error("Can't fireOnPostWrite: " + propertyName, eee);
            }
        }
    }

    public void fireOnPostWrite(PropertyChangeSupport listeners,
                                TopiaEntity entity,
                                String propertyName,
                                int index,
                                Object oldValue,
                                Object newValue) {

        if (log.isDebugEnabled()) {
            log.debug("fireOnPostWrite");
        }
        warnOnUpdateEntity(entity);
        try {
            listeners.fireIndexedPropertyChange(propertyName, index, oldValue,
                    newValue);
        } catch (Exception eee) {
            if (log.isErrorEnabled()) {
                log.error("Can't fireOnPostWrite", eee);
            }
        }
    }

    /**
     * Object permettant de faire le lien entre les events hibernate et topia
     *
     * @author poussin &lt;poussin@codelutin.com&gt;
     */
    static public class TopiaHibernateEvent implements PreInsertEventListener,
            PostInsertEventListener, PreLoadEventListener,
            PostLoadEventListener, PreUpdateEventListener,
            PostUpdateEventListener, PreDeleteEventListener,
            PostDeleteEventListener {

        private static final long serialVersionUID = 7303593133642169218L;

        protected TopiaContextImplementor rootContext;

        public TopiaHibernateEvent(TopiaContextImplementor rootContext) {
            this.rootContext = rootContext;
        }

        // Hibernate 4.3.x
        @Override
        public boolean requiresPostCommitHanding(EntityPersister persister) {
            // TODO AThimel 17/12/13 I don't know what to return
            return false;
        }

        /**
         * Recherche le context utilisant la session hibernate passe en
         * parametre
         *
         * @param parent    le context parent
         * @param hibernate la session hibernate que doit utiliser le
         *                  TopiaContext pour etre retourne
         * @return le TopiaContext utilisant cette session hibernate ou null si
         *         aucun TopiaContext n'utilise cette session.
         */
        protected TopiaContextImplementor getContext(
                TopiaContextImplementor parent, Session hibernate) {
            TopiaContextImplementor result = null;

            // FD-20100421 : Ano #546 : no need to copy childContext, the
            // {@link #getChildContext()} provides a thread-safe copy to iterate
            // on it.
//            Set<TopiaContextImplementor> contextChilds = new HashSet<TopiaContextImplementor>(parent.getChildContext());
            for (TopiaContextImplementor context : parent.getChildContext()) {

// by sletellier 24/09/09 : Fix concurent acces error
//            ArrayList<TopiaContextImplementor> children = new ArrayList(parent.getChildContext());
//            for (TopiaContextImplementor context : children) {
                try {
                    if (context.getHibernate() == hibernate) {
                        result = context;
                    } else {
                        // TODO: poussin 20090706 on pourrait ameliorer en ne faisant pas un parcours recursif, en utilisant la liste children (sans doute a transformer en stack)
                        result = getContext(context, hibernate);
                    }
                    if (result != null) {
                        break;
                    }
                } catch (TopiaException eee) {
                    if (log.isWarnEnabled()) {
                        log.warn("Error durant la recherche d'un context pour"
                                + " lancer un event", eee);
                    }
                }
            }
            return result;
        }

        private void attachContext(Object entity,
                                   TopiaContextImplementor context) {
            if (entity instanceof TopiaEntityAbstract) {
                TopiaEntityAbstract entityAbstract = (TopiaEntityAbstract) entity;
                if (entityAbstract.getTopiaContext() == null) {
                    try {
                        entityAbstract.setTopiaContext(context);
                    } catch (TopiaException eee) {
                        if (log.isWarnEnabled()) {
                            log.warn("Impossible d'initialiser le TopiaContext"
                                    + " sur cette entité : " + entityAbstract,
                                    eee);
                        }
                    }
                }
            }
        }

        /* Création */

        @Override
        public boolean onPreInsert(PreInsertEvent event) {
            TopiaContextImplementor context = getContext(rootContext, event
                    .getSession());
            if (context != null && event.getEntity() instanceof TopiaEntity) {
                context.getFiresSupport().fireOnPreCreate(context,
                        (TopiaEntity) event.getEntity(), event.getState());
            }
            return false;
        }

        @Override
        public void onPostInsert(PostInsertEvent event) {
            TopiaContextImplementor context = getContext(rootContext, event
                    .getSession());
            if (context != null && event.getEntity() instanceof TopiaEntity) {
                context.getFiresSupport().fireOnPostCreate(context,
                        (TopiaEntity) event.getEntity(), event.getState());
            }
        }

        /* Chargement */

        @Override
        public void onPreLoad(PreLoadEvent event) {
            TopiaContextImplementor context = getContext(rootContext, event
                    .getSession());
            if (context != null && event.getEntity() instanceof TopiaEntity) {
                //                try {
                context.getFiresSupport().fireOnPreLoad(context,
                        (TopiaEntity) event.getEntity(), event.getState());
                //TODO (thimel 20071213) On commente pour le moment @see(TopiaDAOHibernate#filterElements)
                //                } catch (TopiaVetoException tve) {
                //                    //On ne fait pas de remontee d'exception
                // vers Hibernate pour le preLoad, on va agir au niveau du DAO
                //                }
            }
        }

        @Override
        public void onPostLoad(PostLoadEvent event) {
            TopiaContextImplementor context = getContext(rootContext, event
                    .getSession());
            if (context != null && event.getEntity() instanceof TopiaEntity) {
                attachContext(event.getEntity(), context);
                context.getFiresSupport().fireOnPostLoad(context,
                        (TopiaEntity) event.getEntity(), new Object[]{});
            }
        }

        /* Modification */

        @Override
        public boolean onPreUpdate(PreUpdateEvent event) {
            TopiaContextImplementor context = getContext(rootContext, event
                    .getSession());
            if (context != null && event.getEntity() instanceof TopiaEntity) {
                context.getFiresSupport().fireOnPreUpdate(context,
                        (TopiaEntity) event.getEntity(), event.getOldState(), event.getOldState());
            }
            return false;
        }

        @Override
        public void onPostUpdate(PostUpdateEvent event) {
            TopiaContextImplementor context = getContext(rootContext, event
                    .getSession());
            if (context != null && event.getEntity() instanceof TopiaEntity) {
                context.getFiresSupport().fireOnPostUpdate(context,
                        (TopiaEntity) event.getEntity(), event.getState(), event.getOldState(), event.getDirtyProperties());
            }
            // FIXME indexation
            // if (!(entity instanceof NotIndexable) && context.isIndexEnabled()) {
            // context.getIndexEnginImplementor().recordForIndexation(id, event.getState());
            //                }
        }

        /* Suppression */

        @Override
        public boolean onPreDelete(PreDeleteEvent event) {
            TopiaContextImplementor context = getContext(rootContext, event
                    .getSession());
            if (context != null && event.getEntity() instanceof TopiaEntity) {
                context.getFiresSupport().fireOnPreDelete(context,
                        (TopiaEntity) event.getEntity(),
                        event.getDeletedState());
            }
            return false;
        }

        @Override
        public void onPostDelete(PostDeleteEvent event) {
            TopiaContextImplementor context = getContext(rootContext, event
                    .getSession());
            if (context != null && event.getEntity() instanceof TopiaEntity) {
                context.getFiresSupport().fireOnPostDelete(context,
                        (TopiaEntity) event.getEntity(),
                        event.getDeletedState());
            }
//             FIXME indexation
//             if (!(entity instanceof NotIndexable) && context.isIndexEnabled()) {
//             context.getIndexEnginImplementor().recordForIndexation(id, null);
//                            }
        }
    }

    /**
     * Notify topia context listeners for create schema pre operation
     *
     * @param context topia context
     */
    public void firePreCreateSchema(TopiaContext context) {
        if (log.isDebugEnabled()) {
            log.debug("firePreCreateSchema");
        }
        if (isNotEmpty(topiaContextListeners)) {
            TopiaContextEvent event = new TopiaContextEvent(context);
            for (TopiaContextListener topiaContextListener : topiaContextListeners) {
                try {
                    topiaContextListener.preCreateSchema(event);
                } catch (Exception eee) {
                    throw new TopiaVetoException(eee);
                }
            }
        }
    }

    /**
     * Notify topia context listeners for create schema post operation
     *
     * @param context topia context
     */
    public void firePostCreateSchema(TopiaContext context) {
        if (log.isDebugEnabled()) {
            log.debug("firePostCreateSchema");
        }
        if (isNotEmpty(topiaContextListeners)) {
            TopiaContextEvent event = new TopiaContextEvent(context);
            for (TopiaContextListener topiaContextListener : topiaContextListeners) {
                try {
                    topiaContextListener.postCreateSchema(event);
                } catch (Exception eee) {
                    throw new TopiaVetoException(eee);
                }
            }
        }
    }

    /**
     * Notify topia context listeners for create schema pre operation
     *
     * @param context topia context
     */
    public void firePreUpdateSchema(TopiaContext context) {
        if (log.isDebugEnabled()) {
            log.debug("firePostCreateSchema");
        }
        if (isNotEmpty(topiaContextListeners)) {
            TopiaContextEvent event = new TopiaContextEvent(context);
            for (TopiaContextListener topiaContextListener : topiaContextListeners) {
                try {
                    topiaContextListener.preUpdateSchema(event);
                } catch (Exception eee) {
                    throw new TopiaVetoException(eee);
                }
            }
        }
    }

    /**
     * Notify topia context listeners for create schema post operation
     *
     * @param context topia context
     */
    public void firePostUpdateSchema(TopiaContext context) {
        if (log.isDebugEnabled()) {
            log.debug("firePostCreateSchema");
        }
        if (isNotEmpty(topiaContextListeners)) {
            TopiaContextEvent event = new TopiaContextEvent(context);
            for (TopiaContextListener topiaContextListener : topiaContextListeners) {
                try {
                    topiaContextListener.postUpdateSchema(event);
                } catch (Exception eee) {
                    throw new TopiaVetoException(eee);
                }
            }
        }
    }

    /**
     * Notify topia context listeners for schema restore pre operation
     *
     * @param context topia context
     */
    public void firePreRestoreSchema(TopiaContext context) {
        if (log.isDebugEnabled()) {
            log.debug("firePreRestoreSchema");
        }
        if (isNotEmpty(topiaContextListeners)) {
            TopiaContextEvent event = new TopiaContextEvent(context);
            for (TopiaContextListener topiaContextListener : topiaContextListeners) {
                try {
                    topiaContextListener.preRestoreSchema(event);
                } catch (Exception eee) {
                    throw new TopiaVetoException(eee);
                }
            }
        }
    }

    /**
     * Notify topia context listeners for schema restore post operation
     *
     * @param context topia context
     */
    public void firePostRestoreSchema(TopiaContext context) {
        if (log.isDebugEnabled()) {
            log.debug("firePostRestoreSchema");
        }
        if (isNotEmpty(topiaContextListeners)) {
            TopiaContextEvent event = new TopiaContextEvent(context);
            for (TopiaContextListener topiaContextListener : topiaContextListeners) {
                try {
                    topiaContextListener.postRestoreSchema(event);
                } catch (Exception eee) {
                    throw new TopiaVetoException(eee);
                }
            }
        }
    }

    /**
     * Notify entities listeners for load operation
     *
     * @param <E>      type of entities
     * @param context  context used
     * @param entities entities loaded
     * @return the list of entities loaded
     */
    public <E extends TopiaEntity> List<E> fireEntitiesLoad(
            TopiaContextImplementor context, List<E> entities) {
        if (log.isDebugEnabled()) {
            log.debug("fireEntitiesLoad");
        }

        List<E> result = entities;
        for (TopiaEntitiesVetoable entitiesVetoable : entitiesVetoables) {
            try {
                //FIXME tchemit 20100513 Why instanciate n events? if necessary MUST add a comment
                TopiaEntitiesEvent<E> event = new TopiaEntitiesEvent<E>(context, result);
                result = entitiesVetoable.load(event);
            } catch (Exception eee) {
                throw new TopiaVetoException(eee);
            }
        }
        return result;
    }

    /* Getters */

    public CategorisedListenerSet<TopiaEntityListener> getEntityListeners() {
        return entityListeners;
    }

    public CategorisedListenerSet<TopiaEntityVetoable> getEntityVetoables() {
        return entityVetoables;
    }

    public ListenerSet<TopiaTransactionListener> getTransactionListeners() {
        return transactionListeners;
    }

    public ListenerSet<TopiaTransactionVetoable> getTransactionVetoable() {
        return transactionVetoables;
    }

    public ListenerSet<TopiaContextListener> getTopiaContextListeners() {
        return topiaContextListeners;
    }

    public ListenerSet<TopiaEntitiesVetoable> getTopiaEntitiesVetoable() {
        return entitiesVetoables;
    }

    /* Adders */

    public void addTopiaEntityListener(TopiaEntityListener listener) {
        addTopiaEntityListener(TopiaEntity.class, listener);
    }

    public void addTopiaEntityListener(
            Class<? extends TopiaEntity> entityClass,
            TopiaEntityListener listener) {
        if (listener == null) {
            throw new NullPointerException("listener can not be null.");
        }
        entityListeners.add(entityClass, listener);
    }

    public void addTopiaEntityVetoable(TopiaEntityVetoable vetoable) {
        addTopiaEntityVetoable(TopiaEntity.class, vetoable);
    }

    public void addTopiaEntityVetoable(
            Class<? extends TopiaEntity> entityClass,
            TopiaEntityVetoable vetoable) {
        if (vetoable == null) {
            throw new NullPointerException("listener can not be null.");
        }
        entityVetoables.add(entityClass, vetoable);
    }

    public void addTopiaTransactionListener(TopiaTransactionListener listener) {
        if (listener == null) {
            throw new NullPointerException("listener can not be null.");
        }
        transactionListeners.add(listener);
    }

    public void addTopiaTransactionVetoable(TopiaTransactionVetoable vetoable) {
        if (vetoable== null) {
            throw new NullPointerException("listener can not be null.");
        }
        transactionVetoables.add(vetoable);
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        if (listener == null) {
            throw new NullPointerException("listener can not be null.");
        }
        propertyChangeListeners.add(listener);
    }

    public void addTopiaContextListener(TopiaContextListener listener) {
        if (listener == null) {
            throw new NullPointerException("listener can not be null.");
        }
        topiaContextListeners.add(listener);
    }

    public void addTopiaEntitiesVetoable(TopiaEntitiesVetoable vetoable) {
        if (vetoable == null) {
            throw new NullPointerException("listener can not be null.");
        }
        entitiesVetoables.add(vetoable);
    }

    /* Removers */

    public void removeTopiaEntityListener(TopiaEntityListener listener) {
        removeTopiaEntityListener(TopiaEntity.class, listener);
    }

    public void removeTopiaEntityListener(
            Class<? extends TopiaEntity> entityClass,
            TopiaEntityListener listener) {
        if (listener == null) {
            throw new NullPointerException("listener can not be null.");
        }
        entityListeners.remove(entityClass, listener);
    }

    public void removeTopiaEntityVetoable(TopiaEntityVetoable vetoable) {
        removeTopiaEntityVetoable(TopiaEntity.class, vetoable);
    }

    public void removeTopiaEntityVetoable(
            Class<? extends TopiaEntity> entityClass,
            TopiaEntityVetoable vetoable) {
        if (vetoable == null) {
            throw new NullPointerException("listener can not be null.");
        }
        entityVetoables.remove(entityClass, vetoable);
    }

    public void removeTopiaTransactionListener(TopiaTransactionListener listener) {
        if (listener == null) {
            throw new NullPointerException("listener can not be null.");
        }
        transactionListeners.remove(listener);
    }

    public void removeTopiaTransactionVetoable(TopiaTransactionVetoable vetoable) {
        if (vetoable == null) {
            throw new NullPointerException("listener can not be null.");
        }
        transactionVetoables.remove(vetoable);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        if (listener == null) {
            throw new NullPointerException("listener can not be null.");
        }
        propertyChangeListeners.remove(listener);
    }

    public void removeTopiaContextListener(TopiaContextListener listener) {
        if (listener == null) {
            throw new NullPointerException("listener can not be null.");
        }
        topiaContextListeners.remove(listener);
    }

    public void removeTopiaEntitiesVetoable(TopiaEntitiesVetoable vetoable) {
        if (vetoable == null) {
            throw new NullPointerException("listener can not be null.");
        }
        entitiesVetoables.remove(vetoable);
    }

}
