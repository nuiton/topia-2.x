/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.topia.framework;

import java.beans.PropertyChangeListener;

/**
 * Created: 3 juin 2010
 *
 * @author fdesbois &lt;fdesbois@codelutin.com&gt;
 * @version $Id$
 */
public interface EntityFilter {

    String PROPERTY_START_INDEX = "startIndex";

    String PROPERTY_END_INDEX = "endIndex";

    String PROPERTY_ORDER_BY = "orderBy";

    String PROPERTY_REFERENCE_ID = "referenceId";

    String PROPERTY_REFERENCE_PROPERTY = "referenceProperty";

    /**
     * Get the value of startIndex
     *
     * @return the value of startIndex
     */
    Integer getStartIndex();

    /**
     * Set the value of startIndex
     *
     * @param startIndex new value of startIndex
     */
    void setStartIndex(Integer startIndex);

    /**
     * Get the value of orderBy
     *
     * @return the value of orderBy
     */
    String getOrderBy();

    /**
     * Set the value of orderBy
     *
     * @param orderBy new value of orderBy
     */
    void setOrderBy(String orderBy);

    /**
     * Get the value of endIndex
     *
     * @return the value of endIndex
     */
    Integer getEndIndex();

    /**
     * Set the value of endIndex
     *
     * @param endIndex new value of endIndex
     */
    void setEndIndex(Integer endIndex);

    /**
     * Get the value of referenceId
     *
     * @return the value of referenceId
     */
    String getReferenceId();

    /**
     * Set the value of referenceId
     *
     * @param referenceId
     */
    void setReferenceId(String referenceId);

    /**
     * Set the value of referenceId from {@code entity}
     *
     * @param entity
     */
    void setReference(Object entity) throws IllegalArgumentException;

    /**
     * Used to check if the filter contains a reference.
     *
     * @return true if the filter contains a reference
     */
    boolean hasReference();

    /**
     * Use to check if {@code reference} class is supported by the current
     * filter reference. The reference can be not {@code mandatory}. Exceptions
     * are thrown if the check failed. If you prefer to have a boolean instead
     * of exceptions, you can use {@link #isClassReference(Class)}.
     *
     * @param reference Class reference to check
     * @param mandatory If the existence of the reference is mandatory
     * @throws IllegalArgumentException for errors on check
     * @see #hasReference()
     * @see #isClassReference(Class)
     */
    void checkReference(Class<?> reference, boolean mandatory)
            throws IllegalArgumentException;

    /**
     * Test if the {@code entityClass} is corresponding to the current reference
     * in the filter. Will return false if no reference is set in the filter.
     *
     * @param entityClass Class reference to test
     * @return true if the classReference is corresponding, false otherwise
     * @see #checkReference(Class, boolean)
     */
    boolean isClassReference(Class<?> entityClass);

    /**
     * Get the value of referenceProperty
     *
     * @return the value of referenceProperty
     */
    String getReferenceProperty();

    /**
     * Set the value of referenceProperty
     *
     * @param referenceProperty
     */
    void setReferenceProperty(String referenceProperty);

    /**
     * Add PropertyChangeListener.
     *
     * @param listener
     */
    void addPropertyChangeListener(PropertyChangeListener listener);

    /**
     * Remove PropertyChangeListener.
     *
     * @param listener
     */
    void removePropertyChangeListener(PropertyChangeListener listener);

}
