/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.topia.framework;

import org.nuiton.topia.TopiaContext;

/**
 * Use this contract on a object which use a {@code TopiaContext} as a
 * transaction.
 * 
 * The method {@link #getTransaction()} returns the internal transaction used.
 * 
 * the method {@link #setTransaction(TopiaContext)} put the internal
 * transaction.
 *
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @since 2.6.1
 */
public interface TopiaTransactionAware {

    /**
     * Obtains the internal transaction.
     * 
     * If no transaction was opened, can return the {@code null} object.
     *
     * @return the current transaction (can be null or closed...).
     */
    TopiaContext getTransaction();

    /**
     * Put in the instance, the given transaction.
     *
     * @param transaction the transaction to push
     */
    void setTransaction(TopiaContext transaction);
}
