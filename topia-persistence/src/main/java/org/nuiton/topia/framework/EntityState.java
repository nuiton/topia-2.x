/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.topia.framework;

/**
 * Used to know the state of entity during transaction.
 *
 * @author poussin &lt;poussin@codelutin.com&gt;
 */
public class EntityState implements Comparable<EntityState>{

    final static int NULL = 0;

    final static int LOAD = 1;

    final static int READ = 2;

    final static int CREATE = 4;

    final static int UPDATE = 8;

    final static int DELETE = 16;

    /** internal representation of all states of entity (use bitwise operation). */
    protected int state = NULL;

    /**
     * Add load state.
     * 
     * After the invocation, method {@link #isLoad()} will always return {@code true}.
     */
    public void addLoad() {
        state |= LOAD;
    }

    /**
     * Add read state
     * 
     * After the invocation, method {@link #isRead()} will always return {@code true}.
     */
    public void addRead() {
        state |= READ;
    }

    /**
     * Add create state.
     * 
     * After the invocation, method {@link #isCreate()} will always return {@code true}.
     */
    public void addCreate() {
        state |= CREATE;
    }

    /**
     * Add update state.
     * 
     * After the invocation, method {@link #isUpdate()} will always return {@code true}.
     */
    public void addUpdate() {
        state |= UPDATE;
    }

    /**
     * Add delete state.
     * 
     * After the invocation, method {@link #isDelete()} will always return {@code true}.
     */
    public void addDelete() {
        state |= DELETE;
    }

    /**
     * Tells if the {@code #LOAD} state is on.
     *
     * @return {@code true} if {@code #LOAD} state is on, {@code false} otherwise.
     */
    public boolean isLoad() {
        return (state & LOAD) == LOAD;
    }

    /**
     * Tells if the {@code #READ} state is on.
     *
     * @return {@code true} if {@code #READ} state is on, {@code false} otherwise.
     */
    public boolean isRead() {
        return (state & READ) == READ;
    }

    /**
     * Tells if the {@code #CREATE} state is on.
     *
     * @return {@code true} if {@code #CREATE} state is on, {@code false} otherwise.
     */
    public boolean isCreate() {
        return (state & CREATE) == CREATE;
    }

    /**
     * Tells if the {@code #UPDATE} state is on.
     *
     * @return {@code true} if {@code #UPDATE} state is on, {@code false} otherwise.
     */
    public boolean isUpdate() {
        return (state & UPDATE) == UPDATE;
    }

    /**
     * Tells if the {@code #DELETE} state is on.
     *
     * @return {@code true} if {@code #DELETE} state is on, {@code false} otherwise.
     */
    public boolean isDelete() {
        return (state & DELETE) == DELETE;
    }

    @Override
    public int compareTo(EntityState o) {
        return state - o.state;
    }
}
