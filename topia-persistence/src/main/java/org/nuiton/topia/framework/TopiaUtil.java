/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2022 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.topia.framework;

import java.io.Closeable;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.regex.Pattern;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.model.relational.SqlStringGenerationContext;
import org.hibernate.boot.model.relational.internal.SqlStringGenerationContextImpl;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.boot.spi.SessionFactoryOptions;
import org.hibernate.engine.config.spi.ConfigurationService;
import org.hibernate.engine.jdbc.connections.spi.ConnectionProvider;
import org.hibernate.engine.jdbc.spi.JdbcServices;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.mapping.PersistentClass;
import org.hibernate.mapping.Table;
import org.hibernate.resource.transaction.spi.DdlTransactionIsolator;
import org.hibernate.service.Service;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.spi.ServiceRegistryImplementor;
import org.hibernate.service.spi.SessionFactoryServiceRegistry;
import org.hibernate.service.spi.Stoppable;
import org.hibernate.tool.schema.extract.internal.DatabaseInformationImpl;
import org.hibernate.tool.schema.extract.spi.DatabaseInformation;
import org.hibernate.tool.schema.extract.spi.TableInformation;
import org.hibernate.tool.schema.internal.ExceptionHandlerLoggedImpl;
import org.hibernate.tool.schema.internal.Helper;
import org.hibernate.tool.schema.internal.HibernateSchemaManagementTool;
import org.hibernate.tool.schema.internal.exec.JdbcContext;
import org.hibernate.tool.schema.spi.ExecutionOptions;
import org.hibernate.tool.schema.spi.SchemaManagementTool;
import org.hibernate.tool.schema.spi.SchemaManagementToolCoordinator;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaContextFactory;
import org.nuiton.topia.TopiaNotFoundException;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.util.RecursiveProperties;
import org.nuiton.util.Resource;

import com.google.common.base.Supplier;
import com.google.common.util.concurrent.Runnables;

/**
 * TODO-fdesbois-20100507 : Need javadoc + translations for existing methods.
 *
 * @author bpoussin &lt;poussin@codelutin.com&gt;
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @version $Id$
 */
public class TopiaUtil {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(TopiaUtil.class);

    /** @deprecated since 2.5.3, use now the constant {@link TopiaContextFactory#CONFIG_DEFAULT_SCHEMA}. */
    @Deprecated
    public final static String HIBERNATE_DEFAULT_SCHEMA =
            TopiaContextFactory.CONFIG_DEFAULT_SCHEMA;

    /** @deprecated since 2.5.3, use the already present {@link TopiaContextImpl#TOPIA_PERSISTENCE_CLASSES}. */
    @Deprecated
    public final static String TOPIA_PERSISTENCE_CLASSES = TopiaContextImpl.TOPIA_PERSISTENCE_CLASSES;

    /**
     * Permet de récupérer le fichier de propriété ayant le nom passé en
     * argument.
     *
     * @param pathOrUrl le nom du fichier de propriété à charger, s'il est null
     *                  ou vide retourne un objet Properties vide.
     * @return Un nouvel objet de propriete
     * @throws TopiaNotFoundException Si pathOrUrl n'est pas null ou vide et que
     *                                le fichier devant contenir les propriétés
     *                                n'est pas retrouvé.
     */
    static public Properties getProperties(String pathOrUrl)
            throws TopiaNotFoundException {
        return getProperties(null, pathOrUrl);
    }

    /**
     * Permet de récupérer le fichier de propriété ayant le nom passé en
     * argument.
     *
     * @param parent    l'objet properties utilisé comme parent de l'objet
     *                  retourné
     * @param pathOrUrl le nom du fichier de propriété à charger, s'il est null
     *                  ou vide retourne un objet Properties vide.
     * @return Un nouvel objet de propriete
     * @throws TopiaNotFoundException Si pathOrUrl n'est pas null ou vide et que
     *                                le fichier devant contenir les propriétés
     *                                n'est pas retrouvé.
     */
    static public Properties getProperties(Properties parent, String pathOrUrl)
            throws TopiaNotFoundException {
        Properties result = new RecursiveProperties(parent);

        // load properties for helper
        if (pathOrUrl != null && !pathOrUrl.equals("")) {
            try {
                URL propURL = Resource.getURL(pathOrUrl);
                log.info("Properties file used for " + pathOrUrl + " is: " + propURL);
                result.load(propURL.openStream());
            } catch (Exception eee) {
                throw new TopiaNotFoundException(
                        "Properties file can't be found: " + pathOrUrl, eee);
            }
        }
        return result;
    }

    /**
     * Compute a regex pattern given a format string.
     * 
     * A {@link String#format(String, Object...)} will be apply to
     * <code>format</code>, with for parameters the list of <code>klass</code>
     * transformed in topia pattern via method {@link #getTopiaIdPattern(Class)}
     * ready to be capture (enclosed by ()).
     *
     * @param format  the format
     * @param classes the list of class to use
     * @return the pattern computed
     */
    public static Pattern getTopiaPattern(String format,
                                          Class<? extends TopiaEntity>... classes) {
        String[] entityPatterns = new String[classes.length];
        for (int i = 0; i < classes.length; i++) {
            Class<? extends TopiaEntity> aClass = classes[i];
            entityPatterns[i] = "(" + getTopiaIdPattern(aClass) + ")";
        }
        String s = String.format(format, (Object[]) entityPatterns);
        if (log.isDebugEnabled()) {
            log.debug(s);
        }
        return Pattern.compile(s);
    }

    /**
     * Compute the pattern to be used to capture a topia id for a given entity
     * class.
     *
     * @param klass the entity class
     * @return the pattern to capture a topia id for the given entity class.
     */
    public static String getTopiaIdPattern(Class<? extends TopiaEntity> klass) {
        StringBuilder buffer = new StringBuilder();
        StringTokenizer stk = new StringTokenizer(klass.getName(), ".");
        while (stk.hasMoreTokens()) {
            buffer.append("\\.").append(stk.nextToken());
        }
        buffer.append("#(?:\\d+?)#(?:\\d+)\\.(?:\\d+)");
        return buffer.substring(2);
    }

    /**
     * Test si une entite donnee correspondant a une configuration existe en
     * base.
     *
     * @param tx         la session topia
     * @param metadata   mapping des entités
     * @param entityName le nom de l'entite a tester
     * @return {@code true} si le schema de la table existe
     * @since 2.6.4
     */
    public static boolean isSchemaExist(TopiaContext tx,
                                        Metadata metadata,
                                        String entityName) {

        TopiaContextImplementor txi = (TopiaContextImplementor) tx;

        ConnectionProviderSupplier connectionProviderSupplier = null;

        boolean exist = false;

        try {
            connectionProviderSupplier = new ConnectionProviderSupplier(txi);

            PersistentClass classMapping = metadata.getEntityBinding(entityName);
            if (classMapping == null) {
                if (log.isInfoEnabled()) {
                    Collection<PersistentClass> classes = metadata.getEntityBindings();
                    for (PersistentClass clazz : classes) {
                        log.info("available mapping " + clazz);
                    }
                }
                throw new IllegalArgumentException(
                        "could not find entity with name " + entityName);
            }
            Table testTable = classMapping.getTable();

            if (testTable == null) {
                throw new IllegalArgumentException(
                        "could not find entity with name " + entityName);
            }

            ConnectionProvider connectionProvider = connectionProviderSupplier.get();
            ServiceRegistry serviceRegistry = txi.getServiceRegistry();

            Connection connection = null;
            try {
                connection = connectionProvider.getConnection();

                Map config = serviceRegistry.getService( ConfigurationService.class ).getSettings();
                ExecutionOptions executionOptions = SchemaManagementToolCoordinator.buildExecutionOptions(
                        config,
                        ExceptionHandlerLoggedImpl.INSTANCE
                );
                HibernateSchemaManagementTool tool = (HibernateSchemaManagementTool)serviceRegistry.getService( SchemaManagementTool.class );
                JdbcServices jdbcServices = serviceRegistry.getService( JdbcServices.class );
                JdbcContext jdbcContext = tool.resolveJdbcContext( executionOptions.getConfigurationValues() );
                DdlTransactionIsolator ddlTransactionIsolator = tool.getDdlTransactionIsolator( jdbcContext );
                SqlStringGenerationContext sqlStringGenerationContext = SqlStringGenerationContextImpl.fromConfigurationMap(
                        jdbcServices.getJdbcEnvironment(), metadata.getDatabase(), config);

                final DatabaseInformation databaseInformation = Helper.buildDatabaseInformation(
                        tool.getServiceRegistry(),
                        ddlTransactionIsolator,
                        sqlStringGenerationContext,
                        tool
                );

                TableInformation tmd = databaseInformation.getTableInformation(testTable.getQualifiedTableName());

                if (tmd != null) {
                    //table exist
                    exist = true;
                }
            } finally {
                if (connection != null) {
                    connection.close();
                }
            }

        } catch (SQLException e) {
            log.error("Cant connect to database", e);
        } catch (TopiaNotFoundException e) {
            log.error("Cant connect to database", e);
        } finally {
            if (connectionProviderSupplier != null) {
                try {
                    connectionProviderSupplier.close();
                } catch (IOException e) {
                    log.error("Cant close connection provider", e);
                }
            }
        }

        return exist;
    }

    /**
     * Test si une entite donnee correspondant a une configuration existe en
     * base.
     *
     * @param tx            le context hibernate
     * @param entityName    le nom de l'entite a tester
     * @return {@code true} si le schema de la table existe
     */
    public static boolean isSchemaExist(TopiaContext tx,
                                        String entityName) {

        TopiaContextImplementor txi = (TopiaContextImplementor) tx;
        boolean exist = isSchemaExist(txi, txi.getMetadata(), entityName);
        return exist;
    }

    /**
     * Test if the db associated to the given {@code configuration} contaisn any of
     * the dealed entities.
     *
     * @param tx topia context
     * @return {@code true} if there is no schema for any of the dealed entities,
     *         {@code false} otherwise.
     * @since 2.5.3
     */
    public static boolean isSchemaEmpty(TopiaContext tx) {

        TopiaContextImplementor txi = (TopiaContextImplementor) tx;
        ConnectionProviderSupplier connectionProviderSupplier = new ConnectionProviderSupplier(txi);

        try {

            ConnectionProvider connectionProvider = connectionProviderSupplier.get();

            Connection connection = null;
            try {
                connection = connectionProvider.getConnection();

                Metadata metadata = txi.getMetadata();

                ServiceRegistry serviceRegistry = txi.getServiceRegistry();

                Map config = serviceRegistry.getService( ConfigurationService.class ).getSettings();
                final ExecutionOptions executionOptions = SchemaManagementToolCoordinator.buildExecutionOptions(
                        config,
                        ExceptionHandlerLoggedImpl.INSTANCE
                );
                HibernateSchemaManagementTool tool = (HibernateSchemaManagementTool)serviceRegistry.getService( SchemaManagementTool.class );
                JdbcServices jdbcServices = serviceRegistry.getService( JdbcServices.class );
                JdbcContext jdbcContext = tool.resolveJdbcContext( executionOptions.getConfigurationValues() );
                DdlTransactionIsolator ddlTransactionIsolator = tool.getDdlTransactionIsolator( jdbcContext );
                SqlStringGenerationContext sqlStringGenerationContext = SqlStringGenerationContextImpl.fromConfigurationMap(
                        jdbcServices.getJdbcEnvironment(), metadata.getDatabase(), config);

                final DatabaseInformation databaseInformation = Helper.buildDatabaseInformation(
                        tool.getServiceRegistry(),
                        ddlTransactionIsolator,
                        sqlStringGenerationContext,
                        tool
                );

                Collection<PersistentClass> classes = metadata.getEntityBindings();
                for (PersistentClass classMapping : classes) {
                    Table testTable = classMapping.getTable();

                    if (testTable == null) {
                        throw new IllegalArgumentException(
                                "could not find entity with name " +
                                classMapping.getClassName());
                    }


                    TableInformation tmd = databaseInformation.getTableInformation(testTable.getQualifiedTableName());

                    if (tmd != null) {
                        //table exist


                        if (log.isDebugEnabled()) {
                            log.debug("Existing table found " +
                                      testTable.getName() + " for entity " +
                                      classMapping.getClassName() +
                                      ", db is not empty.");
                        }

                        return false;
                    }
                }

            } finally {
                if (connection != null) {
                    connection.close();
                }
            }

        } catch (SQLException e) {
            log.error("Cant connect to database", e);
        } finally {
            try {
                connectionProviderSupplier.close();
            } catch (IOException e) {
                log.error("Cant close connection provider", e);
            }
        }

        return true;
    }

    /**
     * Method to extract from the given Hibernate SessionFactory a working instance of StandardServiceRegistry
     * 
     * IMPORTANT : As much as possible, prefer using the
     * {@link #getSessionFactoryServiceRegistry(org.hibernate.SessionFactory)} mthod instead of the current one because
     * the SessionFactoryServiceRegistry is a child of the StandardServiceRegistry
     * 
     * NB: This method is static to make sure it does not depend on the current instance
     *
     * @param sessionFactory the Hibernate's SessionFactory instance
     * @return the StandardServiceRegistry instance used by the given SessionFactory
     */
    protected static StandardServiceRegistry getStandardServiceRegistry(SessionFactory sessionFactory) {

        // AThimel 03/04/14 The next two lines are the good way to get the StandardServiceRegistry in Hibernate 4.3
        SessionFactoryOptions sessionFactoryOptions = sessionFactory.getSessionFactoryOptions();
        StandardServiceRegistry result = sessionFactoryOptions.getServiceRegistry();

        return result;
    }

    /**
     * Method to extract from the given Hibernate SessionFactory a working instance of SessionFactoryServiceRegistry
     * 
     * IMPORTANT : If possible, prefer using this method instead of
     * {@link #getStandardServiceRegistry(org.hibernate.SessionFactory)} because the SessionFactoryServiceRegistry is a
     * child of the StandardServiceRegistry
     * 
     * NB: This method is static to make sure it does not depend on the current instance
     *
     * @param sessionFactory the Hibernate's SessionFactory instance
     * @return the SessionFactoryServiceRegistry instance used by the given SessionFactory
     */
    protected static SessionFactoryServiceRegistry getSessionFactoryServiceRegistry(SessionFactory sessionFactory) {

        // AThimel 03/04/14 The next two lines are the good way to get the SessionFactoryServiceRegistry in Hibernate 4.3
        SessionFactoryImplementor sessionFactoryImplementor = (SessionFactoryImplementor) sessionFactory;
        SessionFactoryServiceRegistry result = (SessionFactoryServiceRegistry)sessionFactoryImplementor.getServiceRegistry();

        return result;
    }

    /**
     * Method to get an Hibernate service instance from a given Hibernate SessionFactory
     * 
     * NB: This method is static to make sure it does not depend on the current instance
     *
     * @param sessionFactory the Hibernate's SessionFactory instance
     * @param serviceClass   the expected service class
     * @return the found service instance
     * @throws org.hibernate.service.UnknownServiceException Indicates the service was not known.
     * @see org.hibernate.service.ServiceRegistry#getService(Class)
     */
    public static <S extends Service> S getHibernateService(SessionFactory sessionFactory, Class<S> serviceClass) {

        // Hibernate 4.3.x : prefer using the SessionFactoryServiceRegistry method instead of StandardServiceRegistry
        // because SessionFactoryServiceRegistry is a child of the StandardServiceRegistry
        ServiceRegistry serviceRegistry = getSessionFactoryServiceRegistry(sessionFactory);

        S result = serviceRegistry.getService(serviceClass);
        return result;
    }

    /**
     * Hibernate 4.3.x compatible Supplier&lt;ConnectionProvider&gt;. The provider will choose the best way to find the
     * ConnectionProvider depending on the way is has been created.
     */
    public static class ConnectionProviderSupplier implements Supplier<ConnectionProvider>, Closeable {

        /**
         * StandardServiceRegistry will be used if no SessionFactory is provided
         */
        protected StandardServiceRegistry standardServiceRegistry;

        protected ConnectionProvider connectionProvider;

        protected boolean embeddedRegistry;

        public ConnectionProviderSupplier(TopiaContextImplementor topiaContextImplementor) throws TopiaNotFoundException {
            this(topiaContextImplementor.getConfig());
            embeddedRegistry = false;
        }

        public ConnectionProviderSupplier(Properties properties) {
            StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder();
            builder.applySettings(properties);
            this.standardServiceRegistry = builder.build();
            embeddedRegistry = true;
        }

        @Override
        public ConnectionProvider get() {
            if (connectionProvider == null) {
                    // otherwise use the StandardServiceRegistry
                    connectionProvider = standardServiceRegistry.getService(ConnectionProvider.class);
            }
            return connectionProvider;
        }

        @Override
        public void close() throws IOException {
            // Do not close the SessionFactory, it is probably used somewhere else
            if (connectionProvider != null && connectionProvider instanceof Stoppable) {
                Stoppable connectionProviderStoppable = (Stoppable)connectionProvider;
                connectionProviderStoppable.stop();
            }
            // On the over hand, if standardServiceRegistry is provided, that means the its has been created explicitly
            // for the current instance, close it
            if (standardServiceRegistry != null && embeddedRegistry) {
                StandardServiceRegistryBuilder.destroy(standardServiceRegistry);
            }
        }
    }

    /**
     * Return hibernate schema name
     *
     * @param tx topia context
     * @return schema name
     */
    public static String getSchemaName(TopiaContext tx) {
        TopiaContextImplementor txi = (TopiaContextImplementor) tx;
        return txi.getConfig().getProperty(TopiaContextFactory.CONFIG_DEFAULT_SCHEMA);
    }
}
