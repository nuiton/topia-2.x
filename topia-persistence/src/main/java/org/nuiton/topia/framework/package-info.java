/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
/**
 * Contains two interfaces, one {@link org.nuiton.topia.framework.TopiaContextImplementor} for internal
 * manipulations of {@link org.nuiton.topia.TopiaContext} and one {@link org.nuiton.topia.framework.TopiaService} for topia
 * services. This package contains also the main implementation {@link org.nuiton.topia.framework.TopiaContextImpl}
 * for both {@link org.nuiton.topia.TopiaContext} for final applications and
 * {@link org.nuiton.topia.framework.TopiaContextImplementor} for internal.
 * 
 * You can also use {@link org.nuiton.topia.framework.TopiaQuery} class for query manipulation with
 * {@link org.nuiton.topia.persistence.TopiaDAO}.
 * 
 * {@link org.nuiton.topia.framework.TopiaUtil} is a helper class used for TopiaContext manipulations.
 *
 * @author fdesbois &lt;fdesbois@codelutin.com&gt;
 * @version $Id$
 * @see org.nuiton.topia.TopiaContext
 * @see org.nuiton.topia.persistence.TopiaDAO
 */
package org.nuiton.topia.framework;
