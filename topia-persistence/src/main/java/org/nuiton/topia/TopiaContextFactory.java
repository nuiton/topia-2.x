/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.topia;

import org.apache.commons.collections4.map.AbstractReferenceMap.ReferenceStrength;
import org.apache.commons.collections4.map.ReferenceMap;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.cfg.Environment;
import org.nuiton.topia.framework.TopiaContextImpl;
import org.nuiton.topia.framework.TopiaUtil;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * TODO-FD20100507 : Need javadoc + translate the one on methods.
 * 
 * Created: 3 janv. 2006 21:19:37
 *
 * @author poussin &lt;poussin@codelutin.com&gt;
 * @author tchemit &lt;tchemit@codelutin.com&gt;
 * @version $Id$
 */
public class TopiaContextFactory {

    private static final Log log = LogFactory.getLog(TopiaContextFactory.class);

    private static final String DEFAULT_CONFIG_PROPERTIES =
            "TopiaContextImpl.properties";

    /** Cache contenant tous les contexts deja créé. */
    protected static Map<Properties, TopiaContext> contextCache =
            new ReferenceMap(ReferenceStrength.HARD, ReferenceStrength.SOFT);

    public final static String CONFIG_DEFAULT_SCHEMA = Environment.DEFAULT_SCHEMA;

    public final static String CONFIG_USER = Environment.USER;

    public final static String CONFIG_PASS = Environment.PASS;

    public final static String CONFIG_DRIVER = Environment.DRIVER;

    public final static String CONFIG_DIALECT = Environment.DIALECT;

    public final static String CONFIG_CONNECTION_PROVIDER = Environment.CONNECTION_PROVIDER;

    public final static String CONFIG_BYTECODE_PROVIDER = Environment.BYTECODE_PROVIDER;

    public final static String CONFIG_CURRENT_SESSION_CONTEXT_CLASS = Environment.CURRENT_SESSION_CONTEXT_CLASS;

    public final static String CONFIG_GENERATE_STATISTICS = Environment.GENERATE_STATISTICS;

    public final static String CONFIG_FORMAT_SQL = Environment.FORMAT_SQL;

    public final static String CONFIG_HBM2DDL_AUTO = Environment.HBM2DDL_AUTO;

    public final static String CONFIG_POOL_SIZE = Environment.POOL_SIZE;

    public final static String CONFIG_SHOW_SQL = Environment.SHOW_SQL;

    public final static String CONFIG_URL = Environment.URL;

    public final static String CONFIG_PERSISTENCE_DIRECTORIES =
            "topia.persistence.directories";

    public final static String CONFIG_PERSISTENCE_CLASSES =
            "topia.persistence.classes";

    public final static String CONFIG_PERSISTENCE_PROPERTIES_FILE =
            "topia.persistence.properties.file";

    /**
     * Permet de connaitre la liste des contexts encore en memoire, utile pour
     * du debuggage.
     *
     * @return la liste des urls de connexion
     */
    public static List<String> getContextOpened() {
        List<String> result = new ArrayList<String>();
        for (Properties e : contextCache.keySet()) {
            // Useless test : will never happened that e.getValue() is null,
            // not allowed for {@link AbstractReferenceMap#SOFT}.
//            if (e.getValue() != null) {
                result.add(e.getProperty(CONFIG_URL));
//            }
        }
        return result;
    }

    /**
     * Used when TopiaContext root is closed
     *
     * @param context closed
     */
    public static void removeContext(TopiaContext context) {
//        Properties key = null;
//        for (Entry<Properties, TopiaContextImpl> e : contextCache.entrySet()) {
//            if (e.getValue() == context) {
//                key = e.getKey();
//                break;
//            }
//        }
//        if (key != null) {
//            contextCache.remove(key);
//        }
        
        // Replaced by more powerful algorithm using iterator to remove context
        
        Iterator<TopiaContext> it = contextCache.values().iterator();
        
        while (it.hasNext()) {
            TopiaContext curr = it.next();
            if (curr == context) {
                it.remove();
                break;
            }
        }
    }

    /**
     * Utilise par defaut le fichier de propriete TopiaContextImpl.properties
     *
     * @return the context using the default configuration file
     * @throws TopiaNotFoundException Si le fichier de configuration par defaut
     *                                n'est pas retrouvé.
     */
    public static TopiaContext getContext() throws TopiaNotFoundException {
        Properties config = TopiaUtil.getProperties(DEFAULT_CONFIG_PROPERTIES);
        TopiaContext result = getContext(config);
        return result;
    }

    /**
     * Methode static permettant de recuperer un context. Si on donne plusieurs
     * fois le meme objet config, on obtient la meme instance de
     * TopiaContextImpl. Si le context qui devrait etre retourné est ferme,
     * alors un nouveau est creer et retourné.
     *
     * @param config the configuration of the context
     * @return Un TopiaContext ouvert
     * @throws TopiaNotFoundException if any pb
     */
    public static TopiaContext getContext(Properties config)
            throws TopiaNotFoundException {
        // Put all properties from a hierarchy in the current properties object.
        // Resolve problem with hibernate which used iterator to get properties
        // and so only values from the current properties object and not all
        // hierarchy
        Properties cloned = new Properties();
        for (String key : config.stringPropertyNames()) {
            cloned.setProperty(key, config.getProperty(key));
        }
        TopiaContext result = contextCache.get(cloned);
        // useless test, context is automatically removed from Factory when closed
        if (result == null/* || result.isClosed()*/) {
            result = new TopiaContextImpl(cloned);
            if (log.isDebugEnabled()) {
                log.debug("instantiate new topiaContext : " + result);
            }
            contextCache.put(cloned, result);
        } else if (log.isDebugEnabled()) {
            log.debug("topiaContext found : " + result);
        }
        return result;
    }

}
