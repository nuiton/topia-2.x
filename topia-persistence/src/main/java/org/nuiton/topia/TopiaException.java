/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.topia;

/**
 * Main exception for Topia errors on hibernate manipulations.
 *
 * Created: 23 déc. 2005 23:03:36
 *
 * @author poussin &lt;poussin@codelutin.com&gt;
 * @version $Id$
 */
public class TopiaException extends RuntimeException {

    /** Version UID */
    private static final long serialVersionUID = -1251439453383121393L;

    /**
     * Default constructor. 
     */
    public TopiaException() {
    }

    /**
     * Constructor with {@code message}.
     *
     * @param message exception message
     */
    public TopiaException(String message) {
        super(message);
    }

    /**
     * Constructor for a wrapped TopiaException over a {@code cause}
     * with a {@code message}.
     *
     * @param message exception message
     * @param cause exception cause
     */
    public TopiaException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Constructor for a wrapped TopiaException over a {@code cause}.
     *
     * @param cause exception cause
     */
    public TopiaException(Throwable cause) {
        super(cause);
    }

}
