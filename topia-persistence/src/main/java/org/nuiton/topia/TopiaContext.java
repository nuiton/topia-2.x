/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.topia;

import org.nuiton.topia.event.TopiaContextListener;
import org.nuiton.topia.event.TopiaEntitiesVetoable;
import org.nuiton.topia.event.TopiaEntityListener;
import org.nuiton.topia.event.TopiaEntityVetoable;
import org.nuiton.topia.event.TopiaTransactionListener;
import org.nuiton.topia.event.TopiaTransactionVetoable;
import org.nuiton.topia.framework.TopiaQuery;
import org.nuiton.topia.framework.TopiaService;
import org.nuiton.topia.persistence.TopiaEntity;

import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.Reader;
import java.io.Writer;
import java.util.List;

/**
 * TODO-FD20100507 : Need javadoc + translate the one on methods.
 * 
 * Created: 3 janv. 2006 21:18:34
 *
 * @author poussin &lt;poussin@codelutin.com&gt;
 * @author tchemit &lt;tchemit@codelutin.com&gt;
 * @version $Id$
 */
public interface TopiaContext {

    /* Adders */

    void addTopiaEntityListener(TopiaEntityListener listener);

    void addTopiaEntityListener(
            Class<? extends TopiaEntity> entityClass,
            TopiaEntityListener listener);

    void addTopiaEntityVetoable(TopiaEntityVetoable vetoable);

    void addTopiaEntityVetoable(
            Class<? extends TopiaEntity> entityClass,
            TopiaEntityVetoable vetoable);

    void addTopiaTransactionListener(TopiaTransactionListener listener);

    void addTopiaTransactionVetoable(TopiaTransactionVetoable vetoable);

    void addPropertyChangeListener(PropertyChangeListener listener);

    void addTopiaContextListener(TopiaContextListener listener);

    void addTopiaEntitiesVetoable(TopiaEntitiesVetoable vetoable);

    /* Removers */

    void removeTopiaEntityListener(TopiaEntityListener listener);

    void removeTopiaEntityListener(
            Class<? extends TopiaEntity> entityClass,
            TopiaEntityListener listener);

    void removeTopiaEntityVetoable(TopiaEntityVetoable vetoable);

    void removeTopiaEntityVetoable(
            Class<? extends TopiaEntity> entityClass,
            TopiaEntityVetoable vetoable);

    void removeTopiaTransactionListener(TopiaTransactionListener listener);

    void removeTopiaTransactionVetoable(TopiaTransactionVetoable vetoable);

    void removePropertyChangeListener(PropertyChangeListener listener);

    void removeTopiaContextListener(TopiaContextListener listener);

    void removeTopiaEntitiesVetoable(TopiaEntitiesVetoable vetoable);

    /**
     * Return true if specific service is available.
     *
     * @param <E>              type of service
     * @param interfaceService fqn of the service
     * @return the service
     */
    <E extends TopiaService> boolean serviceEnabled(
            Class<E> interfaceService);

    /**
     * Return the service. This service must be valid with public static final
     * SERVICE_NAME property.
     *
     * @param <E>              type of service
     * @param interfaceService class of the service
     * @return the service
     * @throws TopiaNotFoundException if service can't be retrieved
     */
    <E extends TopiaService> E getService(Class<E> interfaceService)
            throws TopiaNotFoundException;

    /**
     * Permet de créer le schema de la base de données.
     *
     * @throws TopiaException if any exception
     */
    void createSchema() throws TopiaException;

    /**
     * Permet d'afficher les requetes SQL de creation de base.
     *
     * @throws TopiaException if any exception
     */
    void showCreateSchema() throws TopiaException;

    /**
     * Permet de mettre à jour le schema de la base de données.
     *
     * @throws TopiaException if any exception
     */
    void updateSchema() throws TopiaException;

    /**
     * Return a new context containing his own transaction.
     *
     * @return new context with transaction
     * @throws TopiaException if any exception
     */
    TopiaContext beginTransaction() throws TopiaException;

    /**
     * applique les modifications apporté a ce context sur la base de données.
     *
     * @throws TopiaException if any exception
     */
    void commitTransaction() throws TopiaException;

    /**
     * annule les modifications apporté a ce context.
     *
     * @throws TopiaException if any exception
     */
    void rollbackTransaction() throws TopiaException;

    /**
     * Retrieve {@link TopiaEntity} using its unique {@code id}.
     *
     * @param topiaId unique identifier of the entity in all the application.
     * @return the entity found or null if not
     * @throws TopiaException for errors on retrieving the entity
     */
    TopiaEntity findByTopiaId(String topiaId) throws TopiaException;

    /**
     * Retrieve results executing a simple {@code query}. Generally this method
     * is used for complex query where output type is specific (more than one
     * element in the SELECT).
     *
     * @param query TopiaQuery to execute
     * @return a List of results as hibernate give us
     * @throws TopiaException
     * @deprecated since 2.6.12, {@link TopiaQuery} will be removed in version 3.0
     */
    @Deprecated
    List findByQuery(TopiaQuery query) throws TopiaException;

    /**
     * Instantiate a new TopiaQuery.
     *
     * @param entityClass   main entity class for the Query
     * @param alias         alias of the entity in the Query
     * @return a new TopiaQuery
     * @see TopiaQuery
     * @deprecated since 2.6.12, {@link TopiaQuery} will be removed in version 3.0
     */
    @Deprecated
    TopiaQuery createQuery(Class<?> entityClass, String alias);

    /**
     * Permet de faire une requete HQL hibernate directement sur la base.
     *
     * @param hql  la requete a faire
     * @param args les arguments de la requete
     * @return La liste des resultats
     * @throws TopiaException si une erreur survient durant la requete
     */
    List findAll(String hql, Object... args) throws TopiaException;

    /**
     * Permet de faire une requete HQL hibernate directement sur la base en
     * precisant la fenetre des elements a remonter avec les parametres {@code
     * startIndex} et {@code endIndex}.
     *
     * @param hql        la requete a faire
     * @param startIndex la position du premier element a remonter
     * @param endIndex   la position du dernier element a remonter
     * @param args       les arguments de la requete
     * @return La liste des resultats
     * @throws TopiaException si une erreur survient durant la requete
     */
    List find(String hql, int startIndex, int endIndex, Object... args)
            throws TopiaException;

    /**
     * Allow to do some HQL query and return an unique result. If nothing if
     * found by the query, will return null. If more than one result is found,
     * will throw an exception.
     * 
     * WARNING : Depending on the registered service, this method may not
     * support something else than queries on TopiaEntity
     *
     * @param jpaql               the JPA-QL query to execute
     * @param paramNamesAndValues an array of query parameters based on
     *                            [key,value,key,value,...]
     * @return The result instance or null
     * @throws TopiaException for any error during querying or if the the query
     *                        returns more than one result.
     */
    Object findUnique(String jpaql, Object... paramNamesAndValues)
            throws TopiaException;

    /**
     * Execute HQL operation on data (Update, Delete).
     *
     * @param hql  la requete a faire
     * @param args les arguments de la requete
     * @return The number of entities updated or deleted.
     * @throws TopiaException if any exception
     */
    int execute(String hql, Object... args) throws TopiaException;

    /**
     * Permet d'ajouter dans le TopiaContext une TopiaEntity créé par un autre
     * context.
     *
     * @param e l'entity a ajouter
     * @throws TopiaException if any exception
     */
    void add(TopiaEntity e) throws TopiaException;

    /**
     * Permet de dupliquer de ce context vers un context d'une autre base des
     * données sans modification des entites.
     * 
     * <b>Note:</b> Si le parametre <code>entityAndCondition</code> est vide,
     * alors on duplique toutes les entités de la base.
     * 
     * <b>Note 2:</b> Il se peut que la replication simple ne soit pas
     * suffisante (par example si l'on veut repliquer q'une partie d'une
     * entité), on utilisera donc la seconde méthode {@link
     * #replicateEntities(TopiaContext, List)}.
     *
     * @param dstCtxt            le context de la base destination
     * @param entityAndCondition paramètre qui vont par deux, qui represente la
     *                           classe de l'entity a exporter et la condition
     *                           where que doit respecter l'objet pour etre
     *                           exporter (entityClass, condition)
     * @throws TopiaException           si une erreur pendant la duplication
     * @throws IllegalArgumentException si l'un des context n'est pas ouvert, ou
     *                                  si on essaye de dupliquer dans la même
     *                                  base.
     */
    void replicate(TopiaContext dstCtxt, Object... entityAndCondition)
            throws TopiaException, IllegalArgumentException;

    /**
     * Permet de dupliquer une entité du type donné vers un autre context.
     *
     * @param dstCtxt le context de la base destination
     * @param entity  l'entité à répliquer
     * @param <T>     le type des entités à répliquer
     * @throws TopiaException           si une erreur pendant la duplication
     * @throws IllegalArgumentException si l'un des context n'est pas ouvert, ou
     *                                  si on essaye de dupliquer dans la même
     *                                  base.
     */
    <T extends TopiaEntity> void replicateEntity(TopiaContext dstCtxt, T entity)
            throws TopiaException, IllegalArgumentException;

    /**
     * Permet de dupliquer les entités du type donné vers un autre context.
     *
     * @param dstCtxt  le context de la base destination
     * @param entities les entités à répliquer
     * @param <T>      le type des entités à répliquer
     * @throws TopiaException           si une erreur pendant la duplication
     * @throws IllegalArgumentException si l'un des context n'est pas ouvert, ou
     *                                  si on essaye de dupliquer dans la même
     *                                  base.
     */
    <T extends TopiaEntity> void replicateEntities(TopiaContext dstCtxt,
                                                   List<T> entities)
            throws TopiaException, IllegalArgumentException;

    /**
     * Sauve la base de données dans un format natif a la base, la
     * representation n'est pas portable d'une base a l'autre. Cette methode ne
     * doit être utilisé que pour un stockage temporaire utile à une
     * application.
     *
     * @param file     le nom du fichier ou stocker les informations
     * @param compress si vrai compress le fichier avec gzip
     * @throws TopiaException if any exception
     */
    void backup(File file, boolean compress) throws TopiaException;

    /**
     * Supprime toutes les tables et autres elements de la database.
     *
     * @param dropDatabase si vrai alors supprime aussi la base de données si la
     *                     base utilise des fichiers les fichiers seront
     *                     supprimé (ex: h2) ou sera fait sur la base
     *                     (postgresql)
     * @throws TopiaException if any exception
     */
    void clear(boolean dropDatabase) throws TopiaException;

    /**
     * Clear persistence implementation cache.
     * 
     * @since 2.6.13
     */
    void clearCache() throws TopiaException;

    /**
     * l'inverse de la methode {@link #backup(File,boolean)}.
     *
     * @param file le fichier ou prendre les informations, il peut-etre
     *             compressé avec gzip ou non.
     * @throws TopiaException if any exception
     */
    void restore(File file) throws TopiaException;

    /**
     * Ferme le contexte.
     *
     * @throws TopiaException if any exception
     */
    void closeContext() throws TopiaException;

    /**
     * Indique si le contexte a ete ferme.
     *
     * @return {@code true} si le context est ferme, {@code false} autrement
     */
    boolean isClosed();

    /**
     * Execute a given sql code inside this transaction.
     *
     * @param sqlScript the sql script to execute
     * @throws TopiaException if any problem occurs while executing the sql script.
     */
    void executeSQL(String sqlScript) throws TopiaException;

} //TopiaContext
