/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.topia.generator;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.codehaus.plexus.component.annotations.Component;
import org.nuiton.eugene.GeneratorUtil;
import org.nuiton.eugene.Template;
import org.nuiton.eugene.java.ObjectModelTransformerToJava;
import org.nuiton.eugene.models.object.*;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;


/*{generator option: parentheses = false}*/

/*{generator option: writeString = +}*/

/**
 * Created: 20 déc. 2009
 *
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @version $Id$
 * @since 2.3.0
 */
@Component(role = Template.class, hint = "org.nuiton.topia.generator.DTOTransformer")
public class DTOTransformer extends ObjectModelTransformerToJava {

    /**
     * Logger
     */
    private static final Log log = LogFactory.getLog(DTOTransformer.class);

    @Override
    public void transformFromClass(ObjectModelClass clazz) {
        if (!TopiaGeneratorUtil.hasDtoStereotype(clazz)) {
            return;
        }
        String clazzName = clazz.getName();
        ObjectModelClass result;
        result = createClass(clazzName + "DTO", clazz.getPackageName());
        addImport(result, ToStringBuilder.class);
        addImport(result, PropertyChangeListener.class);

        setDocumentation(result, "Implantation DTO pour l'entité " + StringUtils.capitalize(clazzName) + ".");
        String extendClass = "";
        for (ObjectModelClass parent : clazz.getSuperclasses()) {
            extendClass = parent.getQualifiedName() + "DTO";
            // no multi-inheritance in java
            break;
        }
        if (extendClass.length() > 0) {
            setSuperClass(result, extendClass);
        }

        addInterface(result, Serializable.class);
        for (ObjectModelInterface parentInterface : clazz.getInterfaces()) {
            if (TopiaGeneratorUtil.hasDtoStereotype(parentInterface)) {
                addInterface(result, parentInterface.getName() + "DTO");
            } else {
                addInterface(result, parentInterface.getName());
            }
        }

        addAttributes(result, clazz);

        addOperations(result, clazz);
    }

    protected void addAttributes(ObjectModelClass result, ObjectModelClass clazz) {

        String svUID = TopiaGeneratorUtil.findTagValue("dto-serialVersionUID", clazz, model);
        if (StringUtils.isNotEmpty(svUID)) {
            addAttribute(result, "serialVersionUID", long.class, svUID, ObjectModelJavaModifier.FINAL, ObjectModelJavaModifier.PUBLIC, ObjectModelJavaModifier.STATIC);
        }

        addAttribute(result, "p", PropertyChangeSupport.class, null, ObjectModelJavaModifier.PROTECTED);

/*
* Définition des attributs
*/
        ObjectModelAttribute attr2;
        for (ObjectModelAttribute attr : clazz.getAttributes()) {
            ObjectModelAttribute reverse = attr.getReverseAttribute();

            String attributeName;
            String attributeType;
            if (!(attr.isNavigable()
                    || attr.hasAssociationClass())) {
                continue;
            }

            String attrName = attr.getName();
            String attrVisibility = attr.getVisibility();
            String attrType = attr.getType();
            if (!GeneratorUtil.isNMultiplicity(attr)) {
                if (!attr.hasAssociationClass()) {
                    if (isDTO(attrType)) {
                        attrType += "DTO";
                    }
                    attributeType = attrType;
                    attributeName = attrName;
                } else {
                    String assocAttrName = TopiaGeneratorUtil.getAssocAttrName(attr);
                    attributeType = attr.getAssociationClass().getQualifiedName();
                    attributeName = GeneratorUtil.toLowerCaseFirstLetter(assocAttrName);
                }
            } else {
                if (!attr.hasAssociationClass()) {
                    String nMultType;
                    if (attr.isOrdered()) {
                        nMultType = List.class.getName() + "<";
                    } else {
                        nMultType = Collection.class.getName() + "<";
                    }
                    nMultType += attrType;
                    if (isDTO(attrType)) {
                        nMultType += "DTO";
                    }
                    nMultType += ">";

                    attributeType = nMultType;
                    attributeName = attrName;
                } else {
                    String assocAttrName = TopiaGeneratorUtil.getAssocAttrName(attr);
                    String assocClassFQN = attr.getAssociationClass().getQualifiedName();
                    String nMultType;
                    if (attr.isOrdered()) {
                        nMultType = List.class.getName() + "<";
                    } else {
                        nMultType = Collection.class.getName() + "<";
                    }
                    nMultType += assocClassFQN;
                    if (isDTO(attrType)) {
                        nMultType += "DTO";
                    }
                    nMultType += ">";
                    attributeType = nMultType;
                    attributeName = GeneratorUtil.toLowerCaseFirstLetter(assocAttrName);
                }
            }

            attr2 = addAttribute(result, attributeName, attributeType, null, ObjectModelJavaModifier.PROTECTED);

            if (attr2 != null) {
                if (TopiaGeneratorUtil.hasDocumentation(attr)) {
                    setDocumentation(attr2, attr.getDocumentation());
                }
                String annotation = TopiaGeneratorUtil.getAnnotationTagValue(attr);
                if (StringUtils.isNotEmpty(annotation)) {
                    addAnnotation(result, attr2, annotation);
                }
            }
        } /* end for*/

        //Déclaration des attributs d'une classe d'associations
        if (clazz instanceof ObjectModelAssociationClass) {
            ObjectModelAssociationClass assoc = (ObjectModelAssociationClass) clazz;
            for (ObjectModelAttribute attr : assoc.getParticipantsAttributes()) {
                if (attr != null) {
                    String attrName = attr.getName();
                    String attrVisibility = attr.getVisibility();
                    String attrType = attr.getType();
                    if (isDTO(attrType)) {
                        attrType += "DTO";
                    }
                    addAttribute(result, GeneratorUtil.toLowerCaseFirstLetter(attrName), attrType);
                }
            }
        }

    }

    protected void addOperations(ObjectModelClass result, ObjectModelClass clazz) {
        ObjectModelOperation op;
        op = addOperation(result, "addPropertyChangeListener", "void", ObjectModelJavaModifier.PUBLIC);
        addParameter(op, PropertyChangeListener.class, "listener");
        setOperationBody(op, ""
/*{
        p.addPropertyChangeListener(listener);
    }*/
        );

        op = addOperation(result, "addPropertyChangeListener", "void", ObjectModelJavaModifier.PUBLIC);
        addParameter(op, String.class, "propertyName");
        addParameter(op, PropertyChangeListener.class, "listener");
        setOperationBody(op, ""
/*{
        p.addPropertyChangeListener(propertyName, listener);
    }*/
        );

        op = addOperation(result, "removePropertyChangeListener", "void", ObjectModelJavaModifier.PUBLIC);
        addParameter(op, PropertyChangeListener.class, "listener");
        setOperationBody(op, ""
/*{
        p.removePropertyChangeListener(listener);
    }*/
        );

        op = addOperation(result, "removePropertyChangeListener", "void", ObjectModelJavaModifier.PUBLIC);
        addParameter(op, String.class, "propertyName");
        addParameter(op, PropertyChangeListener.class, "listener");
        setOperationBody(op, ""
/*{
        p.removePropertyChangeListener(propertyName, listener);
    }*/
        );
        /*
         * Définition des getteurs et setteurs
         */
        for (ObjectModelAttribute attr : clazz.getAttributes()) {

            ObjectModelAttribute reverse = attr.getReverseAttribute();

//            if (!(attr.isNavigable() || hasUnidirectionalRelationOnAbstractType(reverse, model))) {
            if (!attr.isNavigable()) {
                continue;
            }

            String attrName = attr.getName();
            String attrType = attr.getType();
            String attrTypeDTO = attr.getType();
            if (isDTO(attrType)) {
                attrTypeDTO += "DTO";
            }

            if (!GeneratorUtil.isNMultiplicity(attr)) {
                if (!attr.hasAssociationClass()) {
                    op = addOperation(result, "set" + StringUtils.capitalize(attrName), "void", ObjectModelJavaModifier.PUBLIC);
                    addParameter(op, attrTypeDTO, "value");
                    setOperationBody(op, ""
/*{
        <%=attrTypeDTO%> oldValue = this.<%=attrName%>;
        this.<%=attrName%> = value;
        p.firePropertyChange("<%=attrName%>", oldValue, value);
    }*/
                    );

                    op = addOperation(result, "get" + StringUtils.capitalize(attrName), attrTypeDTO, ObjectModelJavaModifier.PUBLIC);
                    setOperationBody(op, ""
/*{
        return <%=attrName%>;
    }*/
                    );

                } else {
                    String assocAttrName = TopiaGeneratorUtil.getAssocAttrName(attr);
                    String assocClassFQN = attr.getAssociationClass().getQualifiedName();
                    if (log.isTraceEnabled()) {
                        log.trace("assocAttrName: " + assocAttrName);
                    }
                    op = addOperation(result, "set" + StringUtils.capitalize(assocAttrName), "void", ObjectModelJavaModifier.PUBLIC);
                    addParameter(op, assocClassFQN + "DTO", "association");
                    setOperationBody(op, ""
/*{
        <%=assocClassFQN%>DTO oldAssocation = this.<%=GeneratorUtil.toLowerCaseFirstLetter(assocAttrName)%>;
        this.<%=GeneratorUtil.toLowerCaseFirstLetter(assocAttrName)%> = association;
        p.firePropertyChange("<%=attrName%>", oldAssocation, assocation);
    }*/
                    );

                    op = addOperation(result, "get" + StringUtils.capitalize(assocAttrName), assocClassFQN + "DTO", ObjectModelJavaModifier.PUBLIC);
                    setOperationBody(op, ""
/*{
        return <%=GeneratorUtil.toLowerCaseFirstLetter(assocAttrName)%>;
    }*/
                    );
                }
            } else { //NMultiplicity
                if (!attr.hasAssociationClass()) { //Méthodes remplacées par des accesseurs sur les classes d'assoc

                    String nMultType;
                    if (attr.isOrdered()) {
                        nMultType = List.class.getName() + "<" + attrTypeDTO + ">";
                    } else {
                        nMultType = Collection.class.getName() + "<" + attrTypeDTO + ">";
                    }
                    op = addOperation(result, "set" + StringUtils.capitalize(attrName), "void", ObjectModelJavaModifier.PUBLIC);
                    addParameter(op, nMultType, "values");
                    setOperationBody(op, ""
/*{
        <%=nMultType%> oldValues = this.<%=attrName%>;
        this.<%=attrName%> = values;
        p.firePropertyChange("<%=attrName%>", oldValues, values);
    }*/
                    );

                    op = addOperation(result, "addChild" + StringUtils.capitalize(attrName), attrTypeDTO, ObjectModelJavaModifier.PUBLIC);
                    addParameter(op, attrTypeDTO, attrName);
                    StringBuilder buffercode = new StringBuilder();

                    buffercode.append(""
/*{
        this.<%=attrName%>.add(<%=attrName%>);
    }*/
                    );

                    if (reverse != null && reverse.isNavigable()) {
                        String reverseAttrName = reverse.getName();
                        buffercode.append(""
/*{        <%=attrName%>.set<%=StringUtils.capitalize(reverseAttrName)%>(this);
    }*/
                        );
                    }
                    buffercode.append(""
/*{        return <%=attrName%>;
    }*/
                    );
                    setOperationBody(op, buffercode.toString());

                    op = addOperation(result, "removeChild", "void");
                    addParameter(op, attrTypeDTO, attrName);

                    buffercode = new StringBuilder();
                    buffercode.append(""
/*{
        this.<%=attrName%>.remove(<%=attrName%>);
    }*/
                    );

                    if (reverse != null && reverse.isNavigable()) {
                        String reverseAttrName = reverse.getName();
                        buffercode.append(""
/*{ 	<%=attrName%>.set<%=StringUtils.capitalize(reverseAttrName)%>(null);
    }*/
                        );
                    }
                    setOperationBody(op, buffercode.toString());

                } else {
                    String assocAttrName = TopiaGeneratorUtil.getAssocAttrName(attr);
                    String assocClassFQN = attr.getAssociationClass().getQualifiedName();
                    String nMultType;
                    if (attr.isOrdered()) {
                        nMultType = List.class.getName() + "<" + assocClassFQN + "DTO>";
                    } else {
                        nMultType = Collection.class.getName() + "<" + assocClassFQN + "DTO>";
                    }
                    if (log.isTraceEnabled()) {
                        log.trace("assocAttrName: " + assocAttrName);
                    }
                    op = addOperation(result, "set" + StringUtils.capitalize(assocAttrName), "void");
                    addParameter(op, nMultType, "values");
                    setOperationBody(op, ""
/*{
        <%=nMultType%> oldValues = this.<%=GeneratorUtil.toLowerCaseFirstLetter(assocAttrName)%>;
        this.<%=GeneratorUtil.toLowerCaseFirstLetter(assocAttrName)%> = values;
        p.firePropertyChange("<%=attrName%>", oldValues, values);
    }*/
                    );
                }
                if (!attr.hasAssociationClass()) {
                    String nMultType;
                    if (attr.isOrdered()) {
                        nMultType = List.class.getName() + "<" + attrTypeDTO + ">";
                    } else {
                        nMultType = Collection.class.getName() + "<" + attrTypeDTO + ">";
                    }
                    op = addOperation(result, "get" + StringUtils.capitalize(attrName), nMultType);
                    setOperationBody(op, ""
/*{
        return this.<%=attrName%>;
    }*/
                    );
                } else {
                    String assocAttrName = TopiaGeneratorUtil.getAssocAttrName(attr);
                    String assocClassFQN = attr.getAssociationClass().getQualifiedName();
                    String nMultType;
                    if (attr.isOrdered()) {
                        nMultType = List.class.getName() + "<" + assocClassFQN + "DTO>";
                    } else {
                        nMultType = Collection.class.getName() + "<" + assocClassFQN + "DTO>";
                    }
                    if (log.isTraceEnabled()) {
                        log.trace("assocAttrName: " + assocAttrName);
                    }
                    op = addOperation(result, "get" + StringUtils.capitalize(assocAttrName), nMultType);
                    setOperationBody(op, ""
/*{
        return this.<%=GeneratorUtil.toLowerCaseFirstLetter(assocAttrName)%>;
    }*/
                    );
                }
            }
        }

        op = addOperation(result, "toString", String.class, ObjectModelJavaModifier.PUBLIC);
        StringBuilder buffer = new StringBuilder();

        buffer.append(""
/*{
        String result = new ToStringBuilder(this).
}*/
        );

        for (Object o : clazz.getAttributes()) {
            ObjectModelAttribute attr = (ObjectModelAttribute) o;
            if (!(attr.isNavigable()
                    || attr.hasAssociationClass())) {
                continue;
            }
            //FIXME possibilité de boucles (non directes)
            ObjectModelClass attrEntity = null;
            if (model.hasClass(attr.getType())) {
                attrEntity = model.getClass(attr.getType());
            }
            boolean isDTO = attrEntity != null &&
                            TopiaGeneratorUtil.isEntity(attrEntity); //THIMEL : STEREOTYPE ENTITY ???
            ObjectModelAttribute reverse = attr.getReverseAttribute();
            if (isDTO && (reverse == null || !reverse.isNavigable()) && !attr.hasAssociationClass() || !isDTO) {
                String attrName = attr.getName();
                buffer.append(""
/*{            append("<%=attrName%>", this.<%=attrName%>).
}*/
                );
            }
        }
        buffer.append(""
/*{         toString();
        return result;
    }*/
        );
        setOperationBody(op, buffer.toString());
    }

    public boolean isDTO(String type) {
        ObjectModelClassifier clazz = model.getClassifier(type);
        return clazz != null && TopiaGeneratorUtil.hasDtoStereotype(clazz);
    }


}

