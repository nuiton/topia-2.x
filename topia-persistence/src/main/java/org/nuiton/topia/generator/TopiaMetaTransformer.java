/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.topia.generator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.codehaus.plexus.component.annotations.Component;
import org.nuiton.eugene.AbstractMetaTransformer;
import org.nuiton.eugene.Template;
import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.eugene.models.object.validator.AttributeNamesValidator;
import org.nuiton.eugene.models.object.validator.ClassNamesValidator;
import org.nuiton.eugene.models.object.validator.ObjectModelValidator;

import java.util.ArrayList;
import java.util.List;

/**
 * Created: 20 déc. 2009
 *
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @version $Id$
 * @since 2.3.0
 */
@Component(role = Template.class, hint = "org.nuiton.topia.generator.TopiaMetaTransformer")
public class TopiaMetaTransformer extends AbstractMetaTransformer<ObjectModel> {

    /** Logger */
    private static final Log log = LogFactory.getLog(TopiaMetaTransformer.class);

    public TopiaMetaTransformer() {
//        super(
//                EntityTransformer.class,
//                DAOTransformer.class,
//                DAOImplTransformer.class,
//                DAOAbstractTransformer.class,
//                DAOHelperTransformer.class,
//                EntityHibernateMappingGenerator.class
//        );

        setTemplateTypes(
                EntityTransformer.class,
                EntityDAOTransformer.class,
//                DAOTransformer.class,
//                DAOImplTransformer.class,
//                DAOAbstractTransformer.class,
                DAOHelperTransformer.class,
                EntityHibernateMappingGenerator.class
        );
    }

    protected boolean validateModel(ObjectModel model) {
        List<ObjectModelValidator> validators = new ArrayList<ObjectModelValidator>();

        AttributeNamesValidator attrValidator = new AttributeNamesValidator(
                model);
        attrValidator.addNameAndReason("next",
                                       "Le nom d'attribut \"next\" est incompatible avec HSQL");
        attrValidator.addNameAndReason("value",
                                       "Le nom d'attribut \"value\" est incompatible avec certains SGBD");
        attrValidator.addNameAndReason("values",
                                       "Le nom d'attribut \"values\" est incompatible avec certains SGBD");
        attrValidator.addNameAndReason("begin",
                                       "Le nom d'attribut \"begin\" est incompatible avec certains SGBD");
        attrValidator.addNameAndReason("end",
                                       "Le nom d'attribut \"end\" est incompatible avec certains SGBD");
        attrValidator.addNameAndReason("authorization",
                                       "Le nom d'attribut \"authorization\" est incompatible avec certains SGBD");
        attrValidator.addNameAndReason("order",
                                       "Le nom d'attribut \"order\" est incompatible avec certains SGBD");
        validators.add(attrValidator);

        ClassNamesValidator classValidator = new ClassNamesValidator(model);
        classValidator.addNameAndReason("constraint", "Nom de classe incompatible avec certains SGBD");
        classValidator.addNameAndReason("user", "Nom de classe incompatible avec certains SGBD");
        validators.add(classValidator);

        validators.add(new TopiaJavaValidator(model));
        validators.add(new TopiaRelationValidator(model));

        for (ObjectModelValidator validator : validators) {
            if (!validator.validate()) {
                for (String error : validator.getErrors()) {
                    if (log.isWarnEnabled()) {
                        log.warn("[VALIDATION] " + error);
                    }
                }
            }
        }

        // test before all if there is some entities to generate
        List<ObjectModelClass> classes = TopiaGeneratorUtil.getEntityClasses(model, true);

        if (classes.isEmpty()) {
            // no entity to generate, can stop safely
            if (log.isWarnEnabled()) {
                log.warn("No entity to generate, " + getClass().getName() +
                         " is skipped");
            }
            return false;
        }
        //FIXME-TC20091220 seems we still generate if there is some validation errors ? not very normal
        return true;
    }

}
