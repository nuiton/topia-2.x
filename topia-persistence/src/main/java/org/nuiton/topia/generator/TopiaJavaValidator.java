/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.topia.generator;

import org.apache.commons.lang3.StringUtils;
import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.object.ObjectModelAttribute;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.eugene.models.object.ObjectModelOperation;
import org.nuiton.eugene.models.object.validator.ObjectModelValidator;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Validateur qui valide :
 * - les types des attributs
 * - les nom des attributs
 * - les duplication d'attibuts
 *
 * @author chatellier &lt;chatellier@codelutin.com&gt;
 * @version $Id$
 */
public class TopiaJavaValidator extends ObjectModelValidator {

    /**
     * Constructor.
     *
     * @param model model to validate
     */
    public TopiaJavaValidator(ObjectModel model) {
        super(model);
    }

    @Override
    protected boolean validateAttribute(ObjectModelAttribute attr) {

        boolean isValid = super.validateAttribute(attr);

        // type null ou vide
        if (attr.getType() == null || attr.getType().isEmpty()) {
            isValid = false;

            addError(attr, "Invalid type \"" + attr.getType() + "\"");
        }

        // name = java reserved keywords
        if (!isJavaIdentifier(attr.getName())) {
            isValid = false;

            addError(attr, "Attribute name " + attr.getName()
                           + " is not valid java identifier");
        }

        // test sur les mots réservés ?

        return isValid;

    }

    @Override
    protected boolean validateClass(ObjectModelClass clazz) {

        boolean isValid = super.validateClass(clazz);

        // test attribute names duplication
        Set<String> attributesName = new HashSet<String>();
        for (ObjectModelAttribute attr : clazz.getAttributes()) {
            if (!attr.isNavigable()) {

                // not navigable, so will not use it...
                continue;
            }
            String attrName = attr.getName();
            if (!attributesName.add(attrName)) {
                addError(attr, "Attribute name " + attrName
                               + " already exists");

                isValid = false;
            }
        }

        if (TopiaGeneratorUtil.isEntity(clazz)) {

            Set<String> methodsName = new HashSet<String>();
            for (ObjectModelAttribute attr : clazz.getAttributes()) {

                String capitalizeAttrName = StringUtils.capitalize(attr.getName());
                methodsName.add(TopiaGeneratorUtil.OPERATION_GETTER_BOOLEAN_PREFIX + capitalizeAttrName);
                methodsName.add(TopiaGeneratorUtil.OPERATION_GETTER_DEFAULT_PREFIX + capitalizeAttrName);
                methodsName.add("set" + capitalizeAttrName);
            }

            // test if there is a method an already reserved name
            for (ObjectModelOperation operation : clazz.getOperations()) {
                String operationName = operation.getName();
                if (methodsName.contains(operationName)) {
                    addError(operation, "Operation name " + operationName
                                        + " is already reserved for a getter/setter of an entity attribute");

                    isValid = false;
                }
            }
        }

        return isValid;
    }

    @Override
    protected boolean validateModel(ObjectModel model) {

        return super.validateModel(model);
    }

    /**
     * Returns true if s is a legal Java identifier.
     *
     * @param s string to test
     * @return true if s is a legal Java identifier
     */
    public static boolean isJavaIdentifier(String s) {
        if (s.length() == 0 || !Character.isJavaIdentifierStart(s.charAt(0))) {
            return false;
        }
        for (int i = 1; i < s.length(); i++) {
            if (!Character.isJavaIdentifierPart(s.charAt(i))) {
                return false;
            }
        }
        return true;
    }
}
