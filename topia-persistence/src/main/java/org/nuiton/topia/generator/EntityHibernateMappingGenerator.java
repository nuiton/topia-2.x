/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/*{generator option: parentheses = true}*/
/*{generator option: writeString = output.write}*/

/* *
* EntityHibernateMappingGenerator.java
*
* Created: 12 déc. 2005
*
* @author Arnaud Thimel &lt;thimel@codelutin.com&gt;
* @version $Revision$
*
*/

package org.nuiton.topia.generator;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.codehaus.plexus.component.annotations.Component;
import org.nuiton.eugene.Template;
import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.object.ObjectModelAssociationClass;
import org.nuiton.eugene.models.object.ObjectModelAttribute;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.eugene.models.object.ObjectModelGenerator;

import java.beans.Introspector;
import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import static org.nuiton.topia.generator.TopiaGeneratorUtil.hasUnidirectionalRelationOnAbstractType;

/**
 *
 * @author poussin &lt;poussin@codelutin.com&gt;
 * @version $Id$
 */
@Component(role = Template.class, hint = "org.nuiton.topia.generator.EntityHibernateMappingGenerator")
public class EntityHibernateMappingGenerator extends ObjectModelGenerator {

    /**
     * Logger.
     */
    private static final Log log = LogFactory
            .getLog(EntityHibernateMappingGenerator.class);

    private static final String HIBERNATE_ATTRIBUTE_DEFAULT = "default";

    private static final String HIBERNATE_ATTRIBUTE_SQL_TYPE = "sql-type";

    private static final String HIBERNATE_ATTRIBUTE_NAME = "name";

    private Map<String, String[]> columnNamesMap = new HashMap<String, String[]>();

    public static final String HIBERNATE_ATTRIBUTE_LAZY = "lazy";

    public static final String HIBERNATE_ATTRIBUTE_FETCH = "fetch";

    public static final String HIBERNATE_ATTRIBUTE_NOT_NULL = "not-null";

    public static final String HIBERNATE_ATTRIBUTE_SCHEMA = "schema";

    public static final String HIBERNATE_ATTRIBUTE_INDEX = "index";

    public static final String HIBERNATE_ATTRIBUTE_UNIQUE = "unique";

    public static final String HIBERNATE_ATTRIBUTE_LENGTH = "length";

    public static final String HIBERNATE_ATTRIBUTE_ORDER_BY = "order-by";

    public static final String HIBERNATE_ATTRIBUTE_FOREIGN_KEY = "foreign-key";

    static class ClassContext {

        private final ObjectModel model;

        private final ObjectModelClass input;

        private final boolean generateForeignKeyNames;

        private final String tableName;

        private final String schema;

        ClassContext(ObjectModel model, ObjectModelClass input) {
            this.model = model;
            this.input = input;
            this.generateForeignKeyNames = TopiaGeneratorUtil.isGenerateForeignKeyNames(input, model);
            this.tableName = TopiaGeneratorUtil.getDbName(input);
            this.schema = TopiaGeneratorUtil.getDbSchemaNameTagValue(input, model);
        }

        public boolean isGenerateForeignKeyNames() {
            return generateForeignKeyNames;
        }

        public String getTableName() {
            return tableName;
        }

        public boolean isUseSchema() {
            return schema != null;
        }

        public String getSchema() {
            return schema;
        }

        public String getForeignKeyName(String attrColumn) {
            return getForeignKeyName(tableName , attrColumn).toLowerCase();
        }

        public String getForeignKeyName(String tableName, String attrColumn) {
            return ("fk_" + tableName + "_" + attrColumn).toLowerCase();
        }

        public ObjectModelClass getInput() {
            return input;
        }

    }

    @Override
    public String getFilenameForClass(ObjectModelClass clazz) {
        String DOName = TopiaGeneratorUtil.getDOType(clazz, model);
        return DOName.replace('.', File.separatorChar) + ".hbm.xml";
    }

    @Override
    public void generateFromClass(Writer output,
                                  ObjectModelClass input) throws IOException {
        String persistenceType = TopiaGeneratorUtil.getPersistenceType(input);
        if (!TopiaGeneratorUtil.isEntity(input) &&
            TopiaGeneratorUtil.PERSISTENCE_TYPE_HIBERNATE.equals(persistenceType)) {
            return;
        }
/*{<?xml version="1.0" encoding="UTF-8"?>
<hibernate-mapping xmlns="http://www.hibernate.org/xsd/hibernate-mapping"
    xsi:schemaLocation="http://www.hibernate.org/xsd/hibernate-mapping classpath://org/hibernate/hibernate-mapping-4.0.xsd" 
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    default-access="field" auto-import="true" package="<%=input.getPackageName()%>">
}*/

        ClassContext classContext = new ClassContext(model, input);

        boolean haveSuper = input.getSuperclasses().size() > 0;
        // la liste des attributs faisant parti de la clef metier
        List<ObjectModelAttribute> naturalAttributes = new ArrayList<ObjectModelAttribute>();
        // la liste des autres attributs
        List<ObjectModelAttribute> noneNaturalAttributes = new ArrayList<ObjectModelAttribute>();

        String clazzDOType = TopiaGeneratorUtil.getDOType(input, model);
        String tableName = classContext.getTableName();
        String isAbstract = BooleanUtils.toStringTrueFalse(input.isAbstract());
        String clazzFQN = input.getQualifiedName();

        String optionalAttributes = "";
        if (classContext.isUseSchema()) {
            optionalAttributes += "schema=\"" + classContext.getSchema() + "\" ";
        }

        //On précise au proxy de quelle interface hérite l'objet
        String proxyTagValue = TopiaGeneratorUtil.getProxyInterfaceTagValue(input, model);
        if (StringUtils.isEmpty(proxyTagValue) || !proxyTagValue.equals("none")) {
            optionalAttributes += "proxy=\"" + clazzFQN + "\" ";
        }

        if (!optionalAttributes.isEmpty()) {
            optionalAttributes = " " + optionalAttributes.trim();
        }
        if (haveSuper) {
            ObjectModelClass superClass = input.getSuperclasses().iterator().next();
            String superClassname = superClass.getQualifiedName();
            if (log.isDebugEnabled()) {
            	log.debug("superClass for " + input.getQualifiedName() + " is " + superClassname);
            }
            String superClassDOType = TopiaGeneratorUtil.getDOType(superClassname, model);

/*{    <union-subclass name="<%=clazzDOType%>" extends="<%=superClassDOType%>" table="<%=tableName%>" abstract="<%=isAbstract%>"<%=optionalAttributes%>>
}*/
            // FIXME mieux gerer le cas haveSuper
            noneNaturalAttributes.addAll(input.getAttributes());
        } else {
/*{    <class name="<%=clazzDOType%>" table="<%=tableName%>" abstract="<%=isAbstract%>"<%=optionalAttributes%>>
        <id name="topiaId" type="string" length="255"/>
}*/
            // on detecte les attributs des clef metiers            
            for (ObjectModelAttribute attr : input.getAttributes()) {
                if (TopiaGeneratorUtil.isNaturalId(attr)) {
                    // attribut metier
                    naturalAttributes.add(attr);
                } else {
                    // attribut normal
                    noneNaturalAttributes.add(attr);
                }
            }
            if (!naturalAttributes.isEmpty()) {
                // generation de la clef metier
                boolean mutable = TopiaGeneratorUtil.isNaturalIdMutable(input);
                String mutableStr = mutable ? " mutable=\"true\"" : "";
                if (log.isDebugEnabled()) {
                    log.debug("natural-id detected for class " + input.getName() + " (" + mutableStr + ") attributes : " + naturalAttributes);
                }
/*{        <natural-id<%=mutableStr%>>
}*/
                generateAttributes(output, classContext, naturalAttributes, "    ");
/*{        </natural-id>
}*/
            }
/*{        <version name="topiaVersion" type="long" />
        <property name="topiaCreateDate" type="timestamp" />
}*/
        }

        generateAttributes(output, classContext, noneNaturalAttributes, "");

        if (haveSuper) {
/*{    </union-subclass>
}*/
        } else {
/*{    </class>
}*/
        }

        generateDatabaseObjects(output, classContext, naturalAttributes);
        generateDatabaseObjects(output, classContext, noneNaturalAttributes);

/*{</hibernate-mapping>
}*/
    }

    protected void generateDatabaseObjects(Writer output,
                                           ClassContext classContext,
                                           List<ObjectModelAttribute> attributes) throws IOException {

        for (ObjectModelAttribute attribute : attributes) {
            if (!attribute.isNavigable() ||
                attribute.hasAssociationClass() ||
                !TopiaGeneratorUtil.isNMultiplicity(attribute) ||
                    attribute.getClassifier() == null ||
                    !TopiaGeneratorUtil.isEntity(attribute.getClassifier())
                    ) {

                // skip for this case (not a nm-multiplicity attribute)
                continue;
            }

            String indexForeignKeys =
                    TopiaGeneratorUtil.getIndexForeignKeys(attribute, model);

            if (StringUtils.isEmpty(indexForeignKeys) || !Boolean.valueOf(indexForeignKeys)) {

                // no index to put of the attribute.
                continue;
            }

            // add database-object to create and drop index

            // add schema if exist (http://nuiton.org/issues/2052)
            String schema = classContext.getSchema();
            boolean withSchema = classContext.isUseSchema();
            String tableName;
            String propertyName;


            if (TopiaGeneratorUtil.isNMultiplicity(attribute.getReverseMaxMultiplicity())) {

                // many to many
                tableName = TopiaGeneratorUtil.getManyToManyTableName(attribute);
                //propertyName = TopiaGeneratorUtil.getDbName(attribute.getReverseAttribute());
                // FIX https://forge.nuiton.org/issues/3674
                propertyName = TopiaGeneratorUtil.getReverseDbNameOnReverseAttribute(attribute);
            } else {

                // one to many
                tableName =TopiaGeneratorUtil.getDbName(attribute.getClassifier());
                //propertyName = TopiaGeneratorUtil.getDbName(attribute.getReverseAttribute());
                // FIX https://forge.nuiton.org/issues/3674
                propertyName = TopiaGeneratorUtil.getReverseDbNameOnReverseAttribute(attribute);
            }

            String indexName = "idx";
            if (withSchema) {
                indexName += '_' + schema;
            }
            indexName += '_' + tableName+ '_' + propertyName ;
            indexName = indexName.toLowerCase();

            if (withSchema) {
                tableName = schema + "." + tableName;
            }
/*{    <database-object>
        <create>CREATE INDEX <%=indexName%> ON <%=tableName%>(<%=propertyName%>)</create>
        <drop>DROP INDEX <%=indexName%></drop>
    </database-object>
}*/

        }
    }

    protected void generateAttributes(Writer output,
                                      ClassContext classContext,
                                      List<ObjectModelAttribute> attributes,
                                      String prefix) throws IOException {
        for (ObjectModelAttribute attr : attributes) {
            ObjectModelAttribute reverse = attr.getReverseAttribute();

            // pour les asso quoi qu'il arrive il faut les lier des 2 cotes
            // pour pouvoir supprimer en cascade l'asso lors de la suppression
            // d'un des cotes
            if (attr.isNavigable()
                    || hasUnidirectionalRelationOnAbstractType(reverse, model)
                    || attr.hasAssociationClass()) {
                if (!TopiaGeneratorUtil.isNMultiplicity(attr)) {
                    if (attr.getClassifier() != null && TopiaGeneratorUtil.isEntity(attr.getClassifier())) {
                        if (TopiaGeneratorUtil.isNMultiplicity(attr.getReverseMaxMultiplicity()) && !attr.hasAssociationClass()) {
                            generateHibernateManyToOne(output, classContext, attr, prefix);
                        } else {
                            generateHibernateOneToOne(output, classContext, attr, prefix);
                        }
                    } else {
                        generateHibernateProperty(output, classContext, attr, prefix);
                    }
                } else {
                    if (attr.getClassifier() != null && TopiaGeneratorUtil.isEntity(attr.getClassifier())) {
                        if (TopiaGeneratorUtil.isNMultiplicity(attr.getReverseMaxMultiplicity()) && !attr.hasAssociationClass()) {
                            generateHibernateManyToMany(output, classContext, attr, prefix);
                        } else {
                            generateHibernateOneToMany(output, classContext, attr, prefix);
                        }
                    } else {
                        generateHibernateMany(output, classContext, attr, prefix);
                    }
                }
            }
        }

        //Attributs pour les classes d'association
        ObjectModelClass clazz = classContext.getInput();
        if (clazz instanceof ObjectModelAssociationClass) {
            ObjectModelAssociationClass assoc = (ObjectModelAssociationClass) clazz;
            for (ObjectModelAttribute attr : assoc.getParticipantsAttributes()) {
                if (attr != null) {

// Note(poussin) pour moi quoi qu'il arrive sur la classe d'association il faut
// un many-to-one, sinon on a des problemes.
//                    if ((!attr.getReverseAttribute().isNavigable()) || !Util.isNMultiplicity(attr.getReverseAttribute())) {
// / *{        <one-to-one name="<%=getName(attr, true)%>" class="<%=getType(attr, true)%>"<%=(TopiaGeneratorUtil.notEmpty(attr.getTagValue(TopiaGeneratorUtil.TAG_LENGTH))?(" length=\"" + attr.getTagValue(TopiaGeneratorUtil.TAG_LENGTH) + "\""):"")%><%=(attr.isComposite()?" cascade=\"delete\"":"")%>/>
// } */
//                    } else {
                    String notNull = " " + generateFromTagValue(HIBERNATE_ATTRIBUTE_NOT_NULL, TopiaGeneratorUtil.getNotNullTagValue(attr));
                    String attrName = getName(attr, true);
                    String attrType = getType(attr, true);
                    String lazy = generateFromTagValue(HIBERNATE_ATTRIBUTE_LAZY, TopiaGeneratorUtil.getLazyTagValue(attr));
                    String attrColumn = TopiaGeneratorUtil.getDbName(attr);
                    String foreignKeyName ="";
                    if (classContext.isGenerateForeignKeyNames()) {
                        foreignKeyName = " " + generateFromTagValue(HIBERNATE_ATTRIBUTE_FOREIGN_KEY, classContext.getForeignKeyName(attrColumn)).trim();
                    }
/*{<%=prefix%>        <many-to-one name="<%=attrName%>" class="<%=attrType%>" <%=lazy%>column="<%=attrColumn%>" <%=notNull%><%=foreignKeyName%>/>
}*/
//                    }
                    //Ne sert plus grâce à l'utilisation de la navigabilité
//                    if (!attr.getReverseAttribute().isNavigable()) {
//                        String type = TopiaGeneratorUtil.getDOType(((ObjectModelClassifier)attr.getDeclaringElement()).getQualifiedName(), model);
//                        String name = Util.toLowerCaseFirstLetter(attr.getDeclaringElement().getName());
//                        if (log.isTraceEnabled()) {log.trace("reverse: " + type + " " + name);}
//                        if (!Util.isNMultiplicity(attr)) {
//{<!--        <one-to-one name="<%=name%>" class="<%=type%>"/>
//}
//                        } else {
//{        <many-to-one name="<%=name%>" class="<%=type%>" column="<%=name.toLowerCase()%>"/> -->
//}
//                        }
//                    }
                }
            }
        }
    }

    protected String getName(ObjectModelAttribute attr) {
        return getName(attr, false);
    }

    protected String getName(ObjectModelAttribute attr, boolean isAssoc) {
        String result = Introspector.decapitalize(attr.getName());
        if (attr.hasAssociationClass() && !isAssoc) {
            result = TopiaGeneratorUtil.getAssocAttrName(attr);
        }
        return result;
    }

    protected String getType(ObjectModelAttribute attr) {
        return getType(attr, false);
    }

    protected String getType(ObjectModelAttribute attr, boolean isAssoc) {
        String type = attr.getType();
        String attrType = TopiaGeneratorUtil.getTypeTagValue(attr);
        if (StringUtils.isNotEmpty(attrType)) {

            // tag value detected of the attribute
            type = attrType;
        } else {

            String modelType = model.getTagValue(type);
            if (StringUtils.isNotEmpty(modelType)) {

                // tag value detected of the model

                //TODO tchemit 20100507 Explain What todes it do ? Dont understand the story of columnNamesMap
                int bracketIndex = modelType.indexOf('(');
                if (bracketIndex != -1) {
                    type = modelType.substring(0, bracketIndex);
                    int bracketEndIndex = modelType.indexOf(')', bracketIndex + 1);
                    String colmunList;
                    if (bracketEndIndex != -1) {
                        colmunList = modelType.substring(bracketIndex + 1, bracketEndIndex);
                    } else {
                        colmunList = modelType.substring(bracketIndex);
                    }
                    columnNamesMap.put(type, colmunList.split(","));
                } else {
                    type = modelType;
                }
            }
        }
        if (attr.hasAssociationClass() && !isAssoc) {
            type = attr.getAssociationClass().getQualifiedName();
        }
        return TopiaGeneratorUtil.getDOType(type, model);
    }

    protected void generateHibernateProperty(Writer output,
                                             ClassContext classContext,
                                             ObjectModelAttribute attr,
                                             String prefix) throws IOException {
        String attrType = getType(attr);

        String accessField = "field";
        String tagValue = TopiaGeneratorUtil.getAccessTagValue(attr);
        if (StringUtils.isNotEmpty(tagValue)) {
        	accessField = tagValue;
        }
        String attrName = attr.getName();
        String declaringElementDBName = TopiaGeneratorUtil.getDbName(attr.getDeclaringElement());
        String tableName = declaringElementDBName + "_" + attrName;

        boolean attrIsEnumeration = attr.getClassifier() != null
                                 && attr.getClassifier().isEnum();

        if (attrType.trim().endsWith("[]")) {
            attrType = attrType.trim().substring(0, attrType.trim().length()-2);

            String optionalAttributes = "";
            if (classContext.isUseSchema()) {
                optionalAttributes += generateFromTagValue(HIBERNATE_ATTRIBUTE_SCHEMA, classContext.getSchema());
            }

            if (TopiaGeneratorUtil.hasIndexedCollectionStereotype(attr)) {
            	String indexName = tableName + "_idx";
                optionalAttributes += generateFromTagValue(HIBERNATE_ATTRIBUTE_INDEX, indexName);
            }
            if (!optionalAttributes.isEmpty()) {
                optionalAttributes = " " + optionalAttributes.trim();
            }

/*{<%=prefix%>        <primitive-array name="<%=attrName%>" table="<%=tableName%>" access="<%=accessField%>"<%=optionalAttributes%>>
<%=prefix%>          <key column="<%=declaringElementDBName%>"/>
<%=prefix%>          <list-index column="<%=attrName%>_idx"/>
<%=prefix%>          <element type="<%=attrType%>"/>
<%=prefix%>        </primitive-array>
}*/
        } else {
            String optionalAttributes = "";
            if (TopiaGeneratorUtil.hasIndexedCollectionStereotype(attr)) {
            	String indexName = tableName + "_idx";
                optionalAttributes += generateFromTagValue(HIBERNATE_ATTRIBUTE_INDEX, indexName);
//            	optionalAttributes += "index=\"" + indexName + "\"";
            }

            if (TopiaGeneratorUtil.hasUniqueStereotype(attr)) {
                // the trim method is called on optionalAttributes after this set to suppress unusual space if no index is set on this attribute
                optionalAttributes += generateFromTagValue(HIBERNATE_ATTRIBUTE_UNIQUE, "true");
//                optionalAttributes += " unique=\"true\"";
            }
            optionalAttributes += generateFromTagValue(HIBERNATE_ATTRIBUTE_NOT_NULL, TopiaGeneratorUtil.getNotNullTagValue(attr));
/*{<%=prefix%>        <property name="<%=attrName%>" access="<%=accessField%>"}*/
            if ( ! attrIsEnumeration) {
/*{ type="<%=attrType%>"}*/
            }
            optionalAttributes = optionalAttributes.trim();
            String[] columnNames = columnNamesMap.get(attrType);

            // contains all required attributes for a column node
            Map<String,String> columnAttributes = new TreeMap<String, String>();
            if (StringUtils.isNotEmpty(attr.getDefaultValue())) {
                //TC-20100129 with a default value we must use the column child tag

                String defaultValue = attr.getDefaultValue().trim();
                columnAttributes.put(HIBERNATE_ATTRIBUTE_DEFAULT, defaultValue);
            }
            String sqlType = TopiaGeneratorUtil.getSqlTypeTagValue(attr);
            if (!StringUtils.isEmpty(sqlType)) {

                // an specific sql type was specified for the attribute, use it
                columnAttributes.put(HIBERNATE_ATTRIBUTE_SQL_TYPE, sqlType);
            }

            // add length attribute if required
            String lengthTagValue = TopiaGeneratorUtil.getLengthTagValue(attr);
            if (!StringUtils.isEmpty(lengthTagValue)) {

                optionalAttributes += generateFromTagValue(HIBERNATE_ATTRIBUTE_LENGTH, lengthTagValue);
            }

            optionalAttributes = optionalAttributes.trim();
            if (StringUtils.isNotEmpty(optionalAttributes)) {
                optionalAttributes = " " + optionalAttributes;
            }

            // to know if specific column name mapping is given
            boolean noSpecifiedColumn = columnNames == null || columnNames.length == 0;

            if (noSpecifiedColumn) {

                String attrColumn = TopiaGeneratorUtil.getDbName(attr);

                if (columnAttributes.isEmpty()) {

                    // simple case with no column node to generate

/*{ column="<%=attrColumn%>"<%=optionalAttributes%>}*/
                    if (attrIsEnumeration) {
/*{>
<%=prefix%>            <type name="org.hibernate.type.EnumType">
<%=prefix%>                <param name="<%=org.hibernate.type.EnumType.ENUM%>"><%=attrType%></param>}*/

                        // if the user tuned the model to use name instead of
                        // ordinal to store the values, we must add a clause
                        boolean useEnumerationName = TopiaGeneratorUtil.hasUseEnumerationNameTagValue(attr, model);
                        if (useEnumerationName) {
                            String enumSQLType = String.valueOf(Types.VARCHAR);
/*{
<%=prefix%>                <!-- using name instead of ordinal to store enumeration value -->
<%=prefix%>                <param name="<%=org.hibernate.type.EnumType.TYPE%>"><%=enumSQLType%></param>}*/
                        }

/*{
<%=prefix%>            </type>
<%=prefix%>        </property>
}*/
                    } else {
/*{/>
}*/
                    }
                } else {

                    // there is some attributes to write for the column node

                    columnAttributes.put(HIBERNATE_ATTRIBUTE_NAME, attrColumn);

                    String columnAttributesAsString ="";
                    for (Map.Entry<String, String> entry :
                            columnAttributes.entrySet()) {
                        String name = entry.getKey();
                        String value = entry.getValue();
                        columnAttributesAsString += generateFromTagValue(name, value, null);
                    }
                    columnAttributesAsString = " " + columnAttributesAsString.trim();
/*{<%=optionalAttributes%>>
<%=prefix%>            <column<%=columnAttributesAsString%>/>
<%=prefix%>        </property>
}*/
                }
            } else {

                // there is a colum name mapping specified, must use it
                //FIXME tchemit 2010-12-29 Really don't know how to apply columnAttributes for multi-columns...
/*{<%=optionalAttributes%>>
}*/
                for (String columnName : columnNames) {
                    columnName = attrName + "_" + columnName.trim();
/*{<%=prefix%>            <column name="<%=columnName%>"/>
}*/
                }
/*{<%=prefix%>        </property>
}*/
            }
        }
    }

    protected void generateHibernateOneToOne(Writer output,
                                             ClassContext classContext,
                                             ObjectModelAttribute attr,
                                             String prefix) throws IOException {
//      boolean accessField = hasUnidirectionalRelationOnAbstractType(attr.getReverseAttribute(), model);
/// *{        <one-to-one name="<%=getName(attr)%>" class="<%=getType(attr)%>"<%=(TopiaGeneratorUtil.notEmpty(attr.getTagValue(TopiaGeneratorUtil.TAG_LENGTH))?(" length=\"" + attr.getTagValue(TopiaGeneratorUtil.TAG_LENGTH) + "\""):"")%><%=((attr.isComposite() || attr.hasAssociationClass())?" cascade=\"delete\"":"")%><%=((accessField)?" access=\"field\"":"")%> />
//} */

        // for hibernate many-to-one with unique="true" => one-to-one
        // but if it is one-to-zero-or-one unique contraints is violated
        // with null values
        boolean unique = TopiaGeneratorUtil.isOneMultiplicity(attr);
        generateHibernateManyToOne(output, classContext, attr, unique, prefix);

    }

    protected void generateHibernateOneToMany(Writer output,
                                              ClassContext classContext,
                                              ObjectModelAttribute attr,
                                              String prefix) throws IOException {
        boolean needsIndex = TopiaGeneratorUtil.hasIndexedCollectionStereotype(attr);
        boolean isInverse = attr.getReverseAttribute().isNavigable();
        isInverse |= hasUnidirectionalRelationOnAbstractType(attr, model);

        String attrName = getName(attr); // ???
        String attrType = getType(attr);
        String reverseAttrDBName = TopiaGeneratorUtil.getReverseDbName(attr);
        String orderBy = generateFromTagValue(HIBERNATE_ATTRIBUTE_ORDER_BY, TopiaGeneratorUtil.getOrderByTagValue(attr));

        String cascade = "";
        if (attr.isComposite() || attr.hasAssociationClass()) {
            cascade += "cascade=\"all,delete-orphan\" ";
        }

        String lazy = generateFromTagValue(HIBERNATE_ATTRIBUTE_LAZY, TopiaGeneratorUtil.getLazyTagValue(attr), "true");

        String fetch = generateFromTagValue(HIBERNATE_ATTRIBUTE_FETCH, TopiaGeneratorUtil.getFetchTagValue(attr));

        String collType = TopiaGeneratorUtil.getNMultiplicityHibernateType(attr);
        String inverse = "";
        if (isInverse) {
        	inverse = "inverse=\"true\" ";
        }
        String foreignKeyAttribute = "";
        if (classContext.isGenerateForeignKeyNames()) {
            String columnName = TopiaGeneratorUtil.getDbName(attr);
            foreignKeyAttribute = " " + generateFromTagValue(HIBERNATE_ATTRIBUTE_FOREIGN_KEY, classContext.getForeignKeyName(columnName)).trim();
        }
        if (needsIndex) {
/*{<%=prefix%>        <<%=collType%> name="<%=attrName%>" <%=inverse%><%=lazy%><%=cascade%>>
<%=prefix%>            <key column="<%=reverseAttrDBName%>"<%=foreignKeyAttribute%>/>
<%=prefix%>            <list-index column="<%=reverseAttrDBName%>_idx"/>
<%=prefix%>            <one-to-many class="<%=attrType%>"/>
<%=prefix%>        </<%=collType%>>
}*/
        } else {
/*{<%=prefix%>        <<%=collType%> name="<%=attrName%>" <%=inverse%><%=orderBy%><%=fetch%><%=lazy%><%=cascade%>>
<%=prefix%>            <key column="<%=reverseAttrDBName%>"<%=foreignKeyAttribute%>/>
<%=prefix%>            <one-to-many class="<%=attrType%>" />
<%=prefix%>        </<%=collType%>>
}*/
        }
    }

    private String generateFromTagValue(String attributeName, String tagValue) {
		return generateFromTagValue(attributeName, tagValue, null);
	}

    /**
     * Generate hibernate xml attribute with a final space.
     * @param attributeName
     * @param tagValue
     * @param defaultValue
     * @return
     */
    private String generateFromTagValue(String attributeName, String tagValue, String defaultValue) {
		String result = "";
        if (StringUtils.isNotEmpty(tagValue)) {
            result+= attributeName + "=\"" + tagValue+"\" ";
        } else if (defaultValue != null) {
            result+= attributeName + "=\"" + defaultValue +"\" ";
        }
//		if (attr.hasTagValue(tagName) || defaultValue != null) {
//			result+= attributeName + "=\"";
//			if (attr.hasTagValue(tagName)) {
//				result += attr.getTagValue(tagName);
//			} else {
//				result += defaultValue;
//			}
//			result += "\" ";
//		}
		return result;
	}

    protected void generateHibernateMany(Writer output,
                                         ClassContext classContext,
                                         ObjectModelAttribute attr,
                                         String prefix) throws IOException {
        boolean needsIndex = TopiaGeneratorUtil.hasIndexedCollectionStereotype(attr);
        String attrName = getName(attr);
        String attrType = getType(attr);
        String collType = TopiaGeneratorUtil.getNMultiplicityHibernateType(attr);
        String lazy = generateFromTagValue(HIBERNATE_ATTRIBUTE_LAZY, TopiaGeneratorUtil.getLazyTagValue(attr));
        String attrColumn = TopiaGeneratorUtil.getDbName(attr);
        String foreignKeyAttribute = "";
        if (classContext.isGenerateForeignKeyNames()) {
            foreignKeyAttribute = " " + HIBERNATE_ATTRIBUTE_FOREIGN_KEY + "=\"" + classContext.getTableName() + "_" + attrColumn + "\"";
        }

/*{<%=prefix%>        <<%=collType%> name="<%=attrName%>" <%=lazy%>>
<%=prefix%>            <key column="OWNER"<%=foreignKeyAttribute%>/>
}*/
        if (needsIndex) {
/*{<%=prefix%>            <list-index/>
}*/
        }
/*{<%=prefix%>            <element type="<%=attrType%>" column="<%=attrColumn%>" />
<%=prefix%>        </<%=collType%>>
}*/
    }

    protected void generateHibernateManyToOne(Writer output,
                                              ClassContext classContext,
                                              ObjectModelAttribute attr,
                                              String prefix) throws IOException {
        generateHibernateManyToOne(output, classContext, attr, false, prefix);
    }

    protected void generateHibernateManyToOne(Writer output,
                                              ClassContext classContext,
                                              ObjectModelAttribute attr,
                                              boolean isUnique,
                                              String prefix) throws IOException {
    	String attrName = getName(attr);
    	String attrType = getType(attr);
    	String attrColumn = TopiaGeneratorUtil.getDbName(attr);
/*{<%=prefix%>        <many-to-one name="<%=attrName%>" class="<%=attrType%>" column="<%=attrColumn%>" }*/
        if (attr.isComposite() || attr.hasAssociationClass()) {
/*{cascade="delete" }*/
        }
        if (classContext.isGenerateForeignKeyNames()) {
            String foreignKeyName = generateFromTagValue(HIBERNATE_ATTRIBUTE_FOREIGN_KEY, classContext.getForeignKeyName(attrColumn));
/*{<%=foreignKeyName%>}*/
        }
        // Pour le test suivant, on verifie d'abord que l'attribut a un reverse.
        // S'il n'en a pas, cela signifie qu'il ne s'agit pas d'un entite
        // (au sens stereotype entity), donc a donc pas besoin de faire un access=field.
        if (attr.getReverseAttribute() != null && hasUnidirectionalRelationOnAbstractType(attr.getReverseAttribute(), model)) {
/*{access="field" }*/
        }
        // vérifier si le tag lazy est defini par defaut dans le fichier de proprietes
        String lazy = generateFromTagValue(HIBERNATE_ATTRIBUTE_LAZY, TopiaGeneratorUtil.getLazyTagValue(attr));
/*{<%=lazy%>}*/
        String notNull = generateFromTagValue(HIBERNATE_ATTRIBUTE_NOT_NULL, TopiaGeneratorUtil.getNotNullTagValue(attr));
/*{<%=notNull%>}*/
        if (isUnique) {
/*{unique="true" }*/
        }
/*{/>
}*/
    }

    protected void generateHibernateManyToMany(Writer output,
                                               ClassContext classContext,
                                               ObjectModelAttribute attr,
                                               String prefix) throws IOException {
        // On ne met le inverse="true" uniquement pour un seul coté de la relation.
        // Dans le cas contraire, les modifications dans la relation ne seront
        // pas sauvegardées. Ceci n'est vrai que si les deux coté sont navigable
        boolean isInverse = attr.isNavigable() && attr.getReverseAttribute().isNavigable();
        //isInverse |= !Util.isFirstAttribute(attr);
        //isInverse = false; // 20070117 poussin: pour du many, jamais de inverse

        // Modification FD-2010-04-01 :
        // Le tagvalue "inverse" permet de spécifier qui possède le
        // inverse="true". Il est impératif de l'utiliser sur les deux
        // extrémités pour ne pas avoir de surprise.
        String inverseValue = TopiaGeneratorUtil.getInverseTagValue(attr);
        if (StringUtils.isNotEmpty(inverseValue)) {
            isInverse &= Boolean.parseBoolean(inverseValue);
        // Si aucun tagvalue n'est défini, le choix est arbitraire : le
        // premier attribut dans l'ordre alphabétique sera choisi pour porter le
        // inverse="true"
        } else {
            isInverse &= TopiaGeneratorUtil.isFirstAttribute(attr);
        }

        boolean needsIndex = TopiaGeneratorUtil.hasIndexedCollectionStereotype(attr);
        String cascade = "";
        if (attr.isComposite() || attr.hasAssociationClass()) {
            cascade = " cascade=\"delete,delete-orphan\"";
        }

        String attrType = getType(attr);
        String attrName = getName(attr);
        String attrColumn = TopiaGeneratorUtil.getDbName(attr);
        String lazy = generateFromTagValue(HIBERNATE_ATTRIBUTE_LAZY, TopiaGeneratorUtil.getLazyTagValue(attr), "true");
        String orderBy = generateFromTagValue(HIBERNATE_ATTRIBUTE_ORDER_BY, TopiaGeneratorUtil.getOrderByTagValue(attr));
        String collType = TopiaGeneratorUtil.getNMultiplicityHibernateType(attr);
        String tableName = TopiaGeneratorUtil.getManyToManyTableName(attr);
        String inverse = "";
        if (isInverse) {
        	inverse = "inverse=\"true\" ";
        }
        String reverseAttrDBName = TopiaGeneratorUtil.getReverseDbName(attr);
        String optionalAttributes="";

        if (classContext.isUseSchema()) {
            optionalAttributes += generateFromTagValue(HIBERNATE_ATTRIBUTE_SCHEMA, classContext.getSchema());
        }
        if (!optionalAttributes.isEmpty()) {
            optionalAttributes = " " + optionalAttributes.trim();
        }
        String foreignKeyName = "";
        String reverseForeignKeyName = "";
        if (classContext.isGenerateForeignKeyNames()) {
            foreignKeyName = " " + generateFromTagValue(HIBERNATE_ATTRIBUTE_FOREIGN_KEY, classContext.getForeignKeyName(tableName, reverseAttrDBName)).trim();
            reverseForeignKeyName = " " + generateFromTagValue(HIBERNATE_ATTRIBUTE_FOREIGN_KEY, classContext.getForeignKeyName(tableName, attrColumn)).trim();
        }

/*{<%=prefix%>        <<%=collType%> name="<%=attrName%>" table="<%=tableName%>" <%=inverse%><%=lazy%><%=cascade%><%=optionalAttributes%>>
<%=prefix%>            <key column="<%=reverseAttrDBName%>"<%=foreignKeyName%>/>
}*/
        if (needsIndex) {
/*{<%=prefix%>            <list-index column="<%=reverseAttrDBName%>_idx"/>
}*/
        }
/*{<%=prefix%>            <many-to-many class="<%=attrType%>" column="<%=attrColumn%>" <%=orderBy%><%=reverseForeignKeyName%>/>
<%=prefix%>        </<%=collType%>>
}*/
    }

} //EntityHibernateMappingGenerator
