/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2019 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.topia.generator;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.codehaus.plexus.component.annotations.Component;
import org.nuiton.eugene.EugeneCoreTagValues;
import org.nuiton.eugene.Template;
import org.nuiton.eugene.java.ObjectModelTransformerToJava;
import org.nuiton.eugene.models.object.ObjectModelAssociationClass;
import org.nuiton.eugene.models.object.ObjectModelAttribute;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.eugene.models.object.ObjectModelClassifier;
import org.nuiton.eugene.models.object.ObjectModelInterface;
import org.nuiton.eugene.models.object.ObjectModelJavaModifier;
import org.nuiton.eugene.models.object.ObjectModelModifier;
import org.nuiton.eugene.models.object.ObjectModelOperation;
import org.nuiton.eugene.models.object.ObjectModelParameter;
import org.nuiton.topia.TopiaException;
import org.nuiton.topia.framework.TopiaContextImplementor;
import org.nuiton.topia.persistence.EntityVisitor;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaEntityAbstract;
import org.nuiton.topia.persistence.TopiaEntityContextable;
import org.nuiton.topia.persistence.util.TopiaEntityHelper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import static org.nuiton.topia.generator.TopiaGeneratorUtil.hasUnidirectionalRelationOnAbstractType;

/*{generator option: parentheses = false}*/
/*{generator option: writeString = +}*/

/**
 * A template to generate all the {@link TopiaEntity} api for all classifier
 * with a {@code entity} stereotype.
 *
 * For example, given a {@code House} entity, it will generates :
 * <ul>
 * <li>{@code House} : contract of entity</li>
 * <li>{@code AbstractHouse} : default abstract implementation of entity</li>
 * <li>{@code HouseImpl} : default impl of abstract entity</li>
 * </ul>
 *
 * <b>Note: </b> The impl will ony be generated in these cases :
 * <ul>
 * <li>There is no abstract method</li>
 * <li>There is no already defined such class in class-path</li>
 * </ul>
 *
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @since 2.3.4
 */
@Component(role = Template.class, hint = "org.nuiton.topia.generator.EntityTransformer")
public class EntityTransformer extends ObjectModelTransformerToJava {

    /** Logger */
    private static final Log log = LogFactory.getLog(EntityTransformer.class);

    protected ObjectModelInterface outputInterface;

    protected ObjectModelClass outputAbstract;

    protected ObjectModelClass outputImpl;

    private boolean associationClass;

    protected boolean generateInterface;

    protected boolean generateAbstract;

    protected boolean generateImpl;

    protected boolean generateBooleanGetMethods;

    protected void clean() {
        outputInterface = null;
        outputAbstract = null;
        outputImpl = null;
    }

    @Override
    public void transformFromClass(ObjectModelClass input) {

        if (!TopiaGeneratorUtil.isEntity(input)) {

            // not an entity, skip class.
            return;
        }

        if (log.isDebugEnabled()) {
            log.debug("for entity : " + input.getQualifiedName());
            log.debug("Will use classLoader " + getClassLoader());
        }
        
        // fix once for all the constant prefix to use
        String prefix = getConstantPrefix(input);
        if (StringUtils.isEmpty(prefix)) {

            // no specific prefix, so no prefix
            if (log.isWarnEnabled()) {
                log.warn("[" + input.getName() + "] Will generate constants with NO prefix, not a good idea... \n" +
                         "Use '" + EugeneCoreTagValues.Store.constantPrefix +
                         "' tagvalue in your xmi properties. For example " +
                         "for all the model : model.tagvalue." + EugeneCoreTagValues.Store.constantPrefix + "=PROPERTY_");
            }
        }
        setConstantPrefix(prefix);

        generateInterface = isGenerateInterface(input);
        generateAbstract = isGenerateAbstract(input);
        generateImpl = isGenerateImpl(input);

        generateBooleanGetMethods = eugeneTagValues.isGenerateBooleanGetMethods(input, null, model);

        if (generateInterface) {
            
            // Create Entity Interface and its header
            createEntityInterface(input);
        }

        if (generateAbstract) {

            // Create Entity Abstract class and its header
            createEntityAbstractClass(input);
        }

        // Generate i18n block
        String i18nPrefix = eugeneTagValues.getI18nPrefixTagValue(input, null, model);
        if (!StringUtils.isEmpty(i18nPrefix)) {
            generateI18nBlock(input, outputAbstract, i18nPrefix);
        }

        // Create accept operation, will be updated during property generation
        createAcceptOperation();
        createAcceptInternalOperation(input);

        // Add constant, attribute and operations for each property
        generateProperties(input.getAttributes());

        // Case of association class : properties from participants/extremities
        // of the association class.
        if (input instanceof ObjectModelAssociationClass) {
            ObjectModelAssociationClass association =
                    (ObjectModelAssociationClass)input;
            associationClass = true;
            generateProperties(association.getParticipantsAttributes());
            associationClass = false;
        }

        closeAcceptInternalOperation();

        // Add extra constants (from uml dependency)
        generateExtraConstants(input);

        // Add extra operations (defined on the entity)
        generateExtraOperations(input);

        // Implement aggregate and composite operations
        generateAggregateOperation(input);
        generateCompositeOperation(input);

        // Implement toString operation
        if (TopiaGeneratorUtil.generateToString(input, model)) {
            generateToStringOperation(input);
        }

        // Generate serialVersionUID on abstract class
        generateSerialVersionUID(input, outputAbstract);

        // Generate Entity Implementation class
        if (generateImpl) {
            generateImpl(input);
            generateSerialVersionUID(input, outputImpl);
        }



        // Clean data output after transformation
        clean();
    }

    protected void generateSerialVersionUID(ObjectModelClass input,
                                            ObjectModelClass ouput) {

        // serialVersionUID
        String svUID = TopiaGeneratorUtil.findTagValue(TopiaGeneratorUtil.SERIAL_VERSION_UID,
                                                       input,
                                                       model
        );
        if (svUID == null) {

            // use a default one
            svUID = TopiaGeneratorUtil.generateSerialVersionUID(ouput) + "L";
        }
        addConstant(ouput, TopiaGeneratorUtil.SERIAL_VERSION_UID, long.class, svUID,
                        ObjectModelJavaModifier.PRIVATE);
    }

    protected void createEntityInterface(ObjectModelClass input) {

        outputInterface = createInterface(input.getName(),
                                          input.getPackageName());

        // Documentation
        if (TopiaGeneratorUtil.hasDocumentation(input)) {
            setDocumentation(outputInterface, input.getDocumentation());
        }

        if (log.isTraceEnabled()) {
            log.trace("Will add interfaces on " +
                      outputInterface.getQualifiedName());
        }

        List<String> interfaceAlreadyDone = new LinkedList<String> ();
        // Extends
        for (ObjectModelClassifier parent : input.getInterfaces()) {
            addInterface(interfaceAlreadyDone, outputInterface, parent);
        }

        // Extends from inheritance
        boolean needTopiaEntity = true;
        for (ObjectModelClassifier parent : input.getSuperclasses()) {
            if (TopiaGeneratorUtil.isEntity(parent)) {
                addInterface(interfaceAlreadyDone, outputInterface, parent);
                needTopiaEntity = false;
            }
        }

        // Extends TopiaEntity (only if hasn't parent entity)
        if (needTopiaEntity) {

            Class<?> interfaze = TopiaEntity.class;

            if (TopiaGeneratorUtil.isContextable(input)) {
                interfaze = TopiaEntityContextable.class;
            }
            
            addInterface(interfaceAlreadyDone,
                         outputInterface,
                         interfaze);
        }
    }

    protected void createEntityAbstractClass(ObjectModelClass input) {

        outputAbstract = createAbstractClass(input.getName() + "Abstract",
                input.getPackageName());

        // Documentation
        StringBuilder doc = new StringBuilder();
        doc.append("Implantation POJO pour l'entité {@link ");
        doc.append(StringUtils.capitalize(outputInterface.getName()));
        doc.append("}\n");

        String dbName = TopiaGeneratorUtil.getDbName(input);
        if (dbName != null) {
            doc.append("<p>Nom de l'entité en BD : ");
            doc.append(dbName);
            doc.append(".</p>");
        }

        setDocumentation(outputAbstract, doc.toString());

        // Implements
        addInterface(outputAbstract, outputInterface.getName());

        // Extends
        for (ObjectModelClass parent : input.getSuperclasses()) {
            //tchemit-2011-09-12 What ever abstract or not, we alwyas use an Impl, moreover use the util method instead
            String extendClass = TopiaGeneratorUtil.getDOType(parent, model);
//            String extendClass = parent.getQualifiedName();
//            //Si une des classes parentes définies des méthodes abstraites, son
//            // impl ne sera pas créé
//            boolean abstractParent = TopiaGeneratorUtil.shouldBeAbstract(parent);
//            if (TopiaGeneratorUtil.isEntity(parent)) {
//                if (abstractParent) {
//                    extendClass += "Abstract";
//                } else {
//                    extendClass += "Impl";
//                }
//            }
            setSuperClass(outputAbstract, extendClass);
        }

        // Extends TopiaEntityAbstract (only if hasn't parent entity)
        if (outputAbstract.getSuperclasses().isEmpty()) {
            setSuperClass(outputAbstract, TopiaEntityAbstract.class);
        }

        addContextableMethods(input, outputAbstract);
    }

    /**
     * Ajout les methodes necessaire à l'interface {@link TopiaEntityContextable}
     * si le tagValue {@link TopiaTagValues#TAG_CONTEXTABLE} est renseigné.
     * @param input
     * @param outputAbstract
     */
    protected void addContextableMethods(ObjectModelClass input,
                                         ObjectModelClass outputAbstract) {

        if (TopiaGeneratorUtil.isContextable(input)) {

            addImport(outputAbstract, TopiaContextImplementor.class);

            ObjectModelOperation op = addOperation(outputAbstract, "update", "void",
                    ObjectModelJavaModifier.PUBLIC);
            addException(op, TopiaException.class);
//            setDocumentation(op,"@since 2.5.3");
            addAnnotation(outputAbstract, op, Override.class.getSimpleName());
            setOperationBody(op, ""
/*{
        ((TopiaContextImplementor)getTopiaContext()).getDAO(<%=input.getName()%>.class).update(this);
}*/
            );
            
            op = addOperation(outputAbstract, "delete", "void", ObjectModelJavaModifier.PUBLIC);
            addException(op, TopiaException.class);
//            setDocumentation(op,"@since 2.5.3");
            addAnnotation(outputAbstract, op, Override.class.getSimpleName());
            setOperationBody(op, ""
/*{
        ((TopiaContextImplementor)getTopiaContext()).getDAO(<%=input.getName()%>.class).delete(this);
}*/
            );
        }
    }

    protected boolean isGenerateInterface(ObjectModelClass input) {

        boolean alreadyInClassPath = !isInClassPath(input);
        return  alreadyInClassPath;
    }

    protected boolean isGenerateAbstract(ObjectModelClass input) {

        String fqn = input.getQualifiedName() + " Abstract";
        boolean alreadyInClassPath = !isInClassPath(fqn);
        return  alreadyInClassPath;
    }

    protected boolean isGenerateImpl(ObjectModelClass input) {

        Collection<ObjectModelOperation> operations = input.getOperations();
        String fqn = input.getQualifiedName() + "Impl";

        boolean alreadyInClassPath = isInClassPath(fqn);
        if (alreadyInClassPath) {

            return false;
        }

        // On ne génère pas le impl si l'entité a des opérations
        if (!operations.isEmpty()) {

            log.info("Will not generate [" + fqn + "], there is some operations to manually implement");
            return false;
        }

        //De même, on ne génère pas le impl si il y a des opérations venant des
        // superclasses non implémentées
        for (ObjectModelOperation otherOp : input.getAllOtherOperations(false)) {
            if (otherOp.isAbstract()) {
                log.info("Will not generate [" + fqn + "], there is an abstract operation [" + otherOp.getName() + "] in allOtherOperations.");
                return false;
            }
        }

        return true;
    }

    protected void generateImpl(ObjectModelClass input) {

        String implName = input.getName() + "Impl";
        String packageName = input.getPackageName();
        if (isVerbose()) {
            log.info("Will generate [" + implName + "]");
        }

        if (isAbstract(input)) {
            outputImpl = createAbstractClass(implName, packageName);
        } else {
            outputImpl = createClass(implName, packageName);
        }

        setDocumentation(outputImpl, "Implantation des operations pour l'entité " +
                                 input.getName() + ".");
        setSuperClass(outputImpl, input.getQualifiedName() + "Abstract");
    }

    /**
     * Generate extra constants if {@code input} has dependencies on
     * enum used as constant injector.
     *
     * @param input Entity class to treate
     */
    protected void generateExtraConstants(ObjectModelClass input) {
        Set<String> constants = addConstantsFromDependency(input, outputInterface);

        if (log.isDebugEnabled()) {
            log.debug("Add constants from dependency : " + constants);
        }
    }

    protected void generateExtraOperations(ObjectModelClass input) {
        for (ObjectModelOperation operation : input.getOperations()) {

            String opName = operation.getName();
            String opType = operation.getReturnType();
            ObjectModelModifier visibility =
                    ObjectModelJavaModifier.fromVisibility(operation.getVisibility());

            if (log.isDebugEnabled()) {
                log.debug("Extra operation for : " + input.getQualifiedName() +
                        " - method : " + opName +
                        " - returnType : " + opType +
                        " - visibility : " + visibility);
            }

            // Deprecated from 2.3.4
            // Pas de génération des signatures de méthodes pour celles à intégrer au DAO de l'entité
            if (TopiaGeneratorUtil.hasDaoStereotype(operation)) {
                return;

            // Generate entity methods which have not a public visibility.
            // Only in abstract entity class as abstract operation.
            } else if (!visibility.equals(ObjectModelJavaModifier.PUBLIC)) {
                addOperation(outputAbstract, opName, opType, visibility,
                        ObjectModelJavaModifier.ABSTRACT);

            // Other operations, only in entity interface, implementations
            // need to be done in implementation class created by developper
            } else {
                cloneOperationSignature(operation, outputInterface, true);
            }
        }
    }

    /**
     * Generate properties from {@code attributes}. Generate
     * constant, attribute and operations for each property.
     *
     * @param attributes Input attributes
     */
    protected void generateProperties(Collection<ObjectModelAttribute> attributes) {
        for (ObjectModelAttribute attribute : attributes) {

            if (!associationClass) {

                // FIXME-fdesbois-2010-06-25 : Strange behavior to keep those links, will break hibernate, may be a problem in mapping
                if (!attribute.isNavigable() && attribute.hasAssociationClass()) {
                    generatePropertyConstant(attribute);

                    generatePropertyAttribute(attribute);

                    // 20161004 poussin
                    // If attribute is not navigable then visitor must not try to visit it
                    // updateAcceptOperation(attribute);
                }

                if (!attribute.isNavigable() &&
                        !TopiaGeneratorUtil.hasUnidirectionalRelationOnAbstractType(
                                attribute.getReverseAttribute(), model)) {
                    continue;
                }
            }

            // constant
            generatePropertyConstant(attribute);

            // attribute
            generatePropertyAttribute(attribute);

            // operations
            generatePropertyOperations(attribute);

            // update accept body
            updateAcceptOperation(attribute);
        }
    }

    // -------------------------------------------------------------------------
    // Generate for property
    // -------------------------------------------------------------------------

    /**
     * Generate constant in interface for {@code attribute}.
     *
     * @param attribute Input attribute to treate
     * @see #getPropertyName(ObjectModelAttribute)
     */
    protected void generatePropertyConstant(ObjectModelAttribute attribute) {
        String attrName = getPropertyName(attribute);

        if (log.isDebugEnabled()) {
            log.debug("Generate constant for property : " + attrName);
        }

        addAttribute(outputInterface, getConstantName(attrName), String.class,
                "\"" + attrName + "\"");
    }

    protected void generatePropertyAttribute(ObjectModelAttribute attribute) {

        String attrName = getPropertyName(attribute);
        String attrType = getPropertyType(attribute);
        String collectionType = getCollectionType(attribute);

        if (collectionType != null) {
            attrType = collectionType + "<" + attrType + ">";
        }

        //String attrVisibility = attr.getVisibility();

        // Declaration
        ObjectModelAttribute property =
                addAttribute(outputAbstract, attrName, attrType, null,
                //ObjectModelJavaModifier.toValue(attrVisibility),
                ObjectModelJavaModifier.PROTECTED
        );

        // Documentation
        StringBuilder buffer = new StringBuilder();
        if (TopiaGeneratorUtil.hasDocumentation(attribute)) {
            String attrDocumentation = attribute.getDocumentation();
            buffer.append(attrDocumentation).append('\n');
        }

        String dbName = TopiaGeneratorUtil.getDbName(attribute);
        if (!StringUtils.isEmpty(dbName)) {
            buffer.append("Nom de l'attribut en BD : ").append(dbName).append('\n');
        }
        setDocumentation(property, buffer.toString());

        // Annotation
        String annotation =  TopiaGeneratorUtil.getAnnotationTagValue(attribute);
        if (!StringUtils.isEmpty(annotation)) {
            //FIXME Make annotation works...
            //TODO tchemit 20100513 Test it still works
            addAnnotation(outputAbstract, property, annotation);
        }
    }

    /**
     * Generation operations for {@code attributes}.
     * One method exists for each operation to generate. Methods starting
     * with 'addSingle' is for maxMultiplicity attribute = 1 and for collection
     * case, methods start with 'addMultiple'. Other case are take care in each
     * method (association class, reverse, entity reference, ...).
     *
     * @param attribute Input attribute to treate
     * @see #addSingleGetOperation(ObjectModelAttribute, String, String)
     * @see #addSingleSetOperation(ObjectModelAttribute)
     * @see #addMultipleAddOperation(ObjectModelAttribute, String)
     * @see #addMultipleAddAllOperation(ObjectModelAttribute, String)
     * @see #addMultipleSetOperation(ObjectModelAttribute, String, String)
     * @see #addMultipleRemoveOperation(ObjectModelAttribute)
     * @see #addMultipleClearOperation(ObjectModelAttribute, String, String)
     * @see #addMultipleGetOperation(ObjectModelAttribute, String)
     * @see #addMultipleGetTopiaIdOperation(ObjectModelAttribute)
     * @see #addMultipleGetOperationFromEntity(ObjectModelAttribute)
     * @see #addMultipleSizeOperation(ObjectModelAttribute)
     * @see #addMultipleIsEmptyOperation(ObjectModelAttribute)
     */
    protected void generatePropertyOperations(ObjectModelAttribute attribute) {

        if (attribute.getMaxMultiplicity() == 1 || associationClass) {

            // setXXX
            addSingleSetOperation(attribute);

            boolean booleanProperty =
                    TopiaGeneratorUtil.isBooleanPrimitive(attribute);
            
            String attrType = getPropertyType(attribute);

            if (booleanProperty) {

                // isXXX
                addSingleGetOperation(
                        attribute,
                        attrType,
                        TopiaGeneratorUtil.OPERATION_GETTER_BOOLEAN_PREFIX);
            }

            if (!booleanProperty || generateBooleanGetMethods) {
                
                // getXXX
                addSingleGetOperation(
                        attribute,
                        attrType,
                        TopiaGeneratorUtil.OPERATION_GETTER_DEFAULT_PREFIX);
            }

        } else {

            // List, Set or Collection ?
            String collectionInterface =
                    TopiaGeneratorUtil.getNMultiplicityInterfaceType(attribute);
            String collectionImpl =
                    TopiaGeneratorUtil.getNMultiplicityObjectType(attribute);

            addImport(outputInterface, collectionInterface);
            addImport(outputAbstract, collectionInterface);
            addImport(outputAbstract, collectionImpl);

            collectionInterface =
                    TopiaGeneratorUtil.getSimpleName(collectionInterface);
            collectionImpl =
                    TopiaGeneratorUtil.getSimpleName(collectionImpl);

            // addXXX
            addMultipleAddOperation(attribute, collectionImpl);

            // addAllXXX
            addMultipleAddAllOperation(attribute, collectionInterface);

            // setXXX
            addMultipleSetOperation(attribute, collectionInterface, collectionImpl);

            // removeXXX
            addMultipleRemoveOperation(attribute);

            // clearXXX
            addMultipleClearOperation(attribute, collectionInterface, collectionImpl);

            // getXXX
            addMultipleGetOperation(attribute, collectionInterface);

            if (TopiaGeneratorUtil.isEntity(attribute, model)) {

                // getXXXByTopiaId
                addMultipleGetTopiaIdOperation(attribute);
            }

            if (attribute.hasAssociationClass()) {
                // getXXX with entity parameter
                addMultipleGetOperationFromEntity(attribute);
            }

            // sizeXXX
            addMultipleSizeOperation(attribute);

            // isXXXEmpty
            addMultipleIsEmptyOperation(attribute);
        }
    }

    protected void addSingleSetOperation(ObjectModelAttribute attribute) {

        String attrName = getPropertyName(attribute);
        String attrType = getPropertyType(attribute);

        if (log.isDebugEnabled()) {
            log.debug("Generate single 'set' operation for property : " + attrName +
                    " [" + attrType + "]");
        }

        // Interface operation
        ObjectModelOperation interfaceOperation =
                createPropertySetterSignature(outputInterface, attrType, attrName,
                        "");

        // Implementation
        ObjectModelOperation implOperation = createImplOperation(interfaceOperation);

        attrType = TopiaGeneratorUtil.getSimpleName(attrType);

        String constantName  = getConstantName(attrName);

        setOperationBody(implOperation, ""
/*{
        <%=attrType%> oldValue = this.<%=attrName%>;
        fireOnPreWrite(<%=constantName%>, oldValue, <%=attrName%>);
        this.<%=attrName%> = <%=attrName%>;
        fireOnPostWrite(<%=constantName%>, oldValue, <%=attrName%>);
    }*/
        );
    }

    /**
     * Add getter for simple property (neither association nor multiple).
     * Will add two different operations for boolean case ('is' method and
     * 'get' method). This method add the operation in both {@code
     * outputAbstract} and {@code outputInterface}.
     *
     * @param attribute         ObjectModelAttribute for getter operation
     * @param attrType          type of the attribute
     * @param operationPrefix   Operation prefix : 'get' by default, if prefix
     *                          is null
     */
    protected void addSingleGetOperation(ObjectModelAttribute attribute,
                                         String attrType,
                                         String operationPrefix) {

        String attrName = getPropertyName(attribute);

        if (log.isDebugEnabled()) {
            log.debug("Generate single '" + operationPrefix + "' operation for property : "
                    + attrName + " [" + attrType + "]");
        }

        String constantName  = getConstantName(attrName);
        
        // Interface operation
        ObjectModelOperation interfaceOperation =
                addOperation(outputInterface, getJavaBeanMethodName(operationPrefix, attrName),
                    attrType, ObjectModelJavaModifier.PACKAGE);

        boolean generateReadListeners = !TopiaGeneratorUtil.isDoNotGenerateReadListeners(attribute, model);

        // Implementation
        ObjectModelOperation implOperation =
                createImplOperation(interfaceOperation);

        attrType = TopiaGeneratorUtil.getSimpleName(attrType);

        StringBuilder body = new StringBuilder();
        if (generateReadListeners) {
            body.append(""
/*{
        fireOnPreRead(<%=constantName%>, <%=attrName%>);
}*/);
        }

        body.append(""
/*{
        <%=attrType%> result = this.<%=attrName%>;
}*/);
        
        if (generateReadListeners) {
            body.append(""
/*{
        fireOnPostRead(<%=constantName%>, <%=attrName%>);
}*/);
        }

        body.append(""
/*{
        return result;
    }*/);
        setOperationBody(implOperation, body.toString());
    }

    protected void addMultipleAddOperation(ObjectModelAttribute attribute,
                                           String collectionImpl) {

        String attrName = getPropertyName(attribute);
        String attrType = getPropertyType(attribute);
        ObjectModelAttribute reverse = attribute.getReverseAttribute();

        if (log.isDebugEnabled()) {
            log.debug("Generate multiple 'add' operation for property : " + attrName +
                    " [" + attrType + "]");
        }

        String constantName = getConstantName(attrName);
        
        // Interface operation
        ObjectModelOperation interfaceOperation =
                addOperation(outputInterface, getJavaBeanMethodName("add", attrName),
                        void.class, ObjectModelJavaModifier.PACKAGE);
        ObjectModelParameter param =
                addParameter(interfaceOperation, attrType, attrName);

        // Implementation
        ObjectModelOperation implOperation = createImplOperation(interfaceOperation);

        attrType = TopiaGeneratorUtil.getSimpleName(attrType);

        StringBuilder body = new StringBuilder();

        body.append(""
/*{
        fireOnPreWrite(<%=constantName%>, null, <%=attrName%>);
        if (this.<%=attrName%> == null) {
            this.<%=attrName%> = new <%=collectionImpl%><<%=attrType%>>();
        }
}*/
        );

        if (reverse != null && (reverse.isNavigable() ||
                hasUnidirectionalRelationOnAbstractType(attribute, model))) {
            String getterName = getJavaBeanMethodName("get", reverse.getName());
            String setterName = getJavaBeanMethodName("set", reverse.getName());

            String reverseAttrType = TopiaGeneratorUtil.getSimpleName(reverse.getType());
            
            if (!TopiaGeneratorUtil.isNMultiplicity(reverse)) {
                body.append(""
/*{
        <%=attrName%>.<%=setterName%>(this);
}*/
            );
        // Don't manage reverse attribute add if attribute has associationClass
        } else if (!attribute.hasAssociationClass()) {
            body.append(""
/*{
        if (<%=attrName%>.<%=getterName%>() == null) {
            <%=attrName%>.<%=setterName%>(new <%=collectionImpl%><<%=reverseAttrType%>>());
        }
        <%=attrName%>.<%=getterName%>().add(this);
}*/
                );
            }
        }
        body.append(""
/*{
        this.<%=attrName%>.add(<%=attrName%>);
        fireOnPostWrite(<%=constantName%>, this.<%=attrName%>.size(), null, <%=attrName%>);
    }*/
        );
        setOperationBody(implOperation, body.toString());
    }

    protected void addMultipleAddAllOperation(ObjectModelAttribute attribute,
                                              String collectionInterface) {

        String attrName = getPropertyName(attribute);
        String attrType = getPropertyType(attribute);

        if (log.isDebugEnabled()) {
            log.debug("Generate multiple 'addAll' operation for property : " + attrName +
                    " [" + attrType + "]");
        }

        // Interface operation
        ObjectModelOperation interfaceOperation =
                addOperation(outputInterface, getJavaBeanMethodName("addAll", attrName),
                        void.class, ObjectModelJavaModifier.PACKAGE);
        ObjectModelParameter param =
                addParameter(interfaceOperation, collectionInterface + "<" + attrType + ">", attrName);

        // Implementation
        ObjectModelOperation implOperation =
                createImplOperation(interfaceOperation);

        attrType = TopiaGeneratorUtil.getSimpleName(attrType);
        String addMethodName = getJavaBeanMethodName("add", attrName);
        setOperationBody(implOperation, ""
/*{
        if (<%=attrName%> == null) {
            return;
        }
        for (<%=attrType%> item : <%=attrName%>) {
            <%=addMethodName%>(item);
        }
    }*/
        );
    }

    protected void addMultipleSetOperation(ObjectModelAttribute attribute,
                                           String collectionInterface,
                                           String collectionImpl) {

        String attrName = getPropertyName(attribute);
        String referenceType = getPropertyType(attribute);
        String attrType = collectionInterface + "<" + referenceType + ">";
        String constantName = getConstantName(attrName);
        
        if (log.isDebugEnabled()) {
            log.debug("Generate multiple 'set' operation for property : " + attrName +
                    " [" + attrType + "]");
        }

        // Interface operation
        ObjectModelOperation interfaceOperation =
                createPropertySetterSignature(outputInterface, attrType, attrName,
                        "");

        ObjectModelOperation implOperation = createImplOperation(interfaceOperation);

        attrType = TopiaGeneratorUtil.getSimpleName(attrType);
        referenceType = TopiaGeneratorUtil.getSimpleName(referenceType);

        // Force fire for collection
        setOperationBody(implOperation, ""
/*{
        // Copy elements to keep data for fire with new reference
        <%=attrType%> oldValue = this.<%=attrName%> != null ? new <%=collectionImpl%><<%=referenceType%>>(this.<%=attrName%>) : null;
        fireOnPreWrite(<%=constantName%>, oldValue, <%=attrName%>);
        this.<%=attrName%> = <%=attrName%>;
        fireOnPostWrite(<%=constantName%>, oldValue, <%=attrName%>);
    }*/
        );
    }

    protected void addMultipleRemoveOperation(ObjectModelAttribute attribute) {

        String attrName = getPropertyName(attribute);
        String attrType = getPropertyType(attribute);
        ObjectModelAttribute reverse = attribute.getReverseAttribute();
        String constantName = getConstantName(attrName);
        
        if (log.isDebugEnabled()) {
            log.debug("Generate 'remove' operation for property : " + attrName +
                    " [" + attrType + "]");
        }

        // Interface operation
        ObjectModelOperation interfaceOperation =
                addOperation(outputInterface, getJavaBeanMethodName("remove" , attrName),
                        void.class, ObjectModelJavaModifier.PACKAGE);
        ObjectModelParameter param =
                addParameter(interfaceOperation, attrType, attrName);

        // Implementation
        ObjectModelOperation implOperation = createImplOperation(interfaceOperation);

        attrType = TopiaGeneratorUtil.getSimpleName(attrType);

        StringBuilder body = new StringBuilder();

        body.append(""
/*{
        fireOnPreWrite(<%=constantName%>, <%=attrName%>, null);
        if (this.<%=attrName%> == null || !this.<%=attrName%>.remove(<%=attrName%>)) {
            throw new IllegalArgumentException("List does not contain given element");
        }
}*/
        );

        if (reverse != null && (reverse.isNavigable() ||
                hasUnidirectionalRelationOnAbstractType(attribute, model))) {
            String getterName = getJavaBeanMethodName("get", reverse.getName());
            String setterName = getJavaBeanMethodName("set", reverse.getName());
            if (!TopiaGeneratorUtil.isNMultiplicity(reverse)) {
                body.append(""
/*{
        <%=attrName%>.<%=setterName%>(null);
}*/
            );
        // Don't manage reverse attribute remove if attribute has associationClass
        } else if (!attribute.hasAssociationClass()) {
            body.append(""
/*{
        <%=attrName%>.<%=getterName%>().remove(this);
}*/
                );
            }
        }
        body.append(""
/*{
        fireOnPostWrite(<%=constantName%>, this.<%=attrName%>.size() + 1, <%=attrName%>, null);
    }*/
        );
        setOperationBody(implOperation, body.toString());
    }

    protected void addMultipleClearOperation(ObjectModelAttribute attribute,
                                             String collectionInterface,
                                             String collectionImpl) {

        String attrName = getPropertyName(attribute);
        String attrType = getPropertyType(attribute);
        ObjectModelAttribute reverse = attribute.getReverseAttribute();
        String constantName = getConstantName(attrName);
        
        if (log.isDebugEnabled()) {
            log.debug("Generate multiple 'clear' operation for property : " + attrName +
                    " [" + attrType + "]");
        }

        // Interface operation
        ObjectModelOperation interfaceOperation =
                addOperation(outputInterface, getJavaBeanMethodName("clear" , attrName),
                        void.class, ObjectModelJavaModifier.PACKAGE);

        // Implementation
        ObjectModelOperation implOperation = createImplOperation(interfaceOperation);

        attrType = TopiaGeneratorUtil.getSimpleName(attrType);

        StringBuilder body = new StringBuilder(""
/*{
        if (this.<%=attrName%> == null) {
            return;
        }
}*/
        );

        if (reverse != null && (reverse.isNavigable() ||
                hasUnidirectionalRelationOnAbstractType(attribute, model))) {
            String getterName = getJavaBeanMethodName("get", reverse.getName());
            String setterName = getJavaBeanMethodName("set", reverse.getName());
            body.append(""
/*{        for (<%=attrType%> item : this.<%=attrName%>) {
}*/
            );
            if (!TopiaGeneratorUtil.isNMultiplicity(reverse)) {
                body.append(""
/*{            item.<%=setterName%>(null);
}*/
                );
            // Don't manage reverse attribute remove if attribute has associationClass
            }  else if (!attribute.hasAssociationClass())  {
                body.append(""
/*{            item.<%=getterName%>().remove(this);
}*/
                );
            }
            body.append(""
/*{        }
}*/
            );
        }
        body.append(""
/*{        <%=collectionInterface%><<%=attrType%>> oldValue = new <%=collectionImpl%><<%=attrType%>>(this.<%=attrName%>);
        fireOnPreWrite(<%=constantName%>, oldValue, this.<%=attrName%>);
        this.<%=attrName%>.clear();
        fireOnPostWrite(<%=constantName%>, oldValue, this.<%=attrName%>);
    }*/
        );
        setOperationBody(implOperation, body.toString());
    }

    protected void addMultipleGetOperation(ObjectModelAttribute attribute,
                                           String collectionInterface) {

        String attrName = getPropertyName(attribute);
        String attrType = collectionInterface + "<" + getPropertyType(attribute) + ">";

        if (log.isDebugEnabled()) {
            log.debug("Generate multiple 'get' operation for property : " + attrName +
                    " [" + attrType + "]");
        }

        // Interface operation
        ObjectModelOperation interfaceOperation =
                addOperation(outputInterface, getJavaBeanMethodName("get" , attrName),
                        attrType, ObjectModelJavaModifier.PACKAGE);

        // Implementation
        ObjectModelOperation implOperation = createImplOperation(interfaceOperation);

        setOperationBody(implOperation, ""
/*{
        return <%=attrName%>;
    }*/
        );
    }

    protected void addMultipleGetTopiaIdOperation(ObjectModelAttribute attribute) {

        String attrName = getPropertyName(attribute);
        String attrType = getPropertyType(attribute);

        if (log.isDebugEnabled()) {
            log.debug("Generate multiple 'getByTopiaId' operation for property : " + attrName +
                    " [" + attrType + "]");
        }

        // Interface operation
        ObjectModelOperation interfaceOperation =
                addOperation(outputInterface, getJavaBeanMethodName("get", attrName) + "ByTopiaId",
                            attrType, ObjectModelJavaModifier.PACKAGE);
        ObjectModelParameter param =
                addParameter(interfaceOperation, String.class, "topiaId");

        // Implementation
        ObjectModelOperation implOperation =
                createImplOperation(interfaceOperation);

        addImport(outputAbstract, TopiaEntityHelper.class);

        setOperationBody(implOperation, ""
/*{
        return TopiaEntityHelper.getEntityByTopiaId(<%=attrName%>, topiaId);
    }*/
        );
    }

    protected void addMultipleGetOperationFromEntity(ObjectModelAttribute attribute) {

        // reference to the real attribute name
        String referenceName = attribute.getName();
        String referenceType = attribute.getType();

        String referenceGetterName = getJavaBeanMethodName("get", referenceName);
        // association attribute name
        String attrName = getPropertyName(attribute);
        String attrType = getPropertyType(attribute);

        if (log.isDebugEnabled()) {
            log.debug("Generate multiple 'getFromEntity' operation for property : " + attrName +
                    " [" + attrType + "]");
        }

        // Interface operation
        ObjectModelOperation interfaceOperation =
                addOperation(outputInterface,
                        getJavaBeanMethodName("get", attrName), attrType);

        addParameter(interfaceOperation, referenceType, referenceName);

        // Implementation
        ObjectModelOperation implOperation = createImplOperation(interfaceOperation);

        attrType = TopiaGeneratorUtil.getSimpleName(attrType);

        setOperationBody(implOperation, ""
/*{
        if (<%=referenceName%> == null || this.<%=attrName%> == null) {
            return null;
        }
        for (<%=attrType%> item : this.<%=attrName%>) {
            if (<%=referenceName%>.equals(item.<%=referenceGetterName%>())) {
                return item;
            }
        }
        return null;
    }*/
        );
    }

    protected void addMultipleSizeOperation(ObjectModelAttribute attribute) {

        String attrName = getPropertyName(attribute);

        if (log.isDebugEnabled()) {
            log.debug("Generate multiple 'size' operation for property : " + attrName);
        }

        // Interface operation
        ObjectModelOperation interfaceOperation =
                addOperation(outputInterface, getJavaBeanMethodName("size", attrName),
                        int.class, ObjectModelJavaModifier.PACKAGE);

        // Implementation
        ObjectModelOperation implOperation = createImplOperation(interfaceOperation);

        setOperationBody(implOperation, ""
/*{
        if (<%=attrName%> == null) {
            return 0;
        }
        return <%=attrName%>.size();
    }*/
        );
    }

    protected void addMultipleIsEmptyOperation(ObjectModelAttribute attribute) {

        String attrName = getPropertyName(attribute);

        if (log.isDebugEnabled()) {
            log.debug("Generate multiple 'isEmpty' operation for property : " + attrName);
        }

        String sizeMethodName = getJavaBeanMethodName("size", attrName);
        // Interface operation
        ObjectModelOperation interfaceOperation =
                addOperation(outputInterface, getJavaBeanMethodName("is", attrName)+ "Empty",
                        boolean.class, ObjectModelJavaModifier.PACKAGE);

        // Implementation
        ObjectModelOperation implOperation = createImplOperation(interfaceOperation);

        setOperationBody(implOperation, ""
/*{
        int size = <%=sizeMethodName%>();
        return size == 0;
    }*/
        );
    }

    // -------------------------------------------------------------------------
    // Generate util operations
    // -------------------------------------------------------------------------

    private ObjectModelOperation internalAcceptOperation;

    private StringBuilder internalAcceptOperationBody;

    protected void createAcceptOperation() {

        ObjectModelOperation acceptOperation = addOperation(outputAbstract, "accept", void.class);
        addAnnotation(outputAbstract, acceptOperation , Override.class);
        addParameter(acceptOperation, EntityVisitor.class, "visitor");

        addException(acceptOperation, TopiaException.class);

        setOperationBody(acceptOperation,""
/*{
        visitor.start(this);
        accept0(visitor);
        visitor.end(this);
    }*/
        );
    }

    protected void createAcceptInternalOperation(ObjectModelClass input) {

        boolean withSuperEntity = false;
        for (ObjectModelClassifier parent : input.getSuperclasses()) {
            if (TopiaGeneratorUtil.isEntity(parent)) {
                withSuperEntity= true;
                break;
            }
        }

        internalAcceptOperation = addOperation(outputAbstract, "accept0", void.class, ObjectModelJavaModifier.PROTECTED);
        addParameter(internalAcceptOperation, EntityVisitor.class, "visitor");
        addException(internalAcceptOperation, TopiaException.class);

        if (withSuperEntity) {
            addAnnotation(outputAbstract, internalAcceptOperation, Override.class);
            internalAcceptOperationBody = new StringBuilder(""
/*{
        super.accept0(visitor);
}*/
            );
        } else {
            internalAcceptOperationBody= new StringBuilder(""
/*{
}*/
            );
        }
    }

    protected void updateAcceptOperation(ObjectModelAttribute attribute) {
        String attrName =
                TopiaGeneratorUtil.getSimpleName(getPropertyName(attribute));
        String attrType =
                TopiaGeneratorUtil.getSimpleName(getPropertyType(attribute));
        String collectionType = getCollectionType(attribute);
        String constantName = getConstantName(attrName);
        if (collectionType != null) {
            collectionType = TopiaGeneratorUtil.getSimpleName(collectionType);
            internalAcceptOperationBody.append(""
/*{        visitor.visit(this, <%=constantName%>, <%=collectionType%>.class, <%=attrType%>.class, <%=attrName%>);
}*/
            );
        } else {
            internalAcceptOperationBody.append(""
/*{        visitor.visit(this, <%=constantName%>, <%=attrType%>.class, <%=attrName%>);
}*/
            );
        }
    }

    protected void closeAcceptInternalOperation() {

        setOperationBody(internalAcceptOperation, internalAcceptOperationBody.toString() + ""
/*{    }*/
        );
    }

    protected void generateToStringOperation(ObjectModelClass input) {

        if (log.isDebugEnabled()) {
            log.debug("generate toString method for entity " +
                    outputInterface.getQualifiedName());
        }
        ObjectModelOperation operation =
                addOperation(outputAbstract, "toString", String.class);

        addAnnotation(outputAbstract, operation, Override.class.getSimpleName());

        addImport(outputAbstract, ToStringBuilder.class);

        StringBuilder body = new StringBuilder(""
/*{
        String result = new ToStringBuilder(this).
}*/
        );
        for (ObjectModelAttribute attr : input.getAttributes()) {

            //FIXME possibilité de boucles (non directes)

            ObjectModelClass attrEntity = null;

            if (model.hasClass(attr.getType())) {
                attrEntity = model.getClass(attr.getType());
            }

            boolean isEntity = attrEntity != null &&
                    TopiaGeneratorUtil.isEntity(attrEntity);

            ObjectModelAttribute reverse = attr.getReverseAttribute();
            if (isEntity && (reverse == null || !reverse.isNavigable())
                    && !attr.hasAssociationClass() || !isEntity) {
                String attrName = attr.getName();
                String constantName = getConstantName(attrName);
                body.append(""
/*{            append(<%=constantName%>, this.<%=attrName%>).
}*/
                );
            }
        }
        body.append(""
/*{         toString();
        return result;
    }*/
        );
        setOperationBody(operation, body.length() == 0 ? " " : body.toString());

    }

    protected void generateCompositeOperation(ObjectModelClass input) {

        ObjectModelOperation operation =
                addOperation(outputAbstract, "getComposite",
                List.class.getName() + '<' + TopiaEntity.class.getName() + '>');

        addException(operation, TopiaException.class);
        addAnnotation(outputAbstract, operation, Override.class.getSimpleName());

        addImport(outputAbstract, ArrayList.class);
        addImport(outputAbstract, List.class);

        StringBuilder body = new StringBuilder(""
/*{
        List<TopiaEntity> tmp = new ArrayList<TopiaEntity>();

        // pour tous les attributs rechecher les composites et les class d'asso
        // on les ajoute dans tmp
}*/
        );
        for (ObjectModelAttribute attr : input.getAttributes()) {

            if (attr.referenceClassifier() &&
                    TopiaGeneratorUtil.isEntity(attr.getClassifier())) {

                if (attr.isComposite()) {
                    String attrName = attr.getName();
                    String getterName = getJavaBeanMethodName("get", attrName);
                    if (TopiaGeneratorUtil.isNMultiplicity(attr)) {
                        body.append(""
/*{        if (<%=getterName%>() != null) {
              tmp.addAll(<%=getterName%>());
           }
}*/
                        );
                    } else {
                        body.append(""
/*{        tmp.add(<%=getterName%>());
}*/
                        );
                    }
                } else if (attr.hasAssociationClass()) {
                    String assocAttrName = TopiaGeneratorUtil.getAssocAttrName(
                            attr);
                    String assocClassFQN = TopiaGeneratorUtil.getSimpleName(
                            attr.getAssociationClass().getQualifiedName());
                    String ref = "this." + TopiaGeneratorUtil.toLowerCaseFirstLetter(
                            assocAttrName);
                    if (!TopiaGeneratorUtil.isNMultiplicity(attr)) {
                        body.append(""
/*{
        if (<%=ref%> != null) {
            tmp.add(<%=ref%>);
        }
}*/
                        );
                    } else {
                        ObjectModelAttribute reverse = attr.getReverseAttribute();
                        String reverseAttrName = reverse.getName();
                        // On utilise pas l'attribut car il est potentiellement
                        // pas a jour, car  pour les asso avec cardinalité
                        // personne ne fait de add. Ce qui est normal, mais
                        // pour pouvoir faire tout de meme des delete en cascade
                        // sur les asso, le champs est dans le mapping
                        // hibernate et donc il le faut aussi dans la classe
                        // sinon hibernate rale lorsqu'il charge l'objet
//                        if (<%=ref%> != null) {
//                            tmp.addAll(<%=ref%>);
//                        }

                        addImport(outputAbstract, TopiaContextImplementor.class);
                        body.append(""
/*{
        {
            org.nuiton.topia.persistence.TopiaDAO<<%=assocClassFQN%>> dao = ((TopiaContextImplementor) getTopiaContext()).getDAO(<%=assocClassFQN%>.class);
            List<<%=assocClassFQN%>> findAllByProperties = dao.findAllByProperties("<%=reverseAttrName%>", this);
            if (findAllByProperties != null) {
                tmp.addAll(findAllByProperties);
            }
        }
}*/
                        );
                    }
                }
            }
        }
        body.append(""
/*{
        // on refait un tour sur chaque entity de tmp pour recuperer leur
        // composite
        List<TopiaEntity> result = new ArrayList<TopiaEntity>();
        for (TopiaEntity entity : tmp) {
            if (entity != null) {
                result.add(entity);
                result.addAll(entity.getComposite());
            }
        }
        return result;
    }*/
        );

        setOperationBody(operation, body.length() == 0 ? " " : body.toString());
    }

    protected void generateAggregateOperation(ObjectModelClass input) {

        ObjectModelOperation operation =
                addOperation(outputAbstract, "getAggregate",
                List.class.getName() + '<' + TopiaEntity.class.getName() + '>');

        addException(operation, TopiaException.class);
        addAnnotation(outputAbstract, operation, Override.class.getSimpleName());

        addImport(outputAbstract, ArrayList.class);
        addImport(outputAbstract, List.class);

        StringBuilder body = new StringBuilder(""
/*{
        List<TopiaEntity> tmp = new ArrayList<TopiaEntity>();

        // pour tous les attributs rechecher les composites et les class d'asso
        // on les ajoute dans tmp
}*/
        );
        for (ObjectModelAttribute attr : input.getAttributes()) {

            if (attr.referenceClassifier() &&
                    TopiaGeneratorUtil.isEntity(attr.getClassifier()) &&
                    attr.isAggregate()) {

                String attrName = attr.getName();
                String getterName = getJavaBeanMethodName("get", attrName);
                if (TopiaGeneratorUtil.isNMultiplicity(attr)) {
                    body.append(""
/*{        tmp.addAll(<%=getterName%>());
}*/
                    );
                } else {
                    body.append(""
/*{        tmp.add(<%=getterName%>());
}*/
                    );
                }
            }
        }
        body.append(""
/*{
        // on refait un tour sur chaque entity de tmp pour recuperer leur
        // composite
        List<TopiaEntity> result = new ArrayList<TopiaEntity>();
        for (TopiaEntity entity : tmp) {
            result.add(entity);
            result.addAll(entity.getAggregate());
        }
        return result;
    }*/
        );

        setOperationBody(operation, body.length() == 0 ? " " : body.toString());
    }

    // -------------------------------------------------------------------------
    // Helpers
    // -------------------------------------------------------------------------

    protected boolean isAbstract(ObjectModelClass clazz) {
        if (clazz.isAbstract()) {
            return true;
        }

        //Une classe peut être abstraite si elle a des méthodes définies dans
        // ses superinterface et non implantées dans ses superclasses
        Collection<ObjectModelOperation> allInterfaceOperations =
                clazz.getAllInterfaceOperations(true);
        allInterfaceOperations.removeAll(clazz.getAllOtherOperations(true));
        for (ObjectModelOperation op : allInterfaceOperations) {
            boolean implementationFound = false;
            for (ObjectModelClass superClazz : clazz.getSuperclasses()) {
                for (ObjectModelOperation matchingOp :
                        superClazz.getOperations(op.getName())) {
                    implementationFound = op.equals(matchingOp) &&
                                          !matchingOp.isAbstract();
                    if (implementationFound) {
                        break;
                    }
                }
                if (implementationFound) {
                    break;
                }
            }
            if (!implementationFound) {
                if (log.isDebugEnabled()) {
                    log.debug(clazz.getName() + " : abstract operation " + op);
                }
                return true;
            }
        }
        return false;
    }

    protected String getCollectionType(ObjectModelAttribute attribute) {
        String result = null;
        if (!associationClass && TopiaGeneratorUtil.isNMultiplicity(attribute)) {
            result = TopiaGeneratorUtil.getNMultiplicityInterfaceType(attribute);
        }
        return result;
    }

    protected String getPropertyName(ObjectModelAttribute attribute) {
        String propertyName = attribute.getName();
        if (!associationClass && attribute.hasAssociationClass()) {
            propertyName = TopiaGeneratorUtil.getAssocAttrName(attribute);
        }
        return propertyName;
    }

    protected String getPropertyType(ObjectModelAttribute attribute) {
        String propertyType = attribute.getType();
        if (!associationClass && attribute.hasAssociationClass()) {
            propertyType = attribute.getAssociationClass().getQualifiedName();
        }
        return propertyType;
    }

    protected ObjectModelOperation createImplOperation(ObjectModelOperation interfaceOperation) {
        ObjectModelOperation implOperation =
                cloneOperationSignature(interfaceOperation, outputAbstract, false);
        addAnnotation(outputAbstract, implOperation, Override.class.getSimpleName());
        return implOperation;
    }

    /**
     * TODO-fdesbois-2010-06-25 : This method can be put in JavaBuilder or ObjectModelTransformerToJava
     *
     * This method create an set operation in {@code classifier} with
     * {@code propertyType} as return type and {@code propertyName} used for
     * operation name ('set[propertyName]'). {@code operationDocument} can
     * also be added to the operation created. Only signature with default
     * visibility will be added.
     *
     * @param classifier             Classifier where the operation will be added
     * @param propertyType           Type of the property (better if qualified name)
     * @param propertyName           Name of the property to set
     * @param operationDocumentation Documentation for the operation
     * @return the created operation
     */
    protected ObjectModelOperation createPropertySetterSignature(ObjectModelClassifier classifier,
                                                                String propertyType,
                                                                String propertyName,
                                                                String operationDocumentation) {
        // Operation
        ObjectModelOperation operation =
                addOperation(classifier,
                        getJavaBeanMethodName("set", propertyName), void.class);

        ObjectModelParameter param =
                addParameter(operation, propertyType, propertyName);

        // Documentation
        if (StringUtils.isNotEmpty(operationDocumentation)) {
            setDocumentation(operation, operationDocumentation);
            setDocumentation(param, "La valeur de l'attribut à positionner.");
        }

        return operation;
    }

    protected void addInterface(List<String> interfaceAlreadyDone,
                                ObjectModelClassifier output,
                                ObjectModelClassifier interfaze) {
        String qualifiedName = interfaze.getQualifiedName();
        if (!interfaceAlreadyDone.contains(qualifiedName)) {

            interfaceAlreadyDone.add(qualifiedName);

            if (output != null) {

                // add it to output
                addInterface(output, qualifiedName);

                if (log.isTraceEnabled()) {
                    log.trace("Add interface " + qualifiedName + " on " +
                              output.getQualifiedName());
                }
            } else {
                if (log.isTraceEnabled()) {
                    log.trace("Skip included interface " + qualifiedName);
                }
            }

            // scan also all interfaces or super-classes of it
            for (ObjectModelClassifier parent : interfaze.getInterfaces()) {
                addInterface(interfaceAlreadyDone, null, parent);
            }
        }
    }

    protected void addInterface(List<String> interfaceAlreadyDone,
                                ObjectModelClassifier output,
                                Class<?> clazz) {
        String qualifiedName = clazz.getName();
        if (!interfaceAlreadyDone.contains(qualifiedName)) {

            // add it to output
            addInterface(output, qualifiedName);

        }
    }
}
