/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.topia.generator;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.codehaus.plexus.component.annotations.Component;
import org.nuiton.eugene.GeneratorUtil;
import org.nuiton.eugene.Template;
import org.nuiton.eugene.java.ObjectModelTransformerToJava;
import org.nuiton.eugene.models.object.ObjectModelAssociationClass;
import org.nuiton.eugene.models.object.ObjectModelAttribute;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.eugene.models.object.ObjectModelClassifier;
import org.nuiton.eugene.models.object.ObjectModelJavaModifier;
import org.nuiton.eugene.models.object.ObjectModelModifier;
import org.nuiton.eugene.models.object.ObjectModelOperation;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;

import static org.nuiton.topia.generator.TopiaGeneratorUtil.hasUnidirectionalRelationOnAbstractType;
import static org.nuiton.topia.generator.TopiaGeneratorUtil.shouldGenerateDTOTopiaIdTagValue;


/*{generator option: parentheses = false}*/

/*{generator option: writeString = +}*/

/**
 * Created: 14 déc. 2009
 *
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @version $Id$ 
 * @since 2.3.0
 */
@Component(role = Template.class, hint = "org.nuiton.topia.generator.EntityDTOTransformer")
public class EntityDTOTransformer extends ObjectModelTransformerToJava {

    public boolean isEntity(String type) {
        ObjectModelClassifier clazz = model.getClassifier(type);
        return clazz != null && ! clazz.isEnum()
               && TopiaGeneratorUtil.isEntity(clazz);
    }

    @Override
    public void transformFromClass(ObjectModelClass clazz) {
        if (!TopiaGeneratorUtil.isEntity(clazz)) {
            return;
        }
        String clazzName = clazz.getName();
        ObjectModelClass result;
        result = createClass(clazzName + "DTO", clazz.getPackageName());
        addImport(result, ToStringBuilder.class);
        addImport(result, PropertyChangeListener.class);

        setDocumentation(result, "Implantation DTO pour l'entité " + StringUtils.capitalize(clazzName) + ".");
        String extendClass = "";
        for (ObjectModelClass parent : clazz.getSuperclasses()) {
            extendClass = parent.getQualifiedName() + "DTO";
            // no multi-inheritance in java
            break;
        }
        if (extendClass.length() > 0) {
            setSuperClass(result, extendClass);
        }
        addInterface(result, Serializable.class);


        addAttributes(result,clazz);
        
        addOperations(result,clazz);

    }

    protected void addAttributes(ObjectModelClass result, ObjectModelClass clazz) {

        String svUID = TopiaGeneratorUtil.findTagValue("dto-serialVersionUID", clazz, model);
        if (svUID != null) {
            addAttribute(result, "serialVersionUID", "long", svUID, ObjectModelJavaModifier.FINAL, ObjectModelJavaModifier.PUBLIC, ObjectModelJavaModifier.STATIC);
        }

        boolean generateDTOId = shouldGenerateDTOTopiaIdTagValue(clazz, model);
        if (generateDTOId) {
            addAttribute(result, "topiaId", "String");
        }

        ObjectModelAttribute attr2;
        for (ObjectModelAttribute attr : clazz.getAttributes()) {
            ObjectModelAttribute reverse = attr.getReverseAttribute();

            // pour les asso quoi qu'il arrive il faut les lier des 2 cotes
            // pour pouvoir supprimer en cascade l'asso lors de la suppression
            // d'un des cotes
            if (!(attr.isNavigable()
                    || hasUnidirectionalRelationOnAbstractType(reverse, model)
                    || attr.hasAssociationClass())) {
                continue;
            }

            String attrVisibility = attr.getVisibility();
            ObjectModelModifier modifier = ObjectModelJavaModifier.fromVisibility(attrVisibility);
            if (!attr.hasAssociationClass()) {
                String attrType = attr.getType();
                String attrName = attr.getName();
                if (isEntity(attrType)) {
                    attrType += "DTO";
                }
                if (!GeneratorUtil.isNMultiplicity(attr)) {
                    attr2 = addAttribute(result, attrName, attrType, null, modifier);
                } else {
                    attr2 = addAttribute(result, attrName, attrType + "[]", null, modifier);
                }
            } else {
                String assocAttrName = TopiaGeneratorUtil.getAssocAttrName(attr);
                String assocClassFQN = attr.getAssociationClass().getQualifiedName();
                if (!GeneratorUtil.isNMultiplicity(attr)) {
                    attr2 = addAttribute(result, GeneratorUtil.toLowerCaseFirstLetter(assocAttrName), assocClassFQN + "DTO", null, modifier);
                } else {
                    attr2 = addAttribute(result, GeneratorUtil.toLowerCaseFirstLetter(assocAttrName), assocClassFQN + "DTO[]", null, modifier);
                }
            }
            if (attr2 != null) {
                if (TopiaGeneratorUtil.hasDocumentation(attr)) {
                    setDocumentation(attr2, attr.getDocumentation());
                }

                String annotation = TopiaGeneratorUtil.getAnnotationTagValue(attr);
                if (!StringUtils.isEmpty(annotation)) {
                    addAnnotation(result, attr2, annotation);
                }
            }
        }

        //Déclaration des attributs d'une classe d'associations
        if (clazz instanceof ObjectModelAssociationClass) {
            ObjectModelAssociationClass assoc = (ObjectModelAssociationClass) clazz;
            for (ObjectModelAttribute attr : assoc.getParticipantsAttributes()) {
                if (attr != null) {
                    String attrVisibility = attr.getVisibility();
                    ObjectModelModifier modifier = ObjectModelJavaModifier.fromVisibility(attrVisibility);
                    String attrType = attr.getType();
                    String attrName = attr.getName();
                    if (isEntity(attrType)) {
                        attrType += "DTO";
                    }
                    addAttribute(result, GeneratorUtil.toLowerCaseFirstLetter(attrName), attrType, null, modifier);
                }
            }
        }

        addAttribute(result,"p", PropertyChangeSupport.class,"new PropertyChangeSupport(this)",ObjectModelJavaModifier.PROTECTED,ObjectModelJavaModifier.FINAL);
    }

    protected void addOperations(ObjectModelClass result,ObjectModelClass clazz) {

        boolean generateDTOId = shouldGenerateDTOTopiaIdTagValue(clazz, model);
        ObjectModelOperation op;
        if (generateDTOId) {
            op = addOperation(result, "setTopiaId", "void", ObjectModelJavaModifier.PUBLIC);
            addParameter(op, "String", "topiaId");
            setOperationBody(op, ""
/*{
        this.topiaId = topiaId;
    }*/
            );

            op = addOperation(result, "getTopiaId", "String", ObjectModelJavaModifier.PUBLIC);
            setOperationBody(op, ""
/*{
        return topiaId;
    }*/
            );
        }

        op = addOperation(result, "addPropertyChangeListener", "void");
        addParameter(op,PropertyChangeListener.class,"listener");
        setOperationBody(op,""
/*{
        p.addPropertyChangeListener(listener);
    }*/
        );

        op = addOperation(result, "addPropertyChangeListener", "void");
        addParameter(op, String.class, "propertyName");
        addParameter(op, PropertyChangeListener.class, "listener");
        setOperationBody(op, ""
/*{
        p.addPropertyChangeListener(propertyName, listener);
    }*/
        );

        op = addOperation(result, "removePropertyChangeListener", "void");
        addParameter(op, PropertyChangeListener.class, "listener");
        setOperationBody(op, ""
/*{
        p.removePropertyChangeListener(listener);
    }*/
        );

        op = addOperation(result, "removePropertyChangeListener", "void");
        addParameter(op, String.class, "propertyName");
        addParameter(op, PropertyChangeListener.class, "listener");
        setOperationBody(op, ""
/*{
        p.removePropertyChangeListener(propertyName, listener);
    }*/
        );

        for (ObjectModelAttribute attr : clazz.getAttributes()) {

            ObjectModelAttribute reverse = attr.getReverseAttribute();

            if (!(attr.isNavigable() || hasUnidirectionalRelationOnAbstractType(reverse, model))) {
                continue;
            }

            String attrName = attr.getName();

            if (!attr.hasAssociationClass()) {
                String attrType = attr.getType();
                if (isEntity(attrType)) {
                    attrType += "DTO";
                }
                String setterName = getJavaBeanMethodName("set", attrName);
                String getterName = getJavaBeanMethodName("get", attrName);
                if (!GeneratorUtil.isNMultiplicity(attr)) {
                    op = addOperation(result, setterName, "void", ObjectModelJavaModifier.PUBLIC);
                    addParameter(op, attrType, "value");
                    setOperationBody(op, ""
/*{
        <%=attrType%> oldValue = this.<%=attrName%>;
        this.<%=attrName%> = value;
        p.firePropertyChange("<%=attrName%>", oldValue, value);
    }*/
                    );

                    op = addOperation(result, getterName, attrType, ObjectModelJavaModifier.PUBLIC);
                    setOperationBody(op, ""
/*{
        return <%=attrName%>;
    }*/
                    );

                       } else {

                    op = addOperation(result, setterName, "void", ObjectModelJavaModifier.PUBLIC);
                    addParameter(op, attrType+"[]", "values");
                    setOperationBody(op, ""
/*{
        <%=attrType%>[] oldValues = this.<%=attrName%>;
        this.<%=attrName%> = values;
        p.firePropertyChange("<%=attrName%>", oldValues, values);
    }*/
                    );

                    op = addOperation(result, getterName, attrType+"[]", ObjectModelJavaModifier.PUBLIC);
                    setOperationBody(op, ""
/*{
        return <%=attrName%>;
    }*/
                    );
                }
            } else {
                String assocAttrName = TopiaGeneratorUtil.getAssocAttrName(attr);
                String propertyName = GeneratorUtil.toLowerCaseFirstLetter(assocAttrName);
                String assocClassFQN = attr.getAssociationClass().getQualifiedName();
                String setterName = getJavaBeanMethodName("set", assocAttrName);
                String getterName = getJavaBeanMethodName("get", assocAttrName);
                if (!GeneratorUtil.isNMultiplicity(attr)) {
                    op = addOperation(result, setterName, "void", ObjectModelJavaModifier.PUBLIC);
                    addParameter(op, assocClassFQN + "DTO", "association");
                    setOperationBody(op, ""
/*{
        <%=assocClassFQN%>DTO oldAssocation= this.<%=propertyName%>;
        this.<%=propertyName%> = association;
        p.firePropertyChange("<%=attrName%>", oldAssocation, assocation);
    }*/
                    );

                    op = addOperation(result, getterName, assocClassFQN + "DTO", ObjectModelJavaModifier.PUBLIC);
                    setOperationBody(op, ""
/*{
        return <%=propertyName%>;
    }*/
                    );

                } else {
                    op = addOperation(result, setterName, "void", ObjectModelJavaModifier.PUBLIC);
                    addParameter(op, assocClassFQN + "DTO[]", "values");
                    setOperationBody(op, ""
/*{
        <%=assocClassFQN%>DTO[] oldValues = this.<%=propertyName%>;
        this.<%=propertyName%> = values;
        p.firePropertyChange("<%=attrName%>", oldValues, values);
    }*/
                    );

                    op = addOperation(result, getterName, assocClassFQN + "DTO[]", ObjectModelJavaModifier.PUBLIC);
                    setOperationBody(op, ""
/*{
        return this.<%=propertyName%>;
    }*/
                    );
                }
            }
        }

        op = addOperation(result,"toString",String.class, ObjectModelJavaModifier.PUBLIC);
        StringBuilder buffer = new StringBuilder();

        buffer.append(""
/*{
        String result = new ToStringBuilder(this).
}*/
                );

        for (Object o : clazz.getAttributes()) {
            ObjectModelAttribute attr = (ObjectModelAttribute) o;
            //FIXME possibilité de boucles (non directes)
            ObjectModelClass attrEntity = null;
            if (model.hasClass(attr.getType())) {
                attrEntity = model.getClass(attr.getType());
            }
            boolean isEntity = attrEntity != null && TopiaGeneratorUtil.isEntity(attrEntity);
            ObjectModelAttribute reverse = attr.getReverseAttribute();
            if (isEntity && (reverse == null || !reverse.isNavigable()) && !attr.hasAssociationClass() || !isEntity) {
            	String attrName = attr.getName();
                buffer.append(""
/*{            append("<%=attrName%>", this.<%=attrName%>).
}*/
                );
            }
        }
        buffer.append(""
/*{         toString();
        return result;
}*/
        );
        setOperationBody(op, buffer.toString());
    }
}
