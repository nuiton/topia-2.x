/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.topia.generator;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.codehaus.plexus.component.annotations.Component;
import org.nuiton.eugene.Template;
import org.nuiton.eugene.java.ObjectModelTransformerToJava;
import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.object.ObjectModelAttribute;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.eugene.models.object.ObjectModelEnumeration;
import org.nuiton.eugene.models.object.ObjectModelJavaModifier;
import org.nuiton.eugene.models.object.ObjectModelOperation;
import org.nuiton.eugene.models.object.ObjectModelType;
import org.nuiton.eugene.models.object.xml.ObjectModelAttributeImpl;
import org.nuiton.eugene.models.object.xml.ObjectModelEnumerationImpl;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaException;
import org.nuiton.topia.TopiaRuntimeException;
import org.nuiton.topia.framework.TopiaContextImplementor;
import org.nuiton.topia.persistence.TopiaDAO;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaEntityEnum;
import org.nuiton.topia.persistence.util.EntityOperator;
import org.nuiton.topia.persistence.util.EntityOperatorStore;
import org.nuiton.topia.persistence.util.TopiaEntityHelper;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;
import java.util.Set;


/*{generator option: parentheses = false}*/

/*{generator option: writeString = +}*/

/**
 * Created: 13 nov. 2009 09:05:17
 *
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @version $Id$
 * @since 2.3.0
 */
@Component(role = Template.class, hint = "org.nuiton.topia.generator.DAOHelperTransformer")
public class DAOHelperTransformer extends ObjectModelTransformerToJava {

    private static final Log log =
            LogFactory.getLog(DAOHelperTransformer.class);

    @Override
    public void transformFromModel(ObjectModel model) {
        String packageName = getDefaultPackageName();
        String modelName = model.getName();
        String daoHelperClazzName = modelName + "DAOHelper";
        String entityEnumName = modelName + "EntityEnum";

        List<ObjectModelClass> classes =
                TopiaGeneratorUtil.getEntityClasses(model, true);

        boolean generateOperator =
                TopiaGeneratorUtil.shouldGenerateOperatorForDAOHelper(model);

        boolean generateStandaloneEnum =
                TopiaGeneratorUtil.shouldGenerateStandaloneEnumForDAOHelper(model);

        ObjectModelClass daoHelper = createClass(daoHelperClazzName,
                                                 packageName);

        ObjectModelEnumeration entityEnum;

        if (generateStandaloneEnum) {
            if (log.isDebugEnabled()) {
                log.debug("Will generate standalone " + entityEnumName +
                     " in package " + packageName);
            }
            entityEnum = createEnumeration(entityEnumName, packageName);
            addImport(entityEnum, TopiaEntity.class);
            addImport(entityEnum, EntityOperatorStore.class);
            addImport(entityEnum, Arrays.class);
            addImport(entityEnum, TopiaRuntimeException.class);
            addImport(entityEnum, ArrayUtils.class);
            addImport(entityEnum, TopiaEntityHelper.class);

        } else {
            entityEnum = (ObjectModelEnumerationImpl)
                    addInnerClassifier(daoHelper,
                                       ObjectModelType.OBJECT_MODEL_ENUMERATION,
                                       entityEnumName
                    );
            addImport(daoHelper, TopiaRuntimeException.class);
            addImport(daoHelper, TopiaEntityEnum.class);
            addImport(daoHelper, EntityOperatorStore.class);
            addImport(daoHelper, Arrays.class);
            addImport(daoHelper, ArrayUtils.class);
            addImport(daoHelper, TopiaEntityHelper.class);
        }

        // generate DAOHelper
        createDAOHelper(model,
                        daoHelper,
                        daoHelperClazzName,
                        entityEnumName,
                        generateOperator,
                        classes
        );

        // generate TopiaEntityEnum
        createEntityEnum(entityEnum,
                         daoHelperClazzName,
                         entityEnumName,
                         generateOperator,
                         generateStandaloneEnum,
                         classes
        );
    }

    protected void createDAOHelper(ObjectModel model,
                                   ObjectModelClass daoHelper,
                                   String daoHelperClazzName,
                                   String entityEnumName,
                                   boolean generateOperator,
                                   List<ObjectModelClass> classes) {

        String modelName = model.getName();
        String modelVersion = model.getVersion();

        addImport(daoHelper, TopiaContextImplementor.class);
        addImport(daoHelper, TopiaDAO.class);
        addImport(daoHelper, TopiaEntity.class);
        addImport(daoHelper, TopiaContext.class);
        addImport(daoHelper, Array.class);

        if (generateOperator) {
            addImport(daoHelper,EntityOperator.class);
            addImport(daoHelper, EntityOperatorStore.class);
        }

        // add non public constructor
        ObjectModelOperation constructor =
                addConstructor(daoHelper, ObjectModelJavaModifier.PROTECTED);
        setOperationBody(constructor," ");

        ObjectModelOperation op;

        // getModelVersion method
        op = addOperation(daoHelper, "getModelVersion", "String", ObjectModelJavaModifier.PUBLIC, ObjectModelJavaModifier.STATIC);
        setOperationBody(op, ""
/*{
        return "<%=modelVersion%>";
    }*/
        );

        // getModelName method
        op = addOperation(daoHelper, "getModelName", "String", ObjectModelJavaModifier.PUBLIC, ObjectModelJavaModifier.STATIC);
        setOperationBody(op, ""
/*{
        return "<%=modelName%>";
    }*/
        );


        for (ObjectModelClass clazz : classes) {
            String clazzName = clazz.getName();
            String daoClazzName = clazzName + "DAO";

            // specialized getXXXDao method
            op = addOperation(daoHelper, "get" + daoClazzName, clazz.getPackageName() + '.' + daoClazzName, ObjectModelJavaModifier.PUBLIC, ObjectModelJavaModifier.STATIC);
            addParameter(op, TopiaContext.class, "context");
            addImport(daoHelper, clazz);
            addException(op, TopiaException.class);
            setOperationBody(op, ""
/*{
        TopiaContextImplementor ci = (TopiaContextImplementor) context;
        <%=daoClazzName%> result = ci.getDAO(<%=clazzName%>.class, <%=daoClazzName%>.class);
        return result;
    }*/
            );

        }

        // generic getDao method
        op = addOperation(daoHelper, "getDAO", "<T extends TopiaEntity, D extends TopiaDAO<? super T>> D", ObjectModelJavaModifier.PUBLIC, ObjectModelJavaModifier.STATIC);
        addParameter(op, TopiaContext.class, "context");
        addParameter(op, "Class<T>", "klass");
        addException(op, TopiaException.class);
        setOperationBody(op, ""
/*{
        TopiaContextImplementor ci = (TopiaContextImplementor) context;
        <%=entityEnumName%> constant = <%=entityEnumName%>.valueOf(klass);
        D dao = (D) ci.getDAO(constant.getContract());
        return dao;
    }*/
        );

        op = addOperation(daoHelper, "getDAO", "<T extends TopiaEntity, D extends TopiaDAO<? super T>> D", ObjectModelJavaModifier.PUBLIC, ObjectModelJavaModifier.STATIC);
        addParameter(op, TopiaContext.class, "context");
        addParameter(op, "T", "entity");
        addException(op, TopiaException.class);
        setOperationBody(op, ""
/*{
        TopiaContextImplementor ci = (TopiaContextImplementor) context;
        <%=entityEnumName%> constant = <%=entityEnumName%>.valueOf(entity);
        D dao = (D) ci.getDAO(constant.getContract());
        return dao;
    }*/
        );

        // getContractClass method
        op = addOperation(daoHelper, "getContractClass", "<T extends TopiaEntity> Class<T>", ObjectModelJavaModifier.PUBLIC, ObjectModelJavaModifier.STATIC);
        addParameter(op, "Class<T>", "klass");
        setOperationBody(op, ""
/*{
        <%=entityEnumName%> constant = <%=entityEnumName%>.valueOf(klass);
        return (Class<T>) constant.getContract();
    }*/
        );

        // getImplementationClass method
        op = addOperation(daoHelper, "getImplementationClass", "<T extends TopiaEntity> Class<T>", ObjectModelJavaModifier.PUBLIC, ObjectModelJavaModifier.STATIC);
        addParameter(op, "Class<T>", "klass");
        setOperationBody(op, ""
/*{
        <%=entityEnumName%> constant = <%=entityEnumName%>.valueOf(klass);
        return (Class<T>) constant.getImplementation();
    }*/
        );

        // getContractClasses method
        op = addOperation(daoHelper, "getContractClasses", "Class<? extends TopiaEntity>[]", ObjectModelJavaModifier.PUBLIC, ObjectModelJavaModifier.STATIC);
        setOperationBody(op, ""
/*{
        <%=entityEnumName%>[] values = <%=entityEnumName%>.values();
        Class<? extends TopiaEntity>[] result = (Class<? extends TopiaEntity>[]) Array.newInstance(Class.class, values.length);
        for (int i = 0; i < values.length; i++) {
            result[i] = values[i].getContract();
        }
        return result;
    }*/
        );

        // getImplementationClasses method
        op = addOperation(daoHelper, "getImplementationClasses", "Class<? extends TopiaEntity>[]", ObjectModelJavaModifier.PUBLIC, ObjectModelJavaModifier.STATIC);
        setOperationBody(op, ""
/*{
        <%=entityEnumName%>[] values = <%=entityEnumName%>.values();
        Class<? extends TopiaEntity>[] result = (Class<? extends TopiaEntity>[]) Array.newInstance(Class.class, values.length);
        for (int i = 0; i < values.length; i++) {
            result[i] = values[i].getImplementation();
        }
        return result;
    }*/
        );

        // getImplementationClassesAsString method
        op = addOperation(daoHelper, "getImplementationClassesAsString", "String", ObjectModelJavaModifier.PUBLIC, ObjectModelJavaModifier.STATIC);
        setOperationBody(op, ""
/*{
        StringBuilder buffer = new StringBuilder();
        for (Class<? extends TopiaEntity> aClass : getImplementationClasses()) {
            buffer.append(',').append(aClass.getName());
        }
        return buffer.substring(1);
    }*/
        );

        // getContracts method
        op = addOperation(daoHelper, "getContracts", entityEnumName+"[]", ObjectModelJavaModifier.PUBLIC, ObjectModelJavaModifier.STATIC);
        setOperationBody(op, ""
/*{
        return <%=entityEnumName%>.values();
    }*/
        );

        if (generateOperator) {
            // getOperator method
            op = addOperation(daoHelper, "getOperator", "<T extends TopiaEntity> EntityOperator<T>", ObjectModelJavaModifier.PUBLIC, ObjectModelJavaModifier.STATIC);
            addParameter(op,"Class<T>","klass");
            setOperationBody(op, ""
/*{
        <%=entityEnumName%> constant = <%=entityEnumName%>.valueOf(klass);
        return EntityOperatorStore.getOperator(constant);
    }*/
            );
        }
    }

    protected void createEntityEnum(ObjectModelEnumeration entityEnum,
                                    String daoHelperClazzName,
                                    String entityEnumName,
                                    boolean generateOperator,
                                    boolean generateStandaloneEnum,
                                    List<ObjectModelClass> classes) {

        ObjectModelAttributeImpl attr;
        ObjectModelOperation op;

        addInterface(entityEnum, TopiaEntityEnum.class);

        for (ObjectModelClass clazz : classes) {
            String clazzName = clazz.getName();

            boolean withNatural = false;
            boolean withNotNull = false;
            StringBuilder naturalIdsParams = new StringBuilder();
            StringBuilder notNullParams = new StringBuilder();

            Set<ObjectModelAttribute> naturalIdsAttributes = TopiaGeneratorUtil.getNaturalIdAttributes(clazz);
            for (ObjectModelAttribute attribute: naturalIdsAttributes) {
                withNatural = true;
                // attribut metier
                naturalIdsParams.append(", \"").append(attribute.getName()).append("\"");
            }
            Set<ObjectModelAttribute> notNullIdsAttributes = TopiaGeneratorUtil.getNotNullAttributes(clazz);
            for (ObjectModelAttribute attribute : notNullIdsAttributes) {
                withNotNull = true;
                // attribut not-null
                notNullParams.append(", \"").append(attribute.getName()).append("\"");
            }

            StringBuilder params = new StringBuilder(clazzName + ".class");

            String dbSchema = TopiaGeneratorUtil.getDbSchemaNameTagValue(clazz, model);
            if (dbSchema == null) {
                params.append(", null");
            } else {
                params.append(", \"").append(dbSchema.toLowerCase()).append("\"");
            }

            String dbTable  = TopiaGeneratorUtil.getDbName(clazz);
            params.append(", \"").append(dbTable.toLowerCase()).append("\"");

            if (withNotNull) {
                params.append(", new String[]{ ").append(notNullParams.substring(2)).append(" }");
            } else {
                params.append(", ArrayUtils.EMPTY_STRING_ARRAY");
            }
            if (withNatural) {
                params.append(", ").append(naturalIdsParams.substring(2));
            }
            addLiteral(entityEnum, clazzName + '(' + params.toString() + ')');

            if (generateStandaloneEnum) {
                addImport(entityEnum, clazz);
            }
        }

        attr = (ObjectModelAttributeImpl) addAttribute(entityEnum, "contract", "Class<? extends TopiaEntity>");
        attr.setDocumentation("The contract of the entity.");

        attr = (ObjectModelAttributeImpl) addAttribute(entityEnum, "dbSchemaName", "String");
        attr.setDocumentation("The optional name of database schema of the entity (if none was filled, will be {@code null}).");

        attr = (ObjectModelAttributeImpl) addAttribute(entityEnum, "dbTableName", "String");
        attr.setDocumentation("The name of the database table for the entity.");

        attr = (ObjectModelAttributeImpl) addAttribute(entityEnum, "implementationFQN", "String");
        attr.setDocumentation("The fully qualified name of the implementation of the entity.");

        attr = (ObjectModelAttributeImpl) addAttribute(entityEnum, "implementation", "Class<? extends TopiaEntity>");
        attr.setDocumentation("The implementation class of the entity (will be lazy computed at runtime).");

        attr = (ObjectModelAttributeImpl) addAttribute(entityEnum, "naturalIds", "String[]");
        attr.setDocumentation("The array of property involved in the natural key of the entity.");

        attr = (ObjectModelAttributeImpl) addAttribute(entityEnum, "notNulls", "String[]");
        attr.setDocumentation("The array of not null properties of the entity.");

        // constructor
        op = addConstructor(entityEnum, ObjectModelJavaModifier.PACKAGE);
        addParameter(op,"Class<? extends TopiaEntity >","contract");
        addParameter(op,"String","dbSchemaName");
        addParameter(op,"String","dbTableName");
        addParameter(op,"String[]","notNulls");
        addParameter(op,"String...","naturalIds");
        setOperationBody(op, ""
/*{
        this.contract = contract;
        this.dbSchemaName = dbSchemaName;
        this.dbTableName = dbTableName;
        this.notNulls = notNulls;
        this.naturalIds = naturalIds;
        implementationFQN = contract.getName() + "Impl";
    }*/
        );

        // getContract method
        op = addOperation(entityEnum, "getContract", "Class<? extends TopiaEntity>", ObjectModelJavaModifier.PUBLIC);
        addAnnotation(entityEnum,op,Override.class.getSimpleName());
        setOperationBody(op, ""
/*{
        return contract;
    }*/
        );

        // dbSchemaName method
        op = addOperation(entityEnum, "dbSchemaName", String.class, ObjectModelJavaModifier.PUBLIC);
        addAnnotation(entityEnum,op,Override.class.getSimpleName());
        setOperationBody(op, ""
/*{
        return dbSchemaName;
    }*/
        );

        // dbTableName method
        op = addOperation(entityEnum, "dbTableName", String.class, ObjectModelJavaModifier.PUBLIC);
        addAnnotation(entityEnum,op,Override.class.getSimpleName());
        setOperationBody(op, ""
/*{
        return dbTableName;
    }*/
        );

        // getNaturalIds method
        op = addOperation(entityEnum, "getNaturalIds", "String[]", ObjectModelJavaModifier.PUBLIC);
        addAnnotation(entityEnum,op,Override.class.getSimpleName());
        setOperationBody(op, ""
/*{
        return naturalIds;
    }*/
        );

        // isUseNaturalIds method
        op = addOperation(entityEnum, "isUseNaturalIds", "boolean", ObjectModelJavaModifier.PUBLIC);
        addAnnotation(entityEnum,op,Override.class.getSimpleName());
        setOperationBody(op, ""
/*{
        return naturalIds.length > 0;
    }*/
        );

        // getNotNulls method
        op = addOperation(entityEnum, "getNotNulls", "String[]", ObjectModelJavaModifier.PUBLIC);
        addAnnotation(entityEnum,op,Override.class.getSimpleName());
        setOperationBody(op, ""
/*{
        return notNulls;
    }*/
        );

        // isUseNotNulls method
        op = addOperation(entityEnum, "isUseNotNulls", "boolean", ObjectModelJavaModifier.PUBLIC);
        addAnnotation(entityEnum,op,Override.class.getSimpleName());
        setOperationBody(op, ""
/*{
        return notNulls.length > 0;
    }*/
        );

        // getImplementationFQN method
        op = addOperation(entityEnum, "getImplementationFQN","String",ObjectModelJavaModifier.PUBLIC);
        addAnnotation(entityEnum,op,Override.class.getSimpleName());
        setOperationBody(op, ""
/*{
        return implementationFQN;
    }*/
        );

        // setImplementationFQN method
        op = addOperation(entityEnum, "setImplementationFQN","void",ObjectModelJavaModifier.PUBLIC);
        addAnnotation(entityEnum,op,Override.class.getSimpleName());
        addParameter(op,"String","implementationFQN");
        if (generateOperator) {
               setOperationBody(op, ""
/*{
        this.implementationFQN = implementationFQN;
        implementation = null;
        // reinit the operators store
        EntityOperatorStore.clear();
    }*/
            );
        } else {
            setOperationBody(op, ""
/*{
        this.implementationFQN = implementationFQN;
        this.implementation = null;
    }*/
            );
        }

        // accept method
        op = addOperation(entityEnum, "accept","boolean",ObjectModelJavaModifier.PUBLIC);
        addAnnotation(entityEnum,op,Override.class.getSimpleName());
        addParameter(op,"Class<? extends TopiaEntity>","klass");
        setOperationBody(op, ""
/*{
        return <%=daoHelperClazzName%>.getContractClass(klass) == contract;
    }*/
        );

        // getImplementation method
        op = addOperation(entityEnum, "getImplementation","Class<? extends TopiaEntity>",ObjectModelJavaModifier.PUBLIC);
        addAnnotation(entityEnum,op,Override.class.getSimpleName());
        setOperationBody(op, ""
/*{
        if (implementation == null) {
        try {
                implementation = (Class<? extends TopiaEntity>) Class.forName(implementationFQN);
            } catch (ClassNotFoundException e) {
                throw new TopiaRuntimeException("could not find class " + implementationFQN, e);
            }
        }
        return implementation;
    }*/
        );

        // valueOf method
        op = addOperation(entityEnum, "valueOf", entityEnumName, ObjectModelJavaModifier.PUBLIC,ObjectModelJavaModifier.STATIC);
        addParameter(op,"TopiaEntity","entity");
        setOperationBody(op, ""
/*{
        return valueOf(entity.getClass());
    }*/
        );

        // valueOf method
        op = addOperation(entityEnum, "valueOf", entityEnumName, ObjectModelJavaModifier.PUBLIC,ObjectModelJavaModifier.STATIC);
        addParameter(op,"Class<?>","klass");
        setOperationBody(op, ""
/*{
        if (klass.isInterface()) {
           return valueOf(klass.getSimpleName());
        }

        Class<?> contractClass = TopiaEntityHelper.getContractClass(<%=entityEnumName%>.values(), (Class) klass);

        if (contractClass != null) {

            return valueOf(contractClass.getSimpleName());
        }

        throw new IllegalArgumentException("no entity defined for the class " + klass + " in : " + Arrays.toString(<%=entityEnumName%>.values()));
    }*/
        );
    }
}
