/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.topia.generator;

import org.codehaus.plexus.component.annotations.Component;
import org.nuiton.eugene.models.object.ObjectModelAttribute;
import org.nuiton.eugene.models.object.ObjectModelClassifier;
import org.nuiton.eugene.models.object.ObjectModelOperation;

/**
 * All extra stereotypes usable in topia generators.
 *
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @since 2.5
 */
public class TopiaStereoTypes {

    /**
     * Stéréotype pour les attributs considérés comme des tableaux.
     *
     * @see TopiaGeneratorUtil#hasArrayStereotype(ObjectModelAttribute)
     * @deprecated since 2.5 : only BeanTransformer use it and it is a deprecated transformer, will be remove in version 3.0
     */
    @Deprecated
    public static final String STEREOTYPE_ARRAY = "array";

    /**
     * Stéréotype pour les interfaces devant être générées sous forme de facades.
     *
     * @deprecated since 2.5 : nobydy use it, will be remove in version 3.0
     */
    @Deprecated
    public static final String STEREOTYPE_FACADE = "facade";

    /**
     * Stéréotype pour les objets devant être générées sous forme d'entités
     *
     * @see TopiaGeneratorUtil#isEntity(ObjectModelClassifier)
     * @see TopiaGeneratorUtil#hasEntityStereotype(ObjectModelAttribute)
     */
    public static final String STEREOTYPE_ENTITY = "entity";

    /**
     * Stéréotype pour les objets devant être générées sous forme de DTO.
     *
     * @see TopiaGeneratorUtil#hasDtoStereotype(ObjectModelClassifier)
     */
    public static final String STEREOTYPE_DTO = "dto";

    /**
     * Stéréotype pour les interfaces devant être générées sous forme de
     * services.
     *
     * @see ServiceTransformer
     * @see TopiaGeneratorUtil#hasServiceStereotype(ObjectModelClassifier)
     */
    public static final String STEREOTYPE_SERVICE = "service";

    /**
     * Stéréotype pour les interfaces devant être générées sous forme de DAO.
     *
     * @see TopiaGeneratorUtil#hasDaoStereotype(ObjectModelClassifier)
     * @see TopiaGeneratorUtil#hasDaoStereotype(ObjectModelOperation)
     */
    public static final String STEREOTYPE_DAO = "dao";

    /**
     * Stéréotype pour les collections avec unicité.
     *
     * @see TopiaGeneratorUtil#hasUniqueStereotype(ObjectModelAttribute)
     */
    public static final String STEREOTYPE_UNIQUE = "unique";

    /**
     * Stéréotype pour les attributs étant des clés primaires.
     *
     * @see TopiaGeneratorUtil#hasPrimaryKeyStereotype(ObjectModelAttribute)
     */
    public static final String STEREOTYPE_PRIMARYKAY = "primaryKey";

    /**
     * Stéréotype pour utiliser une collectionné indexée sur un attribut.
     *
     * @see TopiaGeneratorUtil#hasIndexedCollectionStereotype(ObjectModelAttribute)
     * @since 2.10
     */
    public static final String STEREOTYPE_INDEXED_COLLECTION = "indexedCollection";
}
