/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.topia.generator;

import org.nuiton.eugene.models.Model;
import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.object.ObjectModelAttribute;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.eugene.models.object.ObjectModelClassifier;
import org.nuiton.eugene.models.object.ObjectModelElement;
import org.nuiton.eugene.models.object.ObjectModelOperation;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.persistence.TopiaEntityContextable;
import org.nuiton.topia.persistence.TopiaEntityEnum;

/**
 * All extra tag values usable in topia generators.
 *
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @since 2.5
 */
public class TopiaTagValues {

    /**
     * Tag pour ajouter specifier le copyright d'un fichier.
     *
     * @since 2.5
     * @deprecated since 2.5 only use in a deprecated method {@link TopiaGeneratorUtil#getCopyright(Model)}
     */
    @Deprecated
    public static final String TAG_COPYRIGHT = "copyright";

    /**
     * Tag pour le type de persistence.
     *
     * @see TopiaGeneratorUtil#getPersistenceType(ObjectModelClassifier)
     * @see TopiaGeneratorUtil#getPersistenceTypeTagValue(ObjectModelClassifier)
     * @since 2.5
     */
    public static final String TAG_PERSISTENCE_TYPE = "persistenceType";

    /**
     * Tag pour que les entités etendent {@link TopiaEntityContextable} et
     * se fasse injecter le {@link TopiaContext} par rapport aux autres
     * entités qui ne l'ont pas.
     *
     * @since 2.5.3
     */
    public static final String TAG_CONTEXTABLE = "contextable";

    /**
     * Tag pour le nom du champ / entité en BD.
     *
     * @see TopiaGeneratorUtil#getDbNameTagValue(ObjectModelElement)
     * @see TopiaGeneratorUtil#getDbName(ObjectModelElement)
     * @see TopiaGeneratorUtil#getReverseDbName(ObjectModelAttribute)
     */
    public static final String TAG_DB_NAME = "dbName";

    /**
     * Tag to specify the reverse db name of an attribute in database.
     *
     * @see TopiaGeneratorUtil#getReverseDbNameTagValue(ObjectModelAttribute)
     * @see TopiaGeneratorUtil#getReverseDbName(ObjectModelAttribute)
     */
    public static final String TAG_REVERSE_DB_NAME = "reverseDbName";

    /**
     * Tag to specify the reverse db name of an attribute in database.
     *
     * @see TopiaGeneratorUtil#getManytoManyTableNameTagValue(ObjectModelAttribute)
     * @see TopiaGeneratorUtil#getManyToManyTableName(ObjectModelAttribute)
     * @since 2.9.2
     */
    public static final String TAG_MANY_TO_MANY_TABLE_NAME = "manyToManyTableName";

    /**
     * Tag pour le nom du schema en BD.
     *
     * @see TopiaGeneratorUtil#getDbSchemaNameTagValue(ObjectModelClassifier, ObjectModel)
     * @since 2.5
     */
    public static final String TAG_SCHEMA_NAME = "dbSchema";

    /**
     * Tag pour spécifier la caractère embed-xml d'une association.
     *
     * @see TopiaGeneratorUtil#getPersistenceTypeTagValue(ObjectModelClassifier)
     * @deprecated @since 2.5, use nowhere, will be remove soon
     */
    @Deprecated
    public static final String TAG_EMBED_XML = "embedXml";

    /**
     * Tag pour la taille du champ en BD.
     *
     * @see TopiaGeneratorUtil#getLengthTagValue(ObjectModelAttribute)
     * @since 2.5
     */
    public static final String TAG_LENGTH = "length";

    /**
     * Tag pour ajouter une annotation à un champ.
     *
     * @see TopiaGeneratorUtil#getAnnotationTagValue(ObjectModelAttribute)
     * @since 2.5
     */
    public static final String TAG_ANNOTATION = "annotation";

    /**
     * Tag pour specfier le type d'acces a un champ.
     *
     * @see TopiaGeneratorUtil#getAccessTagValue(ObjectModelAttribute)
     * @since 2.5
     */
    public static final String TAG_ACCESS = "access";

    /**
     * Tag pour ajouter un attribut dans une clef métier.
     *
     * @see TopiaGeneratorUtil#getNaturalIdTagValue(ObjectModelAttribute)
     * @see TopiaGeneratorUtil#isNaturalId(ObjectModelAttribute)
     * @since 2.5
     */
    public static final String TAG_NATURAL_ID = "naturalId";

    /**
     * Tag pour specifier si une clef metier est mutable.
     *
     * @see TopiaGeneratorUtil#getNaturalIdMutableTagValue(ObjectModelClassifier)
     * @see TopiaGeneratorUtil#isNaturalIdMutable(ObjectModelClass)
     * @since 2.5
     */
    public static final String TAG_NATURAL_ID_MUTABLE = "naturalIdMutable";

    /**
     * Tag pour permettre de choisir qui contrôle la relation N-N
     * bidirectionnelle. A utiliser sur les deux extremités de l'association.
     * Mettre inverse=false sur le rôle fils et inverse=true sur le rôle père.
     * Par défaut le inverse=true est placé sur le premier rôle trouvé dans
     * l'ordre alphabétique.
     *
     * @see TopiaGeneratorUtil#getInverseTagValue(ObjectModelAttribute)
     * @since 2.5
     */
    public static final String TAG_INVERSE = "inverse";

    /**
     * Tag pour spécifier la caractère lazy d'une association multiple.
     *
     * @see TopiaGeneratorUtil#getLazyTagValue(ObjectModelAttribute)
     * @since 2.5
     */
    public static final String TAG_LAZY = "lazy";

    /**
     * Tag pour spécifier la caractère fetch d'une association multiple.
     *
     * @see TopiaGeneratorUtil#getFetchTagValue(ObjectModelAttribute)
     * @since 2.5
     */
    public static final String TAG_FETCH = "fetch";

    /**
     * Tag pour spécifier la caractère order-by d'une association multiple.
     *
     * @see TopiaGeneratorUtil#getOrderByTagValue(ObjectModelAttribute)
     * @since 2.5
     */
    public static final String TAG_ORDER_BY = "orderBy";

    /**
     * Tag pour spécifier la caractère not-null d'un attribut.
     *
     * @see TopiaGeneratorUtil#getNotNullTagValue(ObjectModelAttribute)
     * @since 2.5
     */
    public static final String TAG_NOT_NULL = "notNull";

    /**
     * Tag à placer sur un l'attribut d'une entité. Cet attribut est de type
     * énumération : l'ajout de la tagValue indique qu'il faut utiliser le
     * {@code name} de l'énumération et non l'ordinal pour stocker la valeur en
     * base
     */
    public static final String TAG_USE_ENUMERATION_NAME = "useEnumerationName";

    /**
     * Tag pour configurer l'interface du proxy sur autre chose que l'implementation par defaut.
     * 
     * Par defaut :
     * null &gt; generere le proxy sur l'interface de l'implementation
     * Autre valeur :
     * "none" &gt; laisse la configuration par defaut d'hibernate
     *
     * @see TopiaGeneratorUtil#getPersistenceTypeTagValue(ObjectModelClassifier)
     * @since 2.5
     */
    public static final String TAG_PROXY_INTERFACE = "hibernateProxyInterface";

    /**
     * Tag pour spécifier le permissions à la création.
     *
     * @see TopiaGeneratorUtil#getSecurityCreateTagValue(ObjectModelClassifier)
     * @since 2.5
     */
    public static final String TAG_SECURITY_CREATE = "securityCreate";

    /**
     * Tag pour spécifier le permissions au chargement.
     *
     * @see TopiaGeneratorUtil#getSecurityLoadTagValue(ObjectModelClassifier)
     * @since 2.5
     */
    public static final String TAG_SECURITY_LOAD = "securityLoad";

    /**
     * Tag pour spécifier le permissions à la mise à jour.
     *
     * @see TopiaGeneratorUtil#getSecurityUpdateTagValue(ObjectModelClassifier)
     * @since 2.5
     */
    public static final String TAG_SECURITY_UPDATE = "securityUpdate";

    /**
     * Tag pour spécifier le permissions à la suppression.
     *
     * @see TopiaGeneratorUtil#getSecurityDeleteTagValue(ObjectModelClassifier)
     * @since 2.5
     */
    public static final String TAG_SECURITY_DELETE = "securityDelete";

    /**
     * Tag pour specifier de ne pas generer la methode toString.
     *
     * @see TopiaGeneratorUtil#getNotGenerateToStringTagValue(ObjectModelClassifier, ObjectModel)
     * @see TopiaGeneratorUtil#generateToString(ObjectModelClass, ObjectModel)
     * @since 2.5
     */
    public static final String TAG_NOT_GENERATE_TO_STRING = "notGenerateToString";

    /**
     * Tag pour specifier de trier les attributs par nom lors de la generation.
     *
     * @see TopiaGeneratorUtil#getSortAttributeTagValue(ObjectModelClassifier, ObjectModel)
     * @see TopiaGeneratorUtil#sortAttribute(ObjectModelClass, ObjectModel)
     * @since 2.5
     */
    public static final String TAG_SORT_ATTRIBUTE = "sortAttribute";

    /**
     * Tag pour specfier si on doit générer la methode getOperator dans les daohelpers.
     *
     * @see TopiaGeneratorUtil#getGenerateOperatorForDAOHelperTagValue(ObjectModel)
     * @see TopiaGeneratorUtil#shouldGenerateOperatorForDAOHelper(ObjectModel)
     * @since 2.5
     */
    public static final String TAG_GENERATE_OPERATOR_FOR_DAO_HELPER = "generateOperatorForDAOHelper";

    /**
     * Tag pour spécifier si on doit générer le {@link TopiaEntityEnum} en tant qu'inner classe
     * du dao helper ou pas.
     * 
     * <b>Note:</b> Par défaut, on génère en tant qu'inner classe.
     *
     * @see TopiaGeneratorUtil#getGenerateStandaloneEnumForDAOHelperTagValue(ObjectModel)
     * @see TopiaGeneratorUtil#shouldGenerateStandaloneEnumForDAOHelper(ObjectModel)
     * @since 2.5
     */
    public static final String TAG_GENERATE_STANDALONE_ENUM_FOR_DAO_HELPER = "generateStandaloneEnumForDAOHelper";

    /**
     * Tag pour spécifier le type d'une propriété dans le mapping hibernate.
     *
     * @see TopiaGeneratorUtil#getTypeTagValue(ObjectModelAttribute)
     * @since 2.5
     */
    public static final String TAG_TYPE = "type";

    /**
     * Tag pour spécifier le type sql d'une propriété dans le mapping hibernate.
     *
     * @see TopiaGeneratorUtil#getSqlTypeTagValue(ObjectModelAttribute)
     * @since 2.5
     */
    public static final String TAG_SQL_TYPE = "sqlType";

    /**
     * To use the legacy DAO generation.
     *
     * @see TopiaGeneratorUtil#getTypeTagValue(ObjectModelAttribute)
     * @since 2.5
     * @deprecated since 2.5, prefer use the tag value
     */
    @Deprecated
    public static final String TAG_USE_LEGACY_DAO = "useLegacyDAO";

    /**
     * To specify the abstract dao to use.
     * 
     * If none given, will use the {@code org.nuiton.topia.persistence.TopiaDAOImpl}.
     * 
     * Other value possible is {@code org.nuiton.topia.persistence.TopiaDAOLegacy}
     *
     * @see TopiaGeneratorUtil#getDaoImplementationTagValue(ObjectModel)
     * @since 2.5
     */
    public static final String TAG_DAO_IMPLEMENTATION = "daoImplementation";


    /**
     * Tag pour specifier l'exception principale de l'application.
     * Utiliser dans le ServiceTransformer ou  QueryHelperTransformer pour etre
     * automatiquement jeter
     * depuis les methodes des services.
     *
     * @see ServiceTransformer
     * @see QueryHelperTransformer
     * @see TopiaGeneratorUtil#getExceptionClassTagValue(ObjectModel)
     * @since 2.3.2
     */
    public static final String TAG_EXCEPTION_CLASS = "exceptionClass";

    // -------------------------------------------------------------------------
    // ServiceTransformer specific tag values
    // -------------------------------------------------------------------------

    /**
     * Tag pour specifier si une methode a besoin d'une transaction
     * (TopiaContext) ou non
     *
     * @see ServiceTransformer
     * @see TopiaGeneratorUtil#getTransactionTagValue(ObjectModelClassifier)
     * @see TopiaGeneratorUtil#getTransactionTagValue(ObjectModelOperation)
     * @since 2.3.1
     */
    public static final String TAG_TRANSACTION = "transaction";

    /**
     * Tag pour specifier si une methode a besoin d'un commit après son
     * exécution.
     *
     * @see ServiceTransformer
     * @see TopiaGeneratorUtil#getDoCommitTagValue(ObjectModelOperation, ObjectModel)
     * @since 2.5
     */
    public static final String TAG_DO_COMMIT = "doCommit";

    /**
     * Tag pour specifier si une methode de service a besoin d'arguments pour
     * le message d'erreur ou non
     *
     * @see ServiceTransformer
     * @see TopiaGeneratorUtil#getErrorArgsTagValue(ObjectModelOperation)
     * @since 2.3.1
     */
    public static final String TAG_ERROR_ARGS = "errorArgs";

    /**
     * Tag to specify if we want to add logs in any method of service
     * generated by {@link ServiceTransformer}.
     * 
     * <b>Note:</b> To have no log just use this tag on services or gloabaly
     * on model (for all services).
     *
     * @see ServiceTransformer
     * @see TopiaGeneratorUtil#getNoLogInServiceTagValue(ObjectModelClassifier, ObjectModel)
     * @since 2.5
     * @deprecated since 2.5.4, will not be replaced (no log are any longer
     * generated in {@link ServiceTransformer}
     */
    @Deprecated
    public static final String TAG_NO_LOG_IN_SERVICE = "noLogInService";

    /**
     * Stéréotype pour les attributs avec multiplicité nécessitant la création d'un index.
     *
     * @see TopiaGeneratorUtil#getIndexForeignKeys(ObjectModelAttribute, ObjectModel)
     * @since 2.6.5
     */
    public static final String TAG_INDEX_FOREIGN_KEYS = "indexForeignKeys";

    /**
     * Tag to generate deterministic foreign key names in hibernate mapping files.
     * 
     *
     * @see TopiaGeneratorUtil#isGenerateForeignKeyNames(ObjectModelClassifier, ObjectModel)
     * @since 2.10
     */
    public static final String TAG_GENERATE_FOREIGN_KEY_NAMES = "generateForeignKeyNames";


    /**
     * Tag to specify if we want to add an "id" property in DTO generated by
     * {@link EntityDTOTransformer}.
     * 
     *
     * @see EntityDTOTransformer
     * @see TopiaGeneratorUtil#shouldGenerateDTOTopiaIdTagValue(ObjectModelClassifier, ObjectModel)
     * @since 2.6.7
     */
    public static final String TAG_GENERATE_TOPIA_ID_IN_DTO = "generateDTOTopiaId";

    /**
     * Tag to specify if we want to generate fireOnPreRead and fireOnPostRead method into entity getters.
     *
     * @see TopiaGeneratorUtil#isDoNotGenerateReadListeners(ObjectModelAttribute, ObjectModel)
     * @since 2.9
     */
    public static final String TAG_DO_NOT_GENERATE_READ_LISTENERS = "notGenerateReadListeners";
}
