/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.topia.generator;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.codehaus.plexus.component.annotations.Component;
import org.nuiton.eugene.GeneratorUtil;
import org.nuiton.eugene.Template;
import org.nuiton.eugene.java.JavaBuilder;
import org.nuiton.eugene.java.ObjectModelTransformerToJava;
import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.object.ObjectModelAssociationClass;
import org.nuiton.eugene.models.object.ObjectModelAttribute;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.eugene.models.object.ObjectModelClassifier;
import org.nuiton.eugene.models.object.ObjectModelJavaModifier;
import org.nuiton.eugene.models.object.ObjectModelModifier;
import org.nuiton.eugene.models.object.ObjectModelOperation;
import org.nuiton.eugene.models.object.ObjectModelType;
import org.nuiton.topia.framework.TopiaQuery;
import org.nuiton.topia.persistence.TopiaEntity;

import java.util.HashMap;
import java.util.Map;

/**
 * Created: 23 juin 2010
 *
 * @author fdesbois &lt;fdesbois@codelutin.com&gt;
 * @version $Id$
 * @since 2.4
 */
@Component(role = Template.class, hint = "org.nuiton.topia.generator.QueryHelperTransformer")
public class QueryHelperTransformer extends ObjectModelTransformerToJava {

    private static final Log log = LogFactory.getLog(QueryHelperTransformer.class);

    protected ObjectModelClass helperClass;

    protected ObjectModelClass abstractEntityPropertyClass;

    protected static final String ENTITY_PROPERTY_CLASS_NAME = "EntityProperty";

    protected static final String ENTITY_PROPERTY_SUFFIX = "Property";

    protected static final String ENTITY_PROPERTY_GENERIC_TYPE = "<E>";

    protected String mainException;

    protected Map<String, String> aliases;

    public static final String CONSTANT_PREFIX = "ALIAS_";

    /*************************** MAIN PART OF THE HELPER **********************/

    @Override
    public void transformFromModel(ObjectModel model) {

        aliases = new HashMap<String, String>();

        String modelName = StringUtils.capitalize(model.getName());
        String packageName = getDefaultPackageName();
        helperClass = createClass(modelName + "QueryHelper", packageName);

        addImport(helperClass, TopiaQuery.class);
        addImport(helperClass, TopiaEntity.class);

        String exception = TopiaGeneratorUtil.getExceptionClassTagValue(model);
        if (exception != null) {
            addImport(helperClass, exception);
            mainException = TopiaGeneratorUtil.getSimpleName(exception);
        }

        initConstantPrefixFromModel();

        createInnerAbstractEntityPropertyClass();
        createUtilOperations();
    }

    protected void createInnerAbstractEntityPropertyClass() {

        abstractEntityPropertyClass =  (ObjectModelClass)addInnerClassifier(helperClass,
                ObjectModelType.OBJECT_MODEL_CLASS,
                ENTITY_PROPERTY_CLASS_NAME + ENTITY_PROPERTY_GENERIC_TYPE,
                ObjectModelJavaModifier.ABSTRACT,
                ObjectModelJavaModifier.STATIC);

        addImport(helperClass, HashMap.class);
        addImport(helperClass, Map.class);

        addAttribute(abstractEntityPropertyClass, "alias",
                String.class, null,
                ObjectModelJavaModifier.PROTECTED);

        addAttribute(abstractEntityPropertyClass, "propertiesCache",
                "Map<String, String>", null,
                ObjectModelJavaModifier.PROTECTED);

        // Constructor
        // FIXME-fdesbois-2010-06-23 : need to take care of generic case in JavaBuilder in EUGene
//        ObjectModelOperation constructor =
//                addConstructor(abstractEntityPropertyClass, ObjectModelJavaModifier.PUBLIC);
        ObjectModelOperation constructor =
                builder.addOperation(abstractEntityPropertyClass, ENTITY_PROPERTY_CLASS_NAME, null, ObjectModelJavaModifier.PUBLIC);

        setOperationBody(constructor, ""
    /*{
            propertiesCache = new HashMap<String, String>();
    }*/
        );

        // Getter and setter for alias
        ObjectModelOperation setAlias =
                addOperation(abstractEntityPropertyClass, "setAlias", "void",
                        ObjectModelJavaModifier.PROTECTED);
        addParameter(setAlias, String.class, "alias");


        setOperationBody(setAlias, ""
    /*{
            this.alias = alias;
    }*/
        );

        ObjectModelOperation getAlias =
                addOperation(abstractEntityPropertyClass, "$alias", String.class,
                        ObjectModelJavaModifier.PUBLIC);


        setOperationBody(getAlias, ""
    /*{
            return alias;
    }*/
        );

        // Getter for properties
        ObjectModelOperation getProperty =
                addOperation(abstractEntityPropertyClass, "$property", String.class,
                        ObjectModelJavaModifier.PUBLIC);
        addParameter(getProperty, String.class, "propertyName");


        setOperationBody(getProperty, ""
    /*{
            String result = propertiesCache.get(propertyName);
            if (result == null) {
                result = TopiaQuery.getProperty(alias, propertyName);
                propertiesCache.put(propertyName, result);
            }
            return result;
    }*/
        );

        ObjectModelOperation topiaCreateDate =
                addOperation(abstractEntityPropertyClass, "topiaCreateDate", String.class,
                        ObjectModelJavaModifier.PUBLIC);

        setOperationBody(topiaCreateDate, ""
    /*{
            return $property(TopiaEntity.TOPIA_CREATE_DATE);
    }*/
        );

        ObjectModelOperation topiaId =
                addOperation(abstractEntityPropertyClass, "topiaId", String.class,
                        ObjectModelJavaModifier.PUBLIC);

        setOperationBody(topiaId, ""
    /*{
            return $property(TopiaEntity.TOPIA_ID);
    }*/
        );

        ObjectModelOperation topiaVersion =
                addOperation(abstractEntityPropertyClass, "topiaVersion", String.class,
                        ObjectModelJavaModifier.PUBLIC);

        setOperationBody(topiaVersion, ""
    /*{
            return $property(TopiaEntity.TOPIA_VERSION);
    }*/
        );

        // Abstract methods
        addOperation(abstractEntityPropertyClass, "getEntityClass",
                "Class" + ENTITY_PROPERTY_GENERIC_TYPE,
                ObjectModelJavaModifier.PUBLIC, ObjectModelJavaModifier.ABSTRACT);

        addOperation(abstractEntityPropertyClass, "defaultAlias", String.class,
                ObjectModelJavaModifier.PUBLIC, ObjectModelJavaModifier.ABSTRACT);
    }

    protected void createUtilOperations() {

        // createQuery method with EntityProperty in argument
        ObjectModelOperation createQuery =
                addOperation(helperClass, "createQuery", TopiaQuery.class,
                        ObjectModelJavaModifier.PUBLIC, ObjectModelJavaModifier.STATIC);
        addParameter(createQuery, ENTITY_PROPERTY_CLASS_NAME, "property");

        setOperationBody(createQuery, ""
    /*{
        return new TopiaQuery((Class<? extends TopiaEntity>)property.getEntityClass(), property.$alias());
    }*/
        );

        // format method to format statement using $1, $2 corresponding to property names
        ObjectModelOperation format =
                addOperation(helperClass, "format", String.class,
                        ObjectModelJavaModifier.PUBLIC, ObjectModelJavaModifier.STATIC);
        addParameter(format, String.class, "statement");
        addParameter(format, "String...", "propertyNames");

        setOperationBody(format, ""
    /*{
        for (int i = 1; i <= propertyNames.length; i++) {
           statement = statement.replace("$" + i, propertyNames[i-1]);
        }
        return statement;
    }*/
        );

        // Methods to instantiate EntityProperty
        String genericType = "<P extends " + ENTITY_PROPERTY_CLASS_NAME + "> P";
        ObjectModelOperation newEntityProperty1 =
                addOperation(helperClass, "newEntityProperty", genericType,
                        ObjectModelJavaModifier.PRIVATE, ObjectModelJavaModifier.STATIC);
        addParameter(newEntityProperty1, "Class<P>", "propertyClass");

        setOperationBody(newEntityProperty1, ""
    /*{
        return newEntityProperty(propertyClass, null);
    }*/
        );
        ObjectModelOperation newEntityProperty2 =
                addOperation(helperClass, "newEntityProperty", genericType,
                        ObjectModelJavaModifier.PRIVATE, ObjectModelJavaModifier.STATIC);
        addParameter(newEntityProperty2, "Class<P>", "propertyClass");
        addParameter(newEntityProperty2, String.class, "alias");

        StringBuilder buffer = new StringBuilder(""
    /*{
        try {
            P property = propertyClass.newInstance();
            if (alias == null) {
                alias = property.defaultAlias();
            }
            property.setAlias(alias);
            return property;
        } catch (Exception eee) {
    }*/
        );

        if (mainException != null) {
            addException(newEntityProperty1, mainException);
            addException(newEntityProperty2, mainException);
            buffer.append(""
    /*{
            throw new <%=mainException%>("Error instantiate " + propertyClass.getName(), eee);
    }*/
            );
        } else {
            buffer.append(""
    /*{
            throw new Error("Error instantiate " + propertyClass.getName(), eee);
    }*/
            );
        }
            buffer.append(""
    /*{
        }
    }*/
            );

        setOperationBody(newEntityProperty2, buffer.toString());
    }

    /*************************** INNER PROPERTY CLASSES ***********************/

    @Override
//    public void transformFromClass(ObjectModelClass clazz) {
    public void transformFromClassifier(ObjectModelClassifier clazz) {
        if (!TopiaGeneratorUtil.hasEntityStereotype(clazz)) {
            return;
        }

        // Create default alias for this entity
        String aliasConstant = createAliasConstant(clazz.getName());

        // Create inner class for this entity
        ObjectModelClass entityPropertyClass = createInnerClass(clazz, aliasConstant);

        // Create methods to instantiate the inner class
        createNewOperations(entityPropertyClass);

        addExtraForSubEntity(clazz);
    }

    protected String createAliasConstant(String entityName) {

        String constantName =
               TopiaGeneratorUtil.convertVariableNameToConstantName(entityName);

        String[] words = constantName.split("_");
        String alias = "";
        // Use first letter of each word as alias
        for (String word : words) {
            alias += word.substring(0, 1);
        }

        // Case of existing alias, check other letters in lastWord
        String lastWord = words[words.length - 1];
        while(aliases.containsKey(alias)) {
            // Remove first letter
            lastWord = lastWord.substring(1, lastWord.length());
            if (!lastWord.isEmpty()) {
                // Use first letter of new lastWord to concat the alias
                alias += lastWord.charAt(0);
            } else {
                // Generate an alea char to concat
                alias += StringUtils.upperCase(RandomStringUtils.randomAlphabetic(1));
            }
        }

        String aliasPropertyName = CONSTANT_PREFIX + constantName;

        if (log.isDebugEnabled()) {
            log.debug("Add alias '" + alias + "' named " + aliasPropertyName);
        }

        aliases.put(alias, aliasPropertyName);

        addAttribute(helperClass, aliasPropertyName, String.class, "\"" + alias + "\"",
                ObjectModelJavaModifier.PUBLIC, ObjectModelJavaModifier.STATIC, ObjectModelJavaModifier.FINAL);

        return aliasPropertyName;
    }

    protected ObjectModelClass createInnerClass(ObjectModelClassifier input, String aliasConstant) {
        String className = getPropertyClassName(input);

        ObjectModelClass propertyClass = (ObjectModelClass)
                addInnerClassifier(helperClass,
                        ObjectModelType.OBJECT_MODEL_CLASS,
                        className,
                        ObjectModelJavaModifier.STATIC);

        if (log.isDebugEnabled()) {
            log.debug("Generate for entity : " + input.getQualifiedName());
        }

        // FIXME-fdesbois-2010-06-23 : need to manage imports for inner classes : EUGene ImportsManager
        addImport(helperClass, input.getQualifiedName());

        // Important to keep qualifiedName for setSuperClass
        String superClassQualifiedName = abstractEntityPropertyClass.getQualifiedName().
                replace(ENTITY_PROPERTY_GENERIC_TYPE, "<" + input.getName() + ">");

        setSuperClass(propertyClass, superClassQualifiedName);

        ObjectModelOperation constructor =
                addConstructor(propertyClass, ObjectModelJavaModifier.PROTECTED);

        setOperationBody(constructor, ""
    /*{
    }*/
        );

        ObjectModelOperation getEntityClass =
                addOperation(propertyClass, "getEntityClass", "Class<" + input.getName() + ">",
                        ObjectModelJavaModifier.PUBLIC);

        addAnnotation(propertyClass, getEntityClass, "Override");

        setOperationBody(getEntityClass, ""
    /*{
            return <%=input.getName()%>.class;
    }*/
        );

        ObjectModelOperation defaultAlias =
                addOperation(propertyClass, "defaultAlias", String.class,
                        ObjectModelJavaModifier.PUBLIC);

        addAnnotation(propertyClass, defaultAlias, "Override");

        setOperationBody(defaultAlias, ""
    /*{
            return <%=aliasConstant%>;
    }*/
        );

        createGetterOperations(input, propertyClass);

        return propertyClass;
    }

    protected void createGetterOperations(ObjectModelClassifier input, ObjectModelClass propertyClass) {

        // Generate for all attributes
        for (ObjectModelAttribute attr : input.getAttributes()) {

            // Case we don't want generation for
            if (!attr.isNavigable()) {
                continue;
            }

            String attrName = getReferenceAttributeName(attr);

            if (log.isDebugEnabled()) {
                log.debug("Entity property : name=" + attrName +
                        " _ navigable=" + attr.isNavigable() +
                        " _ maxMultiplicity=" + attr.getMaxMultiplicity() +
                        " _ associationClass=" + attr.hasAssociationClass() +
                        " _ referenceClassifier=" + attr.referenceClassifier());
            }

            ObjectModelOperation propertyNameOperation =
                    createGetPropertyNameOperation(propertyClass, attrName, input.getName());

            createGetPropertyObjectOperation(propertyClass, attr, propertyNameOperation);
        }

        // Case of Association class : generate also for participant properties
        if (input instanceof ObjectModelAssociationClass) {

            ObjectModelAssociationClass assoc = (ObjectModelAssociationClass)input;

            for (ObjectModelAttribute attr : assoc.getParticipantsAttributes()) {
                ObjectModelOperation propertyNameOperation =
                        createGetPropertyNameOperation(propertyClass, attr.getName(), input.getName());

                createGetPropertyObjectOperation(propertyClass, attr, propertyNameOperation);
            }
        }
    }

    protected ObjectModelOperation createGetPropertyNameOperation(ObjectModelClass output, String attrName, String entityClassName) {
        ObjectModelOperation result =
                    addOperation(output, attrName, String.class, ObjectModelJavaModifier.PUBLIC);

        String constantName =
                entityClassName + "." + getConstantName(attrName);

        if (log.isDebugEnabled()) {
            log.debug("Add getter for property : " + attrName +
                    " _ constantName = " + constantName +
                    " _ constantPrefix = " + getConstantPrefix());
        }

        setOperationBody(result, ""
    /*{
            return $property(<%=constantName%>);
    }*/
        );

        return result;
    }

    protected ObjectModelOperation createGetPropertyObjectOperation(ObjectModelClass output,
                                                                    ObjectModelAttribute attrReference,
                                                                    ObjectModelOperation propertyNameOperation) {

        ObjectModelClassifier referenceClass = getReferenceAttributeClassifier(attrReference);

        // No reference, can't add method to getPropertyObject
        // ANO-#1648: Enum are ignored here, they will be used as simple property
        if (referenceClass == null || referenceClass.isEnum()) {
            return null;
        }

        String operationName = getReferenceAttributeName(attrReference) +
                ENTITY_PROPERTY_SUFFIX;

        String referencePropertyClassName = getPropertyClassName(referenceClass);

        ObjectModelOperation result =
                addOperation(output, operationName, referencePropertyClassName);

        if (log.isDebugEnabled()) {
            log.debug("Extra operation : " + operationName +
                    " _ className = " + referencePropertyClassName);
        }

        setOperationBody(result, ""
    /*{
            return new<%=referencePropertyClassName%>(<%=propertyNameOperation.getName()%>());
    }*/
        );

        return result;
    }

    protected void createNewOperations(ObjectModelClass entityProperty) {

        String className = entityProperty.getName();

        String methodName = "new" + className;

        ObjectModelOperation newEntityProperty1 =
                addOperation(helperClass, "new" + className, className,
                        ObjectModelJavaModifier.PUBLIC, ObjectModelJavaModifier.STATIC);

        setOperationBody(newEntityProperty1, ""
    /*{
        return <%=methodName%>(null);
    }*/
        );

        ObjectModelOperation newEntityProperty2 =
                addOperation(helperClass, "new" + className, className,
                        ObjectModelJavaModifier.PUBLIC, ObjectModelJavaModifier.STATIC);
        addParameter(newEntityProperty2, String.class, "alias");

        setOperationBody(newEntityProperty2, ""
    /*{
        return newEntityProperty(<%=className%>.class, alias);
    }*/
        );
    }

    protected void addExtraForSubEntity(ObjectModelClassifier entityClass) {
        for (ObjectModelAttribute attr : entityClass.getAttributes()) {

            if (attr.isNavigable() && attr.referenceClassifier() &&
                    attr.getClassifier().getName().equals(entityClass.getName())) {
                // Same entity

                String propertyClassName = getPropertyClassName(entityClass);

                String subEntityName = entityClass.getName() + StringUtils.capitalize(attr.getName());
                String aliasConstant = createAliasConstant(subEntityName);

                ObjectModelOperation newEntityProperty =
                    addOperation(helperClass, "new" + subEntityName + "Property", propertyClassName,
                        ObjectModelJavaModifier.PUBLIC, ObjectModelJavaModifier.STATIC);

                String callMethodName = "new" + propertyClassName;

                setOperationBody(newEntityProperty, ""
    /*{
        return <%=callMethodName%>(<%=aliasConstant%>);
    }*/
                );

            }
        }
    }

    // Helpers
    protected String getPropertyClassName(ObjectModelClassifier entityClass) {
        return entityClass.getName() + ENTITY_PROPERTY_SUFFIX;
    }

    protected String getReferenceAttributeName(ObjectModelAttribute attrReference) {
        String attrName = attrReference.getName();
        if(attrReference.hasAssociationClass()) {
            attrName = GeneratorUtil.getAssocAttrName(attrReference);
        }
        return attrName;
    }

    protected ObjectModelClassifier getReferenceAttributeClassifier(ObjectModelAttribute attrReference) {
        ObjectModelClassifier referenceClass = null;
        // case for attribute classifier, only for maxMultiplicity = 1
        if (attrReference.referenceClassifier() && attrReference.getMaxMultiplicity() == 1) {
            referenceClass = attrReference.getClassifier();
        // case for association attribute
        } else if (attrReference.hasAssociationClass()) {
            referenceClass = attrReference.getAssociationClass();
        }
        return referenceClass;
    }

    // For tests
    protected void setBuilder(JavaBuilder builder) {
        this.builder = builder;
    }
}
