/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2019 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.topia.generator;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.eugene.AbstractGenerator;
import org.nuiton.eugene.GeneratorUtil;
import org.nuiton.eugene.java.JavaGeneratorUtil;
import org.nuiton.eugene.models.Model;
import org.nuiton.eugene.models.extension.tagvalue.TagValueUtil;
import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.object.ObjectModelAssociationClass;
import org.nuiton.eugene.models.object.ObjectModelAttribute;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.eugene.models.object.ObjectModelClassifier;
import org.nuiton.eugene.models.object.ObjectModelElement;
import org.nuiton.eugene.models.object.ObjectModelInterface;
import org.nuiton.eugene.models.object.ObjectModelOperation;
import org.nuiton.eugene.models.object.ObjectModelPackage;
import org.nuiton.eugene.models.object.ObjectModelParameter;
import org.nuiton.topia.persistence.TopiaDAOImpl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 * Classe regroupant divers méthodes utiles pour la génération des entités
 * 
 * Created: 13 déc. 2005
 *
 * @author Arnaud Thimel &lt;thimel@codelutin.com&gt;
 * @author tchemit &lt;tchemit@codelutin.com&gt;
 * @author fdesbois &lt;fdesbois@codelutin.com&gt;
 * @author chatellier &lt;chatellier@codelutin.com&gt;
 * @version $Id$
 */
public class TopiaGeneratorUtil extends JavaGeneratorUtil {

    /** Logger */
    private static final Log log = LogFactory.getLog(TopiaGeneratorUtil.class);

    /**
     * dependency to add extra operations for entity dao.
     *
     * @since 2.3.4
     */
    public static final String DEPENDENCIES_DAO = "dao";

    /** Type de persistence Hibernate */
    public static final String PERSISTENCE_TYPE_HIBERNATE = "hibernate";

    /** Type de persistence LDAP */
    public static final String PERSISTENCE_TYPE_LDAP = "ldap";

    /** Type de persistence par défaut (si aucun précisé) */
    public static final String PERSISTENCE_TYPE_DEFAULT = PERSISTENCE_TYPE_HIBERNATE;

    /** Propriété des générateurs indiquant le package par défaut */
    public static final String PROPERTY_DEFAULT_PACKAGE = "defaultPackage";

    /** Le package par défaut si aucun n'est spécifié */
    public static final String DEFAULT_PACKAGE = "org.codelutin.malo";

    /**
     * Renvoie le package par défaut pour le générateur donné
     *
     * @param generator le générateur donné
     * @return le package par défaut du générator donné
     */
    public static String getDefaultPackage(AbstractGenerator<?> generator) {
        String packageName = generator.getProperty(PROPERTY_DEFAULT_PACKAGE);
        if (packageName == null || "".equals(packageName)) {
            packageName = DEFAULT_PACKAGE;
        }
        return packageName;
    }

    /**
     * Renvoie l'interface DAO associée à la classe passée en paramètre
     *
     * @param clazz la classe à tester
     * @param model le modele utilisé
     * @return l'interface trouvée ou null sinon
     */
    public static ObjectModelInterface getDAOInterface(ObjectModelClass clazz,
                                                       ObjectModel model) {
        for (Object o : model.getInterfaces()) {
            ObjectModelInterface daoInterface = (ObjectModelInterface) o;
            if (daoInterface.getName().equals(clazz.getName() + "DAO")) {
                if (hasDaoStereotype(daoInterface)) {
                    return daoInterface;
                }
            }
        }
        return null;
    }

    /**
     * Renvoie le type de persistence pour l'élément donné. Si aucun n'est
     * trouvé, le type par défaut est utilisé
     *
     * @param element l'élément à tester
     * @return le type de persitence pour l'élément donné.
     * @deprecated since 2.5, prefer use the method {@link #getPersistenceType(ObjectModelClassifier)}
     */
    @Deprecated
    public static String getPersistenceType(ObjectModelElement element) {
        String tag = element.getTagValue(TopiaTagValues.TAG_PERSISTENCE_TYPE);
        if (tag == null) {
            tag = PERSISTENCE_TYPE_DEFAULT;
        }
        return tag;
    }

    /**
     * Renvoie le type de persistence pour le classifier donné. Si aucun n'est
     * trouvé, le type par défaut est utilisé
     *
     * @param classifier l'élément à tester
     * @return le type de persitence pour l'élément donné.
     * @since 2.5
     */
    public static String getPersistenceType(ObjectModelClassifier classifier) {
        String tag = getPersistenceTypeTagValue(classifier);
        if (StringUtils.isEmpty(tag)) {
            tag = PERSISTENCE_TYPE_DEFAULT;
        }
        return tag;
    }

    /**
     * @param attr the attribute to inspece
     * @return the name of the name in db of the reverse attribute
     * @deprecated since 2.5, prefer use the methode {@link #getReverseDbName(ObjectModelAttribute)}
     */
    @Deprecated
    public static String getReverseDBName(ObjectModelAttribute attr) {
        return getReverseDbName(attr);
    }

    /**
     * Obtain the reverse db name of an attribute.
     * 
     * Try first to get the reverse db Name from the ReverseDbname tag-vlaue, then
     * If attribute has a specific reverse attribute, use his db name, otherwise
     * suffix the db name of the attribute by {@code _id}.
     *
     * @param attr the attribute to seek
     * @return the value of the reverse name
     * @since 2.5
     */
    public static String getReverseDbName(ObjectModelAttribute attr) {

        String result = getReverseDbNameTagValue(attr);
        if (StringUtils.isEmpty(result)) {
            if (attr.getReverseAttribute() != null) {
                result = getDbName(attr.getReverseAttribute());
            } else {
                result = getDbName(attr) + "_id";
            }
        }
        return result;

    }

    /**
     * Obtain the reverse db name of a reverse attribute.
     *
     * <strong>Note that the reverse attribute can't be null here.</strong>
     * <ul>
     *     <li>Try first to get the reverse db Name from the ReverseDbname tag-value</li>
     *     <li>If not found, try then the ReverseDbname tag-value on the same attribute but from this other side of the relation</li>
     *     <li>If not found, try then just get the name of the reverse attribute</li>
     * </ul>
     * @param attr the attribute to seek
     * @return the value of the reverse db name on the revser attribute
     * @since 2.9.5.2
     */
    public static String getReverseDbNameOnReverseAttribute(ObjectModelAttribute attr) {

        ObjectModelAttribute reverseAttribute = attr.getReverseAttribute();

        if (reverseAttribute == null) {
            throw new IllegalArgumentException("The reverse attribute can't be null, but was on " + attr);
        }

        String result = getReverseDbNameTagValue(reverseAttribute);
        if (StringUtils.isEmpty(result)) {

            // Try to get it from the other site of the relation
            ObjectModelAttribute reverseAttribute2 = reverseAttribute.getClassifier().getAttribute(attr.getName());
            result = getReverseDbNameTagValue(reverseAttribute2);

        }

//        if (StringUtils.isEmpty(result)) {
//
//            result = getDbNameTagValue(reverseAttribute);
//
//        }

        if (StringUtils.isEmpty(result)) {

            result = toLowerCaseFirstLetter(reverseAttribute.getName());

        }

        return result;

    }

    /**
     * Renvoie le nom BD de l'élement passé en paramètre. Elle se base sur le
     * tag associé si il existe, sinon sur le nom de l'élément
     *
     * @param element l'élément à tester
     * @return le nom de table
     * @deprecated since 2.5, prefer use the method {@link #getDbName(ObjectModelElement)}
     */
    @Deprecated
    public static String getDBName(ObjectModelElement element) {
        return getDbName(element);
    }

    /**
     * Renvoie le nom BD de l'élement passé en paramètre. Elle se base sur le
     * tag associé si il existe, sinon sur le nom de l'élément
     *
     * @param element l'élément à tester
     * @return le nom de table
     */
    public static String getDbName(ObjectModelElement element) {
        if (element == null) {
            return null;
        }
        String value = getDbNameTagValue(element);
        if (!StringUtils.isEmpty(value)) {
            return value;
        }
        return toLowerCaseFirstLetter(element.getName());
    }

    /**
     * Cherche et renvoie le schema a utiliser sur cet element, sinon sur le
     * model.
     *
     * @param element l'élément à tester
     * @param model   le modele utilisé
     * @return le nom du schema ou null
     * @deprecated since 2.5, prefer use the method {@link #getDbSchemaNameTagValue(ObjectModelClassifier, ObjectModel)}
     */
    @Deprecated
    public static String getSchemaName(ObjectModelElement element,
                                       ObjectModel model) {
        return findTagValue(TopiaTagValues.TAG_SCHEMA_NAME, element, model);
    }

    /**
     * Cherche si le tagvalue {@link TopiaTagValues#TAG_GENERATE_OPERATOR_FOR_DAO_HELPER} a été
     * activé dans le model.
     *
     * @param model le modele utilisé
     * @return {@code true} si le tag value trouvé dans le modèle, {@code false}
     *         sinon.
     * @since 2.5
     */
    public static boolean shouldGenerateOperatorForDAOHelper(ObjectModel model) {
        String tagValue = getGenerateOperatorForDAOHelperTagValue(model);
        boolean generate = StringUtils.isNotEmpty(tagValue) &&
                           Boolean.valueOf(tagValue);
        return generate;
    }

    /**
     * Cherche si le tagvalue {@link TopiaTagValues#TAG_GENERATE_OPERATOR_FOR_DAO_HELPER} a été
     * activé dans le model.
     *
     * @param element l'élément à tester
     * @param model   le modele utilisé
     * @return {@code true} si le tag value trouvé dans le modèle, {@code false}
     *         sinon.
     * @since 2.4.1
     * @deprecated since 2.5, prefer use the method {@link #shouldGenerateStandaloneEnumForDAOHelper(ObjectModel)}
     */
    @Deprecated
    public static boolean shouldGnerateStandaloneEnumForDAOHelper(
            ObjectModelElement element,
            ObjectModel model) {
        return shouldGenerateStandaloneEnumForDAOHelper(model);
//        String tagValue = GeneratorUtil.findTagValue(
//                TopiaTagValues.TAG_GENERATE_STANDALONE_ENUM_FOR_DAO_HELPER, element, model);
//        boolean generate = GeneratorUtil.notEmpty(tagValue) &&
//                           Boolean.valueOf(tagValue);
//        return generate;
    }

    /**
     * Cherche si le tagvalue {@link TopiaTagValues#TAG_GENERATE_OPERATOR_FOR_DAO_HELPER} a été
     * activé dans le model.
     *
     * @param model le modele utilisé
     * @return {@code true} si le tag value trouvé dans le modèle, {@code false}
     *         sinon.
     * @since 2.5
     */
    public static boolean shouldGenerateStandaloneEnumForDAOHelper(ObjectModel model) {
        String tagValue = getGenerateStandaloneEnumForDAOHelperTagValue(model);
        boolean generate = StringUtils.isNotEmpty(tagValue) &&
                           Boolean.valueOf(tagValue);
        return generate;
    }

    /**
     * Cherche et renvoie la liste des attributs constituant la clef metier
     * d'une classe.
     *
     * @param clazz la classe à tester
     * @return la liste des attributs de la clef métier
     */
    public static Set<ObjectModelAttribute> getNaturalIdAttributes(
            ObjectModelClass clazz) {

        // use {@link LinkedHashSet} to keep order and prevent duplicate natural ids found
        Set<ObjectModelAttribute> results =
                new LinkedHashSet<ObjectModelAttribute>();
        for (ObjectModelAttribute attr : clazz.getAttributes()) {
            if (isNaturalId(attr)) {
                results.add(attr);
            }
        }

        // sletellier : #2050 Natural id and not null attributes are not propagated on generalized entities (http://nuiton.org/issues/2050)
        Collection<ObjectModelClass> superclasses = clazz.getSuperclasses();
        for (ObjectModelClass superClass : superclasses) {
            Set<ObjectModelAttribute> naturalIdsOfSuperClass = getNaturalIdAttributes(superClass);
            results.addAll(naturalIdsOfSuperClass);
        }
        return results;
    }

    /**
     * Cherche et renvoie la liste des attributs qui ne doivent pas etre null dans
     * une classe.
     *
     * @param clazz la classe à tester
     * @return la liste des attributs qui ne doivent pas etre null
     */
    public static Set<ObjectModelAttribute> getNotNullAttributes(
            ObjectModelClass clazz) {

        // use {@link LinkedHashSet} to keep order and prevent duplicate not null found
        Set<ObjectModelAttribute> results =
                new LinkedHashSet<ObjectModelAttribute>();
        for (ObjectModelAttribute attr : clazz.getAttributes()) {
            if (isNotNull(attr)) {
                results.add(attr);
            }
        }

        Collection<ObjectModelClass> superclasses = clazz.getSuperclasses();
        for (ObjectModelClass superClass : superclasses) {
            Set<ObjectModelAttribute> notNullOfSuperClass = getNotNullAttributes(superClass);
            results.addAll(notNullOfSuperClass);
        }
        return results;
    }

    /**
     * Test if we need to generate {@code toString} method for the given class.
     *
     * @param clazz class to test
     * @param model model
     * @return {@code true} if {@code toString} should be generated.
     *         clef métier.
     */
    public static boolean generateToString(ObjectModelClass clazz,
                                           ObjectModel model) {
        String value = getNotGenerateToStringTagValue(clazz, model);
        return StringUtils.isEmpty(value);
//        String value;
//        value = model.getTagValue(TAG_NOT_GENERATE_TO_STRING);
//        if (value != null && !value.trim().isEmpty()) {
//            return false;
//        }
//        value = clazz.getTagValue(TAG_NOT_GENERATE_TO_STRING);
//        return value == null || value.trim().isEmpty();
    }


    /**
     * Cherche et renvoie la liste des attributs constituant la clef metier
     * d'une classe.
     *
     * @param clazz la classe à tester
     * @param model le modele
     * @return la liste des attributs de la clef métier ou null si pas de
     *         clef métier.
     */
    public static boolean sortAttribute(ObjectModelClass clazz,
                                        ObjectModel model) {
        String value = getSortAttributeTagValue(clazz, model);
        return "true".equals(value);
//        String value;
//        value = clazz.getTagValue(TopiaTagValues.TAG_SORT_ATTRIBUTE);
//        if (value == null || value.trim().isEmpty() ||
//            "false".equals(value.trim())) {
//            return false;
//        }
//        if ("true".equals(value.trim())) {
//            return true;
//        }
//
//        value = model.getTagValue(TopiaTagValues.TAG_SORT_ATTRIBUTE);
//        if (value == null || value.trim().isEmpty() ||
//            "false".equals(value.trim())) {
//            return false;
//        }
//        if ("true".equals(value.trim())) {
//            return true;
//        }
//        return true;
    }

    /**
     * Detecte si un attribut fait partie d'une clef metier.
     *
     * @param attribute l'attribut à tester
     * @return {@code true} si l'attribut fait partie d'une clef metier,
     *         <code>false</code> sinon.
     */
    public static boolean isNaturalId(ObjectModelAttribute attribute) {
        String value = getNaturalIdTagValue(attribute);
        if (StringUtils.isEmpty(value)) {
            // valeur null, donc pas positionnee
            return false;
        }
        try {
            return Boolean.valueOf(value.trim());
        } catch (Exception e) {
            // on a pas reussi a convertir en boolean.
            //todo peut-être declancher une exception ?
            return false;
        }
    }

    /**
     * Detecte si un attribut est marqué comme non null.
     * Les naturalId {@link #isNaturalId} sont not null par défaut
     *
     * @param attribute l'attribut à tester
     * @return {@code true} si l'attribut doit être non null,
     *         par défaut pour les naturalId, {@code false} sinon..
     * @since 2.6.9
     */
    public static boolean isNotNull(ObjectModelAttribute attribute) {
        String value = getNotNullTagValue(attribute);
        if (StringUtils.isEmpty(value)) {
            // valeur null, donc pas positionnee
            return isNaturalId(attribute);
        }
        try {
            return Boolean.valueOf(value.trim());
        } catch (Exception e) {
            // on a pas reussi a convertir en boolean.
            //todo peut-être declancher une exception ?
            return false;
        }
    }

    /**
     * Cherches et renvoie le copyright a utiliser sur le model.
     *
     * @param model le modele utilisé
     * @return le texte du copyright ou null$
     * @deprecated since 2.5 never use anywhere
     */
    @Deprecated
    public static String getCopyright(Model model) {
        return findTagValue(TopiaTagValues.TAG_COPYRIGHT, null, model);
    }

    public static <Type extends ObjectModelElement> Collection<Type> getElementsWithStereotype(
            Collection<Type> elements, String... stereotypes) {
        Collection<Type> result = new ArrayList<Type>();
        for (Type element : elements) {
            if (hasStereotypes(element, stereotypes)) {
                result.add(element);
            }
        }
        return result;
    }

    public static boolean hasStereotypes(ObjectModelElement element,
                                         String... stereotypes) {
        for (String stereotype : stereotypes) {
            if (!element.hasStereotype(stereotype)) {
                return false;
            }
        }
        return true;
    }

    public static String getPrimaryKeyAttributesListDeclaration(
            ObjectModelClass clazz, boolean includeName) {
        String attributes = "";
        Collection<ObjectModelAttribute> attributeCollection;
        attributeCollection = getElementsWithStereotype(clazz.getAttributes(),
                                                        TopiaStereoTypes.STEREOTYPE_PRIMARYKAY);
        for (ObjectModelAttribute attr : attributeCollection) {
            attributes += attr.getType();
            if (includeName) {
                attributes += ' ' + attr.getName();
            }
            attributes += ", ";
        }
        if (attributes.length() > 0) {
            attributes = attributes.substring(0, attributes.length() - 2);
        }
        return attributes;
    }

//    public static String capitalize(String s) {
//        return StringUtils.capitalize(s);
//    }

    public static boolean isAssociationClassDoublon(ObjectModelAttribute attr) {
        return attr.getReverseAttribute() != null &&
               attr.getDeclaringElement().equals(
                       attr.getReverseAttribute().getDeclaringElement()) &&
               !GeneratorUtil.isFirstAttribute(attr);
    }

    public static String getDOType(ObjectModelElement elem, ObjectModel model) {
        String type = elem.getName();
        if (elem instanceof ObjectModelAttribute) {
            type = ((ObjectModelAttribute) elem).getType();
        }
        if (elem instanceof ObjectModelClass) {
            type = ((ObjectModelClass) elem).getQualifiedName();
        }
        return getDOType(type, model);
    }

    public static String getDOType(String type, ObjectModel model) {
        if (!model.hasClass(type)) {
            return type;
        }
        ObjectModelClass clazz = model.getClass(type);
        if (isEntity(clazz)) {
            //tchemit-2011-09-12 What ever abstract or not, we alwyas use an Impl
            type += "Impl";
//            if (shouldBeAbstract(clazz)) {
//                type += "Abstract";
//            } else {
//                type += "Impl";
//            }
        }
        return type;
    }

    private static final Set<String> numberTypes = new HashSet<String>();

    private static final Set<String> textTypes = new HashSet<String>();

    private static final Set<String> booleanTypes = new HashSet<String>();

    private static final Set<String> primitiveTypes = new HashSet<String>();

    private static final String VOID_TYPE = "void";

    static {
        numberTypes.add("byte");
        numberTypes.add("java.lang.Byte");
        numberTypes.add("Byte");
        numberTypes.add("short");
        numberTypes.add("java.lang.Short");
        numberTypes.add("Short");
        numberTypes.add("int");
        numberTypes.add("java.lang.Integer");
        numberTypes.add("Integer");
        numberTypes.add("long");
        numberTypes.add("java.lang.Long");
        numberTypes.add("Long");
        numberTypes.add("float");
        numberTypes.add("java.lang.Float");
        numberTypes.add("Float");
        numberTypes.add("double");
        numberTypes.add("java.lang.Double");
        numberTypes.add("Double");

        textTypes.add("char");
        textTypes.add("java.lang.Char");
        textTypes.add("Char");
        textTypes.add("java.lang.String");
        textTypes.add("String");

        booleanTypes.add("boolean");
        booleanTypes.add("java.lang.Boolean");
        booleanTypes.add("Boolean");

        primitiveTypes.addAll(numberTypes);
        primitiveTypes.addAll(textTypes);
        primitiveTypes.addAll(booleanTypes);
    }

    public static boolean isNumericType(ObjectModelAttribute attr) {
        return numberTypes.contains(attr.getType());
    }

    public static boolean isTextType(ObjectModelAttribute attr) {
        return textTypes.contains(attr.getType());
    }

    public static boolean isDateType(ObjectModelAttribute attr) {
        return "java.util.Date".equals(attr.getType());
    }

    public static boolean isBooleanType(ObjectModelAttribute attr) {
        return booleanTypes.contains(attr.getType());
    }

    public static boolean isPrimitiveType(ObjectModelAttribute attr) {
        return primitiveTypes.contains(attr.getType());
    }

    /**
     * Indique si la classe specifiee n'a aucune ou que des methodes abstraites
     *
     * @param clazz l'instance de ObjectModelClass
     * @return true si la classe n'a que des operations abstraite ou aucune
     *         operation
     */
    public static boolean hasNothingOrAbstractMethods(ObjectModelClass clazz) {
        boolean result = true;
        Iterator<?> operations = clazz.getOperations().iterator();
        while (result && operations.hasNext()) {
            ObjectModelOperation op = (ObjectModelOperation) operations.next();
            result = op.isAbstract();
        }
        return result;
    }

    /**
     * Indique si la classe specifiee devrait etre abstraite
     *
     * @param clazz l'instance de ObjectModelClass
     * @return true dans ce cas, false sinon
     */
    public static boolean shouldBeAbstract(ObjectModelClass clazz) {
        return clazz != null && clazz.isAbstract() &&
               hasNothingOrAbstractMethods(clazz);
    }

    /**
     * <p>
     * Cette méthode permet de détecter si
     * - l'attribut représente une relation 1-n
     * - cette relation est unidirectionnelle
     * - le type de l'attribut représente un entité
     * - cette entité a des sous-classes dans le modèle
     * 
     * Ce cas correspond à une incompatibilité d'Hibernate qui nous oblige a
     * adopter un comportement particulier.
     * </p>
     *
     * @param attr  l'attribut a tester
     * @param model le model
     * @return true si et seulement si il s'agit bien de ce type de relation
     */
    public static boolean hasUnidirectionalRelationOnAbstractType(
            ObjectModelAttribute attr, ObjectModel model) {
        ObjectModelAttribute reverse = attr.getReverseAttribute();
        //relation 1-n
        if (reverse != null && isNMultiplicity(attr) &&
            !isNMultiplicity(reverse)) {
            //Pas de navigabilité
            if (!reverse.isNavigable()) {
                //Il s'agit d'une entity
                ObjectModelClass clazz = model.getClass(attr.getType());
                if (clazz != null && isEntity(clazz)) {
                    //Cette classe a des sous-classes dans le modèle
                    for (ObjectModelClass subClass : model.getClasses()) {
                        if (subClass.getSuperclasses().contains(clazz)) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    /**
     * Renvoie le nom unique de table pour une relation ManyToMany en fonction
     * de l'attribut <code>attr</code>.
     *
     * @param attr l'attribut servant de base au calcul du nom
     * @return le nom de la table
     */
    public static String getManyToManyTableName(ObjectModelAttribute attr) {
        String result;

        if (attr.hasAssociationClass()) {
            result = getDbName(attr.getAssociationClass());
        } else {
            result = getManytoManyTableNameTagValue(attr);
            if (StringUtils.isEmpty(result)) {
                String name = attr.getName();
                String revers = attr.getReverseAttributeName();

                if (name.compareToIgnoreCase(revers) < 0) {
                    result = name + '_' + revers;
                } else {
                    result = revers + '_' + name;
                }
            }
        }
        return result.toLowerCase();
    }

    /**
     * Renvoie le type d'interface à utiliser en fonction de l'attribut
     *
     * @param attr l'attribut a traiter
     * @return String
     */
    public static String getNMultiplicityInterfaceType(
            ObjectModelAttribute attr) {
        if (hasUniqueStereotype(attr)) {
            return Set.class.getName();
        } else if (hasIndexedCollectionStereotype(attr) || attr.isOrdered()) {
            return List.class.getName();
        }
        return Collection.class.getName();
    }

    /**
     * Renvoie le type d'objet (instance) à utiliser en fonction de l'attribut
     *
     * @param attr l'attribut a traiter
     * @return String
     */
    public static String getNMultiplicityObjectType(ObjectModelAttribute attr) {
        if (hasUniqueStereotype(attr)) {
            return HashSet.class.getName();
        } else if (hasIndexedCollectionStereotype(attr) || attr.isOrdered()) {
            //On considère qu'on ne sait pas traiter vraiment l'attribut "ordered"
            //  puisqu'on va conserver l'ordre d'insertion, et non un ordre en
            //  fonction d'un élément donné. Donc on renvoi une ArrayList
            return ArrayList.class.getName();
        }
        LinkedList.class.getName();
        return ArrayList.class.getName();
    }

    /**
     * Renvoie le type d'interface à utiliser en fonction de l'attribut
     *
     * @param attr l'attribut a traiter
     * @return String
     */
    public static String getNMultiplicityHibernateType(
            ObjectModelAttribute attr) {
        if (hasUniqueStereotype(attr)) {
            return "set";
        } else if (hasIndexedCollectionStereotype(attr) ) {
            return "list";
        }
        //attr.isOrdered() - On génère le ordered en bag  
        return "bag";
    }

    /**
     * Obtain the list of entities classes with the possibility to sort the
     * result.
     *
     * @param model the current model to scan
     * @param sort  flag to allow sort the result
     * @return the list of filtred classes by their stereotype
     */
    public static List<ObjectModelClass> getEntityClasses(ObjectModel model,
                                                          boolean sort) {
        return getClassesByStereotype(TopiaStereoTypes.STEREOTYPE_ENTITY, model, sort);
    }

    /**
     * Obtain the list of classes for a given stereotype with the possibility
     * to sort the result.
     *
     * @param stereotype filter stereotype
     * @param model      the current model to scan
     * @param sort       flag to allow sort the result
     * @return the list of filtred classes by their stereotype
     */
    public static List<ObjectModelClass> getClassesByStereotype(
            String stereotype, ObjectModel model, boolean sort) {
        List<ObjectModelClass> classes = new ArrayList<ObjectModelClass>();
        for (ObjectModelClass clazz : model.getClasses()) {
            if (clazz.hasStereotype(stereotype)) {
                classes.add(clazz);
            }
        }
        if (sort && !classes.isEmpty()) {

            Collections.sort(classes, OBJECT_MODEL_CLASS_COMPARATOR);
        }
        return classes;
    }

    static public final Comparator<ObjectModelClass>
            OBJECT_MODEL_CLASS_COMPARATOR =
            new Comparator<ObjectModelClass>() {

                @Override
                public int compare(ObjectModelClass o1,
                                   ObjectModelClass o2) {
                    return o1.getQualifiedName().compareTo(
                            o2.getQualifiedName());
                }
            };

    /**
     * Detecte si la clef metier d'une classe est mutable ou pas.
     * 
     * On respecte la valeur par defaut d'hibernate, à savoir que par default
     * une clef metier est non mutable.
     *
     * @param clazz la classe a tester
     * @return {@code true} si le tag value a ete positionne sur la classe
     *         via le tag {@link TopiaTagValues#TAG_NATURAL_ID_MUTABLE}, {@code false}
     *         sinon.
     */
    public static boolean isNaturalIdMutable(ObjectModelClass clazz) {
        String value = getNaturalIdMutableTagValue(clazz);
        if (StringUtils.isEmpty(value)) {
            // valeur null, donc par default positionnee
            return false;
        }
        try {
            return Boolean.valueOf(value.trim());
        } catch (Exception e) {
            // on a pas reussi a convertir en boolean.
            //todo peut-être declancher une exception ?
            return false;
        }
    }

    /**
     * Retourne true si le tagValue {@link TopiaTagValues#TAG_CONTEXTABLE}
     * à la valeur {@code true}.
     *
     * @param classifier classifier to test
     * @return {@code true} si {@link TopiaTagValues#TAG_CONTEXTABLE} == {@code true}
     */
    public static boolean isContextable(ObjectModelClassifier classifier) {
        boolean result = false;
        String value = classifier.getTagValue(TopiaTagValues.TAG_CONTEXTABLE);
        if (StringUtils.equalsIgnoreCase(value, "true")) {
            result = true;
        }
        return result;
    }

    /**
     * Obtain the list of fqn of object involed in the given class.
     *
     * @param aClass       the clazz to inspect
     * @param incomingFqns incoming fqns
     * @return the list of fqn of attributes
     */
    public static List<String> getImports(ObjectModelClass aClass,
                                          String... incomingFqns) {
        Set<String> tmp = new HashSet<String>();
        tmp.addAll(Arrays.asList(incomingFqns));
        getImports(aClass, tmp);
        List<String> result = cleanImports(aClass.getPackageName(), tmp);
        return result;
    }

    /**
     * Obtain the list of fqn of object involed in the given interface.
     *
     * @param anInterface  the interface to inspect
     * @param incomingFqns incoming fqns
     * @return the list of fqn of attributes
     */
    public static List<String> getImports(ObjectModelInterface anInterface,
                                          String... incomingFqns) {
        Set<String> tmp = new HashSet<String>();
        tmp.addAll(Arrays.asList(incomingFqns));
        getImports(anInterface, tmp);
        List<String> result = cleanImports(anInterface.getPackageName(), tmp);
        return result;
    }

    /**
     * Obtain the list of fqn of object involed in the given class.
     *
     * @param aClass the class to inspect
     * @param fqns   where to store found fqns
     */
    protected static void getImports(ObjectModelClass aClass,
                                     Set<String> fqns) {
        // scan attributes
        for (ObjectModelAttribute attr : aClass.getAttributes()) {
            fqns.add(attr.getType());
            if (isNMultiplicity(attr)) {
                String collectionType = getNMultiplicityInterfaceType(attr);
                fqns.add(collectionType);
                String collectionObject = getNMultiplicityObjectType(attr);
                fqns.add(collectionObject);
            }
        }
        for (ObjectModelAttribute attribute : aClass.getAllOtherAttributes()) {
            fqns.add(attribute.getType());
        }
        // scan associations
        if (aClass instanceof ObjectModelAssociationClass) {
            ObjectModelAssociationClass assoc =
                    (ObjectModelAssociationClass) aClass;
            for (ObjectModelAttribute attr :
                    assoc.getParticipantsAttributes()) {
                if (attr == null) {
                    continue;
                }
                fqns.add(attr.getType());
                if (isNMultiplicity(attr)) {
                    String collectionType = getNMultiplicityInterfaceType(attr);
                    fqns.add(collectionType);
                    String collectionObject = getNMultiplicityObjectType(attr);
                    fqns.add(collectionObject);
                }
            }
        }
        // scan operations
        for (ObjectModelOperation operation : aClass.getOperations()) {
            getImports(operation, fqns);
        }
        // scan super interfaces
        for (ObjectModelInterface modelInterface : aClass.getInterfaces()) {
            fqns.add(modelInterface.getQualifiedName());
            getImports(modelInterface, fqns);
        }
        // scan super classes
        for (ObjectModelClass modelClass : aClass.getSuperclasses()) {
            fqns.add(modelClass.getQualifiedName());
            getImports(modelClass);
        }
    }

    /**
     * Obtain the list of fqn of object involed in the given interface.
     *
     * @param anInterface the interface to inspect
     * @param fqns        where to store found fqns
     */
    protected static void getImports(ObjectModelInterface anInterface,
                                     Set<String> fqns) {
        // scan operations
        for (ObjectModelOperation operation : anInterface.getOperations()) {
            getImports(operation, fqns);
        }
        // scan super interfaces
        for (ObjectModelInterface modelInterface : anInterface.getInterfaces()) {
            fqns.add(modelInterface.getQualifiedName());
            getImports(modelInterface, fqns);
        }
    }

    /**
     * Obtain the fqn's list of all involed type in a givne operation.
     *
     * @param operation operation to inspect
     * @param fqns      where to store found fqns
     */
    protected static void getImports(ObjectModelOperation operation,
                                     Set<String> fqns) {
        String fqn = operation.getReturnType();
        fqns.add(fqn);
        for (ObjectModelParameter parameter : operation.getParameters()) {
            fqns.add(parameter.getType());
        }
    }

    /**
     * Clean a set of fqns, transform it into a {@link List} and sort it.
     *
     * @param packageName the current package name
     * @param fqns        the dirty set of fqns
     * @return the sorted cleaned list of fqns.
     */
    protected static List<String> cleanImports(String packageName,
                                               Set<String> fqns) {
        fqns.removeAll(primitiveTypes);
        fqns.remove(VOID_TYPE);
        int packageLength = packageName.length();
        List<String> genericType = new ArrayList<String>();
        for (Iterator<String> it = fqns.iterator(); it.hasNext(); ) {
            String fqn = it.next();
            int lastIndex = fqn.lastIndexOf(".");
            if (lastIndex == packageLength && fqn.startsWith(packageName)) {
                // same package
                it.remove();
                continue;
            }
            int genericIndex = fqn.indexOf('<');
            if (genericIndex != -1) {
                genericType.add(fqn.substring(0, genericIndex));
                it.remove();
            }
        }
        fqns.addAll(genericType);

        ArrayList<String> result = new ArrayList<String>(fqns);
        Collections.sort(result);
        return result;
    }

    /**
     * Obtain the class to use as abstract dao.
     * 
     * It will look after a tag value {@link TopiaTagValues#TAG_DAO_IMPLEMENTATION} in model
     * and if not found will use the default value which is {@link TopiaDAOImpl}.
     *
     * @param model the model which could contains
     * @return the type of the abstract dao to use
     * @since 2.5
     */
    public static Class<?> getDAOImplementation(ObjectModel model) {
        String daoImpl = getDaoImplementationTagValue(model);
        Class<?> result;
        if (StringUtils.isEmpty(daoImpl)) {

            // use the default dao implementation of topia
            result = TopiaDAOImpl.class;
        } else {
            try {
                result = Class.forName(daoImpl);
            } catch (ClassNotFoundException e) {
                String message = "Could not find dao implementation named " + daoImpl;
                log.error(message);
                throw new IllegalStateException(message, e);
            }
        }
        return result;
    }

    public static Map<ObjectModelClass, Set<ObjectModelClass>>
    searchDirectUsages(ObjectModel model) {
        List<ObjectModelClass> allEntities;
        Map<String, ObjectModelClass> allEntitiesByFQN;
        Map<ObjectModelClass, Set<ObjectModelClass>> usages;

        allEntities = getEntityClasses(model, true);

        allEntitiesByFQN = new TreeMap<String, ObjectModelClass>();
        usages = new LinkedHashMap<ObjectModelClass, Set<ObjectModelClass>>();

        // prepare usages map and fill allEntitiesByFQN map
        for (ObjectModelClass klass : allEntities) {
            usages.put(klass, new HashSet<ObjectModelClass>());
            allEntitiesByFQN.put(klass.getQualifiedName(), klass);
        }

        // first pass to detect direct usages
        for (ObjectModelClass klass : allEntities) {
            searchDirectUsages(klass, allEntitiesByFQN, usages);
        }
        allEntities.clear();
        allEntitiesByFQN.clear();
        return usages;

    }

    public static void searchDirectUsages(
            ObjectModelClass klass,
            Map<String, ObjectModelClass> allEntitiesByFQN,
            Map<ObjectModelClass, Set<ObjectModelClass>> usages) {

        if (log.isDebugEnabled()) {
            log.debug("for entity " + klass.getQualifiedName());
        }
        for (ObjectModelAttribute attr : klass.getAttributes()) {
            if (!attr.isNavigable()) {
                // skip this case
                continue;
            }
            String type;
            if (attr.hasAssociationClass()) {
                type = attr.getAssociationClass().getQualifiedName();
            } else {
                type = attr.getType();
            }
            if (!allEntitiesByFQN.containsKey(type)) {
                // not a entity, can skip for this attribute
                continue;
            }
            if (log.isDebugEnabled()) {
                log.debug(" uses " + type);
            }
            // register the klass as using the targetEntity
            ObjectModelClass targetEntity = allEntitiesByFQN.get(type);
            Set<ObjectModelClass> classes = usages.get(targetEntity);
            classes.add(klass);
        }
    }

    public static boolean isImportNeeded(Collection<ObjectModelOperation> operations,
                                         String importName) {
        if (CollectionUtils.isNotEmpty(operations)) {
            for (ObjectModelOperation op : operations) {
                if (op.getReturnType().contains(importName)) {
                    return true;
                }
                for (ObjectModelParameter param : op.getParameters()) {
                    if (param.getType().contains(importName)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public static boolean isCollectionNeeded(
            Collection<ObjectModelOperation> operations) {
        return isImportNeeded(operations,
                              Collection.class.getSimpleName());
    }

    public static boolean isSetNeeded(Collection<ObjectModelOperation> operations) {
        return isImportNeeded(operations,
                              Set.class.getSimpleName());
    }

    /**
     * Check if the given classifier has the
     * {@link TopiaStereoTypes#STEREOTYPE_FACADE} stereotype.
     *
     * @param classifier classifier to test
     * @return {@code true} if stereotype was found, {@code false otherwise}
     * @see TopiaStereoTypes#STEREOTYPE_FACADE
     * @since 2.5
     */
    public static boolean hasFacadeStereotype(ObjectModelClassifier classifier) {
        return classifier.hasStereotype(TopiaStereoTypes.STEREOTYPE_FACADE);
    }

    /**
     * Check if the given classifier has the
     * {@link TopiaStereoTypes#STEREOTYPE_ENTITY} stereotype.
     *
     * @param classifier classifier to test
     * @return {@code true} if stereotype was found, {@code false} otherwise
     * @see TopiaStereoTypes#STEREOTYPE_ENTITY
     * @since 2.5
     */
    public static boolean hasEntityStereotype(ObjectModelClassifier classifier) {
        return classifier.hasStereotype(TopiaStereoTypes.STEREOTYPE_ENTITY);
    }

    /**
     * Check if the given attribute type is an entity.
     *
     * @param attribute attribute to test
     * @param model     model containing the attribute
     * @return {@code true} if type of attribute is an entity,
     *         {@code false} otherwise
     * @see TopiaStereoTypes#STEREOTYPE_ENTITY
     * @since 2.7
     */
    public static boolean isEntity(ObjectModelAttribute attribute,
                                   ObjectModel model) {
        if (isPrimitiveType(attribute)) {
            return false;
        }
        String attributeType = attribute.getType();
        ObjectModelClassifier typeclassifier =
                model.getClassifier(attributeType);
        return typeclassifier != null && isEntity(typeclassifier);
    }

    /**
     * Check if the given classifier has the
     * {@link TopiaStereoTypes#STEREOTYPE_ENTITY} and is not an enumeration
     *
     * @param classifier classifier to test
     * @return {@code true} if stereotype was found and classifier is not
     *         enumeration, {@code false} otherwise
     * @see TopiaStereoTypes#STEREOTYPE_ENTITY
     * @since 2.5
     */
    public static boolean isEntity(ObjectModelClassifier classifier) {
        return hasEntityStereotype(classifier) && !classifier.isEnum();
    }


    /**
     * Check if the given attribute has the
     * {@link TopiaStereoTypes#STEREOTYPE_ENTITY} stereotype.
     *
     * @param attribute attribute to test
     * @return {@code true} if stereotype was found, {@code false otherwise}
     * @see TopiaStereoTypes#STEREOTYPE_ENTITY
     * @since 2.5
     */
    public static boolean hasEntityStereotype(ObjectModelAttribute attribute) {
        return attribute.hasStereotype(TopiaStereoTypes.STEREOTYPE_ENTITY);
    }

    /**
     * Check if the given classifier has the
     * {@link TopiaStereoTypes#STEREOTYPE_DTO} stereotype.
     *
     * @param classifier classifier to test
     * @return {@code true} if stereotype was found, {@code false otherwise}
     * @see TopiaStereoTypes#STEREOTYPE_DTO
     * @since 2.5
     */
    public static boolean hasDtoStereotype(ObjectModelClassifier classifier) {
        return classifier.hasStereotype(TopiaStereoTypes.STEREOTYPE_DTO);
    }

    /**
     * Check if the given classifier has the
     * {@link TopiaStereoTypes#STEREOTYPE_SERVICE} stereotype.
     *
     * @param classifier classifier to test
     * @return {@code true} if stereotype was found, {@code false otherwise}
     * @see TopiaStereoTypes#STEREOTYPE_SERVICE
     * @since 2.5
     */
    public static boolean hasServiceStereotype(ObjectModelClassifier classifier) {
        return classifier.hasStereotype(TopiaStereoTypes.STEREOTYPE_SERVICE);
    }

    /**
     * Check if the given classifier has the
     * {@link TopiaStereoTypes#STEREOTYPE_DAO} stereotype.
     *
     * @param classifier classifier to test
     * @return {@code true} if stereotype was found, {@code false otherwise}
     * @see TopiaStereoTypes#STEREOTYPE_DAO
     * @since 2.5
     */
    public static boolean hasDaoStereotype(ObjectModelClassifier classifier) {
        return classifier.hasStereotype(TopiaStereoTypes.STEREOTYPE_DAO);
    }

    /**
     * Check if the given operation has the
     * {@link TopiaStereoTypes#STEREOTYPE_DAO} stereotype.
     *
     * @param operation operation to test
     * @return {@code true} if stereotype was found, {@code false otherwise}
     * @see TopiaStereoTypes#STEREOTYPE_DAO
     * @since 2.5
     */
    public static boolean hasDaoStereotype(ObjectModelOperation operation) {
        return operation.hasStereotype(TopiaStereoTypes.STEREOTYPE_DAO);
    }

    /**
     * Check if the given attribute has the
     * {@link TopiaStereoTypes#STEREOTYPE_UNIQUE} stereotype.
     *
     * @param attribute attribute to test
     * @return {@code true} if stereotype was found, {@code false otherwise}
     * @see TopiaStereoTypes#STEREOTYPE_UNIQUE
     * @since 2.5
     */
    public static boolean hasUniqueStereotype(ObjectModelAttribute attribute) {
        return attribute.hasStereotype(TopiaStereoTypes.STEREOTYPE_UNIQUE);
    }

    /**
     * Check if the given attribute has the
     * {@link TopiaStereoTypes#STEREOTYPE_PRIMARYKAY} stereotype.
     *
     * @param attribute attribute to test
     * @return {@code true} if stereotype was found, {@code false otherwise}
     * @see TopiaStereoTypes#STEREOTYPE_PRIMARYKAY
     * @since 2.5
     */
    public static boolean hasPrimaryKeyStereotype(ObjectModelAttribute attribute) {
        return attribute.hasStereotype(TopiaStereoTypes.STEREOTYPE_PRIMARYKAY);
    }

    /**
     * Check if the given attribute has the
     * {@link TopiaStereoTypes#STEREOTYPE_ARRAY} stereotype.
     *
     * @param attribute attribute to test
     * @return {@code true} if stereotype was found, {@code false otherwise}
     * @see TopiaStereoTypes#STEREOTYPE_ARRAY
     * @since 2.5
     * @deprecated since 2.5 , only BeanTransformer use it and it is a deprecated transformer, will be remove in version 3.0
     */
    @Deprecated
    public static boolean hasArrayStereotype(ObjectModelAttribute attribute) {
        return attribute.hasStereotype(TopiaStereoTypes.STEREOTYPE_ARRAY);
    }

    /**
     * Check if the given attribute has the {@link TopiaStereoTypes#STEREOTYPE_INDEXED_COLLECTION} stereotype.
     *
     * @param attribute attribute to test
     * @return {@code true} if stereotype was found, {@code false} otherwise
     * @see TopiaStereoTypes#STEREOTYPE_INDEXED_COLLECTION
     * @since 2.10
     */
    public static boolean hasIndexedCollectionStereotype(ObjectModelAttribute attribute) {
        return attribute.hasStereotype(TopiaStereoTypes.STEREOTYPE_INDEXED_COLLECTION);
    }

    /**
     * Obtain the value of the {@link TopiaTagValues#TAG_PERSISTENCE_TYPE}
     * tag value on the given classifier.
     * 
     *
     * @param classifier classifier to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see TopiaTagValues#TAG_PERSISTENCE_TYPE
     * @since 2.5
     */
    public static String getPersistenceTypeTagValue(ObjectModelClassifier classifier) {
        String value = findTagValue(TopiaTagValues.TAG_PERSISTENCE_TYPE, classifier, null);
        return value;
    }

    /**
     * Obtain the value of the {@link TopiaTagValues#TAG_DB_NAME}
     * tag value on the given element.
     * 
     *
     * Note that it won't and search on declaring element or anywhere else than on the given element.
     * See https://forge.nuiton.org/issues/2342
     *
     * @param element element to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see TopiaTagValues#TAG_DB_NAME
     * @since 2.5
     */
    public static String getDbNameTagValue(ObjectModelElement element) {
        String value = element.getTagValue(TopiaTagValues.TAG_DB_NAME);
        return value;
    }

    /**
     * Obtain the value of the {@link TopiaTagValues#TAG_REVERSE_DB_NAME}
     * tag value on the given element.
     * 
     *
     * Note that it won't and search on declaring element or anywhere else than on the given element.
     * See https://forge.nuiton.org/issues/2342
     *
     * @param element element to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see TopiaTagValues#TAG_REVERSE_DB_NAME
     * @since 2.5
     */
    public static String getReverseDbNameTagValue(ObjectModelAttribute element) {
        String value = element.getTagValue(TopiaTagValues.TAG_REVERSE_DB_NAME);
        return value;
    }

    /**
     * Obtain the value of the {@link TopiaTagValues#TAG_MANY_TO_MANY_TABLE_NAME}
     * tag value on the given attribute.
     * 
     *
     * Note that it won't and search on declaring element or anywhere else than on the given element.
     * See https://forge.nuiton.org/issues/2342
     *
     * @param attribute attribute to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see TopiaTagValues#TAG_MANY_TO_MANY_TABLE_NAME
     * @since 2.9.2
     */
    public static String getManytoManyTableNameTagValue(ObjectModelAttribute attribute) {
        String value = attribute.getTagValue(TopiaTagValues.TAG_MANY_TO_MANY_TABLE_NAME);
        return value;
    }

    /**
     * Obtain the value of the {@link TopiaTagValues#TAG_SCHEMA_NAME}
     * tag value on the given classifier.
     * 
     *
     * @param classifier classifier to seek
     * @param model      model to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see TopiaTagValues#TAG_SCHEMA_NAME
     * @since 2.5
     */
    public static String getDbSchemaNameTagValue(ObjectModelClassifier classifier, ObjectModel model) {
        ObjectModelPackage aPackage = null;
        if (classifier!=null) {
            aPackage = model.getPackage(classifier);
        }
        String value = TagValueUtil.findTagValue(TopiaTagValues.TAG_SCHEMA_NAME, null, model, aPackage , classifier);
        return value;
    }

    /**
     * Obtain the value of the {@link TopiaTagValues#TAG_LENGTH}
     * tag value on the given attribute.
     * 
     *
     * @param attribute attribute to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see TopiaTagValues#TAG_LENGTH
     * @since 2.5
     */
    public static String getLengthTagValue(ObjectModelAttribute attribute) {
        String value = findTagValue(TopiaTagValues.TAG_LENGTH, attribute, null);
        return value;
    }

    /**
     * Obtain the value of the {@link TopiaTagValues#TAG_ANNOTATION}
     * tag value on the given attribute.
     * 
     *
     * @param attribute attribute to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see TopiaTagValues#TAG_ANNOTATION
     * @since 2.5
     */
    public static String getAnnotationTagValue(ObjectModelAttribute attribute) {
        String value = findTagValue(TopiaTagValues.TAG_ANNOTATION, attribute, null);
        return value;
    }


    /**
     * Obtain the value of the {@link TopiaTagValues#TAG_ACCESS}
     * tag value on the given attribute.
     * 
     *
     * @param attribute attribute to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see TopiaTagValues#TAG_ACCESS
     * @since 2.5
     */
    public static String getAccessTagValue(ObjectModelAttribute attribute) {
        String value = findTagValue(TopiaTagValues.TAG_ACCESS, attribute, null);
        return value;
    }

    /**
     * Obtain the value of the {@link TopiaTagValues#TAG_NATURAL_ID}
     * tag value on the given attribute.
     * 
     *
     * @param attribute attribute to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see TopiaTagValues#TAG_NATURAL_ID
     * @since 2.5
     */
    public static String getNaturalIdTagValue(ObjectModelAttribute attribute) {
        String value = findTagValue(TopiaTagValues.TAG_NATURAL_ID, attribute, null);
        return value;
    }

    /**
     * Obtain the value of the {@link TopiaTagValues#TAG_NATURAL_ID_MUTABLE}
     * tag value on the given classifier.
     * 
     *
     * @param classifier classifier to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see TopiaTagValues#TAG_NATURAL_ID_MUTABLE
     * @since 2.5
     */
    public static String getNaturalIdMutableTagValue(ObjectModelClassifier classifier) {
        String value = findTagValue(TopiaTagValues.TAG_NATURAL_ID_MUTABLE, classifier, null);
        return value;
    }

    /**
     * Obtain the value of the {@link TopiaTagValues#TAG_INVERSE}
     * tag value on the given attribute.
     * 
     *
     * @param attribute attribute to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see TopiaTagValues#TAG_INVERSE
     * @since 2.5
     */
    public static String getInverseTagValue(ObjectModelAttribute attribute) {
        String value = findTagValue(TopiaTagValues.TAG_INVERSE, attribute, null);
        return value;
    }

    /**
     * Obtain the value of the {@link TopiaTagValues#TAG_LAZY}
     * tag value on the given attribute.
     * 
     *
     * @param attribute attribute to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see TopiaTagValues#TAG_LAZY
     * @since 2.5
     */
    public static String getLazyTagValue(ObjectModelAttribute attribute) {
        String value = findTagValue(TopiaTagValues.TAG_LAZY, attribute, null);
        return value;
    }

    /**
     * Obtain the value of the {@link TopiaTagValues#TAG_FETCH}
     * tag value on the given attribute.
     * 
     *
     * @param attribute attribute to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see TopiaTagValues#TAG_FETCH
     * @since 2.5
     */
    public static String getFetchTagValue(ObjectModelAttribute attribute) {
        String value = findTagValue(TopiaTagValues.TAG_FETCH, attribute, null);
        return value;
    }

    /**
     * Obtain the value of the {@link TopiaTagValues#TAG_ORDER_BY}
     * tag value on the given attribute.
     * 
     *
     * @param attribute attribute to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see TopiaTagValues#TAG_ORDER_BY
     * @since 2.5
     */
    public static String getOrderByTagValue(ObjectModelAttribute attribute) {
        String value = findTagValue(TopiaTagValues.TAG_ORDER_BY, attribute, null);
        return value;
    }

    /**
     * Obtain the value of the {@link TopiaTagValues#TAG_NOT_NULL}
     * tag value on the given attribute.
     * 
     *
     * @param attribute attribute to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see TopiaTagValues#TAG_NOT_NULL
     * @since 2.5
     */
    public static String getNotNullTagValue(ObjectModelAttribute attribute) {
        String value = findTagValue(TopiaTagValues.TAG_NOT_NULL, attribute, null);
        return value;
    }

    /**
     * Obtain the value of the {@link TopiaTagValues#TAG_PROXY_INTERFACE}
     * tag value on the given classifier.
     * 
     *
     * @param classifier classifier to seek
     * @param model      model to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see TopiaTagValues#TAG_PROXY_INTERFACE
     * @since 2.5
     */
    public static String getProxyInterfaceTagValue(ObjectModelClassifier classifier, ObjectModel model) {
        String value = findTagValue(TopiaTagValues.TAG_PROXY_INTERFACE, classifier, model);
        return value;
    }

    /**
     * Tests if the given classifier own at least one security tag value.
     *
     * @param classifier the classifier to test
     * @return {@code true} if there is at least one security tag value on the given class
     * @since 2.5
     */
    public static boolean isClassWithSecurity(ObjectModelClassifier classifier) {
        return StringUtils.isNotEmpty(getSecurityCreateTagValue(classifier)) ||
               StringUtils.isNotEmpty(getSecurityLoadTagValue(classifier)) ||
               StringUtils.isNotEmpty(getSecurityUpdateTagValue(classifier)) ||
               StringUtils.isNotEmpty(getSecurityDeleteTagValue(classifier));
    }

    /**
     * Obtain the value of the {@link TopiaTagValues#TAG_SECURITY_CREATE}
     * tag value on the given classifier.
     * 
     *
     * @param classifier classifier to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see TopiaTagValues#TAG_SECURITY_CREATE
     * @since 2.5
     */
    public static String getSecurityCreateTagValue(ObjectModelClassifier classifier) {
        String value = findTagValue(TopiaTagValues.TAG_SECURITY_CREATE, classifier, null);
        return value;
    }

    /**
     * Obtain the value of the {@link TopiaTagValues#TAG_SECURITY_DELETE}
     * tag value on the given classifier.
     * 
     *
     * @param classifier classifier to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see TopiaTagValues#TAG_SECURITY_DELETE
     * @since 2.5
     */
    public static String getSecurityDeleteTagValue(ObjectModelClassifier classifier) {
        String value = findTagValue(TopiaTagValues.TAG_SECURITY_DELETE, classifier, null);
        return value;
    }

    /**
     * Obtain the value of the {@link TopiaTagValues#TAG_SECURITY_LOAD}
     * tag value on the given classifier.
     * 
     *
     * @param classifier classifier to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see TopiaTagValues#TAG_SECURITY_LOAD
     * @since 2.5
     */
    public static String getSecurityLoadTagValue(ObjectModelClassifier classifier) {
        String value = findTagValue(TopiaTagValues.TAG_SECURITY_LOAD, classifier, null);
        return value;
    }

    /**
     * Obtain the value of the {@link TopiaTagValues#TAG_SECURITY_UPDATE}
     * tag value on the given classifier.
     * 
     *
     * @param classifier classifier to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see TopiaTagValues#TAG_SECURITY_UPDATE
     * @since 2.5
     */
    public static String getSecurityUpdateTagValue(ObjectModelClassifier classifier) {
        String value = findTagValue(TopiaTagValues.TAG_SECURITY_UPDATE, classifier, null);
        return value;
    }

    /**
     * Obtain the value of the {@link TopiaTagValues#TAG_NOT_GENERATE_TO_STRING}
     * tag value on the given class.
     * 
     *
     * @param clazz class to seek
     * @param model model to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see TopiaTagValues#TAG_NOT_GENERATE_TO_STRING
     * @since 2.5
     */
    public static String getNotGenerateToStringTagValue(ObjectModelClassifier clazz, ObjectModel model) {
        String value = findTagValue(TopiaTagValues.TAG_NOT_GENERATE_TO_STRING, clazz, model);
        return value;
    }

    /**
     * Obtain the value of the {@link TopiaTagValues#TAG_SORT_ATTRIBUTE}
     * tag value on the given classifier.
     * 
     *
     * @param classifier classifier to seek
     * @param model      model to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see TopiaTagValues#TAG_SORT_ATTRIBUTE
     * @since 2.5
     */
    public static String getSortAttributeTagValue(ObjectModelClassifier classifier, ObjectModel model) {
        String value = findTagValue(TopiaTagValues.TAG_SORT_ATTRIBUTE, classifier, model);
        return value;
    }

    /**
     * Obtain the value of the {@link TopiaTagValues#TAG_GENERATE_STANDALONE_ENUM_FOR_DAO_HELPER}
     * tag value on the given model.
     * 
     *
     * @param model model to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see TopiaTagValues#TAG_GENERATE_STANDALONE_ENUM_FOR_DAO_HELPER
     * @since 2.5
     */
    public static String getGenerateStandaloneEnumForDAOHelperTagValue(ObjectModel model) {
        String value = findTagValue(TopiaTagValues.TAG_GENERATE_STANDALONE_ENUM_FOR_DAO_HELPER, null, model);
        return value;
    }

    /* Obtain the value of the {@link TopiaTagValues#TAG_GENERATE_OPERATOR_FOR_DAO_HELPER}
    * tag value on the given model.
    * 
    *
    * @param model model to seek
    * @return the none empty value of the found tag value or {@code null} if not found nor empty.
    * @see TopiaTagValues#TAG_GENERATE_OPERATOR_FOR_DAO_HELPER
    * @since 2.5
    */

    public static String getGenerateOperatorForDAOHelperTagValue(ObjectModel model) {
        String value = findTagValue(TopiaTagValues.TAG_GENERATE_OPERATOR_FOR_DAO_HELPER, null, model);
        return value;
    }

    /**
     * Obtain the value of the {@link TopiaTagValues#TAG_TYPE}
     * tag value on the given attribute.
     * 
     *
     * @param attribute attribute to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see TopiaTagValues#TAG_TYPE
     * @since 2.5
     */
    public static String getTypeTagValue(ObjectModelAttribute attribute) {
        String value = findTagValue(TopiaTagValues.TAG_TYPE, attribute, null);
        return value;
    }

    /**
     * Obtain the value of the {@link TopiaTagValues#TAG_SQL_TYPE}
     * tag value on the given attribute.
     * 
     *
     * @param attribute attribute to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see TopiaTagValues#TAG_TYPE
     * @since 2.5
     */
    public static String getSqlTypeTagValue(ObjectModelAttribute attribute) {
        String value = findTagValue(TopiaTagValues.TAG_SQL_TYPE, attribute, null);
        return value;
    }

    /**
     * Obtain the value of the {@link TopiaTagValues#TAG_EXCEPTION_CLASS}
     * tag value on the given interface.
     * 
     *
     * @param model model to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see TopiaTagValues#TAG_EXCEPTION_CLASS
     * @since 2.5
     */
    public static String getExceptionClassTagValue(ObjectModel model) {
        String value = findTagValue(TopiaTagValues.TAG_EXCEPTION_CLASS, null, model);
        return value;
    }

    /**
     * Obtain the value of the {@link TopiaTagValues#TAG_TRANSACTION}
     * tag value on the given operation.
     * 
     *
     * @param operation operation to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see TopiaTagValues#TAG_TRANSACTION
     * @since 2.5
     */
    public static String getTransactionTagValue(ObjectModelOperation operation) {
        String value = findTagValue(TopiaTagValues.TAG_TRANSACTION, operation, null);
        return value;
    }

    /**
     * Obtain the value of the {@link TopiaTagValues#TAG_TRANSACTION}
     * tag value on the given classifier.
     * 
     *
     * @param classifier classifier to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see TopiaTagValues#TAG_TRANSACTION
     * @since 2.5
     */
    public static String getTransactionTagValue(ObjectModelClassifier classifier) {
        String value = findTagValue(TopiaTagValues.TAG_TRANSACTION, classifier, null);
        return value;
    }

    /**
     * Obtain the value of the {@link TopiaTagValues#TAG_DO_COMMIT}
     * tag value on the given operation.
     * 
     *
     * @param operation operation to seek
     * @param model     model to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see TopiaTagValues#TAG_DO_COMMIT
     * @since 2.5
     */
    public static String getDoCommitTagValue(ObjectModelOperation operation, ObjectModel model) {
        String value = findTagValue(TopiaTagValues.TAG_DO_COMMIT, operation, model);
        return value;
    }

    /**
     * Obtain the value of the {@link TopiaTagValues#TAG_ERROR_ARGS}
     * tag value on the given operation.
     * 
     *
     * @param operation operation to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see TopiaTagValues#TAG_ERROR_ARGS
     * @since 2.5
     */
    public static String getErrorArgsTagValue(ObjectModelOperation operation) {
        String value = findTagValue(TopiaTagValues.TAG_ERROR_ARGS, operation, null);
        return value;
    }

    /**
     * Obtain the value of the {@link TopiaTagValues#TAG_NO_LOG_IN_SERVICE}
     * tag value on the given classifier.
     * 
     *
     * @param classifier classifier to seek
     * @param model      model to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see TopiaTagValues#TAG_NO_LOG_IN_SERVICE
     * @since 2.5
     * @deprecated since 2.5.4, no more use will be remove soon.
     */
    @Deprecated
    public static String getNoLogInServiceTagValue(ObjectModelClassifier classifier, ObjectModel model) {
        String value = findTagValue(TopiaTagValues.TAG_NO_LOG_IN_SERVICE, classifier, model);
        return value;
    }

    /**
     * Obtains the value of the {@link TopiaTagValues#TAG_DAO_IMPLEMENTATION}
     * tag value on the given model.
     * 
     *
     * @param model model to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see TopiaTagValues#TAG_DAO_IMPLEMENTATION
     * @since 2.5
     */
    public static String getDaoImplementationTagValue(ObjectModel model) {
        String value = findTagValue(TopiaTagValues.TAG_DAO_IMPLEMENTATION, null, model);
        return value;
    }

    /**
     * Obtains the value of the  tag value
     * {@link TopiaTagValues#TAG_INDEX_FOREIGN_KEYS} on the model or on the
     * given attribute.
     *
     * @param attribute attribute to test
     * @param model model to test
     * @return none empty value of the found tag value or {@code null} if not found nor empty.
     * @see TopiaTagValues#TAG_INDEX_FOREIGN_KEYS
     * @since 2.6.5
     */
    public static String getIndexForeignKeys(ObjectModelAttribute attribute, ObjectModel model) {
        String value = findTagValue(TopiaTagValues.TAG_INDEX_FOREIGN_KEYS, attribute, model);
        return value;
    }

    public static boolean hasUseEnumerationNameTagValue(ObjectModelAttribute attr, ObjectModel model) {
        String value = findTagValue(TopiaTagValues.TAG_USE_ENUMERATION_NAME, attr, model);
        return Boolean.parseBoolean(value);
    }

    /**
     * Search if the TagValue {@link TopiaTagValues#TAG_GENERATE_TOPIA_ID_IN_DTO} has been
     * activated in the model.
     *
     * @param classifier classifier to seek
     * @param model      model to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see TopiaTagValues#TAG_GENERATE_TOPIA_ID_IN_DTO
     * @since 2.6.7
     */
    public static boolean shouldGenerateDTOTopiaIdTagValue(ObjectModelClassifier classifier, ObjectModel model) {
        String tagValue = findTagValue(TopiaTagValues.TAG_GENERATE_TOPIA_ID_IN_DTO, classifier, model);
        boolean generate = StringUtils.isNotEmpty(tagValue) && Boolean.valueOf(tagValue);
        return generate;
    }

    /**
     * Obtain the value of the {@link TopiaTagValues#TAG_DO_NOT_GENERATE_READ_LISTENERS}
     * tag value on the given model or classifier.
     *
     * It will first look on the model, and then in the given classifier.
     *
     * @param model      model to seek
     * @param attribute attribute to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see TopiaTagValues#TAG_DO_NOT_GENERATE_READ_LISTENERS
     * @since 2.9
     */
    public static boolean isDoNotGenerateReadListeners(ObjectModelAttribute attribute, ObjectModel model) {
        String value = findTagValue(TopiaTagValues.TAG_DO_NOT_GENERATE_READ_LISTENERS, attribute, model);
        return value != null && "true".equals(value);
    }


    /**
     * Obtain the value of the {@link TopiaTagValues#TAG_DO_NOT_GENERATE_READ_LISTENERS}
     * tag value on the given model or classifier and returns  {@code true} if the tag value was found and value {@code true}.
     *
     * It will first look on the model, and then in the given classifier.
     *
     * @param model      model to seek
     * @param classifier classifier to seek
     * @return {@code true} if tag value was found on classifier or model and his value is {@code true}, otherwise {@code false}.
     * @see TopiaTagValues#TAG_GENERATE_FOREIGN_KEY_NAMES
     * @since 2.10
     */
    public static boolean isGenerateForeignKeyNames(ObjectModelClassifier classifier, ObjectModel model) {
        String value = findTagValue(TopiaTagValues.TAG_GENERATE_FOREIGN_KEY_NAMES, classifier, model);
        return value != null && "true".equals(value);
    }

} // TopiaGeneratorUtil

