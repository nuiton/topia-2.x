/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.topia.generator;

import org.codehaus.plexus.component.annotations.Component;
import org.nuiton.eugene.Template;
import org.nuiton.eugene.java.ObjectModelTransformerToJava;
import org.nuiton.eugene.models.object.ObjectModelClass;

/*{generator option: parentheses = false}*/

/*{generator option: writeString = +}*/
/**
 * Created: 13 déc. 2009
 *
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @version $Id$
 * @since 2.3.0
 * @deprecated 2.5.4, prefer use the transformer {@link EntityDAOTransformer}
 */
@Deprecated
@Component(role = Template.class, hint = "org.nuiton.topia.generator.DAOTransformer")
public class DAOTransformer extends ObjectModelTransformerToJava {

    @Override
    public void transformFromClass(ObjectModelClass clazz) {
        if (!TopiaGeneratorUtil.isEntity(clazz)) {
            return;
        }
        String clazzName = clazz.getName();
        String clazzFQN = clazz.getQualifiedName();
        ObjectModelClass result = createClass(clazzName + "DAO", clazz.getPackageName());
        setDocumentation(result, "/**\n" +
                                 " * Cette classe etend le DAOImpl pour parametrer la classe avec le bon type\n" +
                                 " * Cette classe est marque finale car l'heritage entre les DAO se fait\n" +
                                 " * sur les DOAImpl, c-a-d que DAOAbstract peut etendre le DAOImpl\n" +
                                 " */");
        setSuperClass(result, clazzFQN + "DAOImpl<" + clazzName + ">");
    }


}
