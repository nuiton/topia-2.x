/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.topia.generator;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.codehaus.plexus.component.annotations.Component;
import org.nuiton.eugene.GeneratorUtil;
import org.nuiton.eugene.Template;
import org.nuiton.eugene.java.ObjectModelTransformerToJava;
import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.eugene.models.object.ObjectModelInterface;
import org.nuiton.eugene.models.object.ObjectModelJavaModifier;
import org.nuiton.eugene.models.object.ObjectModelModifier;
import org.nuiton.eugene.models.object.ObjectModelOperation;
import org.nuiton.eugene.models.object.ObjectModelParameter;
import org.nuiton.i18n.I18n;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaException;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

/*{generator option: parentheses = false}*/

/*{generator option: writeString = +}*/

/**
 * This Template is used to create the skeleton of services for a final
 * application which using Topia. 
 * <div>
 * Generation from interfaces with stereotype &lt;&lt;service&gt;&gt; :
 *  <ul>
 *    <li>Service : interface of the service defined in model.</li>
 *    <li><p>ServiceAbstract : abstract class which contains :</p>
 *        <p>* treateError : abstract method used to catch all exception from a
 *          service method.</p>
 *        <p>* closeTransaction : abstract method used to finally the try/catch
 *          of a service method</p>
 *        <p>* beginTransaction : abstract method used to start the transaction
 *          using rootContext.</p>
 *        <p>* constructor with AppContextImplementor in argument</p>
 *        <p>* for each method : the implementation of the method (skeleton with
 *          try/catch and beginTransaction call to open a new TopiaContext from
 *          AppContextImplementor). Usage of i18n keys for error messages in
 *          exception.</p>
 *        <p>* for each method : an abstract method used to execute the business
 *          code of the method : need to be implemented in subclass.</p>
 *    </li>
 *  </ul>
 * </div>
 * <div>
 * Exemple of ServiceImpl utils method implementation. (The AppException
 * is considered if defined in model tagvalue "exceptionClass") :
 * <pre>
 *   public class ServiceImpl implements ServiceAbstract  {
 *
 *      // properties for Topia configuration
 *      protected Properties properties;
 *      ...
 * 
 *      &#64;Override
 *      public void treateError(TopiaContext transaction, Exception eee,
 *              String message, Object... args) throws AppException {
 *
 *          // Note that the message from service doesn't directly use _() for
 *          // i18 messages but n_(). In this log, the _() is used to translate
 *          // correctly the message. But the message must be translate when
 *          // catching the AppException in UI.
 *          if (log.isErrorEnabled()) {
 *              log.error(_(message, args), eee);
 *          }
 *
 *          // rollback of current transaction
 *          if (transaction != null) {
 *              try {
 *                  transaction.rollbackTransaction();
 *              } catch (TopiaException ex) {
 *                  if (log.isErrorEnabled()) {
 *                      log.error(_("app.error.context.rollback"), ex);
 *                  }
 *              }
 *          }
 *          // wrapping the exception in a AppException with message and
 *          // arguments for i18n translation
 *          throw new AppException(eee, message, args);
 *      }
 *
 *      &#64;Override
 *      public void closeTransaction(TopiaContext transaction) {
 *          if (transaction != null) {
 *              try {
 *                  transaction.closeContext();
 *              } catch (TopiaException eee) {
 *                  if (log.isErrorEnabled()) {
 *                      log.error(_("app.error.context.close"), eee);
 *                  }
 *              }
 *          }
 *      }
 *
 *      &#64;Override
 *      public TopiaContext beginTransaction() throws TopiaException {
 *          TopiaContext rootContext = null;
 *          try {
 *              // You have to manage the properties using ApplicationConfig
 *              // or other lib to have configuration for Topia
 *              rootContext = TopiaContextFactory.getContext(properties);
 *
 *              return getTopiaRootContext().beginTransaction();
 *
 *          // only catch exception for rootContext
 *          } catch (TopiaNotFoundException eee) {
 *              treateError(eee, n_("app.error.context.getTopiaRootContext"));
 *          }
 *          return null;
 *      }
 *
 *      // Implementation of abstract method, the interface method is
 *      // called 'createMyEntity(MyEntity entity)' in this case.
 *      &#64;Override
 *      public void executeCreateMyEntity(TopiaContext transaction, 
 *                      MyEntity entity) throws TopiaException {
 *
 *          MyEntityDAO dao = AppDAOHelper.getMyEntityDAO(transaction);
 *          dao.create(entity);
 *          // That's it, no need to manage errors or transaction, the abstract
 *          // service will do this job.
 *      }
 * }
 * </pre>
 * </div>
 * <div>
 * <h2>TAG_TRANSACTION</h2>
 * <p>Default value : true</p>
 * <p>You can use the tagValue 'transaction=false' to specify that a method
 * doesn't need any TopiaContext, so no need to instantiate a new one.
 * This tagValue can only be put directly in the model and not in properties
 * file (because of multiple methods with same name problem).</p>
 * </div>
 * <div>
 * <h2>TAG_ERROR_ARGS</h2>
 * <p>Default value : false</p>
 * <p>You can use the tagValue 'errorArgs=true' to specify that a method
 * need arguments for error message. This tagValue can only be put directly
 * in the model and not in properties file.</p>
 * </div>
 * <div>
 * <h2>TAG_EXCEPTION_CLASS</h2>
 * <p>Default value : null</p>
 * <p>You can use the tagValue 'exceptionClass=my.exception.full.qualified.Name'
 * to specify that all contract methods will throw this exception.</p>
 * </div>
 * <p>It is smooth, isn't it :p ?</p>
 * <p>
 *
 * Created: 23 mars 2010
 *
 * @author fdesbois &lt;fdesbois@codelutin.com&gt;
 * @version $Id$
 * @since 2.3.1
 */
// TODO : may be refactor to integrate JTA or webservice or may be not in this transformer.
@Component(role = Template.class, hint = "org.nuiton.topia.generator.ServiceTransformer")
public class ServiceTransformer extends ObjectModelTransformerToJava {


    private static final Log log = LogFactory.getLog(ServiceTransformer.class);
    protected String modelName;

    protected String defaultPackageName;

    protected String exceptionName;

    private static final String OP_NAME_BEGIN_TRANSACTION = "beginTransaction";

    private static final String OP_NAME_COMMIT_TRANSACTION = "commitTransaction";

    private static final String OP_NAME_CLOSE_TRANSACTION = "closeTransaction";

    private static final String OP_NAME_TREATE_ERROR = "treateError";

    public static final String PARAMETER_TRANSACTION = "transaction";

    protected String getServiceAbstractClassName(String serviceName) {
        return serviceName + "Abstract";
    }

    @Override
    public void transformFromModel(ObjectModel model) {
        exceptionName =  TopiaGeneratorUtil.getExceptionClassTagValue(model);
        modelName = model.getName();
    }

    @Override
    public void transformFromInterface(ObjectModelInterface input) {
        if (!TopiaGeneratorUtil.hasServiceStereotype(input)) {
            return;
        }

        // global transaction needed (if set to false then never use transaction)
        boolean needTransaction = isTransactionNeeded(input);

        ObjectModelInterface serviceContract = createServiceContract(input);

        createServiceAbstract(input,
                              serviceContract,
                              needTransaction);
    }

    /**
     * Create the service contract using {@code source} interface defined
     * in model.
     *
     * @param source interface from model
     * @return the ObjectModelInterface created
     */
    protected ObjectModelInterface createServiceContract(
            ObjectModelInterface source) {

        ObjectModelInterface serviceContract =
                createInterface(source.getName(), source.getPackageName());

        setDocumentation(serviceContract, source.getDocumentation());
        for (ObjectModelOperation op : source.getOperations()) {
            ObjectModelOperation newOp = addOperation(serviceContract,
                                                      op.getName(),
                                                      op.getReturnType()
            );
            setDocumentation(newOp.getReturnParameter(),
                             op.getReturnParameter().getDocumentation()
            );
            for (ObjectModelParameter param : op.getParameters()) {
                ObjectModelParameter newParam = addParameter(newOp,
                                                             param.getType(),
                                                             param.getName()
                );
                setDocumentation(newParam, param.getDocumentation());
            }
            for (String ex : op.getExceptions()) {
                addException(newOp, ex);
            }
            if (exceptionName != null) {
                addException(newOp, exceptionName);
            }
            setDocumentation(newOp, op.getDocumentation());
        }
        return serviceContract;
    }

    protected void createBeginTransactionMethod(ObjectModelInterface source,
                                         ObjectModelInterface serviceContract,
                                         ObjectModelClass serviceAbstract) {
        ObjectModelOperation operation =
                    addOperation(serviceAbstract, OP_NAME_BEGIN_TRANSACTION,
                                 TopiaContext.class,
                                 ObjectModelJavaModifier.ABSTRACT,
                                 ObjectModelJavaModifier.PROTECTED);
            addException(operation, TopiaException.class);
    }

    protected void createCommitTransactionMethod(ObjectModelClass serviceAbstract) {
        ObjectModelOperation operation =
                    addOperation(serviceAbstract,
                                 OP_NAME_COMMIT_TRANSACTION,
                                 "void",
                                 ObjectModelJavaModifier.PROTECTED);
        addParameter(operation, TopiaContext.class, PARAMETER_TRANSACTION);
        addException(operation, TopiaException.class);
        setOperationBody(operation,""
/*{
        transaction.commitTransaction();
}*/
        );

    }

    protected void createCloseTransactionMethod(ObjectModelInterface source,
                                         ObjectModelInterface serviceContract,
                                         ObjectModelClass serviceAbstract) {
        ObjectModelOperation operation =
                        addOperation(serviceAbstract,
                                     OP_NAME_CLOSE_TRANSACTION,
                                     "void",
                                     ObjectModelJavaModifier.ABSTRACT,
                                     ObjectModelJavaModifier.PROTECTED);
                addParameter(operation, TopiaContext.class, PARAMETER_TRANSACTION);
                addException(operation, TopiaException.class);
    }

    protected void createTreateErrorMethod(ObjectModelInterface source,
                                         ObjectModelInterface serviceContract,
                                         ObjectModelClass serviceAbstract,
                                         boolean needTransaction) {

        ObjectModelOperation treateError1 =
                addOperation(serviceAbstract,
                             OP_NAME_TREATE_ERROR,
                             "void",
                             ObjectModelJavaModifier.ABSTRACT,
                             ObjectModelJavaModifier.PROTECTED);
        if (needTransaction) {
            addParameter(treateError1, TopiaContext.class, PARAMETER_TRANSACTION);
        }
        addParameter(treateError1, Exception.class, "eee");
        addParameter(treateError1, String.class, "message");
        addParameter(treateError1, "Object...", "args");
        if (exceptionName != null) {
            addException(treateError1, exceptionName);
        }

        if (needTransaction) {
            ObjectModelOperation treateError2 =
                    addOperation(serviceAbstract, OP_NAME_TREATE_ERROR, "void",
                            ObjectModelJavaModifier.PROTECTED);
            addParameter(treateError2, Exception.class, "eee");
            addParameter(treateError2, String.class, "message");
            addParameter(treateError2, "Object...", "args");
            if (exceptionName != null) {
                addException(treateError2, exceptionName);
            }

            setOperationBody(treateError2, ""
        /*{
            treateError(null, eee, message, args);
        }*/
            );
        }
    }
    /**
     * Create the service abstract for {@code serviceContract}
     * using {@code source} interface defined
     * in model.
     *
     * @param source interface from model
     * @param serviceContract to implement
     * @param needTransaction flag to know if service globally use transaction
     */
    protected void createServiceAbstract(ObjectModelInterface source,
                                         ObjectModelInterface serviceContract,
                                         boolean needTransaction) {

        ObjectModelClass serviceAbstract = createAbstractClass(
                getServiceAbstractClassName(serviceContract.getName()),
                        serviceContract.getPackageName());

        // Imports for implementations
        if (needTransaction) {
            addImport(serviceAbstract, TopiaContext.class);
        }
        addImport(serviceAbstract, I18n.class);

        // Implements contract interface
        addInterface(serviceAbstract, serviceContract.getQualifiedName());

        // Create abstract methods
        
        if (needTransaction) {
            createBeginTransactionMethod(source,
                                         serviceContract,
                                         serviceAbstract);

            createCommitTransactionMethod(serviceAbstract);
            
            createCloseTransactionMethod(source,
                                         serviceContract,
                                         serviceAbstract);
        }

        createTreateErrorMethod(source,
                                serviceContract,
                                serviceAbstract,
                                needTransaction
        );

        // keep execute methods (we want to generate them at the top of the
        // class since they are all abstract)
        // Note: using a LinkedHashMap permits to keep incoming order
        Map<ObjectModelOperation, ObjectModelOperation> abstractExecuteMethods =
                new LinkedHashMap<ObjectModelOperation, ObjectModelOperation>();

        // first generate the abstract execute methods
        for (ObjectModelOperation operation : source.getOperations()) {

            ObjectModelOperation executeOp = createOperationExecuteAbstract(
                    serviceAbstract,
                    operation,
                    needTransaction
            );
            
            abstractExecuteMethods.put(operation , executeOp);
        }

        // Then generates the real operation which boxes the execute methods
        for (Map.Entry<ObjectModelOperation, ObjectModelOperation> entry :
                abstractExecuteMethods.entrySet()) {
            ObjectModelOperation operation = entry.getKey();
            ObjectModelOperation executeOperation = entry.getValue();
            createOperationImplementation(
                    serviceAbstract,
                    executeOperation,
                    operation,
                    source.getName(),
                    needTransaction
            );
        }
    }

    /**
     * Create an operation abstract to execute in contract implementation.
     * You can use tagvalues "errorArgs" (default = false) and "transaction"
     * (default = true) to generate appropriate parameters. This abstract
     * method will throw all exceptions (Exception.class). This is the method
     * which will be implemented by the developper in service implementation
     * class.
     *
     * @param serviceAbstract where the operation will be created
     * @param source ObjectModelOperation from model
     * @param needTransaction flag to know if service globally use transaction
     * @return the abstract operation created
     * @see #isErrorArgsNeeded(ObjectModelOperation)
     * @see #isTransactionNeeded(ObjectModelOperation)
     * @see #isTransactionNeeded(ObjectModelInterface)
     */
    protected ObjectModelOperation createOperationExecuteAbstract(
            ObjectModelClass serviceAbstract,
            ObjectModelOperation source,
            boolean needTransaction) {
        String opName = StringUtils.capitalize(source.getName());

        // Abstract operation to execute method content
        ObjectModelOperation executeOperation =
                addOperation(serviceAbstract, "execute" + opName,
                        source.getReturnType(),
                        ObjectModelJavaModifier.ABSTRACT,
                        ObjectModelJavaModifier.PROTECTED);

        // Throw all exception from abstract method
        // They will be catched by interface method to use treateError
        addException(executeOperation, Exception.class);

        if (needTransaction && isTransactionNeeded(source)) {
            addParameter(executeOperation, TopiaContext.class, PARAMETER_TRANSACTION);
        }

        if (isErrorArgsNeeded(source)) {
            // Add errorArgs to abstract operation
            addParameter(executeOperation, "java.util.List<Object>", "errorArgs");
        }

        // Copy other operation parameters
        for (ObjectModelParameter param : source.getParameters()) {
            addParameter(executeOperation, param.getType(), param.getName());
        }
        return executeOperation;
    }

    /**
     * Create an operation implementation. This is the skeleton of the operation
     * defined from model. This will put a try/catch block over an abstract
     * method {@code abstOp}. You can use tagvalues "errorArgs" and
     * "transaction" for abstract method parameters to call. If the transaction
     * is needed, this will use the beginTransaction() and closeTransaction()
     * methods defined in {@code serviceAbstract} class.
     *
     * @param serviceAbstract where the operation will be created
     * @param abstOp to execute into the implementation body
     * @param source ObjectModelOperation from model
     * @param serviceContractName where the signature method is defined
     * @param needTransaction flag to know if service globally use transaction
     * @see #isErrorArgsNeeded(ObjectModelOperation)
     * @see #isTransactionNeeded(ObjectModelInterface)
     */
    protected void createOperationImplementation(
            ObjectModelClass serviceAbstract,
            ObjectModelOperation abstOp,
            ObjectModelOperation source,
            String serviceContractName,
            boolean needTransaction) {

        // boolean to specify if the method need a transaction or not
        // Default set to true but can be override by a tagvalue on the
        // method
        needTransaction &= isTransactionNeeded(source);

        // boolean to specify if the method need error arguments or not
        // Default set to true but can be override by a tagvalue on the
        // method
        boolean needErrorArgs = isErrorArgsNeeded(source);

        // Implementation of interface operation
        ObjectModelOperation implOp =
                addOperation(serviceAbstract,
                             source.getName(),
                             source.getReturnType(),
                             ObjectModelJavaModifier.PUBLIC);

        addAnnotation(serviceAbstract, implOp, Override.class.getSimpleName());

        String toStringAppend = "";
        String separatorLog = " : ";
        // Copy operation parameters
        for (ObjectModelParameter param : source.getParameters()) {
            String paramName = param.getName();
            addParameter(implOp, param.getType(), paramName);
        }

        // Use buffer for operation body
        StringBuilder buffer = new StringBuilder();

        // Abstract operation parameters
        String abstName = abstOp.getName();
        String abstParams =
                GeneratorUtil.getOperationParametersListName(abstOp);
        
        // Abstract operation return managment
        String abstReturnType = "";
        String abstReturn = "";
        String finalReturn = "";
        String returnType = GeneratorUtil.getSimpleName(abstOp.getReturnType(),
                                                        true
        );
        if (!returnType.equals("void")) {
            abstReturnType = returnType + " result = ";
            abstReturn = "return result;";
            finalReturn = "return " +
                    getReturnValue(abstOp.getReturnType()) + ";";
        }

        // Error key for i18n
        String contract =
            GeneratorUtil.toLowerCaseFirstLetter(serviceContractName);
        String errorKey = StringUtils.lowerCase(modelName) + ".error." +
                            contract + "." + source.getName();

        String treateErrorParams = "eee, I18n.n_(\"" + errorKey + "\")";

        if (needErrorArgs) {
            addImport(serviceAbstract, ArrayList.class);
            // Init errorArgs
            buffer.append(""
    /*{
        List<Object> errorArgs = new ArrayList<Object>();
    }*/             );
            treateErrorParams += ", errorArgs.toArray()";
        }

        if (needTransaction) {
            // Open the transaction
            buffer.append(""
    /*{
        TopiaContext transaction = null;
        try {
            transaction = beginTransaction();

            try {}*/
                    );
            // Add transaction in treateError parameters
            treateErrorParams = "transaction, " + treateErrorParams;
        } else {
            buffer.append(""
    /*{
        try {
    }*/
                    );
        }
        String implName = StringUtils.capitalize(implOp.getName());
        String first = modelName.substring(0, 1);

        buffer.append(""
    /*{
                <%=abstReturnType%><%=abstName%>(<%=abstParams%>);}*/);

        if (needTransaction && isCommit(source, model)) {

            // add the commit instruction
            buffer.append(""
    /*{
                commitTransaction(transaction);}*/);

        }
        buffer.append(""
    /*{
                <%=abstReturn%>}*/);

        if (needTransaction) {
            // Finally block to close transaction
            buffer.append(""
    /*{
            } finally {
                closeTransaction(transaction);
            }
     }*/
            );

        }
        // Copy exceptions
        for (String ex : source.getExceptions()) {
            addException(implOp, ex);
            // Add catch block for known exceptions we want to throw
            String exName = GeneratorUtil.getSimpleName(ex);
            buffer.append(""
    /*{
        } catch (<%=exName%> eee) {
            throw eee; }*/);
        }
        if (exceptionName != null) {
            addException(implOp, exceptionName);
        }

        buffer.append(""
    /*{
        } catch (Exception eee) {
            treateError(<%=treateErrorParams%>); }*/);


        buffer.append(""
    /*{
        }
        <%=finalReturn%>
    }*/
                );

        setOperationBody(implOp, buffer.toString());
    }

    /**
     * boolean to specify if the method need a transaction or not.
     * Default set to true but can be override using a tagvalue "transaction"
     * on the method from model.
     *
     * @param op where the tagvalue is set
     * @return {@code true} if transaction is needed
     */
    protected boolean isTransactionNeeded(ObjectModelInterface op) {
        boolean needTransaction = true;

        String transactionTag = TopiaGeneratorUtil.getTransactionTagValue(op);

        if (transactionTag != null) {
            needTransaction = Boolean.parseBoolean(transactionTag);
        }
        return needTransaction;
    }

    /**
     * boolean to specify if the method need a transaction or not.
     * Default set to true but can be override using a tagvalue "transaction"
     * on the method from model.
     *
     * @param op where the tagvalue is set
     * @return {@code true} if transaction is needed
     */
    protected boolean isTransactionNeeded(ObjectModelOperation op) {
        boolean needTransaction = true;

        String transactionTag = TopiaGeneratorUtil.getTransactionTagValue(op);

        if (transactionTag != null) {
            needTransaction = Boolean.parseBoolean(transactionTag);
        }
        return needTransaction;
    }

    /**
     * boolean to specify if method needs a commit after the executeXXX code invoked.
     *
     * @param op model element where the tagvalue is set
     * @param model  model where to tagvalue can be also set
     * @return {@code true} if a commit must be generated after the executeXXX invocation
     * @see TopiaTagValues#TAG_DO_COMMIT
     * @since 2.5
     */
    protected boolean isCommit(ObjectModelOperation op, ObjectModel model) {
        boolean needCommit = false;

        String tagValue = TopiaGeneratorUtil.getDoCommitTagValue(
                op,
                model
        );
        if (tagValue != null) {
            needCommit = Boolean.parseBoolean(tagValue);
        }
        if (isVerbose()) {
            log.info("commit needed for op [" + op.getName() + "] : " + needCommit);
        }
        return needCommit;
    }

    /**
     * boolean to specify if the method need error arguments or not
     * Default set to false but can be override using a tagvalue "errorArgs" on
     * the method from model.
     *
     * @param op where the tagvalue is set
     * @return true if errorArgs are needed
     */
    protected boolean isErrorArgsNeeded(ObjectModelOperation op) {
        // 
        boolean needErrorArgs = false;

        String errorArgsTag = TopiaGeneratorUtil.getErrorArgsTagValue(op);

        if (errorArgsTag != null) {
            needErrorArgs = Boolean.parseBoolean(errorArgsTag);
        }
        return needErrorArgs;
    }

    /**
     * This method give the return string for an operation {@code returnType}.
     * This use {@link Primitive} enum to provide default values for primitive
     * type. For all other object type, this method will return null.
     *
     * @param returnType
     * @return the defaultValue of the returnType
     */
    protected String getReturnValue(String returnType) {
        try {
            //FIXME-TC20100423 : can not deal with Object types (float != Float)
            Primitive prim =
                Primitive.valueOf(StringUtils.upperCase(returnType));
            return prim.getValue();
        // If not defined in Primitive enum, return null
        } catch (IllegalArgumentException eee) {
            return null;
        }
    }

    //FIXME-TC20100423 : REMOVE THIS!
    protected enum Primitive {
        BYTE("0"),
        SHORT("0"),
        INT("0"),
        LONG("0"),
        FLOAT("0."),
        DOUBLE("0."),
        CHAR("''"),
        BOOLEAN("false");

        private String value;

        Primitive(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }
}
