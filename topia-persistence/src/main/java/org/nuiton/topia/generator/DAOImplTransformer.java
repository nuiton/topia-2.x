/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.topia.generator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.codehaus.plexus.component.annotations.Component;
import org.nuiton.eugene.Template;
import org.nuiton.eugene.java.ObjectModelTransformerToJava;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.eugene.models.object.ObjectModelClassifier;
import org.nuiton.eugene.models.object.ObjectModelDependency;
import org.nuiton.eugene.models.object.ObjectModelInterface;
import org.nuiton.eugene.models.object.ObjectModelOperation;

import java.util.ArrayList;
import java.util.List;

/**
 * Created: 14 déc. 2009
 *
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @version $Id$
 * @since 2.3.0
 * @deprecated 2.5.4, prefer use the transformer {@link EntityDAOTransformer}
 */
@Deprecated
@Component(role = Template.class, hint = "org.nuiton.topia.generator.DAOImplTransformer")
public class DAOImplTransformer extends ObjectModelTransformerToJava {

    /** Logger. */
    static Log log = LogFactory.getLog(DAOImplTransformer.class);

    /**
     * Collection used to identify entities full qualified name that have
     * a dependency for DAO extra operations. So no need to generate the
     * DAOImpl in this case, it will be created by the developper. 
     */
    List<String> noGenerationNeeded = new ArrayList<String>();

    @Override
    public void transformFromInterface(ObjectModelInterface interfacez) {
        if (!TopiaGeneratorUtil.hasDaoStereotype(interfacez)) {
            return;
        }

        /**
         * EVO #636 : Manage extra operations for DAO from "dao" dependency
         * between an interface with stereotype &lt;&lt;dao&gt;&gt; (dependency client) and
         * a class with stereotype &lt;&lt;entity&gt;&gt; (dependency supplier).
         */

        ObjectModelDependency dependency =
                interfacez.getDependency(TopiaGeneratorUtil.DEPENDENCIES_DAO);

        if (dependency == null) {
            if (log.isWarnEnabled()) {
                log.warn("Could not find dependency " +
                         TopiaGeneratorUtil.DEPENDENCIES_DAO +
                         " but DAO stereotype was placed on the interface " +
                         interfacez.getName());

            }
            return;
        }
        ObjectModelClassifier classifier = dependency.getSupplier();

        if (TopiaGeneratorUtil.isEntity(classifier)) {
            noGenerationNeeded.add(classifier.getQualifiedName());
        }
    }

    @Override
    public void transformFromClass(ObjectModelClass clazz) {
        if (!TopiaGeneratorUtil.isEntity(clazz) || hasDAOOperations(clazz)) {
            return;
        }
        String clazzName = clazz.getName();
        String clazzFQN = clazz.getQualifiedName();
        ObjectModelClass result = createClass(clazzName + "DAOImpl<E extends " + clazzName + ">", clazz.getPackageName());
        setDocumentation(result, "/**\n" +
                " Implantation du DAO pour l'entité " + clazzName + ".\n" +
                " * L'utilisateur peut remplacer cette classe par la sienne en la mettant \n" +
                " * simplement dans ces sources. Cette classe générée sera alors simplement\n" +
                " * écrasée\n" +
                " */");
        setSuperClass(result, clazzFQN + "DAOAbstract<E>");
    }

    /**
     * Detect if the class has DAO operations identified with &lt;&lt;dao&gt;&gt; stereotype.
     *
     * @param clazz The ObjectModelClass with operations (Corresponding to the Entity)
     * @return true if the class has some dao operations, false if not
     */
    public boolean hasDAOOperations(ObjectModelClass clazz) {
        // This code will be deprecated
        for (ObjectModelOperation op : clazz.getOperations()) {
            if (TopiaGeneratorUtil.hasDaoStereotype(op)) {
                return true;
            }
        }
        // New method : interface dependency
        return noGenerationNeeded.contains(clazz.getQualifiedName());
    }
}
