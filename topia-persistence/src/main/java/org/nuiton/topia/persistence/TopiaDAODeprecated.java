package org.nuiton.topia.persistence;
/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaException;
import org.nuiton.topia.framework.TopiaQuery;

import java.security.Permission;
import java.util.List;
import java.util.Map;

/**
 * Contains all method that are deprecated from {@link TopiaDAO} in version
 * {@code 2.6.12} and then will be removed in next major version ({@code 3.0}).
 *
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @since 2.6.12
 */
public interface TopiaDAODeprecated<E extends TopiaEntity> {

    /**
     * Crée une requete basé sur l'entité lié au DAO. Résultat attendu : "FROM
     * E"
     *
     * @return une nouvelle TopiaQuery vide. (uniquement avec le From sur le
     *         type d'entité)
     * @since 2.3
     * @deprecated since 2.6.12, {@link TopiaQuery} will be removed in version 3.0
     */
    @Deprecated
    TopiaQuery createQuery();

    /**
     * Crée une requête basé sur l'entité lié au DAO et lui assigne un alias
     * valable dans la requête..
     * 
     * Résultat attendu : "FROM E AS entityAlias"
     *
     * @param entityAlias alias permettant de manipuler l'entité dans la
     *                    requête
     * @return une nouvelle TopiaQuery
     * @since 2.3
     * @deprecated since 2.6.12, {@link TopiaQuery} will be removed in version 3.0
     */
    @Deprecated
    TopiaQuery createQuery(String entityAlias);

    /**
     * Execute une requête basé sur l'entité du DAO. Permet de récupérer une
     * entité correspondant à la requête.
     *
     * @param query la requête
     * @return l'entité correspondant à la recherche ou null si aucune entité
     *         n'a été trouvée
     * @throws TopiaException if any pb while getting datas
     * @see TopiaQuery#executeToEntity(TopiaContext, Class)
     * @since 2.3
     * @deprecated since 2.6.12, {@link TopiaQuery} will be removed in version 3.0
     */
    @Deprecated
    E findByQuery(TopiaQuery query) throws TopiaException;

    /**
     * Execute une requête basé sur l'entité du DAO. Permet de récupérer une
     * liste d'entités correspondant à la requête.
     *
     * @param query la requête
     * @return la liste d'entités correspondant à la recherche
     * @throws TopiaException if any pb while getting datas
     * @see TopiaQuery#executeToEntityList(TopiaContext, Class)
     * @since 2.3
     * @deprecated since 2.6.12, {@link TopiaQuery} will be removed in version 3.0
     */
    @Deprecated
    List<E> findAllByQuery(TopiaQuery query) throws TopiaException;

    /**
     * Execute une requête basé sur l'entité du DAO. Permet de récupérer une map
     * d'entités correspondant à la requête. La clé de la map étant le topiaId
     * de l'entité.
     *
     * @param query la requête
     * @return la map d'entités correspondant à la recherche
     * @throws TopiaException if any pb while getting datas
     * @see TopiaQuery#executeToEntityMap(TopiaContext, Class)
     * @since 2.3
     * @deprecated since 2.6.12, {@link TopiaQuery} will be removed in version 3.0
     */
    @Deprecated
    Map<String, E> findAllMappedByQuery(TopiaQuery query) throws TopiaException;

    /**
     * Execute une requête basé sur l'entité du DAO. Permet de récupérer une map
     * d'entités correspondant à la requête. Le type et le nom de la propriété
     * utilisé comme clé de la map doit être passé en argument.
     *
     * @param <K>      type de la clé de la map
     * @param query    la requête
     * @param keyName  nom de la propriété de l'entité utilisée comme clé
     * @param keyClass type de la propriété de l'entité utilisée comme clé
     * @return la map d'entités correspondant à la recherche
     * @throws TopiaException if any pb while getting datas
     * @see TopiaQuery#executeToEntityMap(TopiaContext, Class)
     * @since 2.3
     * @deprecated since 2.6.12, {@link TopiaQuery} will be removed in version 3.0
     */
    @Deprecated
    <K> Map<K, E> findAllMappedByQuery(TopiaQuery query,
                                       String keyName, Class<K> keyClass) throws TopiaException;

    /**
     * Check the existence of an entity using a {@code query}.
     *
     * @param query query used to test existence
     * @return true if entity exists, false otherwise
     * @throws TopiaException
     * @since 2.3.4
     * @deprecated since 2.6.12, {@link TopiaQuery} will be removed in version 3.0
     */
    @Deprecated
    boolean existByQuery(TopiaQuery query) throws TopiaException;

    /**
     * Count the number of entities based on {@code query}.
     *
     * @param query query
     * @return number of entities filtered by the query
     * @throws TopiaException if any pb while getting datas
     * @since 2.3.4
     * @deprecated since 2.6.12, {@link TopiaQuery} will be removed in version 3.0
     */
    @Deprecated
    int countByQuery(TopiaQuery query) throws TopiaException;

    /**
     * Retourne les permissions a verifier pour l'acces a l'entite pour le
     * service Taas.
     *
     * @param topiaId topiaId d'une entite
     * @param actions encoded actions
     * @return la liste des permissions
     * @throws TopiaException if any pb while getting datas
     * @deprecated since 2.6.14, {@link TopiaQuery} will be removed in version 3.0
     */
    List<Permission> getRequestPermission(String topiaId, int actions)
            throws TopiaException;
}
