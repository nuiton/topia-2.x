/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/* *
 * TopiaDAO.java
 *
 * Created: 30 déc. 2005 03:00:57
 *
 * @author poussin &lt;poussin@codelutin.com&gt;
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
package org.nuiton.topia.persistence;

import org.nuiton.topia.TopiaException;
import org.nuiton.topia.event.TopiaEntityListener;
import org.nuiton.topia.event.TopiaEntityVetoable;
import org.nuiton.topia.framework.TopiaContextImplementor;
import org.nuiton.topia.generator.EntityDAOTransformer;
import org.nuiton.topia.persistence.pager.TopiaPagerBean;

import java.util.List;
import java.util.Map;

/**
 * TopiaDAO is used to manipulate entities corresponding to {@code E} type :
 * create, delete, update or find entities.
 * 
 * This interface is implemented by {@link TopiaDAOImpl} overridden by generation
 * from {@link EntityDAOTransformer}.
 * 
 *
 * @param <E> the entity type managed by the dao
 * @author bpoussin &lt;poussin@codelutin.com&gt;
 * @author fdesbois &lt;fdesbois@codelutin.com&gt;
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @version $Id$
 */
public interface TopiaDAO<E extends TopiaEntity> extends TopiaDAODeprecated<E>, Iterable<E> {

    //------------------------------------------------------------------------//
    //-- Create - update - delete methods ------------------------------------//
    //------------------------------------------------------------------------//

    /**
     * Create a new instance of managed entity <strong>not persisted</strong>.
     *
     * @return new entity instance
     * @throws TopiaException if any pb while creating the entity
     * @since 2.3.1
     */
    E newInstance() throws TopiaException;

    /**
     * Construit une nouvelle instance de l'objet géré par ce DAO
     *
     * @param properties la liste des propriétés que doit avoir l'objet créé les
     *                   arguments vont par paire (propertyName, value)
     * @return un nouvel objet
     * @throws TopiaException           si un problème est rencontré durant
     *                                  l'instanciation
     * @throws IllegalArgumentException Si le nombre on le type des arguments
     *                                  n'est pas bon ou que le type ou le nom
     *                                  d'une propriété est fausse
     */
    E create(Object... properties) throws TopiaException;

    /**
     * Construit une nouvelle instance de l'objet géré par ce DAO
     *
     * @param properties la liste des propriétés que doit avoir l'objet créé
     * @return un nouvel objet
     * @throws TopiaException           si un problème est rencontré durant
     *                                  l'instanciation
     * @throws IllegalArgumentException Si le nombre on le type des arguments
     *                                  n'est pas bon ou que le type ou le nom
     *                                  d'une propriété est fausse
     */
    E create(Map<String, Object> properties) throws TopiaException;

    /**
     * Permet de sauver un object instancié sans le DAO.
     * Utilisé notement dans le cas ou le DAO est situé derriere une couche
     * de webservice et que l'appel à la methode {@link #create(Object...)}
     * serait trop couteux.
     *
     * @param e l'entité instanciée à sauver
     * @return l'entité avec son topiaID valorisé
     * @throws TopiaException if any pb while creating datas
     * @since 2.3.1
     */
    E create(E e) throws TopiaException;

    /**
     * Permet d'ajouter ou de mettre a jour un objet. Cela permet d'ajouter par
     * exemple un objet provenant d'un autre context mais du meme type de DAO.
     *
     * @param e l'entite a ajouter ou mettre a jour
     * @return l'entity passé en paramètre.
     * @throws TopiaException if any pb while updating datas
     */
    E update(E e) throws TopiaException;

    /**
     * Permet de supprimer une entite.
     *
     * @param e l'entite a supprimer
     * @throws TopiaException if any pb while deleting datas
     */
    void delete(E e) throws TopiaException;

    /**
     * Permet de supprimer des entités.
     *
     * @param entities les entités à supprimer
     * @throws TopiaException if any pb while deleting datas
     */
    void deleteAll(Iterable<E> entities) throws TopiaException;

    //------------------------------------------------------------------------//
    //-- findByXXX methods ---------------------------------------------------//
    //------------------------------------------------------------------------//

    /**
     * Find an entity corresponding to the {@code id}. If the {@code id} is
     * null, nothing will be searched.
     *
     * @param id topiaId of the entity to found
     * @return the entity found or null if not
     * @throws TopiaException for Topia errors on query
     */
    E findByTopiaId(String id) throws TopiaException;

    /**
     * Find an entity matching {@code value} for the given {@code propertyName}.
     *
     * @param propertyName property name to filter
     * @param value        value of the property to math
     * @return the first entity matching the request
     * @throws TopiaException
     */
    E findByProperty(String propertyName, Object value) throws TopiaException;

    /**
     * @param propertyName le nom de la propriété
     * @param value        la valeur à tester
     * @param others       les autres proprietes doivent aller par 2
     *                     propertyName, value
     * @return l'entité trouvé
     * @throws TopiaException if any pb while getting datas
     */
    E findByProperties(String propertyName, Object value, Object... others) throws TopiaException;

    /**
     * Find an entity matching {@code properties}.
     *
     * @param properties les propriétés à matcher
     * @return l'entité trouvé
     * @throws TopiaException if any pb while getting datas
     */
    E findByProperties(Map<String, Object> properties) throws TopiaException;

    /**
     * Execute une requête basé sur l'entité du DAO. Permet de récupérer une
     * entité correspondant à la requête.
     *
     * @param hql    la requête hql à executer
     * @param params les paramètres de la requète
     * @return l'entité correspondant à la recherche ou null si aucune entité
     *         n'a été trouvée
     * @throws TopiaException if any pb while getting datas
     * @since 2.6.12
     */
    E findByQuery(String hql, Object... params) throws TopiaException;

    /**
     * Execute une requête basé sur le {@code type} donné.
     * 
     * Permet de récupérer une entité correspondant à la requête.
     *
     * @param hql    la requête hql à executer
     * @param params les paramètres de la requète
     * @return l'entité correspondant à la recherche ou null si aucune entité
     *         n'a été trouvée
     * @throws TopiaException if any pb while getting datas
     * @since 2.6.12
     */
    <R> R findByQuery(Class<R> type, String hql, Object... params) throws TopiaException;

    /**
     * Recherche la classe en utilisant la cle naturelle, chaque champs de la
     * cle naturelle est une entre de la map passe en argument.
     *
     * @param keys la liste des champs de la cle naturelle avec leur valeur
     * @return l'entite trouvé
     * @throws TopiaException if any pb while getting datas
     */
    E findByPrimaryKey(Map<String, Object> keys) throws TopiaException;

    /**
     * Recherche la classe en utilisant la cle naturelle, si la cle naturelle
     * est composé de plusieurs champs alors les arguments passés doivent être
     * dans l'ordre de declaration dans le fichier de mapping
     *
     * @param k l'objet cle naturelle de la classe
     * @return l'entité trouvé
     * @throws TopiaException if any pb while getting datas
     */
    E findByPrimaryKey(Object... k) throws TopiaException;

    /**
     * Récupère la première entité (du type géré par ce dao) dont la
     * collection nommée {@code propertyName} contient la {@code property}
     * donnée.
     *
     * @param propertyName le nom de la propriété
     * @param property     la propriété recherchée
     * @return la première entité recherchée
     * @throws TopiaException pour tout erreur lors de la recherche
     * @since 2.5.4
     */
    E findContains(String propertyName, Object property) throws TopiaException;

    //------------------------------------------------------------------------//
    //-- findAllXXX methods --------------------------------------------------//
    //------------------------------------------------------------------------//

    /**
     * Gets all entitys of the dao type in db.
     *
     * @return all entities of the dao type in db
     * @throws TopiaException
     */
    List<E> findAll() throws TopiaException;

    /**
     * Recuperation de tous les ids en base pour le type d'entite du dao.
     *
     * @return la liste de tous les ids
     * @throws TopiaException si pb en base
     */
    List<String> findAllIds() throws TopiaException;

    /**
     * Gets all entities of the dao type matching the {@code value} for the
     * given {@code propertyName}.
     *
     * @param propertyName property name to filter
     * @param value        value to match
     * @return matching entities
     * @throws TopiaException if any pb while getting datas
     */
    List<E> findAllByProperty(String propertyName, Object value)
            throws TopiaException;

    /**
     * Gets all entities of the dao type matching the {@code value} for the
     * given {@code propertyName} and {@code others}.
     *
     * @param propertyName le nom de la propriété
     * @param value        la valeur à tester
     * @param others       les autres proprietes doivent aller par 2
     *                     propertyName, value
     * @return matching entities
     * @throws TopiaException if any pb while getting datas
     */
    List<E> findAllByProperties(String propertyName, Object value,
                                Object... others) throws TopiaException;

    /**
     * Gets all entities of the dao type matching all the {@code properties}.
     *
     * @param properties properties to match
     * @return matching entities
     * @throws TopiaException if any pb while getting datas
     */
    List<E> findAllByProperties(Map<String, Object> properties)
            throws TopiaException;

    /**
     * Gets all entities when executing the given select query for the dao
     * entity type.
     *
     * @param hql    hql query
     * @param params query params
     * @return entites of the query result
     * @throws TopiaException if any pb while getting datas
     * @since 2.6.12
     */
    List<E> findAllByQuery(String hql, Object... params) throws TopiaException;

    /**
     * Gets all entities when executing the given select query for the given
     * {@code type} which may not be a entity type (int, long, map,...).
     *
     * @param hql    hql query
     * @param params query params
     * @return entites of the query result
     * @throws TopiaException if any pb while getting datas
     * @since 2.6.12
     */
    <R> List<R> findAllByQuery(Class<R> type,
                               String hql,
                               Object... params) throws TopiaException;

    /**
     * Gets all entities in lazy mode when executing the given select query
     * for the dao entity type.
     * 
     * <strong>Important note:</strong>
     *
     * @param hql    hql query
     * @param params query params
     * @return entites of the query result
     * @throws TopiaException if any pb while getting datas
     * @since 2.6.14
     */
    Iterable<E> findAllLazyByQuery(String hql,
                                   Object... params) throws TopiaException;

    /**
     * Gets all entities in lazy mode when executing the given select query
     * for the given {@code type} which may not be a entity type (int, long, map,...).
     * 
     * <strong>Important note:</strong>
     *
     * @param type   type of data to return
     * @param hql    hql query
     * @param params query params
     * @return entites of the query result
     * @throws TopiaException if any pb while getting datas
     * @since 2.6.14
     */
    <R> Iterable<R> findAllLazyByQuery(Class<R> type,
                                       String hql,
                                       Object... params) throws TopiaException;

    /**
     * Gets all entities in lazy mode when executing the given select query
     * for the dao entity type.
     * 
     * <strong>Important note:</strong>
     *
     * @param batchSize batch size
     * @param hql       hql query
     * @param params    query params
     * @return entites of the query result
     * @throws TopiaException if any pb while getting datas
     * @since 2.6.14
     */
    Iterable<E> findAllLazyByQuery(int batchSize,
                                   String hql,
                                   Object... params) throws TopiaException;

    /**
     * Gets all entities in lazy mode when executing the given select query
     * for the given {@code type} which may not be a entity type (int, long, map,...).
     * 
     * <strong>Important note:</strong>
     *
     * @param type      type of data to return
     * @param batchSize batch size
     * @param hql       hql query
     * @param params    query params
     * @return entites of the query result
     * @throws TopiaException if any pb while getting datas
     * @since 2.6.14
     */
    <R> Iterable<R> findAllLazyByQuery(Class<R> type,
                                       int batchSize,
                                       String hql,
                                       Object... params) throws TopiaException;

    /**
     * Gets a page of entities when executing the given select query for the dao
     * entity type (will only return the window of {@code startIndex -
     * endIndex} entities).
     *
     * @param hql        hql query to execute
     * @param startIndex first index of entity to return
     * @param endIndex   last index of entity to return
     * @param params     query params
     * @return entites of the paginated query result
     * @throws TopiaException if any pb while getting datas
     * @since 2.6.12
     */
    <R> List<R> findAllByQueryWithBound(Class<R> type,
                                        String hql,
                                        int startIndex,
                                        int endIndex,
                                        Object... params) throws TopiaException;

    /**
     * Gets a page of entities when executing the given select query for the dao
     * entity type (will only return the window of {@code startIndex -
     * endIndex} entities).
     *
     * @param hql        hql query to execute
     * @param startIndex first index of entity to return
     * @param endIndex   last index of entity to return
     * @param params     query params
     * @return entites of the paginated query result
     * @throws TopiaException if any pb while getting datas
     * @since 2.6.12
     */
    List<E> findAllByQueryWithBound(String hql,
                                    int startIndex,
                                    int endIndex,
                                    Object... params) throws TopiaException;

    /**
     * Gets a page of entities of the given select {@code hql} query using the
     * {@code pager} to obtain the window of entities to return.
     *
     * @param type   type of data to return
     * @param hql    hql query to execute
     * @param pager  pager to obtan the correct window of data
     * @param params params of the query
     * @return entities of the paginated query result
     * @throws TopiaException if any pb while getting datas
     * @see TopiaPagerBean
     * @since 2.6.12
     */
    <R> List<R> findAllByQueryAndPager(Class<R> type,
                                       String hql,
                                       TopiaPagerBean pager,
                                       Object... params) throws TopiaException;

    /**
     * Gets a page of entities of the given select {@code hql} query using the
     * {@code pager} to obtain the window of entities to return.
     *
     * @param hql    hql query to execute
     * @param pager  pager to obtan the correct window of data
     * @param params params of the query
     * @return entities of the paginated query result
     * @throws TopiaException if any pb while getting datas
     * @see TopiaPagerBean
     * @since 2.6.12
     */
    List<E> findAllByQueryAndPager(String hql,
                                   TopiaPagerBean pager,
                                   Object... params) throws TopiaException;

    /**
     * Gets all entites for the dao entity type order by given {@code propertyNames}.
     * 
     * You can add on each {@code property} {@code ASC} ou {@code DESC} to
     * fix order way (by default is {@code ASC}).
     *
     * @param propertyNames property names of order to apply
     * @return all entities of the dao entity type with given order
     * @throws TopiaException if any pb while getting datas
     */
    List<E> findAllWithOrder(String... propertyNames)
            throws TopiaException;

    /**
     * Récupère toutes les entités (du type géré par ce dao) dont la
     * collection nommée {@code propertyName} contient la {@code property}
     * donnée.
     *
     * @param propertyName le nom de la propriété
     * @param property     la propriété recherchée
     * @return toutes les entités recherchées
     * @throws TopiaException pour tout erreur lors de la recherche
     * @since 2.5.4
     */
    List<E> findAllContains(String propertyName,
                            Object property) throws TopiaException;

    //------------------------------------------------------------------------//
    //-- existsByXXX methods -------------------------------------------------//
    //------------------------------------------------------------------------//

    /**
     * Check the existence of an entity with technical {@code id}.
     *
     * @param id unique id of the entity to test existence.
     * @return true if entity exists, false otherwise
     * @throws TopiaException for Topia errors
     * @since 2.3.4
     */
    boolean existByTopiaId(String id) throws TopiaException;

    /**
     * Check the existence of an entity with {@code propertyName} with {@code
     * propertyValue}. {@code others} properties can be added to test
     * existence.
     *
     * @param propertyName  first property name to test existence
     * @param propertyValue first property value to test existence
     * @param others        altern propertyName and propertyValue
     * @return true if entity exists, false otherwise
     * @throws TopiaException for Topia errors
     * @since 2.3.4
     */
    boolean existByProperties(String propertyName, Object propertyValue,
                              Object... others) throws TopiaException;

    /**
     * Check the existence of an entity using a {@code hql} query.
     *
     * @param hql    query used to test existence
     * @param params params used by the query
     * @return true if entity exists, false otherwise
     * @throws TopiaException
     * @since 2.6.12
     */
    boolean existsByQuery(String hql, Object... params) throws TopiaException;

    //------------------------------------------------------------------------//
    //-- countXXX methods ----------------------------------------------------//
    //------------------------------------------------------------------------//

    /**
     * Count the number of existing entities.
     *
     * @return number of total entities
     * @throws TopiaException if any pb while getting datas
     * @since 2.3.4
     */
    long count() throws TopiaException;

    /**
     * Count the number of entities based on a {@code hql}.
     *
     * @param hql hql query to use
     * @return number of entities filtered by the query
     * @throws TopiaException if any pb while getting datas
     * @since 2.6.12
     */
    long countByQuery(String hql, Object... params) throws TopiaException;

    //------------------------------------------------------------------------//
    //-- other request methods -----------------------------------------------//
    //------------------------------------------------------------------------//

    /**
     * Find usages of the given {@code entity} in the entities of the given
     * {@code type}.
     *
     * @param type   the type of entity to search
     * @param entity the entity on which search is done
     * @param <R>    type of entity to search
     * @return the list of entities of the given type which uses the given
     *         entity
     * @throws TopiaException if any problem while getting data
     * @since 2.3.0
     */
    <R extends TopiaEntity> List<R> findUsages(Class<R> type, E entity)
            throws TopiaException;

    /**
     * Find all usages of the given {@code entity}.
     *
     * @param entity the entity
     * @return the dictionnary of usages of the given entities (keys are entity
     *         usage container, values are the list of this type of entity to
     *         use the given entity).
     * @throws TopiaException if any pb while getting data
     * @since 2.3.0
     */

    Map<Class<? extends TopiaEntity>, List<? extends TopiaEntity>> findAllUsages(E entity)
            throws TopiaException;

    /**
     * Execute the count {@code hql} query and then synch the pager to this
     * result (says fill the
     * {@link TopiaPagerBean#getRecords()} field and then adapt
     * the number of pages available and the current number page).
     *
     * @param hql    the count hql to execute
     * @param pager  the page to synch
     * @param params params of the count query
     * @throws TopiaException if any pb while getting datas
     * @see TopiaPagerBean
     * @since 2.6.12
     */
    void computeAndAddRecordsToPager(String hql,
                                     TopiaPagerBean pager,
                                     Object... params) throws TopiaException;

    //------------------------------------------------------------------------//
    //-- Misc methods --------------------------------------------------------//
    //------------------------------------------------------------------------//

    /**
     * Get the entityEnum of the type of entity managed by this DAO.
     *
     * @return entity type enum managed by this DAO
     */
    TopiaEntityEnum getTopiaEntityEnum();

    /**
     * Get the type of entity managed by this DAO.
     *
     * @return entity type managed by this DAO
     */
    Class<E> getEntityClass();

    /**
     * Obtains the batch size used to load data.
     * 
     * Default value if 1000.
     *
     * @return the batch size.
     * @since 2.6.14
     */
    int getBatchSize();

    /**
     * Set a new default batch size.
     *
     * @param batchSize new batch size to use when iterating.
     * @since 2.6.14
     */
    void setBatchSize(int batchSize);

    /**
     * When TopiaContextImpl create the TopiaDAOHibernate, it must call this
     * method just after.
     *
     * @param context     context
     * @param entityClass entity class
     * @throws TopiaException if any pb while init
     */
    void init(TopiaContextImplementor context, Class<E> entityClass)
            throws TopiaException;

    /**
     * Return context used by this DAO.
     *
     * @return the context.
     */
    TopiaContextImplementor getContext();

    /**
     * Create the simple hql query for the entity managed by the dao.
     * 
     * A optional alias can be passed:
     * 
     * <pre>FROM MyEntityImpl myAlias</pre>
     *
     * @param alias optional alias to use in query
     * @return the hql query
     * @since 2.6.14
     */
    String createSimpleQuery(String alias);

    //------------------------------------------------------------------------//
    //-- Listener methods ----------------------------------------------------//
    //------------------------------------------------------------------------//

    void addTopiaEntityListener(TopiaEntityListener listener);

    void addTopiaEntityVetoable(TopiaEntityVetoable vetoable);

    void removeTopiaEntityListener(TopiaEntityListener listener);

    void removeTopiaEntityVetoable(TopiaEntityVetoable vetoable);

} //TopiaDAO

