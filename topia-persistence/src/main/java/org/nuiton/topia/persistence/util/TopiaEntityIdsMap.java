/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.topia.persistence.util;

import org.nuiton.topia.persistence.TopiaEntity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * A map of ids of entities grouped by their type.
 *
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @since 2.4.3
 */
public class TopiaEntityIdsMap extends HashMap<Class<? extends TopiaEntity>, List<String>> {
    private static final long serialVersionUID = 1L;

    public void addIds(Class<? extends TopiaEntity> type, Iterable<String> ids) {
        List<String> list = get(type);
        if (list == null) {
            list = new ArrayList<String>();
            put(type, list);
        }
        for (String id : ids) {
            if (!list.contains(id)) {
                list.add(id);
            }
        }
    }
}
