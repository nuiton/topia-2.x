/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2020 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.topia.persistence.util;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

/**
 * A pager bean.
 *
 * Copied from nuiton-utils-3.0, removed since then.
 */
@Deprecated
public class PagerBean implements Serializable {

    private static final long serialVersionUID = 1L;

    /** Number of records (says numbers of rows displayable. */
    protected long records;

    /** Index of the first record in this page. */
    protected long recordStartIndex;

    /** Index of the last record in this page. */
    protected long recordEndIndex;

    /** Index of the page. */
    protected int pageIndex;

    /** Page size, says number of records in a page. */
    protected int pageSize;

    /** Number of pages. */
    protected long pagesNumber;

    public long getRecords() {
        return records;
    }

    public long getRecordStartIndex() {
        return recordStartIndex;
    }

    public long getRecordEndIndex() {
        return recordEndIndex;
    }

    public int getPageIndex() {
        return pageIndex;
    }

    public int getPageSize() {
        return pageSize;
    }

    public long getPagesNumber() {
        return pagesNumber;
    }

    public void setRecords(long records) {
        this.records = records;
    }

    public void setRecordStartIndex(long recordStartIndex) {
        this.recordStartIndex = recordStartIndex;
    }

    public void setRecordEndIndex(long recordEndIndex) {
        this.recordEndIndex = recordEndIndex;
    }

    public void setPageIndex(int pageIndex) {
        this.pageIndex = pageIndex;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public void setPagesNumber(long pagesNumber) {
        this.pagesNumber = pagesNumber;
    }

    @Override
    public String toString() {
        String result = new ToStringBuilder(this)
                .append("records", records)
                .append("recordStartIndex", recordStartIndex)
                .append("recordEndIndex", recordEndIndex)
                .append("pageIndex", pageIndex)
                .append("pageSize", pageSize)
                .append("pagesNumber", pagesNumber)
                .toString();
        return result;
    }

}
