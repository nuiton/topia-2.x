/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2020 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.topia.persistence.util;

import org.apache.commons.lang3.tuple.Pair;

import java.util.ArrayList;
import java.util.List;

/**
 * Useful method around {@link PagerBean}.
 *
 * Copied from nuiton-utils-3.0, removed since then.
 */
public class PagerBeanUtil {

    /**
     * Given a total count of elements and a page size, compute the number of
     * pages available.
     *
     * @param totalCount total count of elements in the pager
     * @param pageSize   size of a page
     * @return the number of available pages
     */
    public static long getTotalPage(long totalCount, int pageSize) {
        long result = (long) Math.ceil((double) totalCount / (double) pageSize);
        return result;
    }

    /**
     * Compute the pager bound given his datas:
     * <ul>
     * <li>{@code totalCount}: count of all elements of the pager</li>
     * <li>{@code pageSize}: number of elements in a page</li>
     * <li>{@code page}: the requested page number starting at {@code 1}</li>
     * </ul>
     *
     * @param totalCount total count of elements in the pager
     * @param page       pager page number (starting at {@code 1})
     * @param pageSize   number of elements in a page
     * @return the bound of start and end index of the requested elements of the pager
     */
    public static Pair<Long, Long> getPageBound(long totalCount,
                                                int page,
                                                int pageSize) {

        page = Math.max(page, 1);
        long pageSize2 = pageSize == -1 ? totalCount : Math.max(pageSize, 1);

        long start = (page - 1l) * pageSize2;
        long end = page * pageSize2; // End in subList is exclusive

        end = Math.min(end, totalCount); //To be sure that end will not be out of bound in subList

        Pair<Long, Long> result = Pair.of(start, end);
        return result;
    }

    /**
     * Get the elements of the lists using the pager datas:
     *
     * <ul>
     * <li>{@code elements}:all elements of the pager</li>
     * <li>{@code pageSize}: number of elements in a page</li>
     * <li>{@code page}: the requested page number starting at {@code 1}</li>
     * </ul>
     *
     * @param elements all pager elements
     * @param page     pager page number (starting at {@code 1})
     * @param pageSize number of elements in a page
     * @param <E>      n'importe quel type
     * @return la liste une fois filtrée
     */
    public static <E> List<E> getPage(List<E> elements, int page, int pageSize) {

        Pair<Long, Long> bound = getPageBound(elements.size(), page, pageSize);

        long start = bound.getLeft();
        long end = bound.getRight();

        List<E> subList = elements.subList((int) start, (int) end);
        List<E> result = new ArrayList<E>(subList);

        return result;
    }

    /**
     * Given a pagerBean, compute his {@link PagerBean#recordStartIndex},
     * {@link PagerBean#recordEndIndex} and {@link PagerBean#pagesNumber}.
     *
     * @param bean the pagerBean to fill
     * @since 2.4.3
     */
    public static void computeRecordIndexesAndPagesNumber(PagerBean bean) {
        long records = bean.getRecords();
        int pageSize = bean.getPageSize();

        Pair<Long, Long> pageBound =
                getPageBound(records, bean.getPageIndex(), pageSize);
        bean.setRecordStartIndex(pageBound.getLeft());
        bean.setRecordEndIndex(pageBound.getRight());
        bean.setPagesNumber(getTotalPage(records, pageSize));
    }

    public static PagerBean newPagerBean() {
        return new PagerBean();
    }
}
