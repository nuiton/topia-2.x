/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
/* *##% 
 * ToPIA :: Persistence
 * Copyright (C) 2004 - 2009 CodeLutin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * ##%*/

package org.nuiton.topia.persistence;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.FlushMode;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.metadata.ClassMetadata;
import org.nuiton.topia.TopiaException;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Surcharge du {@link TopiaDAOImpl} pour utiliser l'api criteria au lieu du hql
 * pour tout ce qui est requétage.
 * 
 * Created: 31 déc. 2005 13:10:34
 *
 * @param <E> le type de l'entite
 * @author bpoussin &lt;poussin@codelutin.com&gt;
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @deprecated since 2.6.12 Using the hibernate Criteria api is not a good idea as we wants to use in ToPIA next generation (version 3.0) jpa api.
 */
@Deprecated
public class TopiaDAOLegacy<E extends TopiaEntity> extends TopiaDAOImpl<E> { // TopiaDAOLegacy

    /** Logger. */
    private static Log log = LogFactory.getLog(TopiaDAOLegacy.class);

    @SuppressWarnings("unchecked")
    protected E instanciateNew() throws TopiaException {
        E result = newInstance();
        return result;
    }

    @Override
    public E create(Object... properties) throws TopiaException {
        Map<String, Object> map = new HashMap<String, Object>();
        Object propertyName = null;
        Object value = null;
        try {
            for (int i = 0; i < properties.length; ) {
                propertyName = properties[i++];
                value = properties[i++];
                map.put((String) propertyName, value);
            }
        } catch (ArrayIndexOutOfBoundsException eee) {
            throw new IllegalArgumentException("Wrong number of argument "
                                               + properties.length
                                               + ", you must have even number. Last property name read: "
                                               + propertyName);
        } catch (ClassCastException eee) {
            throw new IllegalArgumentException(
                    "Wrong argument type, wait property name as String and have "
                    + propertyName.getClass().getName());
        }

        E result = create(map);
        return result;
    }

    @Override
    public E findByPrimaryKey(Map<String, Object> keys)
            throws TopiaException {
        try {
            // we used hibernate meta information for all persistence type
            // it's more easy than create different for all persistence
            ClassMetadata meta = getClassMetadata();
            if (meta.hasNaturalIdentifier()) {
                E result = findByProperties(keys);
                return result;
            }
        } catch (HibernateException eee) {
            throw new TopiaException(eee);
        }
        throw new TopiaException("La classe " + entityClass.getName()
                                 + " n'a pas de cle primaire naturelle");

    }

    @Override
    public E findByPrimaryKey(Object... k) throws TopiaException {
        // TODO pour une meilleur gestion des problemes a la compilation
        // mettre un premier couple (propName, value) en argument ca evitera
        // de pouvoir appeler cette methode sans argument
        try {
            ClassMetadata meta = getClassMetadata();
            if (meta.hasNaturalIdentifier()) {
                int[] ikeys = meta.getNaturalIdentifierProperties();
                String[] pnames = meta.getPropertyNames();

                Map<String, Object> keys = new HashMap<String, Object>();
                for (int ikey : ikeys) {
                    keys.put(pnames[ikey], k[ikey]);
                }

                E result = findByProperties(keys);
                return result;
            }
        } catch (HibernateException eee) {
            throw new TopiaException(eee);
        }
        throw new TopiaException("La classe " + entityClass.getName()
                                 + " n'a pas de cle primaire naturelle");

    }

    @Override
    public E findByProperties(String propertyName, Object value,
                              Object... others) throws TopiaException {
        Map<String, Object> properties = new HashMap<String, Object>();
        properties.put(propertyName, value);
        Object name = null;
        for (int i = 0; i < others.length; ) {
            try {
                name = others[i++];
                value = others[i++];
                properties.put((String) name, value);
            } catch (ClassCastException eee) {
                throw new IllegalArgumentException(
                        "Les noms des propriétés doivent être des chaines et non pas "
                        + propertyName.getClass().getName(), eee);
            } catch (ArrayIndexOutOfBoundsException eee) {
                throw new IllegalArgumentException(
                        "Le nombre d'argument n'est pas un nombre pair: "
                        + (others.length + 2)
                        + " La dernière propriété était: " + name, eee);
            }
        }
        E result = findByProperties(properties);
        return result;
    }

    @Override
    public List<E> findAllByProperties(String propertyName, Object value,
                                       Object... others) throws TopiaException {
        Map<String, Object> properties = new HashMap<String, Object>();
        properties.put(propertyName, value);
        Object name = null;
        for (int i = 0; i < others.length; ) {
            try {
                name = others[i++];
                value = others[i++];
                properties.put((String) name, value);
            } catch (ClassCastException eee) {
                throw new IllegalArgumentException(
                        "Les noms des propriétés doivent être des chaines et non pas "
                        + propertyName.getClass().getName(), eee);
            } catch (ArrayIndexOutOfBoundsException eee) {
                throw new IllegalArgumentException(
                        "Le nombre d'argument n'est pas un nombre pair: "
                        + (others.length + 2)
                        + " La dernière propriété était: " + name, eee);
            }
        }
        List<E> result = findAllByProperties(properties);
        return result;
    }

//    @Override
//    public E findContainsProperties(Map<String, Collection<?>> properties)
//            throws TopiaException {
//        List<E> results = findAllContainsProperties(properties);
//        E result = null;
//        if (results.size() > 0) {
//            result = results.get(0);
//        }
//        return result;
//    }
//
//    @Override
//    public E findContainsProperties(String propertyName,
//            Collection values, Object... others) throws TopiaException {
//        Map<String, Collection<?>> properties = new HashMap<String, Collection<?>>();
//        properties.put(propertyName, values);
//        Object name = null;
//        for (int i = 0; i < others.length;) {
//            try {
//                name = others[i++];
//                values = (Collection) others[i++];
//                properties.put((String) name, values);
//            } catch (ClassCastException eee) {
//                throw new IllegalArgumentException(
//                        "Les noms des propriétés doivent être des chaines et non pas "
//                                + propertyName.getClass().getName(), eee);
//            } catch (ArrayIndexOutOfBoundsException eee) {
//                throw new IllegalArgumentException(
//                        "Le nombre d'argument n'est pas un nombre pair: "
//                                + (others.length + 2)
//                                + " La dernière propriété était: " + name, eee);
//            }
//        }
//        E result = findContainsProperties(properties);
//        return result;
//    }
//
//    /**
//     * Find all entities with a specific rule :
//     * When the entity have a Collection type property, you want to find all entites where some values are
//     * contained in the collection type property.
//     * Example entity parameter : private Collection<Date> historicalDates;
//     * You want some dates to be contained in historicalDates.
//     * Collection<Date> myDates...
//     * myDates.add(date1) ...
//     * Map<String, Collection> properties = new HashMap<String,Collection>();
//     * properties.put("historicalDates",myDates);
//     * findAllContainsProperties(properties);
//     * @param properties
//     * @return the list of entities corresponding to the request
//     * @throws org.nuiton.topia.TopiaException  if any pb
//     */
//    @Override
//    public List<E> findAllContainsProperties(Map<String, Collection<?>> properties) throws TopiaException {
//        List<E> all = findAll();
//        List<E> result = new ArrayList<E>();
//        for (E e : all) {
//            boolean ok = true;
//            try {
//                for (Entry<String, Collection<?>> kv : properties.entrySet()) {
//                    Collection entityValues = (Collection) PropertyUtils
//                            .getProperty(e, kv.getKey());
//                    Collection values = kv.getValue();
//                    if (!entityValues.containsAll(values)) {
//                        ok = false;
//                        break;
//                    }
//                }
//            } catch (IllegalAccessException eee) {
//                ok = false;
//                if (log.isWarnEnabled()) {
//                    log.warn(
//                            "Impossible d'acceder a la methode demandé pour l'obbjet "
//                                    + e, eee);
//                }
//            } catch (InvocationTargetException eee) {
//                ok = false;
//                if (log.isWarnEnabled()) {
//                    log.warn(
//                            "Impossible d'acceder a la methode demandé pour l'obbjet "
//                                    + e, eee);
//                }
//            } catch (NoSuchMethodException eee) {
//                ok = false;
//                if (log.isWarnEnabled()) {
//                    log.warn(
//                            "Impossible d'acceder a la methode demandé pour l'obbjet "
//                                    + e, eee);
//                }
//            }
//            if (ok) {
//                result.add(e);
//            }
//        }
//        return result;
//    }
//
//    @Override
//    public List<E> findAllContainsProperties(String propertyName,
//            Collection values, Object... others) throws TopiaException {
//        Map<String, Collection<?>> properties = new HashMap<String, Collection<?>>();
//        properties.put(propertyName, values);
//        Object name = null;
//        for (int i = 0; i < others.length;) {
//            try {
//                name = others[i++];
//                values = (Collection) others[i++];
//                properties.put((String) name, values);
//            } catch (ClassCastException eee) {
//                throw new IllegalArgumentException(
//                        "Les noms des propriétés doivent être des chaines et non pas "
//                                + propertyName.getClass().getName(), eee);
//            } catch (ArrayIndexOutOfBoundsException eee) {
//                throw new IllegalArgumentException(
//                        "Le nombre d'argument n'est pas un nombre pair: "
//                                + (others.length + 2)
//                                + " La dernière propriété était: " + name, eee);
//            }
//        }
//        List<E> result = findAllContainsProperties(properties);
//        return result;
//    }

    @Override
    public List<E> findAllByProperty(String propertyName, Object value)
            throws TopiaException {
        Map<String, Object> properties = new HashMap<String, Object>();
        properties.put(propertyName, value);
        List<E> result = findAllByProperties(properties);
        return result;
    }

    @Override
    public E findByProperty(String propertyName, Object value)
            throws TopiaException {
        Map<String, Object> properties = new HashMap<String, Object>();
        properties.put(propertyName, value);
        E result = findByProperties(properties);
        return result;
    }

    @Deprecated
    private Criterion computeCriterions(Object... values) {
        if (values == null) {
            return null;
        }
        Criterion criterion = null;
        for (Object value : values) {
            criterion = or(criterion, computeCriterion(value));
        }
        return criterion;
    }

    @Deprecated
    private Criterion computeCriterion(Object value) {
        Criterion criterion = null;
        SearchFields fields = entityClass.getAnnotation(SearchFields.class);
        String textValue = "%" + value + "%";
        //textFields
        String[] textFields = fields.txtFields();
        for (String propName : textFields) {
            criterion = or(criterion, Restrictions.like(propName, textValue));
        }
        //numFields
        boolean isNumber = value instanceof Number;
        if (value instanceof String) {
            try {
                Double.parseDouble((String) value);
                isNumber = true;
            } catch (NumberFormatException nfe) {
                isNumber = false;
            }

        }
        if (isNumber) {
            String[] numFields = fields.numFields();
            for (String propName : numFields) {
                criterion = or(criterion, Restrictions.sqlRestriction(propName
                                                                      + " like '" + textValue + "'"));
            }
        }
        //boolFields
        boolean isBoolean = value instanceof Boolean;
        if (value instanceof String) {
            isBoolean |= "true".equalsIgnoreCase((String) value) || "false"
                    .equalsIgnoreCase((String) value);
        }
        if (isBoolean) {
            Boolean booleanValue;
            if (value instanceof String) {
                booleanValue = Boolean.valueOf((String) value);
            } else {
                booleanValue = (Boolean) value;
            }
            String[] boolFields = fields.numFields();
            for (String propName : boolFields) {
                criterion = or(criterion, Restrictions.eq(propName,
                                                          booleanValue));
            }
        }
        //timeFields
        String[] timeFields = fields.dateFields();
        for (String propName : timeFields) {
            criterion = or(criterion, Restrictions.sqlRestriction(propName
                                                                  + " like '" + textValue + "'"));
        }
        return criterion;
    }

    @Deprecated
    private Criterion or(Criterion crit1, Criterion crit2) {
        if (crit1 == null) {
            return crit2;
        }
        if (crit2 == null) {
            return crit1;
        }
        return Restrictions.or(crit1, crit2);
    }

    /**
     * Cette methode appelle fireVetoableCreate et fireOnCreated
     * Si vous la surchargé, faites attention a appeler le super
     * ou a appeler vous aussi ces deux methodes.
     */
    @Override
    public E create(Map<String, Object> properties) throws TopiaException {
        E result = instanciateNew();

        // TODO reflechir s'il ne faudrait pas creer l'id avant l'event precedent
        // reflechir toujours dans un context on les E pourrait ne pas
        // etre des TopiaEntity
        if (result instanceof TopiaEntity) {
            String topiaId = TopiaId.create(entityClass);
            TopiaEntityAbstract entity = (TopiaEntityAbstract) result;
            entity.setTopiaId(topiaId);
            entity.setTopiaContext(getContext());
        }
        try {
            for (Map.Entry<String, Object> e : properties.entrySet()) {
                String propertyName = e.getKey();
                Object value = e.getValue();
                PropertyUtils.setProperty(result, propertyName, value);
            }
        } catch (IllegalAccessException eee) {
            throw new IllegalArgumentException(
                    "Can't put properties on new Object", eee);
        } catch (InvocationTargetException eee) {
            throw new IllegalArgumentException(
                    "Can't put properties on new Object", eee);
        } catch (NoSuchMethodException eee) {
            throw new IllegalArgumentException(
                    "Can't put properties on new Object", eee);
        }

        // on fait un save maintenant, car puisqu'on a creer l'entity au
        // travers du DAO, on s'attend a l'avoir a disposition tout de
        // suite pour les requetes sans avoir a faire un update dessus
        getSession().save(result);
        getContext().getFiresSupport().warnOnCreateEntity(result);
        return result;
    }

    @Override
    public E findByTopiaId(String k) throws TopiaException {
        return query(Restrictions.idEq(k));
    }

    @Override
    public List<E> findAll() throws TopiaException {
        try {
            Criteria criteria = createCriteria(FlushMode.AUTO);
            List<E> result = (List<E>) criteria.list();
            result = getContext().getFiresSupport().fireEntitiesLoad(context,
                                                                     result);
            return result;
        } catch (HibernateException eee) {
            throw new TopiaException(eee);
        }
    }

    @Override
    public List<String> findAllIds() throws TopiaException {
//        List<String> findAll = context.findAll("select src.topiaId from " + entityClass.getSimpleName() + "Impl src");
        List<String> find = context.findAll("select src.topiaId from " + getEntityClass() + " src");
        return find;
    }


    @Override
    public List<E> findAllWithOrder(String... propertyNames)
            throws TopiaException {
        try {
            Criteria criteria = createCriteria(FlushMode.AUTO);
            for (String propertyName : propertyNames) {
                criteria.addOrder(Order.asc(propertyName));
            }
            List<E> result = (List<E>) criteria.list();
            result = getContext().getFiresSupport().fireEntitiesLoad(context,
                                                                     result);
            return result;
        } catch (HibernateException eee) {
            throw new TopiaException(eee);
        }
    }

    @Override
    public E findByProperties(Map<String, Object> properties)
            throws TopiaException {
        return query(Restrictions.allEq(properties));
    }

    @Override
    public List<E> findAllByProperties(Map<String, Object> properties)
            throws TopiaException {
        return queryAll(Restrictions.allEq(properties));
    }

    private List<E> queryAll(Criterion criterion) throws TopiaException {
        try {
            Criteria criteria = createCriteria(FlushMode.AUTO);
            criteria.add(criterion);
            List<E> result = (List<E>) criteria.list();
            result = getContext().getFiresSupport().fireEntitiesLoad(context,
                                                                     result);
            return result;
        } catch (HibernateException eee) {
            throw new TopiaException(eee);
        }
    }

    private E query(Criterion criterion) throws TopiaException {
        try {
            Criteria criteria = createCriteria(FlushMode.AUTO);
            criteria.add(criterion);
            criteria.setMaxResults(1);
            List<E> result = (List<E>) criteria.list();
            int sizeBefore = result != null ? result.size() : 0;
            result = getContext().getFiresSupport().fireEntitiesLoad(context,
                                                                     result);
            int sizeAfter = result != null ? result.size() : 0;
            if (sizeAfter < sizeBefore) {
                if (log.isDebugEnabled()) {
                    log.debug((sizeBefore - sizeAfter)
                              + " element(s) removed. Filter entity: "
                              + entityClass.getName() + " - criterion: "
                              + criterion);
                }
            }
            if (result != null && result.size() > 0) {
                E elem = result.get(0);
                return elem;
            }
            return null;
        } catch (HibernateException eee) {
            throw new TopiaException(eee);
        }
    }

    //FIXME : Commenté car impossible de trouver le bon Criterion
    // ATTENTION ancienne methode du TopiaDAOAbstract deja presente dans le fichier

    //    /* (non-Javadoc)
    //     * @see org.nuiton.topia.persistence.TopiaDAO#findContainsProperties(java.util.Map)
    //     */
    //    public E findContainsProperties(Map<String, Collection> properties) throws TopiaException {
    //        try {
    //            Criteria criteria = createCriteria(FlushMode.AUTO);
    //            for (Entry<String, Collection> entry : properties.entrySet()) {
    //                for (Object value : entry.getValue()) {
    //                    criteria.add(Restrictions.eq(entry.getKey(), value));
    //                }
    //            }
    //            criteria.setMaxResults(1);
    //            List<E> results = (List<E>)criteria.list();
    //            if (results.size() > 0) {
    //                return (E)results.get(0);
    //            } else {
    //                return null;
    //            }
    //        } catch (HibernateException eee) {
    //            throw new TopiaException(eee);
    //        }
    //    }
    //
    //    /* (non-Javadoc)
    //     * @see org.nuiton.topia.persistence.TopiaDAO#findAllContainsProperties(java.util.Map)
    //     */
    //    public List<E> findAllContainsProperties(Map<String, Collection> properties) throws TopiaException {
    //        try {
    //            Criteria criteria = createCriteria(FlushMode.AUTO);
    //            for (Entry<String, Collection> entry : properties.entrySet()) {
    //                for (Object value : entry.getValue()) {
    //                    criteria.add(Restrictions.eq(entry.getKey(), value));
    //                }
    //            }
    //            List<E> result = (List<E>)criteria.list();
    //            return result;
    //        } catch (HibernateException eee) {
    //            throw new TopiaException(eee);
    //        }
    //    }

    /**
     * Renvoie un Criteria créé avec l'entityClass
     *
     * @param mode le FlushMode du Criteria
     * @return le Criteria nouvellement créé
     * @throws TopiaException if any pb
     */
    private Criteria createCriteria(FlushMode mode) throws TopiaException {
        Criteria criteria = getSession().createCriteria(entityClass);
        criteria.setFlushMode(mode);
        return criteria;
    }

} //TopiaDAOLegacy
