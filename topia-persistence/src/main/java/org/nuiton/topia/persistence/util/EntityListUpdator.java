/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.topia.persistence.util;

import org.apache.commons.beanutils.PropertyUtilsBean;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.topia.TopiaException;
import org.nuiton.topia.persistence.TopiaEntity;

import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collection;

/**
 * A implementation of {@link ListUpdator} for {@link TopiaEntity} type.
 * 
 * Some factory methods are defined to simplify the generic cast, prefer used them
 * instead of the (protected) constructor.
 *
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @param <P> type of parent of childs
 * @param <E> type of child
 */
public class EntityListUpdator<P extends TopiaEntity, E extends TopiaEntity> implements ListUpdator<P, E> {

    public static <P extends TopiaEntity, E extends TopiaEntity> EntityListUpdator<P, E> newEntityListUpdator(Class<P> parentClass, Class<E> childClass, String propertyName) {
        return new EntityListUpdator<P, E>(parentClass, childClass, propertyName);
    }

    /** name of the field containing the childs */
    protected String propertyName;

    /** descriptor of the filed containing the childs */
    protected PropertyDescriptor descriptor;

    protected Method getMethod;

    protected Method addMethod;

    protected Method removeMethod;

    protected Method removeAllMethod;

    protected Method sizeMethod;

    protected Method emptyMethod;

    protected EntityListUpdator(Class<P> parentClass, Class<E> childClass, String propertyName) {
        this.propertyName = propertyName;

        for (PropertyDescriptor propertyDescriptor : new PropertyUtilsBean().getPropertyDescriptors(parentClass)) {
            if (propertyDescriptor.getName().equals(propertyName)) {
                descriptor = propertyDescriptor;
            }
        }
        String cap = StringUtils.capitalize(propertyName);
        try {
            getMethod = parentClass.getMethod("get" + cap + "ByTopiaId", String.class);
            addMethod = parentClass.getMethod("add" + cap, childClass);
            removeMethod = parentClass.getMethod("remove" + cap, childClass);
            removeAllMethod = parentClass.getMethod("clear" + cap);
            sizeMethod = parentClass.getMethod("size" + cap);
            emptyMethod = parentClass.getMethod("is" + cap + "Empty");
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String getPropertyName() {
        return propertyName;
    }

    @Override
    public E getChild(P parent, String topiaId) {
        return EntityListUpdator.<E>invokeWithResult(getMethod, parent, topiaId);
    }

    @Override
    public Collection<E> getChilds(P parent) {
        return EntityListUpdator.<Collection<E>>invokeWithResult(descriptor.getReadMethod(), parent);
    }

    @Override
    public int size(P parent) {
        return EntityListUpdator.<Integer>invokeWithResult(sizeMethod, parent);
    }

    @Override
    public boolean isEmpty(P parent) {
        return EntityListUpdator.<Boolean>invokeWithResult(emptyMethod, parent);
    }

    @Override
    public void setChilds(P parent, Collection<E> childs) {
        invoke(descriptor.getWriteMethod(), parent, childs);
    }

    @Override
    public void addToList(P parent, E bean) throws TopiaException {
        invoke(addMethod, parent, bean);
    }

    @Override
    public void removeFromList(P parent, E bean) throws TopiaException {
        invoke(removeMethod, parent, bean);
    }

    @Override
    public void removeAll(P parent) {
        invoke(removeAllMethod, parent);
    }

    protected static void invoke(Method m, Object bean, Object... args) {
        try {
            m.invoke(bean, args);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        } catch (InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }

    @SuppressWarnings({"unchecked"})
    protected static <V> V invokeWithResult(Method m, Object bean, Object... args) {
        try {
            return (V) m.invoke(bean, args);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        } catch (InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }
}
