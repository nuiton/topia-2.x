/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.topia.persistence.util;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

/**
 * Pour qualifier l'etat d'une entite lors du calcul d'un differentiel entre
 * deux entites.
 *
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @see TopiaEntityHelper#buildDifferentiel(List, List)
 * @since 2.2.0
 */
public enum DiffState {

    /**
     * nouvel entite dans le storage de reference.
     * 
     * A ajouter dans le storage local.
     */
    NEW,
    /**
     * entite modifie dans le storage de reference (voir topiaVersion).
     * 
     * A mettre a jour dans le storage local.
     */
    MODIFIED,
    /**
     * entité supprimée dans le storage de reference.
     * 
     * A supprimer du storage local (apres remplacement par autre chose...)
     */
    REMOVED;

    /**
     * Construit un dictionnaire avec pour tous les états une liste vide.
     *
     * @return le dictionnaire crée
     */
    public static DiffStateMap newMap() {
        DiffStateMap result =
                new DiffStateMap();
        for (DiffState state : values()) {
            result.put(state, new ArrayList<String>());
        }
        return result;
    }

    /**
     * Ajoute dans le premier dictionnaire, les listes du second dictionnaire.
     *
     * @param mainMap le dictionnaire principale
     * @param toAdd   le dictionne a ajouter dans le dictionnaire principale
     */
    public static void addAll(DiffStateMap mainMap,
                              DiffStateMap toAdd) {
        for (DiffState state : values()) {
            List<String> newList = toAdd.get(state);
            if (newList != null && !newList.isEmpty()) {
                mainMap.get(state).addAll(newList);
            }
        }
    }

    /**
     * Nettoye un dictionnaire donnee de toute ses donnees.
     *
     * @param mainMap le dictionnaire a nettoyer.
     */
    public static void clear(DiffStateMap mainMap) {
        for (DiffState state : values()) {
            List<String> newList = mainMap.get(state);
            if (newList != null) {
                newList.clear();
            }
            mainMap.remove(state);
        }
    }

    /**
     * Supprime toutes les entrees vides du dictionnaire .
     *
     * @param mainMap le dictionnaire a nettoyer
     */
    public static void removeEmptyStates(
            DiffStateMap mainMap) {
        for (DiffState state : values()) {
            List<String> newList = mainMap.get(state);
            if (newList == null || newList.isEmpty()) {
                mainMap.remove(state);
            }
        }
    }

    public static class DiffStateMap extends EnumMap<DiffState, List<String>> {
        private static final long serialVersionUID = 1L;

        public DiffStateMap() {
            super(DiffState.class);
        }

        public DiffStateMap(EnumMap<DiffState, ? extends List<String>> m) {
            super(m);
        }

        public DiffStateMap(Map<DiffState, ? extends List<String>> m) {
            super(m);
        }
    }
}
