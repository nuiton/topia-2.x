/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.topia.persistence;

import com.google.common.base.Function;
import org.nuiton.topia.TopiaNotFoundException;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * TODO-fdesbois-20100508 : Need translation of javadoc.
 * 
 * Classe representant un Id, utilisable par JDO. Cette classe contient aussi un ensemble de methode
 * static utile pour la manipulation des topiaId.
 * 
 * TODO-tchemit-2012-08-16 Rename this class to {@code TopiaIds}.
 *
 * @author poussin &lt;poussin@codelutin.com&gt;
 * @author tchemit &lt;tchemit@codelutin.com&gt;
 * @author chatellier &lt;chatellier@codelutin.com&gt;
 * @version $Id$
 */
public class TopiaId implements Serializable { // TopiaId

    /**  */
    private static final long serialVersionUID = 1L;

    /**
     * Function to obtain {@link TopiaEntity#getTopiaId()} from any entity.
     *
     * @since 2.6.12
     */
    public static final Function<TopiaEntity, String> GET_TOPIA_ID = new Function<TopiaEntity, String>() {

        @Override
        public String apply(TopiaEntity input) {
            return input == null ? null : input.getTopiaId();
        }
    };

    public String topiaId;

    public TopiaId() {
    }

    public TopiaId(String topiaId) {
        this.topiaId = topiaId;
    }

    @Override
    public int hashCode() {
        if (topiaId == null) {
            //TODO-TC20100225 : use commons-loggin api instead of jdk one
            Logger.getLogger(getClass().getName() + ".hashCode").log(
                    Level.WARNING, "Use null topiaId", new Throwable());
            return 0;
        }
        return topiaId.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        return topiaId.equals(o);
    }

    @Override
    public String toString() {
        return topiaId;
    }

    /**
     * Cree un topiaId pour une certaine classe
     *
     * @param clazz
     * @return a generated topiaId
     */
    public static String create(Class clazz) {
        if (!clazz.isInterface()) {
            throw new IllegalArgumentException(
                    "Only interface is permit to create id: " + clazz);
        }
        double random = Math.random();
        while (Double.toString(random).contains("E-")) {
            random = Math.random();
        }
        return clazz.getName() + '#' + System.currentTimeMillis() + '#'
               + random;
    }

    /**
     * Extrait la classe du topiaId.
     *
     * @param topiaId
     * @return class
     * @throws TopiaNotFoundException
     */
    public static Class getClassName(String topiaId)
            throws TopiaNotFoundException {
        String classname = getClassNameAsString(topiaId);
        try {
            Class result = Class.forName(classname);
            return result;
        } catch (ClassNotFoundException eee) {
            throw new TopiaNotFoundException("Can't find class for " + topiaId,
                                             eee);
        }
    }

    /**
     * Return class name id topiaId is id, and empty string if topiaId is not an
     * id.
     *
     * @param topiaId
     * @return class name
     */
    public static String getClassNameAsString(String topiaId) {
        String result = "";
        int i = topiaId.indexOf('#');
        if (i > 0) {
            result = topiaId.substring(0, i);
        }
        return result;
    }

    /**
     * Verifie si l'id passé en paramètre est bien un Id topia, c-a-d si la
     * forme est bien classname#timemillis#random et si le classname est celui
     * d'une classe valide, c-a-d que le systeme arrive a trouver.
     *
     * @param topiaId
     * @return is valid topiaId
     */
    public static boolean isValidId(String topiaId) {
        try {
            if (topiaId.matches(".*?#[0-9]+#[0-9.]+")) {
                getClassName(topiaId);
                return true;
            }
            return false;
        } catch (Exception eee) {
            //TODO-TC20100225 : use commons-loggin api instead of jdk one
            Logger.getLogger(TopiaId.class.getName() + ".isValidId").log(
                    Level.WARNING, "Error during verfication of topiaId", eee);
            return false;
        }
    }

} // TopiaId

