/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.topia.persistence;

import java.io.Serializable;

/**
 * The contract to be realised by the generated enumeration in any DAOHelper.
 * 
 * Example : for a model Test, we will have a
 * <code>TestDOAHelper.TestEntityEnum</code> enumeration generated.
 * 
 * The contract gives some informations about the classes for any entity dealed
 * by the dao helper. More precisely :
 * 
 * - contract class of the entity (this must be an interface class)
 * - the implementation fqn class of an entity (at generation time, we might
 * not have the implementation class)
 * - the implementation class (will be looked up at runtime execution, in that
 * way we make possible to used a different implementation at runtime.
 * 
 * - a method to accept any TopiaEntity class for this entity description
 *
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @version $Id$
 * @since 2.2.0
 */
public interface TopiaEntityEnum extends Serializable {

    /**
     * This is a convinient method, as entity enum offers this
     * method from {@link Enum#name()}.
     *
     * @return the name of the underlying entity type.
     * @since 2.6.12
     */
    String name();

    /**
     * @return the name of database schema (null if none was filled).
     * @since 2.9.2
     */
    String dbSchemaName();

    /**
     * @return the name of database table
     * @since 2.9.2
     */
    String dbTableName();


    /** @return the contract class of the entity */
    Class<? extends TopiaEntity> getContract();

    /**
     * Note : this is a lazy accessor.
     *
     * @return the implementation class of the entity
     */
    Class<? extends TopiaEntity> getImplementation();

    /** @return the fully qualifed name of the implementation class of the entity */
    String getImplementationFQN();

    /**
     * @return the array of property names involved in the natural key
     *         of the entity.
     */
    String[] getNaturalIds();

    /**
     * @return the array of property names which are marked as not-null.
     * @since 2.6.9
     */
    String[] getNotNulls();

    /**
     * @return {@code true} if entity use natural ids, {@code false} otherwise.
     * @since 2.6.9
     */
    boolean isUseNaturalIds();

    /**
     * @return {@code true} if entity use some not-null properties,
     *         {@code false} otherwise.
     * @since 2.6.9
     */
    boolean isUseNotNulls();

    /**
     * Change the implementation class of the entity.
     * 
     * Note : this method should reset all states of the objet
     * (implementation class, operators,...).
     *
     * @param implementationFQN the new fully qualifed name of the new
     *                          implementation class of the entity.
     */
    void setImplementationFQN(String implementationFQN);

    /**
     * Test if a given type of entity is matching the contract of this entity.
     * 
     * Note : make sure to accept type only on the given contract class of this entity,
     * can not accept an ancestor type, since there is a specific contract for this.
     * 
     * Example : A -&gt; B
     * <pre>
     * EntityEnum.A.accept(Class&lt;A&gt;) -&gt; true
     * EntityEnum.A.accept(Class&lt;B&gt;) -&gt; false
     * EntityEnum.B.accept(Class&lt;B&gt;) -&gt; true
     * EntityEnum.B.accept(Class&lt;A&gt;) -&gt; false
     * </pre>
     *
     * @param klass the type of an entity to test.
     * @return {@code true} if given type is dealed directly by this entity,
     *         {@code false} otherwise.
     */
    boolean accept(Class<? extends TopiaEntity> klass);
}
