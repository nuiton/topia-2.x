/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.topia.persistence.util;

import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaException;

/**
 * A simple contract to hook the creation phase of an entity associated (or not!) to
 * a parent entity.
 * 
 *
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @param <P> the type of the parent of the entity to create (if entity has
 * no parent then used the {@link Void} type).
 * @param <E> the type of entity to create
 */
public interface Creator<P, E> {
    /**
     * Perform the creation of an entity.
     * 
     * The given <code>from</code> entity should not have already been created in
     * database ? it should only be here to prepare the creation of the entity.
     * 
     * TODO Review this explanation :)
     *
     * @param tx     the current available transaction
     * @param parent the parent of the entity
     * @param from   the entity to create
     * @return the really created entity in database
     * @throws TopiaException if any db problem.
     */
    E create(TopiaContext tx, P parent, E from) throws TopiaException;
}
