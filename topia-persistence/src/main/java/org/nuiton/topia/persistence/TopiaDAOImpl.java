/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/* *
 * TopiaDAOAbstract.java
 *
 * Created: 31 déc. 2005 13:10:34
 *
 * @author poussin &lt;poussin@codelutin.com&gt;
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */

package org.nuiton.topia.persistence;

import com.google.common.base.Preconditions;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.metadata.ClassMetadata;
import org.nuiton.topia.TopiaException;
import org.nuiton.topia.TopiaRuntimeException;
import org.nuiton.topia.event.TopiaEntityListener;
import org.nuiton.topia.event.TopiaEntityVetoable;
import org.nuiton.topia.framework.TopiaContextImplementor;
import org.nuiton.topia.framework.TopiaQuery;
import org.nuiton.topia.persistence.pager.TopiaPagerBean;
import org.nuiton.topia.persistence.util.PagerBeanUtil;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.security.Permission;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

/**
 * Cette classe permet d'avoir un ensemble de méthode implantée de façon
 * standard et plus spécifiquement pour Hibernate.
 * 
 * Certains accès à Hibernate sont tout de même fait ici, car on a pris le choix
 * de se baser entièrement sur hibernate pour la persistence, et il est ainsi
 * possible d'accèder au meta information hibernate sur les classes lorque l'on
 * en a besoin.
 *
 * @param <E> le type de l'entite
 * @author bpoussin &lt;poussin@codelutin.com&gt;
 * @version $Id$
 */

public class TopiaDAOImpl<E extends TopiaEntity> implements
        TopiaDAO<E> { // TopiaDAOImpl

    /** to use log facility, just put in your code: log.info(\"...\"); */
    private static Log log = LogFactory.getLog(TopiaDAOImpl.class);

    /**
     * Type of entity managed by this dao.
     *
     * @since ever
     */
    protected Class<E> entityClass;

    /**
     * Underlying context used by this dao to do actions on db.
     *
     * @since ever
     */
    protected TopiaContextImplementor context;

    /**
     * Default batch size used to iterate on data.
     *
     * @since 2.6.14
     */
    private int batchSize = 1000;

    @Override
    public TopiaEntityEnum getTopiaEntityEnum() {
        throw new UnsupportedOperationException(
                "This method must be overided in generated DAO");
    }

    @Override
    public Class<E> getEntityClass() {
        throw new UnsupportedOperationException(
                "This method must be overided in generated DAO");
    }

    @Override
    public int getBatchSize() {
        return batchSize;
    }

    @Override
    public void setBatchSize(int batchSize) {
        this.batchSize = batchSize;
    }

    @Override
    public Iterator<E> iterator() {

        Iterator<E> iterator = new FindAllIterator<E, E>(
                this,
                getEntityClass(),
                batchSize,
                "FROM " + getTopiaEntityEnum().getImplementationFQN() + " ORDER BY id");

        return iterator;
    }

    /**
     * Retourne l'id de l'entity
     *
     * @param e l'entity
     * @return l'id de l'entity ou null si pas trouvé
     * @throws TopiaException Si une erreur survient durant la recherche
     */
    protected Serializable getId(E e) throws TopiaException {
        ClassMetadata meta = getClassMetadata();
        String idPropName = meta.getIdentifierPropertyName();

        try {
            Serializable result;
            result = (Serializable) PropertyUtils.getSimpleProperty(e,
                                                                    idPropName);
            return result;
        } catch (Exception eee) {
            throw new TopiaException("Impossible de récuperer l'identifiant "
                                     + idPropName + " de l'entite: " + e);
        }
    }

    /**
     * Retourne l'id de l'entity representer comme une map
     *
     * @param map l'entity en representation map
     * @return l'id de l'entity ou null si pas trouvé
     * @throws TopiaException Si une erreur survient durant la recherche
     */
    protected Serializable getId(Map map) throws TopiaException {
        try {
            ClassMetadata meta = getClassMetadata();
            String idPropName = meta.getIdentifierPropertyName();

            Serializable id = (Serializable) map.get(idPropName);
            return id;
        } catch (HibernateException eee) {
            throw new TopiaException(eee);
        }
    }

    /**
     * When TopiaContextImpl create the TopiaDAOHibernate, it must call this
     * method just after.
     *
     * @param entityClass
     */
    @Override
    public void init(TopiaContextImplementor context, Class<E> entityClass)
            throws TopiaException {
        log.debug("init dao for " + entityClass.getName());
        this.context = context;
        this.entityClass = entityClass;
    }

    @Override
    public TopiaContextImplementor getContext() {
        return context;
    }

    @Override
    public String createSimpleQuery(String alias) {
        String hql = "FROM " + getTopiaEntityEnum().getImplementationFQN();
        if (StringUtils.isNotBlank(alias)) {
            hql += " " + alias;
        }
        return hql;
    }

    @SuppressWarnings("unchecked")
    @Override
    public E newInstance() throws TopiaException {
        if (log.isDebugEnabled()) {
            log.debug("entityClass = " + entityClass);
        }
        Class<E> implementation = (Class<E>)
                getTopiaEntityEnum().getImplementation();

        try {
            E result = implementation.newInstance();
            return result;
        } catch (Exception e) {
            throw new TopiaException(
                    "Impossible de trouver ou d'instancier la classe "
                    + implementation);
        }
    }

    @Override
    public <U extends TopiaEntity> List<U> findUsages(Class<U> type, E e)
            throws TopiaException {
        // must be implemented by specialized dao
        throw new UnsupportedOperationException();
    }

    @Override
    public Map<Class<? extends TopiaEntity>, List<? extends TopiaEntity>> findAllUsages(E e)
            throws TopiaException {
        // must be implemented by specialized dao
        throw new UnsupportedOperationException();
    }

    @Override
    public List<Permission> getRequestPermission(String topiaId, int actions)
            throws TopiaException {
        return null;
    }

    @Override
    public void addTopiaEntityListener(TopiaEntityListener listener) {
        getContext().addTopiaEntityListener(entityClass, listener);
    }

    @Override
    public void addTopiaEntityVetoable(TopiaEntityVetoable vetoable) {
        getContext().addTopiaEntityVetoable(entityClass, vetoable);
    }

    @Override
    public void removeTopiaEntityListener(TopiaEntityListener listener) {
        getContext().removeTopiaEntityListener(entityClass, listener);
    }

    @Override
    public void removeTopiaEntityVetoable(TopiaEntityVetoable vetoable) {
        getContext().removeTopiaEntityVetoable(entityClass, vetoable);
    }

    @Override
    public E create(E entity) throws TopiaException {

        // TODO EC-20100322 this code may be merged with other create() methods
        try {
            // first set topiaId
            String topiaId = TopiaId.create(entityClass);
            TopiaEntityAbstract entityAbstract = (TopiaEntityAbstract) entity;
            entityAbstract.setTopiaId(topiaId);
            entityAbstract.setTopiaContext(getContext());

            // save entity
            getSession().save(entity);
            getContext().getFiresSupport().warnOnCreateEntity(entityAbstract);
            return entity;
        } catch (HibernateException eee) {
            throw new TopiaException(eee);
        }
    }

    @Override
    public E create(Object... properties) throws TopiaException {
        Map<String, Object> map = new HashMap<String, Object>();
        Object propertyName = null;
        Object value;
        try {
            for (int i = 0; i < properties.length; ) {
                propertyName = properties[i++];
                value = properties[i++];
                map.put((String) propertyName, value);
            }
        } catch (ArrayIndexOutOfBoundsException eee) {
            throw new IllegalArgumentException("Wrong number of argument "
                                               + properties.length
                                               + ", you must have even number. Last property name read: "
                                               + propertyName);
        } catch (ClassCastException eee) {
            throw new IllegalArgumentException(
                    "Wrong argument type, wait property name as String and " +
                    "have " + propertyName.getClass().getName());
        }

        E result = create(map);
        return result;
    }

    /**
     * Cette methode appelle fireVetoableCreate et fireOnCreated Si vous la
     * surchargé, faites attention a appeler le super ou a appeler vous aussi
     * ces deux methodes.
     */
    @Override
    public E create(Map<String, Object> properties) throws TopiaException {
        E result = newInstance();

        // TODO reflechir s'il ne faudrait pas creer l'id avant l'event precedent
        // reflechir toujours dans un context on les E pourrait ne pas
        // etre des TopiaEntity
        if (result instanceof TopiaEntity) {
            String topiaId = TopiaId.create(entityClass);
            TopiaEntityAbstract entity = (TopiaEntityAbstract) result;
            entity.setTopiaId(topiaId);
            entity.setTopiaContext(getContext());
        }
        try {
            for (Map.Entry<String, Object> e : properties.entrySet()) {
                String propertyName = e.getKey();
                Object value = e.getValue();
                PropertyUtils.setProperty(result, propertyName, value);
            }
        } catch (IllegalAccessException eee) {
            throw new IllegalArgumentException(
                    "Can't put properties on new Object", eee);
        } catch (InvocationTargetException eee) {
            throw new IllegalArgumentException(
                    "Can't put properties on new Object", eee);
        } catch (NoSuchMethodException eee) {
            throw new IllegalArgumentException(
                    "Can't put properties on new Object", eee);
        }

        // on fait un save maintenant, car puisqu'on a creer l'entity au
        // travers du DAO, on s'attend a l'avoir a disposition tout de
        // suite pour les requetes sans avoir a faire un update dessus
        getSession().save(result);
        getContext().getFiresSupport().warnOnCreateEntity(result);
        return result;
    }

    @Override
    public E update(E e) throws TopiaException {
        try {
            getSession().saveOrUpdate(e);
            getContext().getFiresSupport().warnOnUpdateEntity(e);
            return e;
        } catch (HibernateException eee) {
            throw new TopiaException(eee);
        }
    }

    @Override
    public void delete(E e) throws TopiaException {
        try {
            getSession().delete(e);
            getContext().getFiresSupport().warnOnDeleteEntity(e);
        } catch (HibernateException eee) {
            throw new TopiaException(eee);
        }
    }

    @Override
    public void deleteAll(Iterable<E> entities) throws TopiaException {
        for (E entity : entities) {
            delete(entity);
        }
    }

    @Override
    public TopiaQuery createQuery() {
        return new TopiaQuery(getEntityClass());
    }

    @Override
    public TopiaQuery createQuery(String entityAlias) {
        return new TopiaQuery(getEntityClass(), entityAlias);
    }

    @Override
    public E findByTopiaId(String id) throws TopiaException {
        // Nothing to do if id is null, the result will still be null.
        E result = null;
        if (id != null) {
            TopiaQuery query = createQuery().addEquals(TopiaEntity.TOPIA_ID, id);
            result = findByQuery(query);
        }
        return result;
    }

    @Override
    public E findByProperty(String propertyName, Object value)
            throws TopiaException {
        Map<String, Object> properties = new HashMap<String, Object>();
        properties.put(propertyName, value);
        E result = findByProperties(properties);
        return result;
    }

    @Override
    public E findByProperties(String propertyName, Object value,
                              Object... others) throws TopiaException {
        Map<String, Object> properties =
                convertPropertiesArrayToMap(propertyName, value, others);
        E result = findByProperties(properties);
        return result;
    }

    @Override
    public E findByProperties(Map<String, Object> properties)
            throws TopiaException {
        TopiaQuery query = createQuery().addEquals(properties);
        E result = findByQuery(query);
        return result;
    }

    @Override
    public E findByQuery(TopiaQuery query) throws TopiaException {
        E result = query.executeToEntity(getContext(), getEntityClass());
        return result;
    }

    @Override
    public List<E> findAll() throws TopiaException {
        List<E> results = findAllByQuery(createQuery());
        return results;
    }

    @Override
    public List<String> findAllIds() throws TopiaException {
        TopiaQuery query = createQuery().setSelect(TopiaEntity.TOPIA_ID);
        List<String> results = getContext().findByQuery(query);
        return results;
    }

    @Override
    public List<E> findAllByProperty(String propertyName, Object value)
            throws TopiaException {
        Map<String, Object> properties =
                convertPropertiesArrayToMap(propertyName, value);
        List<E> result = findAllByProperties(properties);
        return result;
    }

    @Override
    public List<E> findAllByProperties(String propertyName, Object value,
                                       Object... others) throws TopiaException {
        Map<String, Object> properties =
                convertPropertiesArrayToMap(propertyName, value, others);
        List<E> result = findAllByProperties(properties);
        return result;
    }

    @Override
    public List<E> findAllByProperties(Map<String, Object> properties)
            throws TopiaException {
        TopiaQuery query = createQuery().addEquals(properties);
        List<E> results = findAllByQuery(query);
        return results;
    }

    @Override
    public List<E> findAllByQuery(TopiaQuery query) throws TopiaException {
        List<E> results = query.executeToEntityList(getContext(), getEntityClass());
        return results;
    }

    @Override
    public Map<String, E> findAllMappedByQuery(TopiaQuery query)
            throws TopiaException {
        Map<String, E> results =
                query.executeToEntityMap(getContext(), getEntityClass());
        return results;
    }

    @Override
    public <K> Map<K, E> findAllMappedByQuery(TopiaQuery query, String keyName,
                                              Class<K> keyClass)
            throws TopiaException {
        Map<K, E> results =
                query.executeToEntityMap(getContext(), getEntityClass(),
                                         keyName, keyClass);
        return results;
    }

    @Override
    public List<E> findAllWithOrder(String... propertyNames)
            throws TopiaException {
        TopiaQuery query = createQuery().addOrder(propertyNames);
        List<E> results = findAllByQuery(query);
        return results;
    }

    @Override
    public E findContains(String propertyName,
                          Object property) throws TopiaException {
        TopiaQuery k = createQuery().
                addInElements(":K", propertyName).
                addParam("K", property);
        return findByQuery(k);
    }

    @Override
    public List<E> findAllContains(String propertyName,
                                   Object property) throws TopiaException {
        TopiaQuery k = createQuery().
                addInElements(":K", propertyName).
                addParam("K", property);
        return findAllByQuery(k);
    }

    @Override
    public boolean existByTopiaId(String id) throws TopiaException {
        boolean result = existByProperties(TopiaEntity.TOPIA_ID, id);
        return result;
    }

    @Override
    public boolean existByProperties(String propertyName, Object propertyValue,
                                     Object... others) throws TopiaException {
        Map<String, Object> properties =
                convertPropertiesArrayToMap(propertyName, propertyValue, others);
        TopiaQuery query = createQuery().addEquals(properties);
        boolean result = existByQuery(query);
        return result;
    }

    @Override
    public boolean existByQuery(TopiaQuery query) throws TopiaException {
        int count = countByQuery(query);
        boolean result = count > 0;
        return result;
    }

    @Override
    public long count() throws TopiaException {
        int result = countByQuery(createQuery());
        return result;
    }

    @Override
    public int countByQuery(TopiaQuery query) throws TopiaException {
        int result = query.executeCount(getContext());
        return result;
    }

    /**
     * Convert a properties array to a proper map used to find entities in
     * methods {@link #findByProperties(String, Object, Object...)} and {@link
     * #findAllByProperties(String, Object, Object...)}.
     *
     * @param propertyName  first property name to test existence
     * @param propertyValue first property value to test existence
     * @param others        altern propertyName and propertyValue
     * @return a Map with properties, propertyName as key.
     * @throws IllegalArgumentException for ClassCast or ArrayIndexOutOfBounds
     *                                  errors
     */
    private Map<String, Object> convertPropertiesArrayToMap(String propertyName,
                                                            Object propertyValue,
                                                            Object... others)
            throws IllegalArgumentException {
        Map<String, Object> properties = new HashMap<String, Object>();
        properties.put(propertyName, propertyValue);
        Object name = null;
        for (int i = 0; i < others.length; ) {
            try {
                name = others[i++];
                propertyValue = others[i++];
                properties.put((String) name, propertyValue);
            } catch (ClassCastException eee) {
                throw new IllegalArgumentException(
                        "Les noms des propriétés doivent être des chaines et " +
                        "non pas " + propertyName.getClass().getName(),
                        eee);
            } catch (ArrayIndexOutOfBoundsException eee) {
                throw new IllegalArgumentException(
                        "Le nombre d'argument n'est pas un nombre pair: "
                        + (others.length + 2)
                        + " La dernière propriété était: " + name, eee);
            }
        }
        return properties;
    }

    @Override
    public E findByPrimaryKey(Map<String, Object> keys)
            throws TopiaException {
        try {
            // we used hibernate meta information for all persistence type
            // it's more easy than create different for all persistence
            ClassMetadata meta = getClassMetadata();
            if (meta.hasNaturalIdentifier()) {
                E result = findByProperties(keys);
                return result;
            }
        } catch (HibernateException eee) {
            throw new TopiaException(eee);
        }
        throw new TopiaException("La classe " + entityClass.getName()
                                 + " n'a pas de cle primaire naturelle");

    }

    @Override
    public E findByPrimaryKey(Object... k) throws TopiaException {
        // TODO pour une meilleur gestion des problemes a la compilation
        // mettre un premier couple (propName, value) en argument ca evitera
        // de pouvoir appeler cette methode sans argument
        try {
            ClassMetadata meta = getClassMetadata();
            if (meta.hasNaturalIdentifier()) {
                int[] ikeys = meta.getNaturalIdentifierProperties();
                String[] pnames = meta.getPropertyNames();

                Map<String, Object> keys = new HashMap<String, Object>();
                for (int ikey : ikeys) {
                    keys.put(pnames[ikey], k[ikey]);
                }

                E result = findByProperties(keys);
                return result;
            }
        } catch (HibernateException eee) {
            throw new TopiaException(eee);
        }
        throw new TopiaException("La classe " + entityClass.getName()
                                 + " n'a pas de cle primaire naturelle");
    }

    @Override
    public boolean existsByQuery(String hql,
                                 Object... params) throws TopiaException {
        long count = countByQuery(hql, params);
        return count > 0;
    }

    @Override
    public long countByQuery(String hql,
                             Object... params) throws TopiaException {

        Preconditions.checkNotNull(StringUtils.isNotBlank(hql));
        Preconditions.checkArgument(hql.toUpperCase().trim().startsWith("SELECT COUNT("));

        return findByQuery(Long.class, hql, params);
    }

    @Override
    public E findByQuery(String hql,
                         Object... params) throws TopiaException {
        return findByQuery(getEntityClass(), hql, params);
    }

    @Override
    public <R> R findByQuery(Class<R> type,
                             String hql,
                             Object... params) throws TopiaException {

        Preconditions.checkNotNull(type);
        Preconditions.checkNotNull(hql);

        Object unique = getContext().findUnique(hql, params);
        Preconditions.checkState(unique == null ||
                                 type.isAssignableFrom(unique.getClass()));
        return (R) unique;
    }

    @Override
    public List<E> findAllByQuery(String hql,
                                  Object... params) throws TopiaException {

        return findAllByQuery(getEntityClass(), hql, params);
    }

    @Override
    public <R> List<R> findAllByQuery(Class<R> type,
                                      String hql,
                                      Object... params) throws TopiaException {

        Preconditions.checkNotNull(type);
        Preconditions.checkNotNull(hql);

        List<R> result = getContext().findAll(hql, params);
        return result;
    }

    @Override
    public Iterable<E> findAllLazyByQuery(String hql,
                                          Object... params) throws TopiaException {
        return findAllLazyByQuery(batchSize, hql, params);
    }

    @Override
    public <R> Iterable<R> findAllLazyByQuery(Class<R> type,
                                              String hql,
                                              Object... params) throws TopiaException {
        return findAllLazyByQuery(type, batchSize, hql, params);
    }

    @Override
    public Iterable<E> findAllLazyByQuery(int batchSize,
                                          String hql,
                                          Object... params) throws TopiaException {
        return findAllLazyByQuery(getEntityClass(), batchSize, hql, params);
    }

    @Override
    public <R> Iterable<R> findAllLazyByQuery(Class<R> type,
                                              int batchSize,
                                              String hql,
                                              Object... params) throws TopiaException {

        final Iterator<R> iterator = new FindAllIterator<E, R>(this,
                                                               type,
                                                               batchSize,
                                                               hql,
                                                               params);
        return new Iterable<R>() {
            @Override
            public Iterator<R> iterator() {
                return iterator;
            }
        };
    }

    @Override
    public <R> List<R> findAllByQueryWithBound(Class<R> type,
                                               String hql,
                                               int startIndex,
                                               int endIndex,
                                               Object... params) throws TopiaException {
        Preconditions.checkNotNull(type);
        Preconditions.checkNotNull(hql);

        List<R> result = getContext().find(hql, startIndex, endIndex, params);
        return result;
    }

    @Override
    public List<E> findAllByQueryWithBound(String hql,
                                           int startIndex,
                                           int endIndex,
                                           Object... params) throws TopiaException {
        return findAllByQueryWithBound(getEntityClass(),
                                       hql,
                                       startIndex,
                                       endIndex,
                                       params);
    }

    @Override
    public <R> List<R> findAllByQueryAndPager(Class<R> type,
                                              String hql,
                                              TopiaPagerBean pager,
                                              Object... params) throws TopiaException {
        Preconditions.checkNotNull(pager);
        Preconditions.checkNotNull(hql);

        if (StringUtils.isNotBlank(pager.getSortColumn())) {
            hql += " ORDER BY " + pager.getSortColumn();
            if (!pager.isSortAscendant()) {
                hql += " DESC";
            }
        }
        List<R> result = findAllByQueryWithBound(type, hql,
                                                 (int) pager.getRecordStartIndex(),
                                                 (int) pager.getRecordEndIndex() - 1,
                                                 params);
        return result;
    }

    @Override
    public List<E> findAllByQueryAndPager(String hql,
                                          TopiaPagerBean pager,
                                          Object... params) throws TopiaException {

        return findAllByQueryAndPager(getEntityClass(),
                                      hql,
                                      pager,
                                      params);
    }

    @Override
    public void computeAndAddRecordsToPager(String hql,
                                            TopiaPagerBean pager,
                                            Object... params) throws TopiaException {

        long records = countByQuery(hql, params);

        pager.setRecords(records);
        PagerBeanUtil.computeRecordIndexesAndPagesNumber(pager);
    }

    /**
     * Renvoie la Session contenue dans le contexte
     *
     * @return hibernate session
     * @throws TopiaException if any pb
     */
    Session getSession() throws TopiaException {
        return getContext().getHibernate();
    }

    /**
     * package locale method because this is hibernate specific method and
     * we don't want expose it.
     *
     * @return the meta-data of the entity
     * @throws TopiaException if any pb
     */
    ClassMetadata getClassMetadata() throws TopiaException {
        ClassMetadata meta = getContext().getHibernateFactory()
                .getClassMetadata(entityClass);
        if (meta == null) {
            meta = getContext().getHibernateFactory().getClassMetadata(
                    getTopiaEntityEnum().getImplementationFQN());
        }
        return meta;
    }

    public static class FindAllIterator<E extends TopiaEntity, R> implements Iterator<R> {

        protected Iterator<R> data;

        protected final TopiaDAO<E> dao;

        protected final Class<R> type;

        protected final String hql;

        protected final Object[] params;

        protected TopiaPagerBean pager;

        public FindAllIterator(TopiaDAO<E> dao,
                               Class<R> type,
                               int batchSize,
                               String hql,
                               Object... params) {
            this.dao = dao;
            this.type = type;
            this.hql = hql;
            this.params = params;
            try {
                String hql2 = hql.toLowerCase();
                int i = hql2.indexOf("order by");
                if (i == -1) {
                    throw new IllegalStateException(
                            "must have a *order by* in hql, " +
                            "but did not find it in " + hql);
                }

                // get the count (removing the order-by)
                long count2 = dao.countByQuery("SELECT COUNT(*) " +
                                               hql.substring(0, i), params);
                pager = new TopiaPagerBean();
                pager.setRecords(count2);
                pager.setPageSize(batchSize);
                PagerBeanUtil.computeRecordIndexesAndPagesNumber(pager);
            } catch (TopiaException e) {
                throw new TopiaRuntimeException(e);
            }

            // empty iterator (will be changed at first next call)
            data = Collections.emptyIterator();
        }

        @Override
        public boolean hasNext() {
            return data.hasNext() || // no more data
                   pager.getPageIndex() < pager.getPagesNumber();
        }

        @Override
        public R next() {
            if (!hasNext()) {
                throw new NoSuchElementException();
            }

            if (!data.hasNext()) {

                // must load iterator

                // increments page index
                pager.setPageIndex(pager.getPageIndex() + 1);
                PagerBeanUtil.computeRecordIndexesAndPagesNumber(pager);

                // load new window of data
                try {
                    data = dao.findAllByQueryAndPager(type,
                                                      hql,
                                                      pager,
                                                      params).iterator();
                } catch (TopiaException e) {
                    throw new TopiaRuntimeException(e);
                }
            }

            R next = data.next();
            return next;
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException(
                    "This iterator does not support remove operation.");
        }
    }
} //TopiaDAOImpl
