/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.topia.persistence.util;

import org.nuiton.topia.TopiaContext;

/**
 * A simple contract to hook the deletion of an entity.
 *
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @param <P> the parent of the entity to delete (if the entity has no parent
 *            says is not in a association)
 * of another entity, just used the {@link Void} type).
 * @param <E> the type of the entity to delete.
 */
public interface Deletor<P, E> {
    /**
     * Hook to delete an entity from a prent entity.
     *
     * @param tx current transaction
     * @param parent the parent of the entity
     * @param from   the entity to delete.
     */
    void delete(TopiaContext tx, P parent, E from);
}
