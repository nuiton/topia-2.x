package org.nuiton.topia.persistence.csv.in;
/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.persistence.TopiaEntityEnum;
import org.nuiton.topia.persistence.csv.CsvProgressModel;

import java.io.Serializable;

/**
 * A simple csv result bean just to keep the number of created or
 * updated entities.
 *
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @since 0.2
 */
public class CsvImportResult<T extends TopiaEntityEnum> implements Serializable {

    private static final long serialVersionUID = 1L;

    /** type of entity to import csv datas. */
    protected final T entityType;

    /** Name of the csv file to import. */
    protected final String importFileName;

    /** Flag to authorize to create entities not found in db. */
    protected final boolean createIfNotFound;

    /** Count of created entities. */
    protected int numberCreated;

    /** Count of updated entities. */
    protected int numberUpdated;

    protected final CsvProgressModel progressModel;

    public static <T extends TopiaEntityEnum> CsvImportResult<T> newResult(T entityType,
                                                                           String importFileName,
                                                                           boolean createIfNotFound) {
        CsvImportResult<T> result = newResult(entityType, importFileName,
                                              createIfNotFound, null);
        return result;
    }

    public static <T extends TopiaEntityEnum> CsvImportResult<T> newResult(T entityType,
                                                                           String importFileName,
                                                                           boolean createIfNotFound,
                                                                           CsvProgressModel progressModel) {
        CsvImportResult<T> result = new CsvImportResult<T>(entityType, importFileName,
                                                           createIfNotFound, progressModel);
        return result;
    }

    protected CsvImportResult(T entityType,
                              String importFileName,
                              boolean createIfNotFound,
                              CsvProgressModel progressModel) {
        this.entityType = entityType;
        this.importFileName = importFileName;
        this.createIfNotFound = createIfNotFound;
        this.progressModel = progressModel;
    }

    public T getEntityType() {
        return entityType;
    }


    public String getImportFileName() {
        return importFileName;
    }

    public int getNumberCreated() {
        return numberCreated;
    }

    public int getNumberUpdated() {
        return numberUpdated;
    }

    public boolean isCreateIfNotFound() {
        return createIfNotFound;
    }

    public void incrementsNumberCreated() {
        numberCreated++;
    }

    public void incrementsNumberUpdated() {
        numberUpdated++;
    }

    public CsvProgressModel getProgressModel() {
        return progressModel;
    }
}
