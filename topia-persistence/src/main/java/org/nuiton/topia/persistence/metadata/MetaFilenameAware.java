/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.topia.persistence.metadata;

import org.nuiton.topia.persistence.TopiaEntityEnum;

import java.io.File;
import java.io.Writer;

/**
 * Contract to import or export some metas.
 *
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @since 2.6.12
 */
public interface MetaFilenameAware<T extends TopiaEntityEnum> {

    String CSV_EXTENSION = ".csv";

    T getSource();

    String getName();

    String getFilename();

    File newFile(File container);

    Writer newWriter(File container);

}
