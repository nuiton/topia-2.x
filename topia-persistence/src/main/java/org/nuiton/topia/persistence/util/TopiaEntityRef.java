/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.topia.persistence.util;

import org.nuiton.topia.persistence.TopiaEntity;

/**
 * Definition of a reference of an entity from a root entity.
 * 
 * the method {@link #getAccessorExpression()} return the jxpath to access the
 * ref from the root object.
 * 
 * TODO Javadoc...
 *
 * @since 2.2.0
 */
public class TopiaEntityRef implements Comparable<TopiaEntityRef> {

    public static final String SEPARATOR = "/";

    /** the root entity */
    TopiaEntity root;

    /** the entity ref */
    TopiaEntity ref;

    /** the jxpath used to acces ref from root */
    String accessorExpression;

    TopiaEntity[] path;

    public TopiaEntityRef(TopiaEntity root, TopiaEntity ref, String accessorExpression, TopiaEntity[] path) {
        this.root = root;
        this.ref = ref;
        this.accessorExpression = accessorExpression;
        this.path = path;
    }

    public TopiaEntity getRoot() {
        return root;
    }

    public TopiaEntity getRef() {
        return ref;
    }

    public String getAccessorExpression() {
        return accessorExpression;
    }

    public TopiaEntity[] getPath() {
        return path;
    }

    public TopiaEntity getInvoker() {
        return path == null || path.length < 2 ? null : path[path.length - 2];
    }

    public String getInvokerProperty() {
        if (path == null || path.length < 2) {
            // no invoker defined
            return null;
        }
        int pos = path.length - 1;
        String[] expressions = accessorExpression.split(SEPARATOR);
        String invokerProperty = expressions[pos];
        return invokerProperty;
    }

    @Override
    public String toString() {
        return super.toString() + " : " + accessorExpression;
    }

    @Override
    public int compareTo(TopiaEntityRef o) {
        return ref.getTopiaId().compareTo(o.getRef().getTopiaId());
    }
}
