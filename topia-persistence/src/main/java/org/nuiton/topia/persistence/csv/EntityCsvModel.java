package org.nuiton.topia.persistence.csv;
/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Function;
import com.google.common.collect.Maps;

import org.nuiton.decorator.Decorator;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaEntityEnum;
import org.nuiton.topia.persistence.TopiaId;
import org.nuiton.topia.persistence.metadata.TableMeta;
import org.nuiton.csv.ValueParserFormatter;
import org.nuiton.csv.ext.AbstractImportExportModel;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Map;

/**
 * A model to import / export entities into csv files.
 *
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @since 0.2
 */
public class EntityCsvModel<T extends TopiaEntityEnum, E extends TopiaEntity> extends AbstractImportExportModel<E> {

    protected final TableMeta<T> tableMeta;

    protected boolean useOrdinalForEnum;

    public static <T extends TopiaEntityEnum, E extends TopiaEntity> EntityCsvModel<T, E> newModel(
            char separator,
            TableMeta<T> tableMeta) {
        return new EntityCsvModel<T, E>(separator, tableMeta);
    }

    public static <T extends TopiaEntityEnum, E extends TopiaEntity> EntityCsvModel<T, E> newModel(
            char separator,
            TableMeta<T> tableMeta,
            String idHeader) {
        return new EntityCsvModel<T, E>(separator, tableMeta, idHeader);
    }

    @Override
    public E newEmptyInstance() {
        return (E) tableMeta.newEntity();
    }

    public void setUseOrdinalForEnum(boolean useOrdinalForEnum) {
        this.useOrdinalForEnum = useOrdinalForEnum;
    }

    public void addForeignKeyForExport(String propertyName,
                                       Class<TopiaEntity> entityType) {

        Map<String, TopiaEntity> universe = Collections.emptyMap();

        newColumnForExport(propertyName,
                           TopiaCsvCommons.newForeignKeyValue(entityType,
                                                              propertyName,
                                                              universe)
        );
    }

    public <T> void addDecoratedForeignKeyForExport(String headerName,
                                                    String propertyName,
                                                    Decorator<T> decorator) {
        modelBuilder.newColumnForExport(
                headerName,
                propertyName,
                TopiaCsvCommons.newForeignKeyDecoratedValue(decorator));
    }

    public <E extends TopiaEntity> void addForeignKeyForImport(String headerName,
                                                               String propertyName,
                                                               Class<E> entityType,
                                                               Collection<E> entities,
                                                               Function<E, String> transform) {

        Map<String, E> universe = Maps.uniqueIndex(entities, transform);

        newMandatoryColumn(headerName,
                           propertyName,
                           TopiaCsvCommons.newForeignKeyValue(entityType,
                                                              propertyName,
                                                              universe)
        );
    }

    public <E extends TopiaEntity> void addForeignKeyForAssociationForImport(String headerName,
                                                                             String propertyName,
                                                                             Class<E> entityType,
                                                                             Collection<E> entities,
                                                                             Function<E, String> transform) {

        Map<String, E> universe = Maps.uniqueIndex(entities, transform);

        newMandatoryColumn(
                headerName,
                propertyName,
                TopiaCsvCommons.newForeignKeyValueAssociation(entityType,
                                                              propertyName,
                                                              universe)
        );
    }

    public <E extends TopiaEntity> void addForeignKeyForImport(String propertyName,
                                                               Class<E> entityType,
                                                               Collection<E> entities) {

        Map<String, E> universe = Maps.uniqueIndex(entities,
                                                   TopiaId.GET_TOPIA_ID);

        newMandatoryColumn(propertyName,
                           TopiaCsvCommons.newForeignKeyValue(entityType,
                                                              propertyName,
                                                              universe)
        );
    }

    public <E extends TopiaEntity> void addForeignKeyForImport(String propertyName,
                                                               Class<E> entityType,
                                                               Map<String, E> universe) {

        newMandatoryColumn(propertyName,
                           TopiaCsvCommons.newForeignKeyValue(entityType,
                                                              propertyName,
                                                              universe)
        );
    }
    public void addDefaultColumn(String propertyName, Class<?> type) {
        addDefaultColumn(propertyName, propertyName, type);
    }

    public void addDefaultColumn(String headerName,
                                 String propertyName,
                                 Class<?> type) {

        if (Date.class.equals(type)) {
            newColumnForImportExport(
                    headerName,
                    propertyName,
                    TopiaCsvCommons.DAY_TIME_SECOND_WITH_TIMESTAMP);
        } else if (double.class.equals(type)) {
            newColumnForImportExport(
                    headerName,
                    propertyName,
                    TopiaCsvCommons.DOUBLE_PRIMITIVE);
        } else if (Double.class.equals(type)) {
            newColumnForImportExport(
                    headerName,
                    propertyName,
                    TopiaCsvCommons.DOUBLE);
        } else if (long.class.equals(type)) {
            newColumnForImportExport(
                    headerName,
                    propertyName,
                    TopiaCsvCommons.PRIMITIVE_LONG);
        } else if (Long.class.equals(type)) {
            newColumnForImportExport(
                    headerName,
                    propertyName,
                    TopiaCsvCommons.LONG);
        } else if (float.class.equals(type)) {
            newColumnForImportExport(
                    headerName,
                    propertyName,
                    TopiaCsvCommons.PRIMITIVE_FLOAT);
        } else if (Float.class.equals(type)) {
            newColumnForImportExport(
                    headerName,
                    propertyName,
                    TopiaCsvCommons.FLOAT);
        } else if (int.class.equals(type)) {
            newColumnForImportExport(
                    headerName,
                    propertyName,
                    TopiaCsvCommons.PRIMITIVE_INTEGER);
        } else if (Integer.class.equals(type)) {
            newColumnForImportExport(
                    headerName,
                    propertyName,
                    TopiaCsvCommons.INTEGER);
        } else if (boolean.class.equals(type)) {
            newColumnForImportExport(
                    headerName,
                    propertyName,
                    TopiaCsvCommons.PRIMITIVE_BOOLEAN);
        } else if (Boolean.class.equals(type)) {
            newColumnForImportExport(
                    headerName,
                    propertyName,
                    TopiaCsvCommons.BOOLEAN);
        } else if (String.class.equals(type)) {
            newColumnForImportExport(
                    headerName,
                    propertyName);
        } else if (type.isEnum()) {

            Class<Enum> enumType = (Class<Enum>) type;
            ValueParserFormatter<Enum> valueParserFormatter;
            if (useOrdinalForEnum) {
                valueParserFormatter =
                        TopiaCsvCommons.newEnumByOrdinalParserFormatter(enumType);
            } else {
                valueParserFormatter =
                        TopiaCsvCommons.newEnumByNameParserFormatter(enumType);
            }

            newColumnForImportExport(headerName,
                                     propertyName,
                                     valueParserFormatter);

        } else {

            throw new IllegalStateException(String.format(
                    "For header %s, property %s, no specific handler " +
                    "found for type %s", headerName, propertyName, type));
        }
    }

    protected EntityCsvModel(char separator, TableMeta<T> tableMeta) {
        super(separator);
        this.tableMeta = tableMeta;
    }

    protected EntityCsvModel(char separator, TableMeta<T> tableMeta,
                             String idHeader) {
        this(separator, tableMeta);
        newColumnForImportExport(idHeader, TopiaEntity.TOPIA_ID);
    }
}
