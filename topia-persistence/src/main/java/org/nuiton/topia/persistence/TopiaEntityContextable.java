/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin, Chatellier Eric, Chemit Tony
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.topia.persistence;

import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaException;

/**
 * {@link TopiaEntity} with {@link TopiaContext} support (injected by
 * {@link TopiaContext} into entities).
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public interface TopiaEntityContextable extends TopiaEntity {
    
    String TOPIA_CONTEXT = "topiaContext";

    /**
     * Set topia context.
     * 
     * @param topiaContext topia context
     * @throws TopiaException if current topia entity context is not null
     */
    void setTopiaContext(TopiaContext topiaContext) throws TopiaException;

    /**
     * Get topia context.
     * 
     * @return topia context
     */
    TopiaContext getTopiaContext();
    
    /**
     * Update entity in persistence context.
     * 
     * @throws TopiaException 
     */
    void update() throws TopiaException;
    
    /**
     * Delete entity in persistence context.
     * 
     * @throws TopiaException 
     */
    void delete() throws TopiaException;
}
