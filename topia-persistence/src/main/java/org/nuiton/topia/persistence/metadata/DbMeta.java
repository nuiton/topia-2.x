package org.nuiton.topia.persistence.metadata;
/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */


import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.nuiton.topia.persistence.TopiaEntityEnum;
import org.nuiton.topia.persistence.TopiaPersistenceHelper;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * Define metas about a db.
 *
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @since 2.6.12
 */
public class DbMeta<T extends TopiaEntityEnum> implements Iterable<TableMeta<T>> {

    /** All metas of the db. */
    protected final List<TableMeta<T>> tables;

    /** All types non editables. */
    protected final Set<T> nonEditableTypes;

    protected final TopiaPersistenceHelper<T> persistenceHelper;

    public static <T extends TopiaEntityEnum> DbMeta<T> newDbMeta(
            TopiaPersistenceHelper<T> typeProvider,
            T[] universe,
            T... nonEditables) {
        return new DbMeta<T>(typeProvider, universe, nonEditables);
    }

    public void addTables(List<TableMeta<T>> entities,
                          Iterable<T> types) {
        for (T type : types) {

            TableMeta<T> tableMeta = getTable(type);
            if (entities != null) {
                entities.add(tableMeta);
            }
        }
    }

    public void addAssociations(List<AssociationMeta<T>> associations,
                                Iterable<T> types) {
        for (T type : types) {

            TableMeta<T> tableMeta = getTable(type);
            if (associations != null) {
                associations.addAll(tableMeta.getAssociations());
            }
        }
    }

    public List<String> getTableNames() {
        List<String> result = Lists.newArrayList();
        for (TableMeta tableMeta : getTables()) {
            result.add(tableMeta.getName());
        }
        return result;
    }

    public List<TableMeta<T>> getTables() {
        return tables;
    }

    public TableMeta<T> getTable(T entityType) {
        Preconditions.checkNotNull(entityType);
        TableMeta<T> result = null;
        for (TableMeta<T> tableMeta : getTables()) {
            if (entityType.equals(tableMeta.getSource())) {
                result = tableMeta;
                break;
            }
        }
        return result;
    }

    @Override
    public Iterator<TableMeta<T>> iterator() {
        return getTables().iterator();
    }

    protected DbMeta(TopiaPersistenceHelper<T> persistenceHelper,
                     T[] entityTypes,
                     T... nonEditableTypes) {
        this.persistenceHelper = persistenceHelper;
        this.nonEditableTypes = Sets.newHashSet(nonEditableTypes);
        tables = Lists.newArrayList();
        for (T entityEnum : entityTypes) {
            TableMeta<T> tableMeta = TableMeta.newMeta(entityEnum, persistenceHelper);
            tables.add(tableMeta);
        }
    }

    public boolean isEditable(TableMeta<T> meta) {
        return !nonEditableTypes.contains(meta.getSource());
    }

    public TopiaPersistenceHelper<T> getPersistenceHelper() {
        return persistenceHelper;
    }
}
