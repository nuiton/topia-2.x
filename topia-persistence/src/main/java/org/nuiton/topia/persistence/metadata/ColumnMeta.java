package org.nuiton.topia.persistence.metadata;
/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.persistence.TopiaEntity;

import java.io.Serializable;
import java.util.Date;

/**
 * Define the meta data of a entity field.
 *
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @since 2.6.12
 */
public class ColumnMeta implements Serializable {

    protected static ColumnMeta newMeta(String name, Class<?> type) {
        return new ColumnMeta(name, type);
    }

    private static final long serialVersionUID = 1L;

    /** Name of the column. */
    protected String name;

    /** Type of the column. */
    protected Class<?> type;

    public String getName() {
        return name;
    }

    public Class<?> getType() {
        return type;
    }

    public String getTypeSimpleName() {
        return type.getSimpleName();
    }

    public String getColumnType() {
        String result = "string";
        if (boolean.class.equals(type)) {
            result = "boolean";
        } else if (Date.class.equals(type)) {
            result = "date";
        }
        return result;
    }

    public boolean isFK() {
        return TopiaEntity.class.isAssignableFrom(type);
    }

    public boolean isDate() {
        return Date.class.isAssignableFrom(type);
    }

    public boolean isBoolean() {
        return boolean.class.equals(type);
    }

    public boolean isNumber() {
        return !isBoolean() && (type.isPrimitive() || Number.class.isAssignableFrom(type));
    }

    protected ColumnMeta(String name, Class<?> type) {
        this.name = name;
        this.type = type;
    }

}
