/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/* *
* SearchFields.java
*
* Created: 7 juin 2006
*
* @author Arnaud Thimel &lt;thimel@codelutin.com&gt;
* @version $Revision$
*
* Mise a jour: $Date$
* par : $Author$
*/

package org.nuiton.topia.persistence;

import static java.lang.annotation.RetentionPolicy.RUNTIME;
import static java.lang.annotation.ElementType.TYPE;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 *
 * TODO-fdesbois-20100508 : javadoc : where is it used for ? which service use it ?
 *
 * Ces annotations permettent de savoir quels sont les champs sur lesquels
 * la recherche pourra s'effectuer.
 */
@Retention(RUNTIME)
@Target(TYPE)
public @interface SearchFields {

    /**
     * @return la liste des champs textes
     */
    String[] txtFields() default {};

    /**
     * @return la liste des champs numeriques
     */
    String[] numFields() default {};

    /**
     * @return la liste des champs booleens
     */
    String[] boolFields() default {};

    /**
     * @return la liste des champs date 
     */
    String[] dateFields() default {};

} //SearchFields
