package org.nuiton.topia.persistence.metadata;
/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Function;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import org.nuiton.topia.persistence.TopiaEntityEnum;

import java.util.Map;

/**
 * Useful methods around metadatas.
 *
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @since 2.6.12
 */
public class Metadatas {

    public static <T extends TopiaEntityEnum, M extends MetaFilenameAware<T>> Multimap<T, M> split(Iterable<M> metas) {
        Function<M, T> function = newMetaBySourcefunction();
        Multimap<T, M> associationsBySource = Multimaps.index(metas, function);
        return associationsBySource;
    }

    public static <T extends TopiaEntityEnum, M extends MetaFilenameAware<T>> Map<T, M> uniqueIndex(Iterable<M> metas) {
        Function<M, T> function = newMetaBySourcefunction();
        Map<T, M> associationsBySource = Maps.uniqueIndex(metas, function);
        return associationsBySource;
    }

    protected static <T extends TopiaEntityEnum, M extends MetaFilenameAware<T>> Function<M, T> newMetaBySourcefunction() {
        return new Function<M, T>() {

            @Override
            public T apply(M input) {
                return input.getSource();
            }
        };
    }

}
