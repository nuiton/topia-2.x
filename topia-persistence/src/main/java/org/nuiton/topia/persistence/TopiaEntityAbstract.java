/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.topia.persistence;

import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaException;
import org.nuiton.topia.framework.TopiaContextImplementor;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.beans.VetoableChangeListener;
import java.beans.VetoableChangeSupport;
import java.util.Date;
import java.util.List;

/**
 * Classe de base de toutes les entités, cela permet de concentrer le code
 * technique dans cette classe. L'identifiant peut-etre n'importe quoi Aucune
 * restriction n'est faite dessus, il peut meme changer entre deux types
 * d'entité si cela ne pose pas d'autre probleme (heritage entre ces entités).
 *
 * @author poussin &lt;poussin@codelutin.com&gt;
 * @version $Id$
 */
public abstract class TopiaEntityAbstract implements TopiaEntity {

    /** serialVersionUID. */
    private static final long serialVersionUID = -7458577454878852241L;

    protected String topiaId;

    protected long topiaVersion;

    protected Date topiaCreateDate = new Date();

    transient protected TopiaContext topiaContext;

    transient protected VetoableChangeSupport readVetoables;

    transient protected PropertyChangeSupport readListeners;

    transient protected VetoableChangeSupport writeVetoables;

    transient protected PropertyChangeSupport writeListeners;

    /**
     * Initialize {@link #readVetoables} at first use or after deserialisation.
     * 
     * @return readVetoables
     */
    protected VetoableChangeSupport getReadVetoableChangeSupport() {
        if (readVetoables == null) {
            readVetoables = new VetoableChangeSupport(this);
        }
        return readVetoables;
    }

    /**
     * Initialize {@link #readListeners} at first use or after deserialisation.
     * 
     * @return readListeners
     */
    protected PropertyChangeSupport getReadPropertyChangeSupport() {
        if (readListeners == null) {
            readListeners = new PropertyChangeSupport(this);
        }
        return readListeners;
    }

    /**
     * Initialize {@link #writeVetoables} at first use or after deserialisation.
     * 
     * @return writeVetoables
     */
    protected VetoableChangeSupport getWriteVetoableChangeSupport() {
        if (writeVetoables == null) {
            writeVetoables = new VetoableChangeSupport(this);
        }
        return writeVetoables;
    }

    /**
     * Initialize {@link #writeListeners} at first use or after deserialisation.
     * 
     * @return writeListeners
     */
    protected PropertyChangeSupport getWritePropertyChangeSupport() {
        if (writeListeners == null) {
            writeListeners = new PropertyChangeSupport(this);
        }
        return writeListeners;
    }
    
    @Override
    public String getTopiaId() {
        return topiaId;
    }

    @Override
    public void setTopiaId(String v) {
        topiaId = v;
    }

    @Override
    public long getTopiaVersion() {
        return topiaVersion;
    }

    @Override
    public void setTopiaVersion(long v) {
        topiaVersion = v;
    }

    @Override
    public Date getTopiaCreateDate() {
        return topiaCreateDate;
    }

    @Override
    public void setTopiaCreateDate(Date topiaCreateDate) {
        this.topiaCreateDate = topiaCreateDate;
    }

    public TopiaContext getTopiaContext() {
        return topiaContext;
    }

    /**
     * @param context The context to set.
     * @throws TopiaException if any pb ?
     */
    public void setTopiaContext(TopiaContext context) throws TopiaException {
        if (topiaContext == null) {
            topiaContext = context;
        } else {
            throw new TopiaException("Remplacement du contexte interdit");
        }
    }

    @Override
    public List<TopiaEntity> getComposite() throws TopiaException {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<TopiaEntity> getAggregate() throws TopiaException {
        throw new UnsupportedOperationException();
    }

    /**
     * On utilise la date de creation comme hash code, cette date ne varie pas
     * au cours du temps
     */
    @Override
    public int hashCode() {
        Date date = getTopiaCreateDate();
        //TC-20100220 : il se peut que la date de creation soit nulle
        // lorsque l'entite est utilise comme objet d'edition d'un formulaire
        // par exemple...
        int result = date == null ? 0 : date.hashCode();
        return result;
    }

    /**
     * On est sur que les objets sont bien les memes car s'il n'ont pas d'id
     * cela veut dire qu'il ne vienne pas de la meme session donc qu'il sont
     * nouveau et different, ou bien qu'ils viennent de la meme session et dans
     * ce cas l'egalite == fonctionne.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TopiaEntity)) {
            return false;
        }
        TopiaEntity other = (TopiaEntity) obj;
        if (getTopiaId() == null || other.getTopiaId() == null) {
            return false;
        }
        boolean result = getTopiaId().equals(other.getTopiaId());
        return result;
    }

    protected void fireOnPreRead(String propertyName, Object value) {
        TopiaContextImplementor contextImplementor =
                (TopiaContextImplementor) getTopiaContext();
        if (contextImplementor != null) {
            contextImplementor.getFiresSupport().fireOnPreRead(getReadVetoableChangeSupport(),
                    this, propertyName, value);
        }
    }

    protected void fireOnPostRead(String propertyName, Object value) {
        TopiaContextImplementor contextImplementor =
                (TopiaContextImplementor) getTopiaContext();
        if (contextImplementor != null) {
            contextImplementor.getFiresSupport().fireOnPostRead(getReadPropertyChangeSupport(),
                    this, propertyName, value);
        }
    }

    protected void fireOnPostRead(String propertyName, int index,
                                  Object value) {
        TopiaContextImplementor contextImplementor =
                (TopiaContextImplementor) getTopiaContext();
        if (contextImplementor != null) {
            contextImplementor.getFiresSupport().fireOnPostRead(getReadPropertyChangeSupport(),
                    this, propertyName, index, value);
        }
    }

    protected void fireOnPreWrite(String propertyName, Object oldValue,
                                  Object newValue) {
        TopiaContextImplementor contextImplementor =
                (TopiaContextImplementor) getTopiaContext();
        if (contextImplementor != null) {
            contextImplementor.getFiresSupport().fireOnPreWrite(getWriteVetoableChangeSupport(),
                    this, propertyName, oldValue, newValue);
        }
    }

    protected void fireOnPostWrite(String propertyName, Object oldValue,
                                   Object newValue) {
        TopiaContextImplementor contextImplementor =
                (TopiaContextImplementor) getTopiaContext();
        if (contextImplementor != null) {
            contextImplementor.getFiresSupport().fireOnPostWrite(
                    getWritePropertyChangeSupport(), this, propertyName, oldValue, newValue);
        }
    }

    protected void fireOnPostWrite(String propertyName, int index,
                                   Object oldValue, Object newValue) {
        TopiaContextImplementor contextImplementor =
                (TopiaContextImplementor) getTopiaContext();
        if (contextImplementor != null) {
            contextImplementor.getFiresSupport().fireOnPostWrite(
                    getWritePropertyChangeSupport(), this, propertyName, index, oldValue,
                    newValue);
        }
    }

    @Override
    public void addPropertyChangeListener(String propertyName,
                                          PropertyChangeListener listener) {
        getWritePropertyChangeSupport().addPropertyChangeListener(propertyName, listener);
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        getWritePropertyChangeSupport().addPropertyChangeListener(listener);
    }

    @Override
    public void addVetoableChangeListener(String propertyName,
                                          VetoableChangeListener vetoable) {
        getWriteVetoableChangeSupport().addVetoableChangeListener(propertyName, vetoable);
    }

    @Override
    public void addVetoableChangeListener(VetoableChangeListener vetoable) {
        getWriteVetoableChangeSupport().addVetoableChangeListener(vetoable);
    }

    @Override
    public void removePropertyChangeListener(String propertyName,
                                             PropertyChangeListener listener) {
        getWritePropertyChangeSupport().removePropertyChangeListener(propertyName, listener);
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        getWritePropertyChangeSupport().removePropertyChangeListener(listener);
    }

    @Override
    public void removeVetoableChangeListener(String propertyName,
                                             VetoableChangeListener vetoable) {
        getWriteVetoableChangeSupport().removeVetoableChangeListener(propertyName, vetoable);
    }

    @Override
    public void removeVetoableChangeListener(VetoableChangeListener vetoable) {
        getWriteVetoableChangeSupport().removeVetoableChangeListener(vetoable);
    }

    @Override
    public void addPropertyListener(String propertyName,
                                    PropertyChangeListener listener) {
        getReadPropertyChangeSupport().addPropertyChangeListener(propertyName, listener);
    }

    @Override
    public void addPropertyListener(PropertyChangeListener listener) {
        getReadPropertyChangeSupport().addPropertyChangeListener(listener);
    }

    @Override
    public void addVetoableListener(String propertyName,
                                    VetoableChangeListener vetoable) {
        getReadVetoableChangeSupport().addVetoableChangeListener(propertyName, vetoable);
    }

    @Override
    public void addVetoableListener(VetoableChangeListener vetoable) {
        getReadVetoableChangeSupport().addVetoableChangeListener(vetoable);
    }

    @Override
    public void removePropertyListener(String propertyName,
                                       PropertyChangeListener listener) {
        getReadPropertyChangeSupport().removePropertyChangeListener(propertyName, listener);
    }

    @Override
    public void removePropertyListener(PropertyChangeListener listener) {
        getReadPropertyChangeSupport().removePropertyChangeListener(listener);
    }

    @Override
    public void removeVetoableListener(String propertyName,
                                       VetoableChangeListener vetoable) {
        getReadVetoableChangeSupport().removeVetoableChangeListener(propertyName, vetoable);
    }

    @Override
    public void removeVetoableListener(VetoableChangeListener vetoable) {
        getReadVetoableChangeSupport().removeVetoableChangeListener(vetoable);
    }

} //TopiaEntityAbstract
