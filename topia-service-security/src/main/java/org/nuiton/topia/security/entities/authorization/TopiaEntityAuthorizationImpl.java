/*
 * #%L
 * ToPIA :: Service Security
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.topia.security.entities.authorization;

import static org.nuiton.topia.security.util.TopiaSecurityUtil.actionsString2Int;
import java.security.Principal;
import java.util.HashSet;
import java.util.Set;

/**
 * Classe permettant de définir des autorisations sur les entités. 
 * @author ruchaud
 */
//TODO: Rajouter la possibilité de faire des autorisations sur les champs d'une entité
public class TopiaEntityAuthorizationImpl extends TopiaEntityAuthorizationAbstract {

    private static final long serialVersionUID = 1L;

    public TopiaEntityAuthorizationImpl() {
        principals = new HashSet();
    }

    public TopiaEntityAuthorizationImpl(String expression, int actions, Set<Principal> principals) {
        this.expression = expression;
        this.actions = actions;
        this.principals = new HashSet();
        for (Principal principal : principals) {
            this.principals.add(principal.getName());
        }
    }

    public void setActions(String actions) {
        this.actions = actionsString2Int(actions);
    }

    public void setPrincipals(String principals) {
        this.principals.add(principals);
    }

}
