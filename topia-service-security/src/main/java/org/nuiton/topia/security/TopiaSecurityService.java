/*
 * #%L
 * ToPIA :: Service Security
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.topia.security;

import org.nuiton.topia.framework.TopiaService;

public interface TopiaSecurityService extends TopiaService {

    String SERVICE_NAME = "security";

    /**
     * Vérifie si l'utilisateur actuellement loggué a le droit d'accéder à 
     * l'entité passée en paramètre pour les actions spécifiées.
     * @param entityClass l'entité pour laquelle on vérifie les droits
     * @param actions les actions [load, read, write, admin]
     * @throws SecurityException if any security issues
     */
    void checkPermission(Class<?> entityClass, int actions)
            throws SecurityException;

    /**
     * Vérifie si l'utilisateur actuellement loggué a le droit d'accéder à 
     * l'entité passée en paramètre pour les actions spécifiées.
     * @param expression le topiaId de l'entité pour laquelle on vérifie les droits
     * ou tout autre expression
     * @param actions les actions [load, read, write, admin]
     * @throws SecurityException if any security issues
     */
    void checkPermission(String expression, int actions)
            throws SecurityException;

}
