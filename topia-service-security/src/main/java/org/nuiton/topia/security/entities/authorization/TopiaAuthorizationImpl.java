/*
 * #%L
 * ToPIA :: Service Security
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.topia.security.entities.authorization;

import static org.nuiton.topia.security.util.TopiaSecurityUtil.CREATE;
import static org.nuiton.topia.security.util.TopiaSecurityUtil.DELETE;
import static org.nuiton.topia.security.util.TopiaSecurityUtil.LOAD;
import static org.nuiton.topia.security.util.TopiaSecurityUtil.UPDATE;
import java.util.Set;

/**
 * Classe permettant la comparaison des autorisations.
 * @author ruchaud
 */
//TODO: Inversion des autorisations
public abstract class TopiaAuthorizationImpl extends TopiaAuthorizationAbstract {

    /**
     * Compare deux autorisations entres elles.
     * @param other une autre autorisation
     * @return vrai si l'autorisation implique l'autre
     */
    public boolean implies(TopiaAuthorization other) {
        return impliesExpression(getExpression(), other.getExpression()) && 
                impliesActions(getActions(), other.getActions()) &&
                    impliesPrincipals(getPrincipals(), other.getPrincipals());
    }

    /**
     * Comparare deux identifiants entres eux.
     * thisId =&gt; thatId = ?
     * @param thisExpression un identifiant 
     * @param thatExpression un autre identifiant
     * @return vrai si thisId implique thatId
     */
    public boolean impliesExpression(String thisExpression, String thatExpression) {
        return thisExpression.equals(thatExpression) ||
                "*".equals(thisExpression) ||
               thatExpression.startsWith(thisExpression.substring(0, thisExpression.length()-1))
                        && thisExpression.endsWith("*");
    }

    /**
     * Compare deux actions entre elles.
     * thisActions =&gt; thatActions = ?
     * @param thisActions une action
     * @param thatActions une autre action
     * @return vrai si thisActions implique thatActions
     */
    public boolean impliesActions(int thisActions, int thatActions) {
        boolean result = true;
        if ((thatActions & LOAD) == LOAD) {
            result &= (thisActions & LOAD) == LOAD;
        }
        if ((thatActions & CREATE) == CREATE) {
            result &= (thisActions & CREATE) == CREATE;
        }
        if ((thatActions & UPDATE) == UPDATE) {
            result &= (thisActions & UPDATE) == UPDATE;
        }
        if ((thatActions & DELETE) == DELETE) {
            result &= (thisActions & DELETE) == DELETE;
        }
        return result;
    }

    /**
     * Compare deux principals entre eux.
     * thisPrincipals =&gt; thatPrincipals = ?
     * @param thisPrincipals un principal
     * @param thatPrincipals un autre principal
     * @return vrai si thisPrincipals implique thatPrincipals
     */
    public boolean impliesPrincipals(Set thisPrincipals,
            Set thatPrincipals) {
        
        // this should never happen
        if (thisPrincipals == null || thatPrincipals == null)
            return false;

        // Permet de définir une autorisation sur l'ensemble des utilisateurs
        if (thisPrincipals.size() == 0 || thatPrincipals.size() == 0) {
            return true;
        }

        return thatPrincipals != null // that contient bien le principal
                                        // nécessaire !
                && (thisPrincipals.contains("*") || thatPrincipals
                        .containsAll(thisPrincipals));
            // (this contient une étoile (accepte tous)) ou (that contient
            // tout ce que contient this)

    }
}
