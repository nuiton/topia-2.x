/*
 * #%L
 * ToPIA :: Service Security
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.topia.security.listener;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaEntityAbstract;
import org.nuiton.topia.security.TopiaSecurityServiceImpl;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;

import static org.nuiton.topia.security.util.TopiaSecurityUtil.LOAD;

/**
 * Listenner permettant de vérifier les autorisations pour la chargement d'une
 * propriété sur une entités.
 *
 * @author ruchaud
 */
//TODO: Gestion d'une sécurité partiel ou total c'est à dire retour d'une valeur par défaut
public class PropertyReadListener implements VetoableChangeListener {

    private static Log log = LogFactory.getLog(PropertyReadListener.class);

    private TopiaSecurityServiceImpl securityManager;

    public PropertyReadListener(TopiaSecurityServiceImpl securityManager) {
        this.securityManager = securityManager;
    }

    @Override
    public void vetoableChange(PropertyChangeEvent event) throws PropertyVetoException {
        Object source = event.getSource();
        TopiaEntityAbstract entity = (TopiaEntityAbstract) source;

        /* Vérification dans le cache */
        Boolean authorized = securityManager.getEntitiesLoadingCache(entity.getTopiaId());
        if (authorized != null) {
            if (!authorized) {
                throw new SecurityException("Access denied to Read entity " + entity.getTopiaId() + " on " + event.getPropertyName());
            }
        } else { // Sinon
            try {
                securityManager.checkPermission(entity.getTopiaId(), LOAD);
            } catch (SecurityException te) {
                if (log.isWarnEnabled()) {
                    log.warn("[Security] Read denied to : " + entity.getTopiaId(), te);
                }
                throw new SecurityException("Access denied to Read entity " + entity.getTopiaId() + " on " + event.getPropertyName(), te);
            }
        }

    }

}
