/*
 * #%L
 * ToPIA :: Service Security
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.topia.security.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.security.TopiaSecurityService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/**
 * La classe permet de filtrer n'importe quel objet (Collection, List,
 * TopiaEntity, ...) par rapport à une permission.
 *
 * @author ruchaud
 */
public class TopiaSecurityFactoryFilter {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(TopiaSecurityUtil.class);

    private static final long serialVersionUID = 1L;

    private TopiaSecurityService securityManager;

    public TopiaSecurityFactoryFilter(TopiaSecurityService securityManager) {
        this.securityManager = securityManager;
    }

    public void filter(Collection<TopiaEntity> entities, int actions, String... fields) {
        //TODO: Gestion des autorisations sur les champs (cf TopiaEntityAuthorization)
        throw new UnsupportedOperationException();
    }

    /**
     * Filtre une entité
     *
     * @param entity  entité à filtrer
     * @param actions la filtre
     * @return null si non autorisé sinon l'entité
     */
    public TopiaEntity filter(TopiaEntity entity, int actions) {
        try {
            securityManager.checkPermission(entity.getTopiaId(), actions);
        } catch (SecurityException e) {
            if (log.isDebugEnabled()) {
                log.debug("Return Null because : " + e);
            }
            return null;
        }
        return entity;
    }

    /**
     * Filtre une collection
     *
     * @param entities la collection à filtrer
     * @param actions  le filtre
     * @return la collection filtrée
     */
    public <E extends TopiaEntity> Collection<E> filter(Collection<E> entities, int actions) {
        Collection<E> result = new ArrayList<E>(entities);
        for (Iterator<?> iterator = result.iterator(); iterator.hasNext();) {
            TopiaEntity entity = (TopiaEntity) iterator.next();
            try {
                securityManager.checkPermission(entity.getTopiaId(), actions);
            } catch (SecurityException e) {
                iterator.remove();
                if (log.isDebugEnabled()) {
                    log.debug("Removed because : " + e);
                }
            }
        }
        return result;
    }

    /**
     * Filtre sur une liste
     *
     * @param entities la liste à filtrer
     * @param actions  le filtre
     * @return la liste filtrée
     */
    public <E extends TopiaEntity> List<E> filter(List<E> entities, int actions) {
        return (List<E>) filter((Collection<E>) entities, actions);
    }

}
