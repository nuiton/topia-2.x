/*
 * #%L
 * ToPIA :: Service Security
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/* *
* TopiaSecurityVetoableListener.java
*
* Created: 10 févr. 2006
*
* @author Arnaud Thimel &lt;thimel@codelutin.com&gt;
* @version $Revision$
*
* Mise a jour: $Date$
* par : $Author$
*/

package org.nuiton.topia.security.listener;

import org.nuiton.topia.event.TopiaEntityEvent;
import org.nuiton.topia.event.TopiaEntityListener;
import org.nuiton.topia.persistence.TopiaEntity;

/**
 * Ajout en cas de chargement ou de creation d'entités des listeners pour la
 * sécurité sur leurs champs.
 * @author ruchaud
 */
public class PropertyVetoable implements TopiaEntityListener {

    protected PropertyReadListener read;
    protected PropertyWriteListener write;
    
    /**
     * Contructeur avec comme paramètre les listeners à attacher au chargement ou
     * à la création.
     * @param read Listener en lecture d'un champ
     * @param write Listener en écriture d'un champ
     */
    public PropertyVetoable(PropertyReadListener read, PropertyWriteListener write) {
        this.read = read;
        this.write = write;
    }

    private void putVetoables(TopiaEntityEvent event) {
        TopiaEntity entity = event.getEntity();
        if (!(entity instanceof NoSecurityLoad)) {
            entity.addVetoableListener(read);
        }
        entity.addVetoableChangeListener(write);
    }

    /*
     * (non-Javadoc)
     * @see org.nuiton.topia.event.TopiaEntityLoadListener#entityLoaded(org.nuiton.topia.event.TopiaEntityLoadEvent)
     */
    @Override
    public void load(TopiaEntityEvent event) {
        putVetoables(event);
    }

    /*
     * (non-Javadoc)
     * @see org.nuiton.topia.event.TopiaEntityListener#entityCreated(org.nuiton.topia.event.TopiaEntityEvent)
     */
    @Override
    public void create(TopiaEntityEvent event) {
    }
    
    /*
     * (non-Javadoc)
     * @see org.nuiton.topia.event.TopiaEntityListener#entityDeleted(org.nuiton.topia.event.TopiaEntityEvent)
     */
    @Override
    public void delete(TopiaEntityEvent event) {
    }

    /*
     * (non-Javadoc)
     * @see org.nuiton.topia.event.TopiaEntityListener#entityUpdated(org.nuiton.topia.event.TopiaEntityEvent)
     */
    @Override
    public void update(TopiaEntityEvent event) {
    }

} //TopiaSecurityVetoableListener
