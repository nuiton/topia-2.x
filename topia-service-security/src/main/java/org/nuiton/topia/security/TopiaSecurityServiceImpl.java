/*
 * #%L
 * ToPIA :: Service Security
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.topia.security;

import org.apache.commons.collections4.map.ReferenceMap;
import org.apache.commons.collections4.map.AbstractReferenceMap.ReferenceStrength;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaException;
import org.nuiton.topia.TopiaNotFoundException;
import org.nuiton.topia.TopiaSecurityDAOHelper;
import org.nuiton.topia.event.TopiaTransactionEvent;
import org.nuiton.topia.event.TopiaTransactionVetoable;
import org.nuiton.topia.framework.TopiaContextImplementor;
import org.nuiton.topia.persistence.TopiaId;
import org.nuiton.topia.security.entities.authorization.TopiaAssociationAuthorizationDAO;
import org.nuiton.topia.security.entities.authorization.TopiaAuthorization;
import org.nuiton.topia.security.entities.authorization.TopiaAuthorizationDAO;
import org.nuiton.topia.security.entities.authorization.TopiaEntityAuthorization;
import org.nuiton.topia.security.entities.authorization.TopiaEntityAuthorizationDAO;
import org.nuiton.topia.security.entities.authorization.TopiaEntityAuthorizationImpl;
import org.nuiton.topia.security.entities.authorization.TopiaExpressionLink;
import org.nuiton.topia.security.entities.authorization.TopiaExpressionLinkDAO;
import org.nuiton.topia.security.entities.user.TopiaGroupDAO;
import org.nuiton.topia.security.entities.user.TopiaUser;
import org.nuiton.topia.security.entities.user.TopiaUserDAO;
import org.nuiton.topia.security.jaas.TopiaConfiguration;
import org.nuiton.topia.security.jaas.TopiaPermission;
import org.nuiton.topia.security.jaas.TopiaPolicy;
import org.nuiton.topia.security.listener.EntityVetoable;
import org.nuiton.topia.security.listener.NoSecurityLoad;
import org.nuiton.topia.security.listener.PropertyReadListener;
import org.nuiton.topia.security.listener.PropertyVetoable;
import org.nuiton.topia.security.listener.PropertyWriteListener;
import org.nuiton.topia.security.util.TopiaSecurityCaching;
import org.nuiton.topia.security.util.TopiaSecurityUtil;

import javax.security.auth.Subject;
import javax.security.auth.login.Configuration;
import java.security.AccessController;
import java.security.Permission;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.nuiton.topia.security.util.TopiaSecurityUtil.CREATE;
import static org.nuiton.topia.security.util.TopiaSecurityUtil.DELETE;
import static org.nuiton.topia.security.util.TopiaSecurityUtil.TOPIA_SECURITY_PERSISTENCE_CLASSES;
import static org.nuiton.topia.security.util.TopiaSecurityUtil.UPDATE;

/**
 * Implantation du manager pour la securite. C'est le point d'acces a l'ensemble
 * des fonctionnalites de la securite.
 *
 * @author ruchaud
 */
public class TopiaSecurityServiceImpl implements TopiaSecurityService,
        TopiaTransactionVetoable {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(TopiaSecurityServiceImpl.class);

    /* Context ToPIA */

    private TopiaContext rootContext;

    private TopiaContext securityContext;

    /* Listeners */

    private EntityVetoable entityVetoable = new EntityVetoable(this);

    private PropertyReadListener read = new PropertyReadListener(this);

    private PropertyWriteListener write = new PropertyWriteListener(this);

    private PropertyVetoable propertyVetoable = new PropertyVetoable(read, write);

    /* Policy */

    private TopiaPolicy policy = new TopiaPolicy(this);

    /* Cache */

    transient private TopiaSecurityCaching entitiesLoadingCache = new TopiaSecurityCaching(2);

    transient private Map<String, Permission> authorizationsCache =
            Collections.synchronizedMap(new ReferenceMap(ReferenceStrength.SOFT, ReferenceStrength.SOFT));

    transient private Map<String, Set<Permission>> permissionsCache =
            Collections.synchronizedMap(new ReferenceMap(ReferenceStrength.SOFT, ReferenceStrength.SOFT));

    /**
     * Constructeur. Initialise la sécurité à partir du contexte passer en
     * paramètre
     */
    public TopiaSecurityServiceImpl() {
    }

    /* (non-Javadoc)
     * @see org.nuiton.topia.framework.TopiaService#getServiceName()
     */

    @Override
    public String getServiceName() {
        return SERVICE_NAME;
    }

    /*
    * (non-Javadoc)
    * @see org.nuiton.topia.security.TopiaSecurityManager#getPersistenceClasses()
    */

    @Override
    public Class<?>[] getPersistenceClasses() {
        return TOPIA_SECURITY_PERSISTENCE_CLASSES;
    }

    /*
     * (non-Javadoc)
     * @see org.nuiton.topia.security.TopiaSecurityManager#init()
     */

    @Override
    public boolean preInit(TopiaContextImplementor context) {
        return true;
    }

    /*
    * (non-Javadoc)
    * @see org.nuiton.topia.security.TopiaSecurityManager#init()
    */

    @Override
    public boolean postInit(TopiaContextImplementor context) {
        rootContext = context;
        securityContext = null;

        rootContext.addTopiaEntityVetoable(entityVetoable);
        rootContext.addTopiaEntityListener(propertyVetoable);
        rootContext.addTopiaTransactionVetoable(this);

        policy.installPolicy();
        Configuration.setConfiguration(new TopiaConfiguration("topia", this));

        return true;
    }

    /**
     * Permet de propager la sécurité sur l'ensemble des contextes
     *
     * @param event
     */
    @Override
    public void beginTransaction(TopiaTransactionEvent event) {
        TopiaContext context = event.getSource();
        context.addTopiaEntityVetoable(entityVetoable);
        context.addTopiaEntityListener(propertyVetoable);
        context.addTopiaTransactionVetoable(this);
    }

    /*
     * (non-Javadoc)
     * @see org.nuiton.topia.security.TopiaSecurityManager#getSecurityContext()
     */

    public TopiaContext getSecurityContext() throws TopiaException {
        if (securityContext == null) {
            securityContext = rootContext.beginTransaction();
        }
        return securityContext;
    }

    /**
     * Permet de récupérer le DAO dans le contexte de sécurité.
     *
     * @return DAO du TopiaUser
     */
    public TopiaUserDAO getTopiaUserDAO() {
        try {
            return TopiaSecurityDAOHelper.getTopiaUserDAO(getSecurityContext());
        } catch (TopiaException te) {
            log.error("Recuperation du TopiaUserDAO impossible", te);
        }
        return null;
    }

    /**
     * Permet de récupérer le DAO dans le contexte de sécurité.
     *
     * @return DAO du TopiaGroup
     */
    public TopiaGroupDAO getTopiaGroupDAO() {
        try {
            return TopiaSecurityDAOHelper.getTopiaGroupDAO(getSecurityContext());
        } catch (TopiaException te) {
            log.error("Recuperation du TopiaGroupDAO impossible", te);
        }
        return null;
    }

    /**
     * Permet de récupérer le DAO dans le contexte de sécurité.
     *
     * @return DAO du TopiaAuthorization
     */
    public TopiaAuthorizationDAO getTopiaAuthorizationDAO() {
        try {
            return TopiaSecurityDAOHelper.getTopiaAuthorizationDAO(getSecurityContext());
        } catch (TopiaException te) {
            log.error("Recuperation du TopiaAuthorizationDAO impossible", te);
        }
        return null;
    }

    /**
     * Permet de récupérer le DAO dans le contexte de sécurité.
     *
     * @return DAO du TopiaEntityAuthorization
     */
    public TopiaEntityAuthorizationDAO getTopiaEntityAuthorizationDAO() {
        try {
            return TopiaSecurityDAOHelper.getTopiaEntityAuthorizationDAO(getSecurityContext());
        } catch (TopiaException te) {
            log.error("Recuperation du TopiaEntityAuthorizationDAO impossible", te);
        }
        return null;
    }

    /**
     * Permet de récupérer le DAO dans le contexte de sécurité.
     *
     * @return DAO du TopiaExpressionLinkDAO
     */
    public TopiaExpressionLinkDAO getTopiaIdLinkDAO() {
        try {
            return TopiaSecurityDAOHelper.getTopiaExpressionLinkDAO(getSecurityContext());
        } catch (TopiaException te) {
            log.error("Recuperation du TopiaLinkAuthorizationDAO impossible", te);
        }
        return null;
    }

    /**
     * Permet de récupérer le DAO dans le contexte de sécurité.
     *
     * @return DAO du TopiaAssociationAuthorization
     */
    public TopiaAssociationAuthorizationDAO getTopiaAssociationAuthorizationDAO() {
        try {
            return TopiaSecurityDAOHelper.getTopiaAssociationAuthorizationDAO(getSecurityContext());
        } catch (TopiaException te) {
            log.error("Recuperation du TopiaAssociationAuthorizationDAO impossible", te);
        }
        return null;
    }

    /**
     * Permet d'ajouter dans le cache les permissions pour un principal donné.
     *
     * @param principalName nom du principal pour lequel on doit charger les
     *                      permissions
     * @return la liste des permissions
     * @throws TopiaException
     */
    public Set<Permission> putPermissionsCache(String principalName) throws TopiaException {
        TopiaAuthorizationDAO authorizationDAO = getTopiaAuthorizationDAO();
        Collection<TopiaAuthorization> authorizations = authorizationDAO.findAll();

        Set<Permission> permissions = new HashSet<Permission>();
        permissionsCache.put(principalName, permissions);

        String className = TopiaId.getClassNameAsString(principalName);
        // AddEmptyPrincipals pr�cise si il faut les autorisations dont les principals sont vide
        boolean addEmptyPrincipals = className.equals(TopiaUser.class.getName());

        for (TopiaAuthorization authorization : authorizations) {
            Set<?> principals = authorization.getPrincipals();
            if (principals.contains(principalName)
                    || addEmptyPrincipals && principals.isEmpty()) {
                Permission permission = getAuthorizationCache(authorization);
                permissions.add(permission);
            }
        }
        getSecurityContext().commitTransaction();
        return permissions;
    }

    /**
     * Permet de récupérer dans le cache l'encapsulation de l'autorisation en
     * permission et créer l'encapsulation si elle n'existe pas dans le cache
     *
     * @param authorization autorisation recherché
     * @return permission encapsulation de l'autorisation en permission
     */
    private Permission getAuthorizationCache(TopiaAuthorization authorization) {
        String topiaIdAuthorization = authorization.getTopiaId();
        Permission permission = authorizationsCache.get(topiaIdAuthorization);
        if (permission == null) {
            permission = new TopiaPermission(authorization);
            authorizationsCache.put(topiaIdAuthorization, permission);
        }
        return permission;
    }

    /**
     * Permet de récupérer depuis le cache les permissions pour un principal
     * donné.
     *
     * @param principalName nom du principal
     * @return permmissions d'un principal
     */
    public Set<Permission> getPermissionsCache(String principalName) {
        return permissionsCache.get(principalName);
    }

    /**
     * Permet de mettre dans le cache pour l'utilisateur en cours si il a droit
     * l'autorisation ou non de charger une entité.
     *
     * @param topiaId    identification de l'entité
     * @param authorized autorisation sur l'entité, true pour autorisé et false
     *                   pour non autorisé
     */
    public void putEntitiesLoadingCache(String topiaId, boolean authorized) {
        String userPrincipal = TopiaSecurityUtil.getUserPrincipal();
        if (userPrincipal != null) {
            entitiesLoadingCache.put(authorized, userPrincipal, topiaId);
        }
    }

    /**
     * Permet de récupérer dans le cache pour l'utilisateur en cours si il a
     * droit l'autorisation ou non de charger une entité.
     *
     * @param topiaId identification de l'entité
     * @return autorisation sur l'entité, true pour autorisé et false pour non
     *         autorisé
     */
    public Boolean getEntitiesLoadingCache(String topiaId) {
        String userPrincipal = TopiaSecurityUtil.getUserPrincipal();
        if (userPrincipal != null) {
            return (Boolean) entitiesLoadingCache.get(userPrincipal, topiaId);
        }
        return null;
    }

    /**
     * Permet de supprimer un entrée dans le cache pour un utilisateur
     *
     * @param userPrincipal principal de l'utilisateur
     */
    public void removeEntitiesLoadingCache(String userPrincipal) {
        entitiesLoadingCache.clear(userPrincipal);
    }

    /**
     * Permet de tester le cache
     *
     * @param topiaId identification de l'entité
     * @return vrai si il trouve sinon faux
     */
    public boolean containEntitiesLoadingCache(String topiaId) {
        Boolean authorized = getEntitiesLoadingCache(topiaId);
        return authorized != null;
    }

    /**
     * Renvoi les identifiants qui remplacent l'identifiant en cours d'aprés la
     * table de correspondance TopiaExpressionLink.
     *
     * @param topiaId identifiant à remplacer
     * @return retourne l'identifiant remplacé
     */
    //FIXME : Voir si on peut mettre en relation un objet vers plusieurs objets
    protected List<String> getRealExpressions(String topiaId) {
        try {
            List<String> allBy = getSecurityContext().findAll("select distinct link.by from " +
                    TopiaExpressionLink.class.getName() + " link where link.replace=:replace",
                    "replace", topiaId);
            getSecurityContext().commitTransaction();

            if (allBy == null) {
                allBy = new ArrayList<String>();
            }

            allBy.add(topiaId);
            return allBy;
        } catch (TopiaException te) {
            throw new SecurityException("Replace expression for link failed", te);
        }
    }

    /**
     * Permet de determiner seulement les actions pour lesquelles on doit
     * vérifier les actions.
     *
     * @param topiaId identifiant de l'entité ToPIA
     * @param actions actions à vérifier
     * @return actions réellement à vérifer
     */
    protected int getRealActions(String topiaId, int actions) {
        try {
            Class<?> klass = TopiaId.getClassName(topiaId);

            //if(TopiaSecurityUtil.isImplement(klass, NoSecurityLoad.class)) {
            if (NoSecurityLoad.class.isAssignableFrom(klass)) {
                //LOAD
                actions &= UPDATE + DELETE + CREATE;
            }

            /* TODO:
            if(TopiaSecurityUtil.isImplement(klass, NoSecurityUpdate.class)) {
                // UPDATE
                actions &= LOAD + DELETE + CREATE;
            }
            ...
            */
        } catch (TopiaNotFoundException te) {
            if (log.isDebugEnabled()) {
                log.debug("Real actions failed", te);
            }
        }
        return actions;
    }
    /* (non-Javadoc)
     * @see org.nuiton.topia.security.TopiaSecurityService#checkPermission(java.lang.Class, int)
     */

    @Override
    public void checkPermission(Class<?> entityClass, int actions) throws SecurityException {
        if (log.isTraceEnabled()) {
            log.trace("Checking permissions to entity class : " + entityClass);
        }
        if (entityClass == null) {
            throw new SecurityException("Class cannot be null");
        }
        String topiaId = entityClass.getName() + "#*";
        checkPermission(topiaId, actions);
    }

    /* (non-Javadoc)
     * @see org.nuiton.topia.security.TopiaSecurityService#checkPermission(java.lang.String, int)
     */

    @Override
    public void checkPermission(String topiaId, int actions) throws SecurityException {
        int realActions = getRealActions(topiaId, actions);
        /* Il reste des actions à vérifier */
        if (realActions != 0) {
            Subject subject = Subject.getSubject(AccessController.getContext());
            if (subject != null) {
                List<String> expressions = getRealExpressions(topiaId);

                boolean authorized = false;
                for (String expression : expressions) {
                    TopiaEntityAuthorization authorization = new TopiaEntityAuthorizationImpl(
                            expression, realActions, subject.getPrincipals());
                    try {
                        AccessController.checkPermission(new TopiaPermission(authorization));
                        authorized = true;
                        break;
                    } catch (SecurityException se) {
                        authorized = false;
                    }
                }

                if (!authorized) {
                    throw new SecurityException("Access denied to object \"" + topiaId + "\" for \"" + subject + "\"");
                }
            } else {
                if (log.isWarnEnabled()) {
                    log.warn("Use doAs() and login first");
                }
            }
        }
    }
}
