/*
 * #%L
 * ToPIA :: Service Security
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/* *
* TopiaSecurityUtil.java
*
* Created: 15 févr. 2006
*
* @author Arnaud Thimel &lt;thimel@codelutin.com&gt;
* @version $Revision$
*
* Mise a jour: $Date$
* par : $Author$
*/

package org.nuiton.topia.security.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaContextFactory;
import org.nuiton.topia.TopiaException;
import org.nuiton.topia.framework.TopiaContextImpl;
import org.nuiton.topia.persistence.TopiaId;
import org.nuiton.topia.security.entities.authorization.TopiaAssociationAuthorizationImpl;
import org.nuiton.topia.security.entities.authorization.TopiaAuthorizationImpl;
import org.nuiton.topia.security.entities.authorization.TopiaEntityAuthorizationImpl;
import org.nuiton.topia.security.entities.authorization.TopiaExpressionLinkImpl;
import org.nuiton.topia.security.entities.user.TopiaGroupImpl;
import org.nuiton.topia.security.entities.user.TopiaUser;
import org.nuiton.topia.security.entities.user.TopiaUserImpl;
import org.nuiton.topia.security.jaas.TopiaLoginModule;
import sun.misc.BASE64Encoder;

import javax.security.auth.Subject;
import java.security.AccessController;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.Principal;
import java.util.Properties;
import java.util.StringTokenizer;

/**
 * Classe utilitaire
 * 
 * @author ruchaud
 *
 */
public class TopiaSecurityUtil {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(TopiaSecurityUtil.class);
    
    public static final int LOAD = 0x1;
    public static final String LOAD_TEXT = "LOAD";
    public static final int CREATE = 0x2;
    public static final String CREATE_TEXT = "CREATE";
    public static final int UPDATE = 0x4;
    public static final String UPDATE_TEXT = "UPDATE";
    public static final int DELETE = 0x8;
    public static final String DELETE_TEXT = "DELETE";

    public static final String SECURITY_MANAGER_KEY = "topia.app.security.manager";

    public static final String TOPIA_LOGIN_MODULE = TopiaLoginModule.class.getName();

    public static final Class<?>[] TOPIA_SECURITY_PERSISTENCE_CLASSES = new Class [] {
        TopiaUserImpl.class,
        TopiaEntityAuthorizationImpl.class,
        TopiaGroupImpl.class,
        TopiaExpressionLinkImpl.class,
        TopiaAssociationAuthorizationImpl.class,
        TopiaAuthorizationImpl.class,
    };
    
    /**
     * Applique un algorithme de hashage sur la chaine de caratère passée en
     * paramètre
     * @param msg la chaine de caractère sur laquelle on veut opérer le hashage
     * @return La chaine de caractère une fois l'algorithme appliqué
     */
    public static String hash(String msg) {
        return digestSHAHex(msg);
    }
    
    /**
     * Applique un algorithme de hashage sur la chaine de caratère passée en
     * paramètre
     * @param msg la chaine de caratère sur laquelle on veut opérer le hashage
     * @return La chaine de caractère une fois l'algorithme appliqué
     */
    public static String digestSHABase64(String msg) {
        if (msg == null) {
            return null;
        }
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA");
            byte[] bytes = msg.getBytes();
            bytes = digest.digest(bytes);
            BASE64Encoder encoder = new sun.misc.BASE64Encoder();
            String msgHashed = encoder.encode(bytes);
            return msgHashed;
        } catch (NoSuchAlgorithmException nsee) {
            return msg;
        }
    }
    
    /**
     * Fait le checksum SHA de la chaine de caractere le resultat est retourne
     * sous forme de chaine Hexadecimal.
     * @param ch ?
     * @return  ?
     */
     static public String digestSHAHex(String ch){
         if(ch == null){
             return null;
         }
         try{
             MessageDigest md = MessageDigest.getInstance("SHA");
             md.update(ch.getBytes());
             byte[] digest = md.digest();

             StringBuffer result = new StringBuffer();
             for (int i=0; i < digest.length; i++) {
                 String hex = Integer.toHexString(0xFF & digest[i]);
                 if (hex.length() == 1) {
                     result.append("0").append(hex);
                 } else {
                     result.append(hex);
                 }
             }

             return result.toString();
         }catch(NoSuchAlgorithmException eee){
             log.warn("Impossible de trouve l'algo SHA", eee);
             return ch;
         }
     }

    /**
     * Transforme actions en un entier.
     * @param actions -
     *            combinaison de mots cles "load" "update" "create" et "delete"
     *            separes par des virgules. Ex : "load,update"
     * @return 0 si aucune permission. Une combinaison des permissions
     */
    public static int actionsString2Int(String actions) {
        int result = 0x0;
        StringTokenizer tokens = new StringTokenizer(actions, ",");
        while (tokens.hasMoreTokens()) {
            String action = tokens.nextToken().trim();
            if (LOAD_TEXT.equalsIgnoreCase(action)) {
                result |= LOAD;
            } else if (CREATE_TEXT.equalsIgnoreCase(action)) {
                result |= CREATE;
            } else if (UPDATE_TEXT.equalsIgnoreCase(action)) {
                result |= UPDATE;
            } else if (DELETE_TEXT.equalsIgnoreCase(action)) {
                result |= DELETE;
            } else {
                throw new IllegalArgumentException("action not supported: "
                        + action);
            }
        }
        return result;
    }

    /**
     * Transforme actions en une chaine de caractères
     * @param actions les actions sous forme d'un entier
     * @return La chaine des actions passé en paramètre
     */
    public static String actionsInt2String(int actions) {
        StringBuffer result = new StringBuffer();
        if ((actions & LOAD) == LOAD) {
            result.append(LOAD_TEXT);
            result.append(",");
        }
        if ((actions & CREATE) == CREATE) {
            result.append(CREATE_TEXT);
            result.append(",");
        }
        if ((actions & UPDATE) == UPDATE) {
            result.append(UPDATE_TEXT);
            result.append(",");
        }
        if ((actions & DELETE) == DELETE) {
            result.append(DELETE_TEXT);
            result.append(",");
        }

        if (result.length() > 0) {
            return result.substring(0, result.length() - 1);
        } else {
            return "";
        }
    }
 
    /**
     * Permet de récupérer parmis la liste des principals, le principal de type
     * TopiaUser
     * @return topiaId du principal de l'utilisateur
     */
    public static String getUserPrincipal() {
        Subject subject = Subject.getSubject(AccessController.getContext());
        if (subject != null) {
            for (Principal principal : subject.getPrincipals()) {
                String className = TopiaId.getClassNameAsString(principal.getName());
                if(className.equals(TopiaUser.class.getName())) {
                    return principal.getName();
                }
            }
        }
        return null;
    }
    
    /**
     * Création d'un context sans sécurité
     * @param transaction TopiaContext avec lequel le nouveau contexte est crée
     * @return retourne un contexte sans sécurité
     * @throws TopiaException if any topia pb
     */
    public static TopiaContext beginTransactionWithoutSecurity(TopiaContext transaction) throws TopiaException {
        TopiaContextImpl context = (TopiaContextImpl) transaction;
        Properties config = context.getConfig();

        config.setProperty("topia.service.security", "");
        String persistences = config.getProperty("topia.persistence.classes");
        for (Class<?> klass : TOPIA_SECURITY_PERSISTENCE_CLASSES) {
            persistences +=  "," + klass.getName();
        }
        config.setProperty("topia.persistence.classes", persistences);

        TopiaContext topiaContext = TopiaContextFactory.getContext(config);
        return topiaContext.beginTransaction();
    }
} //TopiaSecurityUtil
