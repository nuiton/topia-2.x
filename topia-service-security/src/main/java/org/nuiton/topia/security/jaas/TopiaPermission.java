/*
 * #%L
 * ToPIA :: Service Security
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/* *
* TopiaPermission.java
*
* Created: 16 févr. 2006
*
* @author Arnaud Thimel &lt;thimel@codelutin.com&gt;
* @version $Revision$
*
* Mise a jour: $Date$
* par : $Author$
*/

package org.nuiton.topia.security.jaas;

import static org.nuiton.topia.security.util.TopiaSecurityUtil.actionsInt2String;

import java.security.Permission;

import org.nuiton.topia.security.entities.authorization.TopiaAuthorization;

/**
 * Classe permettant d'encapsuler les autorisations et de déléguer le travail aux
 * autorisations.
 * @author ruchaud
 */
public class TopiaPermission extends Permission {

    private static final long serialVersionUID = 1L;
    
    private TopiaAuthorization authorization;

    public TopiaPermission(TopiaAuthorization authorization) {
        super(authorization.getExpression());
        this.authorization = authorization;
    }

    /*
     * (non-Javadoc)
     * @see java.security.Permission#implies(java.security.Permission)
     */
    @Override
    public boolean implies(Permission permission) {
        if (permission == null)
            return false;
        if (!(permission instanceof TopiaPermission))
            return false;
        TopiaPermission other = (TopiaPermission)permission;
        return authorization.implies(other.getAuthorization());
    }

    /*
     * (non-Javadoc)
     * @see java.security.Permission#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null)
            return false;
        if (obj == this)
            return true;
        if (!(obj instanceof TopiaPermission))
            return false;
        TopiaPermission that = (TopiaPermission)obj;
        return implies(that) && that.implies(this);
    }

    /*
     * (non-Javadoc)
     * @see java.security.Permission#hashCode()
     */
    @Override
    public int hashCode() {
        return authorization.hashCode();
    }

    /*
     * (non-Javadoc)
     * @see java.security.Permission#getActions()
     */
    @Override
    public String getActions() {
        return actionsInt2String(authorization.getActions());
    }

    public TopiaAuthorization getAuthorization() {
        return authorization;
    }
}
