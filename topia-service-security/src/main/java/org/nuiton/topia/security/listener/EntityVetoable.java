/*
 * #%L
 * ToPIA :: Service Security
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/* *
* TopiaSecurityVetoableListener.java
*
* Created: 10 févr. 2006
*
* @author Arnaud Thimel &lt;thimel@codelutin.com&gt;
* @version $Revision$
*
* Mise a jour: $Date$
* par : $Author$
*/

package org.nuiton.topia.security.listener;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.TopiaNotFoundException;
import org.nuiton.topia.event.TopiaEntityEvent;
import org.nuiton.topia.event.TopiaEntityVetoable;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaId;
import org.nuiton.topia.security.TopiaSecurityServiceImpl;

import static org.nuiton.topia.security.util.TopiaSecurityUtil.CREATE;
import static org.nuiton.topia.security.util.TopiaSecurityUtil.DELETE;
import static org.nuiton.topia.security.util.TopiaSecurityUtil.LOAD;

/**
 * Listenner permettant de vérifier les autorisations pour la création ou la
 * suppression d'une entité.
 *
 * @author ruchaud
 */
public class EntityVetoable implements TopiaEntityVetoable {

    private static Log log = LogFactory.getLog(EntityVetoable.class);

    private TopiaSecurityServiceImpl securityManager;

    public EntityVetoable(TopiaSecurityServiceImpl securityManager) {
        this.securityManager = securityManager;
    }

    @Override
    public void create(TopiaEntityEvent event) {
        TopiaEntity entity = event.getEntity();
        String topiaId = entity.getTopiaId();
        Class<? extends TopiaEntity> clazz;
        try {
            clazz = TopiaId.getClassName(topiaId);
        } catch (TopiaNotFoundException e) {
            // Ne devrait jamais ce produire
            throw new SecurityException("Access denied to entity creation", e);
        }

        if (log.isDebugEnabled()) {
            log.debug("[Security] create entity : " + clazz.getName());
        }
        securityManager.checkPermission(clazz, CREATE);
    }

    @Override
    public void delete(TopiaEntityEvent event) {
        String topiaId = event.getEntity().getTopiaId();
        if (log.isDebugEnabled()) {
            log.debug("[Security] delete entity : " + topiaId);
        }
        securityManager.checkPermission(topiaId, DELETE);
    }

    @Override
    public void load(TopiaEntityEvent event) {
        boolean authorized = true;
        TopiaEntity entity = event.getEntity();
        String topiaId = entity.getTopiaId();

        if (log.isDebugEnabled()) {
            log.debug("[Security] load entity : " + topiaId);
        }

        /* Vérification dans le cache */
        boolean contain = securityManager.containEntitiesLoadingCache(topiaId);

        if (!contain) {
            try {
                securityManager.checkPermission(topiaId, LOAD);

            } catch (SecurityException te) {
                authorized = false;
            }

            /* Mise en cache */
            securityManager.putEntitiesLoadingCache(topiaId, authorized);
        }
    }

    @Override
    public void update(TopiaEntityEvent event) {
    }

}
