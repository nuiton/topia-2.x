/*
 * #%L
 * ToPIA :: Service Security
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.topia.security.entities.user;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe permettant la gestion de groupes d'utilisateurs.
 * @author ruchaud
 *
 */
public class TopiaGroupImpl extends TopiaGroupAbstract {

    private static final long serialVersionUID = 1L;

    /**
     * Contructeur permettant l'initialisation des sous-groupes et des utilisateurs
     * à vide.
     */
    public TopiaGroupImpl() {
        topiaUser = new ArrayList<TopiaUser>();
        subGroup = new ArrayList<TopiaGroup>();
    }

    /**
     * Permet de récupérer les utilisateurs du groupe et de ses sous-groupes.
     * @return liste des utilisateurs du groupe
     */
    public List getAllUser() {
        List<TopiaUser> result = new ArrayList<TopiaUser>();
        result.addAll(getTopiaUser());
        for (TopiaGroup group : getSubGroup()) {
            result.addAll(group.getAllUser());
        }
        return result;
    }

    /**
     * Permet de récupérer l'ensemble des groupes parents
     * @return groupes parents
     */
    public List getAllSuperGroup() {
        List<TopiaGroup> result = new ArrayList<TopiaGroup>();
        if(superGroup != null) {
            result.add(superGroup);
            result.addAll(superGroup.getAllSuperGroup());
        }
        return result;
    }

}
