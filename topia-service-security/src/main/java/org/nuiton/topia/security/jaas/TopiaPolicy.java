/*
 * #%L
 * ToPIA :: Service Security
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/* *
 * TopiaPolicy.java
 *
 * Created: 17 févr. 2006
 *
 * @author Arnaud Thimel &lt;thimel@codelutin.com&gt;
 * @version $Revision$
 *
 * Mise a jour: $Date$
 * par : $Author$
 */

package org.nuiton.topia.security.jaas;

import java.security.AccessController;
import java.security.CodeSource;
import java.security.Permission;
import java.security.PermissionCollection;
import java.security.Policy;
import java.security.Principal;
import java.security.ProtectionDomain;
import java.util.Set;

import javax.security.auth.Subject;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.TopiaException;
import org.nuiton.topia.security.TopiaSecurityServiceImpl;

/**
 * Implantation d'un policy avec une prise en compte des permissions à la volée.
 * @author ruchaud
 */
public class TopiaPolicy extends Policy {

    private Log log = LogFactory.getLog(TopiaPolicy.class);

    private TopiaSecurityServiceImpl securityManager;

    protected Policy parentPolicy;

    public TopiaPolicy(TopiaSecurityServiceImpl securityManager) {
        this.securityManager = securityManager;
    }

    /**
     * Renvoie la Policy parente
     * @see #installPolicy()
     * @return l'attribut parentPolicy
     */
    public Policy getParentPolicy() {
        return parentPolicy;
    }

    /**
     * Remplace la Policy parente
     * @param parentPolicy la nouvelle Policy parente
     */
    public void setParentPolicy(Policy parentPolicy) {
        this.parentPolicy = parentPolicy;
    }

    /* (non-Javadoc)
     * @see java.security.Policy#getPermissions(java.security.CodeSource)
     */
    @Override
    public PermissionCollection getPermissions(CodeSource codesource) {
        PermissionCollection pc = parentPolicy.getPermissions(codesource);
        return pc;
    }

    /* (non-Javadoc)
     * @see java.security.Policy#getPermissions(java.security.ProtectionDomain)
     */
    @Override
    public PermissionCollection getPermissions(ProtectionDomain domain) {
        PermissionCollection pc = parentPolicy.getPermissions(domain);
        
        /* Vérification dans le cache */
        Subject subject = Subject.getSubject(AccessController.getContext());
        if (subject != null) {
            for (Principal principal : subject.getPrincipals()) {
                String principalName = principal.getName();
                Set<Permission> permissions = securityManager.getPermissionsCache(principalName);
                if(permissions == null) {
                    try {
                        permissions = securityManager.putPermissionsCache(principalName);
                    } catch (TopiaException e) {
                        log.error("Récupération des TopiaPermission impossible", e);
                    }
                }
                for (Permission permission : permissions) {
                    pc.add(permission);
                }
            }
        } else {
            log.error("Récupération des TopiaPermission impossible");
        }

        return pc;
    }

    /* (non-Javadoc)
     * @see java.security.Policy#refresh()
     */
    @Override
    public void refresh() {
        parentPolicy.refresh();
    }

    /* (non-Javadoc)
     * @see java.security.Policy#implies(java.security.ProtectionDomain,
     *      java.security.Permission)
     */
    @Override
    public boolean implies(ProtectionDomain domain, Permission permission) {
        PermissionCollection pc = getPermissions(domain);
        if (pc == null) {
            return false;
        }
        return pc.implies(permission);
    }

    /**
     * Installe cette TopiaPolicy. Si la Policy existante est déja cette
     * TopiaPolicy alors la méthode n'a pas d'effet. Si une autre Policy existe
     * deja alors cette TopiaPolicy, elle conserve l'ancienne Policy dans
     * parentPolicy et la remplace alors.
     */
    public void installPolicy() {
        Policy policy = Policy.getPolicy();
        if (equals(policy))
            return;
        if (policy instanceof TopiaPolicy) {
            if (log.isDebugEnabled()) {
                log.debug("Policy deja modifie en: " + policy);
            }
        } else {
            if (log.isDebugEnabled()) {
                log.debug("l'ancienne Policy etait: " + policy);
            }
            setParentPolicy(policy);
            Policy.setPolicy(this);
        }
    }

} // TopiaPolicy
