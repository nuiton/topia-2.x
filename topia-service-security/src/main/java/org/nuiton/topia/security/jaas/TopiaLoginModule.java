/*
 * #%L
 * ToPIA :: Service Security
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/* *
* TopiaLoginModule.java
*
* Created: 15 févr. 2006
*
* @author Arnaud Thimel &lt;thimel@codelutin.com&gt;
* @version $Revision$
*
* Mise a jour: $Date$
* par : $Author$
*/

package org.nuiton.topia.security.jaas;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.TopiaException;
import org.nuiton.topia.security.TopiaSecurityServiceImpl;
import org.nuiton.topia.security.entities.user.TopiaGroup;
import org.nuiton.topia.security.entities.user.TopiaUser;
import org.nuiton.topia.security.entities.user.TopiaUserDAO;

import javax.security.auth.Subject;
import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.NameCallback;
import javax.security.auth.callback.PasswordCallback;
import javax.security.auth.login.LoginException;
import javax.security.auth.spi.LoginModule;
import java.security.Principal;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.nuiton.topia.security.util.TopiaSecurityUtil.SECURITY_MANAGER_KEY;

/**
 * LoginModule permettant l'authentification d'un utilisateur au près du
 * système.
 *
 * @author ruchaud
 */
public class TopiaLoginModule implements LoginModule {

    private Log log = LogFactory.getLog(TopiaLoginModule.class);

    private Subject subject;

    private CallbackHandler callbackHandler;

    private Set<Principal> principals;

    private TopiaSecurityServiceImpl securityManager;

    /* (non-Javadoc)
     * @see javax.security.auth.spi.LoginModule#initialize(
     *   javax.security.auth.Subject, 
     *   javax.security.auth.callback.CallbackHandler, 
     *   java.util.Map, 
     *   java.util.Map)
     */

    public void initialize(Subject subject, CallbackHandler callbackHandler,
                           Map<String, ?> sharedState, Map<String, ?> options) {
        this.subject = subject;
        this.callbackHandler = callbackHandler;
        principals = null;
        securityManager = (TopiaSecurityServiceImpl) options.get(SECURITY_MANAGER_KEY);
    }

    /* (non-Javadoc)
    * @see javax.security.auth.spi.LoginModule#login()
    */

    public boolean login() throws LoginException {
        if (callbackHandler == null) {
            throw new LoginException("CallbackHandler cannot be null");
        }
        if (securityManager == null) {
            throw new LoginException("\"" + SECURITY_MANAGER_KEY + "\" property must be set");
        }

        String login, password = null;

        NameCallback nc = new NameCallback("login");
        PasswordCallback pc = new PasswordCallback("password", false);

        Callback[] callbacks = new Callback[2];

        callbacks[0] = nc;
        callbacks[1] = pc;

        try {
            //Récupération du login et mot de passe
            callbackHandler.handle(callbacks);
        } catch (Exception eee) {
            if (log.isWarnEnabled()) {
                log.warn("Utilisation du CallbackHandler impossible", eee);
            }
            LoginException le = new LoginException(
                    "Utilisation du CallbackHandler impossible");
            le.initCause(eee);
            throw le;
        }
        login = nc.getName();
        password = new String(pc.getPassword());
        pc.clearPassword();

        //Vérification du login/pass et récupération des Principals
        try {
            TopiaUserDAO topiaUserDAO = securityManager.getTopiaUserDAO();
            TopiaUser user = topiaUserDAO.findByLogin(login);

            if (user != null && user.checkPassword(password)) {
                // Récupération des principals
                principals = new HashSet<Principal>();

                String topiaIdUser = user.getTopiaId();
                principals.add(new TopiaPrincipal(topiaIdUser));
                securityManager.putPermissionsCache(topiaIdUser);
                securityManager.removeEntitiesLoadingCache(topiaIdUser);

                // Force le rechargement des groupes d'où la non utilisation de
                // la méthode : user.getTopiaGroup()
                Set<TopiaGroup> groups = new HashSet<TopiaGroup>(securityManager.getSecurityContext().findAll(
                        "select topiaGroup from " + TopiaGroup.class.getName() +
                                " topiaGroup join topiaGroup.topiaUser as topiaUser where topiaUser = :user", "user", user));

                if (groups != null) {
                    for (TopiaGroup group : groups) {
                        for (TopiaGroup superGroup : (List<TopiaGroup>) group.getAllSuperGroup()) {
                            String topiaIdGroup = superGroup.getTopiaId();
                            principals.add(new TopiaPrincipal(topiaIdGroup));
                            securityManager.putPermissionsCache(topiaIdGroup);
                        }
                        String topiaIdGroup = group.getTopiaId();
                        principals.add(new TopiaPrincipal(topiaIdGroup));
                        securityManager.putPermissionsCache(topiaIdGroup);
                    }
                }

                securityManager.getSecurityContext().commitTransaction();
            } else {
                // Echec d'authentification
                principals = null;
                throw new LoginException("Erreur lors de l'authentification " + login);
            }
        } catch (TopiaException te) {
            // Echec de récupération de l'utilisateur
            if (log.isWarnEnabled()) {
                log.warn("Erreur lors de l'authentification", te);
            }
            LoginException le = new LoginException("Erreur lors de l'authentification");
            le.initCause(te);
            throw le;
        }

        return true;
    }

    /* (non-Javadoc)
    * @see javax.security.auth.spi.LoginModule#commit()
    */

    public boolean commit() throws LoginException {
        subject.getPrincipals().addAll(principals);
        return true;
    }

    /* (non-Javadoc)
    * @see javax.security.auth.spi.LoginModule#abort()
    */

    public boolean abort() throws LoginException {
        //On effectue les mêmes actions que logout
        return logout();
    }

    /* (non-Javadoc)
    * @see javax.security.auth.spi.LoginModule#logout()
    */

    public boolean logout() throws LoginException {
        //On libère les ressources
        subject.getPrincipals().removeAll(principals);
        subject = null;
        principals.clear();
        principals = null;
        callbackHandler = null;
        return true;
    }

} //TopiaLoginModule
