/*
 * #%L
 * ToPIA :: Service Security
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.topia.security.entities.authorization;


import static org.nuiton.topia.security.util.TopiaSecurityUtil.actionsString2Int;
import java.util.HashSet;
import java.util.List;

import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaException;
import org.nuiton.topia.persistence.TopiaId;

/**
 * Classe permettant les autorisations de type association. C'est à dire l'autorisation
 * permet de donné une autorisation par rapport à une association dans le modèle.
 * Attention il faut avoir une autorisation de chargement sur la l'entité où
 * commence l'association
 * @author ruchaud
 */
//TODO: Gestion d'un identifiant de début et de fin d'association typé
public class TopiaAssociationAuthorizationImpl extends
        TopiaAssociationAuthorizationAbstract {

    private static final long serialVersionUID = 1L;

    /**
     * Constructeur permettant initialisation des principals. 
     */
    public TopiaAssociationAuthorizationImpl() {
        principals = new HashSet<String>();
    }
    
    @Override
    public String getExpression() {
        return idBeginAssociation;
    }

    @Override
    public boolean impliesExpression(String thisExpression, String thatExpression) {
        String select = "count(test)";
        String from = TopiaId.getClassNameAsString(thisExpression) + " test join test." + nameAssociation + " as association";
        String where = "test.topiaId=:test and association.topiaId=:association";
        TopiaContext context = getTopiaContext();
        try {
            List<?> find = context.findAll("select " + select + " from " + from + " where " + where,
                    "test", thisExpression,
                    "association", thatExpression);
            return (Long)find.get(0) >= 1;
        } catch (TopiaException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public void setActions(String actions) {
        this.actions = actionsString2Int(actions);
    }

    @Override
    public void setPrincipals(String principals) {
        this.principals.add(principals);
    }

}
