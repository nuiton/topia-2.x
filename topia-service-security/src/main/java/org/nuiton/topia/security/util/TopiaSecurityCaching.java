/*
 * #%L
 * ToPIA :: Service Security
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.topia.security.util;

import static org.apache.commons.collections4.map.AbstractReferenceMap.ReferenceStrength.SOFT;

import java.util.Collections;
import java.util.Map;

import org.apache.commons.collections4.map.ReferenceMap;

/**
 * La classe permet d'avoir un ensemble de clés pour identifier de manière unique une valeur.
 * @author ruchaud
 */
public class TopiaSecurityCaching {

    /**
     * Le niveau correspond au nombre d'éléments de la clé
     */
    protected int level;
    
    /**
     * Map pour le stockage du première niveau
     */
    protected Map map;
    
    /**
     * Contruit un TopiaSecurityCaching en précisant le nombre d'élément de la clé
     * @param level nombre d'élément de la clé, doit être suppérieur à 0
     */
    public TopiaSecurityCaching(int level) {
        this.level = level;
        map = Collections.synchronizedMap(new ReferenceMap(SOFT, SOFT));
    }

    /**
     * Contruit un TopiaSecurityCaching avec un nombre d'élément de la clé par 
     * défaut à 1
     */
    public TopiaSecurityCaching() {
        this(1);
    }
    
    /**
     * Permet de stocker la valeur pour une clé donnée.
     * @param value valeur à stocker
     * @param keys clé de la valeur
     */
    public void put(Object value, Object ... keys) {
        if(keys.length != level) {
            throw new ArrayIndexOutOfBoundsException();
        }
        
        Map current = map;
        for (int i = 0; i < keys.length -1 ; i++) {
            Object key = keys[i];
            Map next = (Map) current.get(key);
            if (next == null) {
                next = Collections.synchronizedMap(new ReferenceMap(SOFT, SOFT));
                current.put(key, next);
            }
            current = next;
        }
        current.put(keys[keys.length - 1], value);
    }
    
    /**
     * Permet de supprimer une valeur pour une clé donnée.
     * @param keys clé de la valeur
     */
    public void clear(Object ... keys) {
        Map current = map;
        for (int i = 0; i < keys.length -1 ; i++) {
            Object key = keys[i];
            Map next = (Map) current.get(key);
            if (next == null) {
                next = Collections.synchronizedMap(new ReferenceMap(SOFT, SOFT));
                current.put(key, next);
            }
            current = next;
        }
        current.clear();
    }

    /**
     * Permet de récupérer une valeur pour une clé donnée.
     * @param keys clé de la valeur
     * @return valeur stocké pour la clé donnée retourne null si la clé n'est pas
     * valide
     */
    public Object get(Object ... keys) {
        if(keys.length != level) {
            throw new ArrayIndexOutOfBoundsException();
        }
        
        Map current = map;
        for (int i = 0; i < keys.length -1 ; i++) {
            Object key = keys[i];
            Map next = (Map) current.get(key);
            if (next == null) {
                return null;
            } else {
                current = next;
            }
        }

        return current.get(keys[keys.length - 1]);
    }
}
