/*
 * #%L
 * ToPIA :: Service Security
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/* *
* TopiaPrincipal.java
*
* Created: 15 févr. 2006
*
* @author Arnaud Thimel &lt;thimel@codelutin.com&gt;
* @version $Revision$
*
* Mise a jour: $Date$
* par : $Author$
*/

package org.nuiton.topia.security.jaas;

import java.security.Principal;

/**
 * Implantation des principals de JAAS.
 * @author ruchaud
 */
public class TopiaPrincipal implements Principal {

    protected String name;

    /**
     * Contructeur avec comme paramètre le nom du principal.
     * @param name topiaId d'un group ou d'utilisateur
     */
    public TopiaPrincipal(String name) {
        this.name = name;
    }

    /* (non-Javadoc)
     * @see java.security.Principal#getName()
     */
    public String getName() {
       return name;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    public String toString() {
       return getClass().getName() + " : " + name;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    public boolean equals(Object o) {
        if (!(o instanceof TopiaPrincipal))
            return false;
        if (name == null) {
            return ((Principal)o).getName() == null;
        }
        return name.equals(((TopiaPrincipal)o).getName());
    }

} //TopiaPrincipal
