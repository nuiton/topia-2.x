/*
 * #%L
 * ToPIA :: Service Security
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.topia.security.entities.user;

import static org.nuiton.topia.security.util.TopiaSecurityUtil.hash;
import java.util.ArrayList;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Classe permettant la gestion des utilisateurs.
 * @author ruchaud
 */
public class TopiaUserImpl extends TopiaUserAbstract {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(TopiaUserImpl.class);

    private static final long serialVersionUID = 1L;

    /**
     * Contructeur permettant d'initialiser la liste des groupes à vide.
     */
    public TopiaUserImpl() {
        topiaGroup = new ArrayList<TopiaGroup>();
    }
    
    /**
     * Permet de vérifier la validité d'un mot de passe.
     * @param password mot de passe à vérifier
     */
    public boolean checkPassword(String password) {
        if (this.password == null) {
            return password == null;
        } else{
            String hashed = hash(password);            
            log.debug("password is: " + this.password + " and hashed is " + hashed);
            return this.password.equals(hashed);
        }
    }

    /**
     * Permet de remplacer l'ancien mot de passe.
     * @param newPassword nouveau mot de passe
     */
    public void setPassword(String newPassword) {
        password = hash(newPassword);
        log.debug("setPassword to: " +password);
    }

    /**
     * Permet de remplacer l'ancien mot de passe si l'ancien mot de passe est 
     * correct.
     * @param newPassword ancien mot de passe
     * @param oldPassword nouveau mot de passe
     */
    public void setPassword(String oldPassword, String newPassword) {
        if (checkPassword(oldPassword)) {
            setPassword(newPassword);
        }
    }
    
}
