/*
 * #%L
 * ToPIA :: Service Security
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.topia.security.entities.authorization;

/**
 * Permet de spécifier entre deux expressions qu'ils ont le même comportement au niveau
 * de leurs permissions.
 * <p>
 * Exemple :
 * <p>
 * Objet 1 : Permission 1
 * <p>
 * Objet 2 : Permission 2
 * <p>
 * Link : Objet 1 Objet 2
 * <p>
 * Conclusion : Objet 1 a les permissions en plus de l'objet 2
 *  
 * @author ruchaud
 */
public class TopiaExpressionLinkImpl extends TopiaExpressionLinkAbstract {

    private static final long serialVersionUID = 1L;

    public void set(String replace, String by) {
        setReplace(replace);
        setBy(by);
    }

}
