/*
 * #%L
 * ToPIA :: Service Security
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/* *
 * TopiaPolicy.java
 *
 * Created: 17 févr. 2006
 *
 * @author Arnaud Thimel &lt;thimel@codelutin.com&gt;
 * @version $Revision$
 *
 * Mise a jour: $Date$
 * par : */

package org.nuiton.topia.taas.jaas;

import java.security.CodeSource;
import java.security.Permission;
import java.security.PermissionCollection;
import java.security.Policy;
import java.security.Principal;
import java.security.ProtectionDomain;
import java.util.Enumeration;

import javax.security.auth.Subject;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.taas.TaasService;

/**
 * Implantation d'un policy avec une prise en compte des permissions à la volée.
 * @author ruchaud
 */
public class TaasPolicy extends Policy {

    private Log log = LogFactory.getLog(TaasPolicy.class);

    protected Policy parentPolicy;

    protected TaasService taasService;
    
    public TaasPolicy(TaasService taasService) {
        this.taasService = taasService;
    }

    /**
     * Renvoie la Policy parente
     * @see #installPolicy()
     * @return l'attribut parentPolicy
     */
    public Policy getParentPolicy() {
        return parentPolicy;
    }

    /**
     * Remplace la Policy parente
     * @param parentPolicy la nouvelle Policy parente
     */
    public void setParentPolicy(Policy parentPolicy) {
        this.parentPolicy = parentPolicy;
    }

    /* (non-Javadoc)
     * @see java.security.Policy#getPermissions(java.security.CodeSource)
     */
    @Override
    public PermissionCollection getPermissions(CodeSource codesource) {
        PermissionCollection pc = parentPolicy.getPermissions(codesource);
        return pc;
    }

    /* (non-Javadoc)
     * @see java.security.Policy#getPermissions(java.security.ProtectionDomain)
     */
    @Override
    public PermissionCollection getPermissions(ProtectionDomain domain) {
        if (log.isDebugEnabled()) {
            log.debug("Get all permissions for domain : " + (domain==null?null:domain.getClass()));
        }
        try {
            PermissionCollection pc = parentPolicy.getPermissions(domain);

            Subject subject = taasService.findSubject();
            if (subject != null) {
                for (Principal principal : subject.getPrincipals()) {
                    if(principal instanceof TaasPrincipalWrapper) {
                        TaasPrincipalWrapper principalWrapper = (TaasPrincipalWrapper) principal;
                        PermissionCollection permissions = principalWrapper.getPermissions();

                        Enumeration<Permission> enumeration = permissions.elements();
                        while(enumeration.hasMoreElements()){
                            Permission permission = enumeration.nextElement();
                            pc.add(permission);
                        }
                    }
                }
            } else {
                log.error("Récupération des Permissions impossible");
            }
            return pc;
        } catch (Throwable eee) {
            log.error("Cant get permissions : ", eee);
        }
        return null;
    }

    /* (non-Javadoc)
     * @see java.security.Policy#refresh()
     */
    @Override
    public void refresh() {
        parentPolicy.refresh();
    }

    /* (non-Javadoc)
     * @see java.security.Policy#implies(java.security.ProtectionDomain,
     *      java.security.Permission)
     */
    @Override
    public boolean implies(ProtectionDomain domain, Permission permission) {
        PermissionCollection pc = getPermissions(domain);
        if (pc == null) {
            return false;
        }
        return pc.implies(permission);
    }

    /**
     * Installe cette TopiaPolicy. Si la Policy existante est déja cette
     * TopiaPolicy alors la méthode n'a pas d'effet. Si une autre Policy existe
     * deja alors cette TopiaPolicy, elle conserve l'ancienne Policy dans
     * parentPolicy et la remplace alors.
     */
    public void installPolicy() {
        Policy policy = Policy.getPolicy();
        if (equals(policy)) {
            return;
        }
        if (policy instanceof TaasPolicy) {
            if (log.isDebugEnabled()) {
                log.debug("Policy deja modifie en: " + policy);
            }
        } else {
            if (log.isDebugEnabled()) {
                log.debug("l'ancienne Policy etait: " + policy);
            }
            setParentPolicy(policy);
            Policy.setPolicy(this);
        }
    }

} // TopiaPolicy
