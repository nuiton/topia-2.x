/*
 * #%L
 * ToPIA :: Service Security
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/**
 * TopiaSecurityVetoableListener.java
 *
 * Created: 10 févr. 2006
 *
 * @author Arnaud Thimel &lt;thimel@codelutin.com&gt;
 * @version $Revision$
 *
 * Mise a jour: $Date$
 * par : sletellier
 */

package org.nuiton.topia.taas;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaContextFactory;
import org.nuiton.topia.TopiaException;
import org.nuiton.topia.TopiaNotFoundException;
import org.nuiton.topia.event.TopiaTransactionEvent;
import org.nuiton.topia.event.TopiaTransactionVetoable;
import org.nuiton.topia.framework.TopiaContextImplementor;
import org.nuiton.topia.framework.TopiaService;
import org.nuiton.topia.persistence.TopiaDAO;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaEntityAbstract;
import org.nuiton.topia.persistence.TopiaId;
import org.nuiton.topia.taas.entities.TaasAuthorizationImpl;
import org.nuiton.topia.taas.entities.TaasPrincipalImpl;
import org.nuiton.topia.taas.entities.TaasUserImpl;
import org.nuiton.topia.taas.event.TaasAccessEntity;
import org.nuiton.topia.taas.event.TaasEntityVetoable;
import org.nuiton.topia.taas.jaas.TaasConfiguration;
import org.nuiton.topia.taas.jaas.TaasLoginModule;
import org.nuiton.topia.taas.jaas.TaasPermission;
import org.nuiton.topia.taas.jaas.TaasPolicy;
import org.nuiton.topia.taas.jaas.TaasPrincipalWrapper;
import org.nuiton.topia.taas.jaas.TaasSubjectFinder;
import org.nuiton.topia.taas.jaas.TaasSubjectFinderImpl;

import javax.security.auth.Subject;
import javax.security.auth.login.Configuration;
import java.lang.reflect.Constructor;
import java.security.AccessController;
import java.security.Permission;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import static org.nuiton.topia.taas.TaasUtil.getPrincipalNames;

/**
 * Service pour la sécurité
 * 
 * Pour utiliser le service taas, il suffit de rajouter les lignes suivantes
 * dans le TopiaContext.properties :<p> topia.service.taas=org.nuiton.topia.taas.TaasService
 * topia.service.taas.event=org.nuiton.topia.taas.event.TaasEntityVetoable
 * topia.service.taas.subject=org.nuiton.topia.taas.jaas.TaasSubjectImpl
 *
 * @author julien
 */
public class TaasService implements TopiaService, TopiaTransactionVetoable {

    static private Log log = LogFactory.getLog(TaasService.class);

    public static final String SERVICE_NAME = "taas";

    public static final String SERVICE_LOGIN_MODULE =
            TaasLoginModule.class.getName();

    public static final String SERVICE_EVENT =
            "topia.service.taas.event";

    public static final String SERVICE_SUBJECT =
            "topia.service.taas.subject";
    public static final String TOPIA_SERVICE_TAAS = "topia.service.taas";

    private TaasPolicy policy = new TaasPolicy(this);

    private TopiaContextImplementor rootContext;

    private TopiaContext rootContextNoSecure;

    private TaasAccessEntity accessEntity;

    private TaasSubjectFinder subjectFinder;

    /** Contructeur par defaut */
    public TaasService() {
    }

    @Override
    public Class<?>[] getPersistenceClasses() {
        return getTaasPersistenceClasses();
    }

    public static Class<?>[] getTaasPersistenceClasses() {
        return new Class<?>[]{
                TaasUserImpl.class,
                TaasPrincipalImpl.class,
                TaasAuthorizationImpl.class,
        };
    }

    @Override
    public String getServiceName() {
        return SERVICE_NAME;
    }

    @Override
    public boolean preInit(TopiaContextImplementor context) {

        rootContext = context;
        try {
            Properties config =
                    (Properties)rootContext.getConfig().clone();

            // Recuperation du parametre pour l'evenemnt
            String eventString = config.getProperty(SERVICE_EVENT);
            if (eventString != null && !"".equals(eventString)) {
                Class<?> eventClass = Class.forName(eventString);
                Constructor<?> eventConstructor = eventClass.getConstructor(TaasService.class);
                accessEntity = (TaasAccessEntity) eventConstructor.newInstance(this);
            } else {
                accessEntity = new TaasEntityVetoable(this);
            }

            // Recuperation du parametre pour le subject
            String subjectString = config.getProperty(SERVICE_SUBJECT);
            if (subjectString != null && !"".equals(subjectString)) {
                Class<?> subjectClass = Class.forName(subjectString);
                subjectFinder = (TaasSubjectFinder) subjectClass.newInstance();
            } else {
                subjectFinder = new TaasSubjectFinderImpl();
            }

//            // Recupere un context root sans securite
//            Properties config = rootContext.getConfig();

            // Hack to not reload this service
            config.remove(TOPIA_SERVICE_TAAS);

            String persistences = config.getProperty("topia.persistence.classes");
            for (Class<?> klass : getPersistenceClasses()) {
                persistences += "," + klass.getName();
            }
            config.setProperty("topia.persistence.classes", persistences);

            rootContextNoSecure = TopiaContextFactory.getContext(config);

        } catch (Exception e) {
            throw new SecurityException("Init security error", e);
        }

        // Do it after accessEntity instantiation
        initSecurity(rootContext);
        
        return true;
    }

    @Override
    public void beginTransaction(TopiaTransactionEvent event) {
        TopiaContext context = event.getSource();
        if (subjectFinder != null) {
            initSecurity(context);
        }
    }

    /**
     * Initialisation des vetoables
     *
     * @param context context topia en cours
     */
    private void initSecurity(TopiaContext context) {
        List<Class<?>> entitiesClasses = rootContext.getPersistenceClasses();
        for (Class<?> clazz : entitiesClasses) {
            context.addTopiaEntityVetoable((Class) clazz, accessEntity);
        }

        Class<?>[] noLoadClasses = getPersistenceClasses();
        for (Class<?> clazz : noLoadClasses) {
            context.addTopiaEntityVetoable((Class) clazz, accessEntity);
        }

        context.addTopiaEntitiesVetoable(accessEntity);
        context.addTopiaTransactionVetoable(this);
    }

    @Override
    public boolean postInit(TopiaContextImplementor context) {
        policy.installPolicy();

        // Si pas de configuration autre que celle par defaut
        if (Configuration.getConfiguration() == null) {
            Configuration.setConfiguration(
                    new TaasConfiguration(SERVICE_NAME, this));
        }
        return true;
    }

    /**
     * Permet d'obtenir le context root
     *
     * @return context root
     */
    public TopiaContextImplementor getRootContext() {
        return rootContext;
    }

    /**
     * Permet de recuperer un context root sans securite
     *
     * @return context root non securise
     * @throws TopiaException
     */
    public TopiaContext getRootContextNoSecure()
            throws TopiaException {
        return rootContextNoSecure;
    }

    /**
     * Permet de récupérer le subject en cours
     * @return subject
     */
    public Subject findSubject() {
        Subject subject = subjectFinder.findSubject();

        if (log.isDebugEnabled()) {
            log.debug("findSubject : " +
                    subjectFinder + " value " +
                    subject);
        }

        return subject;
    }

    /**
     * Permet de verifier les authorizations sur une collection et de supprimer
     * les donnees non autorisées
     *
     * @param entities collection d'entites
     * @param actions  actions
     * @throws SecurityException en cas d'erreur de sécurité
     */
    public void check(Collection<? extends TopiaEntity> entities, int actions)
            throws SecurityException {
        Subject subj = findSubject();
        if (subj != null) {
            for (Iterator<? extends TopiaEntity> iterator =
                    entities.iterator(); iterator.hasNext();) {
                TopiaEntity entity = iterator.next();
                try {
                    TaasPermission myp = new TaasPermission(entity.getTopiaId(), actions);
                    checkPermission(subj, myp);
                } catch (SecurityException se) {
                    if (log.isDebugEnabled()) {
                        log.debug(getPrincipalNames(subj) +
                                " does not have permissions to load: " + entity, se);
                    }
                    iterator.remove();
                }
            }
        } else {
            throw new SecurityException("Use doAs() and login first");
        }
    }

    /**
     * Permet de vérifier les authorizations
     *
     * @param entity  entité
     * @param actions actions
     * @throws SecurityException en cas d'erreur de sécurité
     */
    public void check(TopiaEntity entity, int actions)
            throws SecurityException {
        check(entity.getTopiaId(), actions);
    }

    /**
     * Permet de vérifier les authorizations
     *
     * @param topiaId id de l'entite
     * @param actions actions
     * @throws SecurityException en cas d'erreur de sécurité
     */
    public void check(String topiaId, int actions) throws SecurityException {
        Subject subj = findSubject();
        if (subj != null) {
            try {
                TaasPermission myp = new TaasPermission(topiaId, actions);
                checkPermission(subj, myp);
            } catch (SecurityException se) {
                throw new SecurityException("Access denied to object \"" +
                        topiaId + "\" for \"" + getPrincipalNames(subj) + "\"", se);
            }
        } else {
            throw new SecurityException("Use doAs() and login first");
        }
    }

    /**
     * Hack pour faire fonctionner la security. Normalement cette methode devrait etre seulement
     * <pre>
     * AccessController.checkPermission(myp);
     * </pre>
     *
     * Mais comme ca ne fonctionne pas et pas vraiment de raison. Que le code au final a seulement besoin
     * de checker les TaasPermissions des principales du subject. Cette methode est plus simple et plus rapide
     * que le mode normal.
     *
     * @param subj
     * @param myp
     */
    protected void checkPermission(Subject subj, Permission myp) {

        // Code that note use realy jaas
        Set<TaasPrincipalWrapper> ps = subj.getPrincipals(TaasPrincipalWrapper.class);
        for (TaasPrincipalWrapper p : ps) {
            if (log.isDebugEnabled()) {
                log.debug("Check permissions for principal wrapper : " + p);
            }
            if (p.getPermissions().implies(myp)) {
                return;
            }
        }
        throw new SecurityException("Access denied to object " + myp);
        
        // Old code that use realy jaas
        //AccessController.checkPermission(myp);
    }

    /**
     * Permet de vérifier les authorizations
     *
     * @param entity  entité
     * @param actions actions
     * @throws SecurityException en cas d'erreur de sécurité
     */
    public void checkRequestPermission(TopiaEntity entity, int actions)
            throws SecurityException {
        Subject subj = findSubject();
        if (subj != null) {

            List<Permission> permissions = getRequestPermission(entity, actions);

            if (permissions == null) {
                try {
                    TaasPermission myp = new TaasPermission(entity.getTopiaId(), actions);
                    checkPermission(subj, myp);
                } catch (SecurityException se) {
                    throw new SecurityException("Access denied to object \"" +
                            entity.getTopiaId() + "\" for \"" +
                            getPrincipalNames(subj) + "\"", se);
                }
            } else {
                for (Permission permission : permissions) {
                    try {
                        checkPermission(subj, permission);
                        break;
                    } catch (SecurityException se) {
                        throw new SecurityException("Access denied to object \"" +
                                entity.getTopiaId() + "\" for \"" +
                                getPrincipalNames(subj) + "\"", se);
                    }
                }
            }
        } else {
            throw new SecurityException("Use doAs() and login first");
        }
    }

    /**
     * Permet de vérifier les authorizations sur une collection et de supprimer
     * les données non autorisées
     *
     * @param entities collection d'entités
     * @param actions  actions
     * @throws SecurityException en cas d'erreur de sécurité
     */
    public void checkRequestPermission(Collection<? extends TopiaEntity> entities,
                                       int actions) throws SecurityException {
        Subject subj = findSubject();
        if (subj != null) {

            for (Iterator<? extends TopiaEntity> iterator = entities.iterator(); iterator.hasNext();) {
                TopiaEntity entity = iterator.next();
                List<Permission> permissions = getRequestPermission(entity, actions);

                if (permissions == null) {
                    try {
                        AccessController.checkPermission(new TaasPermission(entity.getTopiaId(), actions));
                    } catch (SecurityException se) {
                        iterator.remove();
                    }
                } else {
                    for (Permission permission : permissions) {
                        try {
                            AccessController.checkPermission(permission);
                            break;
                        } catch (SecurityException se) {
                            iterator.remove();
                        }
                    }
                }
            }
        } else {
            throw new SecurityException("Use doAs() and login first");
        }
    }

    /**
     * Récupération des requests permissions dans les DAOs
     *
     * @param entity  entité
     * @param actions actions
     * @return permissions à vérifier
     */
    public List<Permission> getRequestPermission(TopiaEntity entity, int actions) {
        String topiaId = entity.getTopiaId();
        Class<? extends TopiaEntity> klass;

        try {
            klass = TopiaId.getClassName(topiaId);
        } catch (TopiaNotFoundException e) {
            throw new SecurityException("Invalid topiaId", e);
        }

        List<Permission> permissions;
        try {
            TopiaContextImplementor transaction = (TopiaContextImplementor)
                    ((TopiaEntityAbstract)entity).getTopiaContext();
            TopiaDAO<?> dao = transaction.getDAO(klass);
            permissions = dao.getRequestPermission(topiaId, actions);
        } catch (TopiaException e) {
            throw new SecurityException("Error in getRequestPermission for " + klass.getName(), e);
        }
        return permissions;
    }

} //TaasService
