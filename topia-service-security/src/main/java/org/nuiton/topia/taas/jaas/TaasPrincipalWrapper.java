/*
 * #%L
 * ToPIA :: Service Security
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/* *
* TopiaPrincipal.java
*
* Created: 15 févr. 2006
*
* @author Arnaud Thimel &lt;thimel@codelutin.com&gt;
* @version $Revision$
*
* Mise a jour: $Date$
* par : $Author$
*/

package org.nuiton.topia.taas.jaas;

import java.security.PermissionCollection;
import java.security.Permissions;
import java.security.Principal;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.taas.entities.TaasAuthorization;
import org.nuiton.topia.taas.entities.TaasPrincipal;

/**
 * Implantation des principals de JAAS.
 * @author ruchaud
 */
public class TaasPrincipalWrapper implements Principal {

    private Log log = LogFactory.getLog(TaasPrincipalWrapper.class);
    
    protected String name;

    protected PermissionCollection permissions;
    
    /**
     * Contructeur avec comme paramètre le nom du principal.
     * @param principal ?
     */
    public TaasPrincipalWrapper(TaasPrincipal principal) {
        name = principal.getName();
        permissions = new Permissions();
        for (TaasAuthorization authorization : principal.getAuthorizations()) {
            TaasPermission permission = new TaasPermission(authorization);
            permissions.add(permission);
        }
    }

    /* (non-Javadoc)
     * @see java.security.Principal#getName()
     */
    @Override
    public String getName() {
       return name;
    }

    /**
     * Récupération des permissions
     * @return permissions
     */
    public PermissionCollection getPermissions() {
        return permissions;
    }
    
    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
       return getClass().getName() + " : " + name;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object o) {
        if (!(o instanceof TaasPrincipalWrapper)) {
            return false;
        }
        if (name == null) {
            return ((Principal)o).getName() == null;
        }
        return name.equals(((TaasPrincipalWrapper)o).getName());
    }

} //TopiaPrincipal
