/*
 * #%L
 * ToPIA :: Service Security
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.topia.taas.event;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.event.TopiaEntitiesEvent;
import org.nuiton.topia.event.TopiaEntityEvent;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.taas.TaasService;
import org.nuiton.topia.taas.entities.TaasAuthorization;
import org.nuiton.topia.taas.entities.TaasPrincipal;
import org.nuiton.topia.taas.entities.TaasUser;

import java.util.List;

import static org.nuiton.topia.security.util.TopiaSecurityUtil.UPDATE;
import static org.nuiton.topia.taas.TaasUtil.CREATE;
import static org.nuiton.topia.taas.TaasUtil.DELETE;
import static org.nuiton.topia.taas.TaasUtil.LOAD;

public class TaasEntityVetoable implements TaasAccessEntity {

    private static Log log = LogFactory.getLog(TaasEntityVetoable.class);

    protected TaasService taasService;

    /** Contructeur par défaut */
    public TaasEntityVetoable(TaasService taasService) {
        this.taasService = taasService;
    }

    /* (non-Javadoc)
    * @see org.nuiton.topia.event.TopiaVetoableEntityListener#createEntity(org.nuiton.topia.event.TopiaVetoableEntityEvent)
    */

    public void create(TopiaEntityEvent event) {
        TopiaEntity entity = event.getEntity();
        String topiaId = entity.getTopiaId();
        Class<? extends TopiaEntity> clazz;

        if (log.isDebugEnabled()) {
            log.debug("[Security] create entity : " + topiaId);
        }
        taasService.check(entity, CREATE);
    }

    /* (non-Javadoc)
     * @see org.nuiton.topia.event.TopiaVetoableEntityListener#deleteEntity(org.nuiton.topia.event.TopiaVetoableEntityEvent)
     */

    public void delete(TopiaEntityEvent event) {
        TopiaEntity entity = event.getEntity();
        String topiaId = entity.getTopiaId();

        if (log.isDebugEnabled()) {
            log.debug("[Security] delete entity : " + topiaId);
        }
        taasService.check(entity, DELETE);
    }

    /*
     * (non-Javadoc)
     * @see org.nuiton.topia.event.TopiaVetoableEntityLoadListener#loadEntity(org.nuiton.topia.event.TopiaVetoableEntityLoadEvent)
     */

    public void load(TopiaEntityEvent event) {
//        TopiaEntity entity = event.getEntity();
//        String topiaId = entity.getTopiaId();
//
//        if (log.isDebugEnabled()) {
//            log.debug("[Security] load entity : " + topiaId);
//        }
//        taasService.check(entity, LOAD);
    }

    /* (non-Javadoc)
     * @see org.nuiton.topia.event.TopiaVetoableEntityListener#updateEntity(org.nuiton.topia.event.TopiaVetoableEntityEvent)
     */

    public void update(TopiaEntityEvent event) {
        TopiaEntity entity = event.getEntity();
        String topiaId = entity.getTopiaId();

        if (log.isDebugEnabled()) {
            log.debug("[Security] update entity : " + topiaId);
        }
        taasService.check(entity, UPDATE);
    }

    /*
    * (non-Javadoc)
    * @see org.nuiton.topia.event.TopiaEntitiesVetoable#load(org.nuiton.topia.event.TopiaEntitiesEvent)
    */

    public <E extends TopiaEntity> List<E> load(TopiaEntitiesEvent<E> event) {
        if (log.isDebugEnabled()) {
            log.debug("[Security] load entities");
        }

        List<E> entities = event.getEntities();

        if (!entities.isEmpty()) {
            E entity = entities.get(0);
            if (!(entity instanceof TaasUser ||
                    entity instanceof TaasPrincipal ||
                    entity instanceof TaasAuthorization)) {
                taasService.check(entities, LOAD);
            }
        }
        return entities;
    }

}
