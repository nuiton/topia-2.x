/*
 * #%L
 * ToPIA :: Service Security
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.topia.taas;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.Principal;
import java.util.HashSet;
import java.util.Set;
import java.util.StringTokenizer;

import javax.security.auth.Subject;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import sun.misc.BASE64Encoder;

/**
 * Classe utilitaire
 * 
 * @author ruchaud
 */
public class TaasUtil {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(TaasUtil.class);
    
    public static final int LOAD = 0x1;
    public static final String LOAD_TEXT = "LOAD";
    public static final int CREATE = 0x2;
    public static final String CREATE_TEXT = "CREATE";
    public static final int UPDATE = 0x4;
    public static final String UPDATE_TEXT = "UPDATE";
    public static final int DELETE = 0x8;
    public static final String DELETE_TEXT = "DELETE";

    /**
     * Applique un algorithme de hashage sur la chaine de caratere passee en
     * parametre
     * @param msg la chaine de caratere sur laquelle on veut operer le hashage
     * @return La chaine de caractere une fois l'algorithme applique
     */
    public static String hash(String msg) {
        return digestSHAHex(msg);
    }
    
    /**
     * Applique un algorithme de hashage sur la chaine de caratère passée en
     * paramètre
     * @param msg la chaine de caratère sur laquelle on veut opérer le hashage
     * @return La chaine de caractère une fois l'algorithme appliqué
     */
    public static String digestSHABase64(String msg) {
        if (msg == null) {
            return null;
        }
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA");
            byte[] bytes = msg.getBytes();
            bytes = digest.digest(bytes);
            BASE64Encoder encoder = new BASE64Encoder();
            String msgHashed = encoder.encode(bytes);
            return msgHashed;
        } catch (NoSuchAlgorithmException nsee) {
            return msg;
        }
    }
    
    /**
     * Fait le checksum SHA de la chaine de caractere le resultat est retourne
     * sous forme de chaine Hexadecimal.
     * @param ch la chaine a traiter
     * @return le checksum SHA en mode hexadecimal
     */
     static public String digestSHAHex(String ch){
         if(ch == null){
             return null;
         }
         try{
             MessageDigest md = MessageDigest.getInstance("SHA");
             md.update(ch.getBytes());
             byte[] digest = md.digest();

             StringBuffer result = new StringBuffer();
             for (int i=0; i < digest.length; i++) {
                 String hex = Integer.toHexString(0xFF & digest[i]);
                 if (hex.length() == 1) {
                     result.append("0" + hex);
                 } else {
                     result.append(hex);
                 }
             }

             return result.toString();
         }catch(NoSuchAlgorithmException eee){
             log.warn("Impossible de trouve l'algo SHA", eee);
             return ch;
         }
     }

    /**
     * Transforme actions en un entier.
     * @param actions -
     *            combinaison de mots cles "load" "update" "create" et "delete"
     *            separes par des virgules. Ex : "load,update"
     * @return 0 si aucune permission. Une combinaison des permissions
     */
    public static int actionsString2Int(String actions) {
        int result = 0x0;
        StringTokenizer tokens = new StringTokenizer(actions, ",");
        while (tokens.hasMoreTokens()) {
            String action = tokens.nextToken().trim();
            if (LOAD_TEXT.equalsIgnoreCase(action)) {
                result |= LOAD;
            } else if (CREATE_TEXT.equalsIgnoreCase(action)) {
                result |= CREATE;
            } else if (UPDATE_TEXT.equalsIgnoreCase(action)) {
                result |= UPDATE;
            } else if (DELETE_TEXT.equalsIgnoreCase(action)) {
                result |= DELETE;
            } else {
                throw new IllegalArgumentException("action not supported: "
                        + action);
            }
        }
        return result;
    }

    /**
     * Transforme actions en une chaîne de caractères
     * @param actions les actions sous forme d'un entier
     * @return La chaine des actions passé en paramètre
     */
    public static String actionsInt2String(int actions) {
        StringBuffer result = new StringBuffer();
        if ((actions & LOAD) == LOAD) {
            result.append(LOAD_TEXT);
            result.append(",");
        }
        if ((actions & CREATE) == CREATE) {
            result.append(CREATE_TEXT);
            result.append(",");
        }
        if ((actions & UPDATE) == UPDATE) {
            result.append(UPDATE_TEXT);
            result.append(",");
        }
        if ((actions & DELETE) == DELETE) {
            result.append(DELETE_TEXT);
            result.append(",");
        }

        if (result.length() > 0) {
            return result.substring(0, result.length() - 1);
        } else {
            return "";
        }
    }
 
    /**
     * Détermine si la classe implémente une interface
     * <p>
     * interface A &lt;---- class B &lt;---- class C
     * <p>
     * interface D &lt;---- class E
     * <p>
     * isImplement(C, A) = true
     * <p>
     * isImplement(E, A) = false
     * 
     * @param klass la classe
     * @param iface l'interface
     * @return vrai si la classe implémente l'interface sinon faux
     */
    public static boolean isImplement(Class<?> klass, Class<?> iface) {
        boolean result = false;
        
        Class<?>[] interfaces = klass.getInterfaces();
        result |= ArrayUtils.contains(interfaces, iface);
        
        Class<?> superclass = klass.getSuperclass();
        if(!result && superclass != null) {
            result |= isImplement(superclass, iface);
        }
        
        return result;
    }

    /**
     * Renvoie dans un Set les attribut 'name' des principals du Subject passe
     * en param
     * @param subj
     * @return les attribut 'name' des principals du Subject passe
     */
    public static Set<String> getPrincipalNames(Subject subj) {
        Set<String> result = new HashSet<String>();
        if (subj != null && subj.getPrincipals() != null) {
            for (Principal p : subj.getPrincipals()) {
                result.add(p.getName());
            }
        }
        return result;
    }

} //TopiaSecurityUtil
