/*
 * #%L
 * ToPIA :: Service Security
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/* *
 * TaasSubjectImpl.java
 *
 * Created: 10 févr. 2006
 *
 * @author Arnaud Thimel &lt;thimel@codelutin.com&gt;
 * @version $Revision$
 *
 * Mise a jour: $Date$
 * par : */

package org.nuiton.topia.taas.jaas;

import java.security.AccessControlContext;
import java.security.AccessController;

import javax.security.auth.Subject;

/**
 * Implémentation d'un récupération du subject
 * 
 * @author julien
 *
 */
public class TaasSubjectFinderImpl implements TaasSubjectFinder {

    @Override
    public Subject findSubject() {
        AccessControlContext context = AccessController.getContext();
        Subject subject = Subject.getSubject(context);
        return subject;
    }

}
