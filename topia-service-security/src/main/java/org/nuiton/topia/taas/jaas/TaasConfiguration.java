/*
 * #%L
 * ToPIA :: Service Security
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/* *
 * TopiaConfiguration.java
 *
 * Created: 20 févr. 2006
 *
 * @author Arnaud Thimel &lt;thimel@codelutin.com&gt;
 * @version $Revision$
 *
 * Mise a jour: $Date$
 * par : */

package org.nuiton.topia.taas.jaas;

import java.util.HashMap;
import java.util.Map;

import javax.security.auth.login.AppConfigurationEntry;
import javax.security.auth.login.Configuration;

import org.nuiton.topia.taas.TaasService;

/**
 * Classe permettant de passer des paramètres entre le LoginModule et le
 * CallbackHandler. Ici on passe le SecurityManager.
 * @author ruchaud
 */
public class TaasConfiguration extends Configuration {

    private Map<String, AppConfigurationEntry[]> appConfEntries;

    /**
     * Créé une instance de TopiConfiguration avec un identifiant de
     * configurationEntry et le nom du fichier de propriétés associé.
     * 
     * @param name
     *            le nom de la configurationEntry
     * @param taasService le service
     */
    public TaasConfiguration(String name, TaasService taasService) {
        appConfEntries = new HashMap<String, AppConfigurationEntry[]>();
        addEntry(name, taasService);
    }

    /**
     * Ajoute une ConfigurationEntry avec le nom de fichier de propriétés
     * associé
     * 
     * @param name
     *            le nom de la configurationEntry
     * @param taasService
     *            le nom du fichier de propriétés
     */
    private void addEntry(String name, TaasService taasService) {
        AppConfigurationEntry[] confEntries = getAppConfigurationEntry(name);
        if (confEntries != null) {
            int i = 0;
            for (; i < confEntries.length; i++) {
                if (TaasService.SERVICE_LOGIN_MODULE.equals(confEntries[i].getLoginModuleName())) {
                    break;
                }
            }
            if (i == confEntries.length) {
                AppConfigurationEntry[] tmpConfEntries = confEntries;
                confEntries = new AppConfigurationEntry[confEntries.length + 1];
                for (int j = 0; j < confEntries.length; j++) {
                    confEntries[j] = tmpConfEntries[j];
                }
                confEntries[confEntries.length - 1] = createEntry(taasService);
            } else {
                if ( /* Mauvais FLAG */
                !AppConfigurationEntry.LoginModuleControlFlag.REQUIRED
                        .equals(confEntries[i].getControlFlag())
                        /* Ne contient pas la propriété */
                        || !confEntries[i].getOptions().containsKey(
                                TaasService.SERVICE_NAME)
                        /* Propriété mal initialisée */
                        || !confEntries[i].getOptions()
                                .get(TaasService.SERVICE_NAME).equals(taasService)) {
                    confEntries[i] = createEntry(taasService, confEntries[i].getOptions());
                }
            }
        } else {
            confEntries = new AppConfigurationEntry[1];
            confEntries[0] = createEntry(taasService);
        }
        appConfEntries.put(name, confEntries);
    }

    /**
     * Créé une entry avec des options vides
     * 
     * @param taasService
     *            le SecurityHelper
     * @return l'entry créée
     */
    private AppConfigurationEntry createEntry(TaasService taasService) {
        return createEntry(taasService, null);
    }

    /**
     * Créé une entry en rajoutant les options nécessaires à l'attribut options
     * 
     * @param taasService
     *            le nom du fichier de propriétés
     * @param options
     *            l'objet contenant les options précédentes
     * @return l'entry créée
     */
    private AppConfigurationEntry createEntry(TaasService taasService, Map options) {
        if (options == null) {
            options = new HashMap<String, Object>();
        }
        options.put(TaasService.SERVICE_NAME, taasService);
        return new AppConfigurationEntry(TaasService.SERVICE_LOGIN_MODULE,
                AppConfigurationEntry.LoginModuleControlFlag.REQUIRED, options);
    }

    /**
     * Renvoie les entries associéess à l'attribut name
     * 
     * @param name
     *            l'identifiant des entries demandées
     * @return un tableau cotenant les entries demandées
     */
    @Override
    public AppConfigurationEntry[] getAppConfigurationEntry(String name) {
        return appConfEntries.get(name);
    }

    /* (non-Javadoc)
     * @see javax.security.auth.login.Configuration#refresh()
     */
    @Override
    public void refresh() {
    }

} // TopiaConfiguration
