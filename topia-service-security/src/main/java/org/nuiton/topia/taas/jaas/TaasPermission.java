/*
 * #%L
 * ToPIA :: Service Security
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.topia.taas.jaas;

import static org.nuiton.topia.taas.TaasUtil.CREATE;
import static org.nuiton.topia.taas.TaasUtil.DELETE;
import static org.nuiton.topia.taas.TaasUtil.LOAD;
import static org.nuiton.topia.taas.TaasUtil.UPDATE;

import java.security.Permission;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.taas.entities.TaasAuthorization;

/**
 * Classe permettant d'encapsuler les autorisations et de déléguer le travail aux
 * autorisations.
 * @author ruchaud
 */
public class TaasPermission extends Permission {

    private static final long serialVersionUID = 1L;
    
    private static Log log = LogFactory.getLog(TaasPermission.class);

    public String authorizationExpression;

    public int authorizationActions;

    /**
     * Contructeur à partir des valeurs
     * @param expression expression
     * @param actions actions
     */
    public TaasPermission(String expression, int actions) {
        super(expression);
        authorizationExpression = expression;
        authorizationActions = actions;
    }

    /**
     * Constructeur à partir d'une autorisation
     * @param authorization autorisation
     */
    public TaasPermission(TaasAuthorization authorization) {
        this(authorization.getExpression(), authorization.getActions());
    }

    /*
     * (non-Javadoc)
     * @see java.security.Permission#implies(java.security.Permission)
     */
    @Override
    public boolean implies(Permission permission) {
        if (permission == null) {
            return false;
        }
        if (!(permission instanceof TaasPermission)) {
            return false;
        }
        TaasPermission other = (TaasPermission)permission;
        boolean isImplies = impliesExpression(authorizationExpression, other.getAuthorizationExpression()) &&
                impliesActions(authorizationActions, other.getAuthorizationActions());

        log.debug("Implies " + permission + " with other " + other + " implies : " + isImplies);
        return isImplies;
    }

    /*
     * (non-Javadoc)
     * @see java.security.Permission#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof TaasPermission)) {
            return false;
        }
        TaasPermission that = (TaasPermission)obj;
        return implies(that) && that.implies(this);
    }

    /*
     * (non-Javadoc)
     * @see java.security.Permission#hashCode()
     */
    @Override
    public int hashCode() {
        return authorizationExpression.hashCode() * 100 + authorizationActions;
    }

    /*
     * (non-Javadoc)
     * @see java.security.Permission#getActions()
     */
    @Override
    public String getActions() {
        return String.valueOf(authorizationActions);
    }

    /**
     * Retourne les actions de l'authorization
     * @return actions
     */
    public int getAuthorizationActions() {
        return authorizationActions;
    }

    /**
     * Retourne l'expression de l'authorization
     * @return expression
     */
    public String getAuthorizationExpression() {
        return authorizationExpression;
    }
    
    /**
     * Comparare deux identifiants entres eux.
     * thisId =&gt; thatId = ?
     * @param thisExpression un identifiant 
     * @param thatExpression un autre identifiant
     * @return vrai si thisId implique thatId
     */
    public boolean impliesExpression(String thisExpression, String thatExpression) {
        boolean result = thisExpression.equals(thatExpression) ||
                "*".equals(thisExpression) ||
                thatExpression.startsWith(thisExpression.substring(0, thisExpression.length() - 1))
                        && thisExpression.endsWith("*");

        if (log.isDebugEnabled()) {
            log.debug("Implies expression : " + thisExpression +
                    " with " + thatExpression +
                    " return " + result);
        }

        return result;
    }

    /**
     * Compare deux actions entre elles.
     * thisActions =&gt; thatActions = ?
     * @param thisActions une action
     * @param thatActions une autre action
     * @return vrai si thisActions implique thatActions
     */
    public boolean impliesActions(int thisActions, int thatActions) {
        boolean result = true;
        if ((thatActions & LOAD) == LOAD) {
            result &= (thisActions & LOAD) == LOAD;
        }
        if ((thatActions & CREATE) == CREATE) {
            result &= (thisActions & CREATE) == CREATE;
        }
        if ((thatActions & UPDATE) == UPDATE) {
            result &= (thisActions & UPDATE) == UPDATE;
        }
        if ((thatActions & DELETE) == DELETE) {
            result &= (thisActions & DELETE) == DELETE;
        }
        return result;
    }
}
