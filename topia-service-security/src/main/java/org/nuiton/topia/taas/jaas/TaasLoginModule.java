/*
 * #%L
 * ToPIA :: Service Security
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/* *
* TopiaLoginModule.java
*
* Created: 15 févr. 2006
*
* @author Arnaud Thimel &lt;thimel@codelutin.com&gt;
* @version $Revision$
*
* Mise a jour: $Date$
* par : $Author$
*/

package org.nuiton.topia.taas.jaas;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.security.auth.Subject;
import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.NameCallback;
import javax.security.auth.callback.PasswordCallback;
import javax.security.auth.login.LoginException;
import javax.security.auth.spi.LoginModule;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.TopiaException;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaSecurityDAOHelper;
import org.nuiton.topia.taas.TaasService;
import org.nuiton.topia.taas.TaasUtil;
import org.nuiton.topia.taas.entities.TaasPrincipal;
import org.nuiton.topia.taas.entities.TaasUser;
import org.nuiton.topia.taas.entities.TaasUserDAO;

/**
 * LoginModule permettant l'authentification d'un utilisateur au près du système.
 * @author ruchaud
 */
public class TaasLoginModule implements LoginModule {

    private Log log = LogFactory.getLog(TaasLoginModule.class);

    protected Subject subject;
    protected CallbackHandler callbackHandler;
    protected Set<TaasPrincipalWrapper> principals;
    protected TaasUser privateCredential;
    protected TaasService taasService;

    /* (non-Javadoc)
     * @see javax.security.auth.spi.LoginModule#initialize(
     *   javax.security.auth.Subject, 
     *   javax.security.auth.callback.CallbackHandler, 
     *   java.util.Map, 
     *   java.util.Map)
     */
    @Override
    public void initialize(Subject subject, CallbackHandler callbackHandler,
            Map<String,?> sharedState, Map<String,?> options) {

        this.subject = subject;
        this.callbackHandler = callbackHandler;
        principals = null;
        privateCredential = null;
        taasService = (TaasService)options.get(TaasService.SERVICE_NAME);
    }

    /* (non-Javadoc)
    * @see javax.security.auth.spi.LoginModule#login()
    */
    @Override
    public boolean login() throws LoginException {
        try {
            if (callbackHandler == null) {
                throw new LoginException("CallbackHandler cannot be null");
            }
            if (taasService == null) {
                throw new LoginException("TaasService property must be set");
            }

            String login, password;

            NameCallback nc = new NameCallback("login");
            PasswordCallback pc = new PasswordCallback("password", false);

            Callback[] callbacks = new Callback[2];

            callbacks[0] = nc;
            callbacks[1] = pc;

            try {
                //Récupération du login et mot de passe
                callbackHandler.handle(callbacks);
            } catch (Exception eee) {
                if (log.isWarnEnabled()) {
                    log.warn("Utilisation du CallbackHandler impossible", eee);
                }
                LoginException le = new LoginException(
                        "Utilisation du CallbackHandler impossible");
                le.initCause(eee);
                throw le;
            }
            login = nc.getName();
            password = new String(pc.getPassword());
            pc.clearPassword();

            String hashed = TaasUtil.hash(password);

            //Vérification du login/pass et récupération des Principals
            TopiaContext transaction = null;
            try {
                TopiaContext rootContext = taasService.getRootContextNoSecure();
                transaction = rootContext.beginTransaction();

                TaasUserDAO userDAO = TopiaSecurityDAOHelper.getTaasUserDAO(transaction);
                TaasUser user = userDAO.findByLogin(login);

                if(user != null && user.getPassword().equals(hashed) && user.isEnabled()) {
                    privateCredential = user;

                    // Update connection information
                    user.setLastConnectionDate(new Date());
                    int numberOfConnection = user.getNumberOfConnection();
                    user.setNumberOfConnection(numberOfConnection + 1);
                    userDAO.update(user);

                    // Récupération des principals
                    principals = new HashSet<TaasPrincipalWrapper>();

                    Collection<TaasPrincipal> taasPrincipals = user.getPrincipals();
                    for (TaasPrincipal taasPrincipal : taasPrincipals) {
                        principals.add(new TaasPrincipalWrapper(taasPrincipal));
                    }
                } else {
                    // Echec d'authentification
                    principals = null;
                    privateCredential = null;
                    throw new LoginException("Erreur lors de l'authentification " + login);
                }
            } catch (Exception e) {
                LoginException le = new LoginException("Erreur lors de l'authentification" + login);
                le.initCause(e);
                log.error("Erreur lors de l'authentification", le);
                throw le;
            } finally {
                if (transaction != null) {
                    try {
                        transaction.commitTransaction();
                        transaction.closeContext();
                    } catch (TopiaException e) {
                        LoginException le = new LoginException("Erreur lors de l'authentification" + login);
                        le.initCause(e);
                        log.error("Erreur lors de l'authentification", le);
                        throw le;
                    }
                }
            }
        } catch(LoginException eee) {
            log.error("LoginException : ", eee);
        } catch(Throwable eee){
            log.error("Exception : ", eee);
            throw new LoginException(eee.getMessage());
        }
        return true;
    }

    /* (non-Javadoc)
    * @see javax.security.auth.spi.LoginModule#commit()
    */
    @Override
    public boolean commit() throws LoginException {
        try {
            subject.getPrincipals().addAll(principals);
            if (log.isDebugEnabled()) {
                for (TaasPrincipalWrapper principal : principals) {
                    log.debug("Permissions for principal " +
                            principal.getName() + " : " +
                            principal.getPermissions());
                }
            }
            subject.getPrivateCredentials().add(privateCredential);
            if (log.isDebugEnabled()) {
                log.debug("Private credential size : " +
                        subject.getPrivateCredentials().size() +
                        " for subject : " + subject);
            }
        } catch (Exception eee) {
            log.error("Cant commit : ", eee);
            throw new LoginException(eee.getMessage());
        }
        return true;
    }

    /* (non-Javadoc)
    * @see javax.security.auth.spi.LoginModule#abort()
    */
    @Override
    public boolean abort() throws LoginException {
        //On effectue les mêmes actions que logout
        return logout();
    }

    /* (non-Javadoc)
    * @see javax.security.auth.spi.LoginModule#logout()
    */
    @Override
    public boolean logout() throws LoginException {
        //On libère les ressources 
        subject.getPrincipals().removeAll(principals);
        subject = null;
        principals.clear();
        principals = null;
        callbackHandler = null;
        return true;
    }
} //TopiaLoginModule
