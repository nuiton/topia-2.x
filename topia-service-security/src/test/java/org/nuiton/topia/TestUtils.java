/*
 * #%L
 * ToPIA :: Service Security
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.topia;

import org.apache.commons.io.FileUtils;
import org.nuiton.util.FileUtil;

import java.io.File;
import java.io.IOException;

/**
 * Une classe pour avoir des choses utiles pour tous les tests.
 *
 * @author Chatellier Eric
 *         
 *         Last update : $Date$
 * @deprecated since 2.5, everything usefull already exists in the class {@link TestHelper} from {@code topia-persistence module}.
 */
@Deprecated
public abstract class TestUtils {

    protected static File basedir;

    protected static File targetdir;

    protected static File dirDatabase;

    public static File getBasedir() {
        if (basedir == null) {
            String base = System.getProperty("basedir");
            if (base == null || base.isEmpty()) {
                base = new File("").getAbsolutePath();
            }
            basedir = new File(base);
            System.out.println("basedir for test " + basedir);
        }
        return basedir;
    }

    public static File getTargetdir() {
        if (targetdir == null) {
            targetdir = new File(getBasedir(), "target");
            System.out.println("targetdir for test " + targetdir);
        }
        return targetdir;
    }

    /**
     * Create a temp dir and init isis with that temp dir as database.
     *
     * @throws Exception
     */
    public static void init() throws Exception {

        File mavenTestDir = new File(getTargetdir() + File.separator + "surefire-workdir");
        dirDatabase = FileUtil.createTempDirectory("topia-test", "", mavenTestDir);
    }

    public static File getDirDatabase() {
        return dirDatabase;
    }

    /** Delete created temp directory. */
    public static void clean() throws IOException {
        if (dirDatabase != null) {
            FileUtils.deleteDirectory(dirDatabase);
            dirDatabase = null;
        }
    }
}
