/*
 * #%L
 * ToPIA :: Service Security
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.topia.security;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;
import org.nuiton.i18n.I18n;
import org.nuiton.topia.TestHelper;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaContextFactory;
import org.nuiton.topia.TopiaException;
import org.nuiton.topia.TopiaSecurityDAOHelper;
import org.nuiton.topia.TopiaTestDAOHelper;
import org.nuiton.topia.security.entities.authorization.TopiaAssociationAuthorization;
import org.nuiton.topia.security.entities.authorization.TopiaAssociationAuthorizationDAO;
import org.nuiton.topia.security.entities.authorization.TopiaEntityAuthorization;
import org.nuiton.topia.security.entities.authorization.TopiaEntityAuthorizationDAO;
import org.nuiton.topia.security.entities.authorization.TopiaExpressionLink;
import org.nuiton.topia.security.entities.authorization.TopiaExpressionLinkDAO;
import org.nuiton.topia.security.entities.user.TopiaGroup;
import org.nuiton.topia.security.entities.user.TopiaGroupDAO;
import org.nuiton.topia.security.entities.user.TopiaUser;
import org.nuiton.topia.security.entities.user.TopiaUserDAO;
import org.nuiton.topia.security.jaas.TopiaCallbackHandler;
import org.nuiton.topia.security.util.TopiaSecurityFactoryFilter;
import org.nuiton.topia.test.entities.Person;
import org.nuiton.topia.test.entities.PersonDAO;
import org.nuiton.topia.test.entities.PersonImpl;
import org.nuiton.topia.test.entities.Pet;
import org.nuiton.topia.test.entities.PetDAO;
import org.nuiton.topia.test.entities.PetImpl;
import org.nuiton.topia.test.entities.RaceImpl;

import javax.security.auth.Subject;
import javax.security.auth.login.LoginContext;
import java.io.File;
import java.security.PrivilegedExceptionAction;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

import static org.nuiton.topia.security.util.TopiaSecurityUtil.LOAD;
import static org.nuiton.topia.security.util.TopiaSecurityUtil.UPDATE;

/**
 * Tests unitaires. Ne pas oublier de lancer le script d'initialisation de la
 * base données.
 *
 * @author ruchaud
 */
public class TopiaSecurityTest {

    private static final Log log = LogFactory.getLog(TopiaSecurityTest.class);

   protected String dbPath;

    protected TopiaContext context;

    protected TopiaSecurityService securityManager;

    protected TopiaSecurityFactoryFilter factoryFilter;
    // FIXME comment il trouve les autres tout seul ?
    // The grande question !!!


    protected static File tesDir;

    protected static boolean init;

    protected static String entitiesList =
            PersonImpl.class.getName() + "," +
            PetImpl.class.getName() + "," +
            RaceImpl.class.getName();

    @BeforeClass
    public static void init() throws Exception {
        I18n.init(null, Locale.FRANCE);
        tesDir = TestHelper.getTestBasedir(TopiaSecurityTest.class);

    }

    @AfterClass
    public static void clean() {
        // tchemit 2010-11-28 : no never delete data after a test...
//        TestUtils.clean();
    }

    @Ignore
    protected Properties getProperties() {
        Properties config = new Properties();
//        Properties config = TestHelper.initTopiaContextConfiguration(tesDir,"topia-security");

        if (log.isDebugEnabled()) {
            config.setProperty("hibernate.show_sql", "true");
        }

        config.setProperty("topia.persistence.classes", entitiesList);

        config.setProperty("hibernate.dialect", "org.hibernate.dialect.H2Dialect");
        config.setProperty("hibernate.connection.username", "sa");
        config.setProperty("hibernate.connection.password", "");
        config.setProperty("hibernate.connection.driver_class", "org.h2.Driver");

//        config.setProperty("hibernate.connection.url", "jdbc:h2:" + new File(tesDir, "topia-security"));
        config.setProperty("hibernate.connection.url", "jdbc:h2:" + dbPath);

        // add this to use security service
        config.setProperty("topia.service.security", "org.nuiton.topia.security.TopiaSecurityServiceImpl");
        return config;
    }

    @Ignore
    public void initDatabase() throws TopiaException {

        Properties config = getProperties();
        config.setProperty("hibernate.hbm2ddl.auto", "create");

        /* Transaction */
        TopiaContext rootContext = TopiaContextFactory.getContext(config);
        TopiaContext childContext = rootContext.beginTransaction();

        /* DAOs */
        PersonDAO personDAO = TopiaTestDAOHelper.getPersonDAO(childContext);
        PetDAO petDAO = TopiaTestDAOHelper.getPetDAO(childContext);

        TopiaUserDAO topiaUserDAO = TopiaSecurityDAOHelper.getTopiaUserDAO(childContext);
        TopiaGroupDAO topiaGroupDAO = TopiaSecurityDAOHelper.getTopiaGroupDAO(childContext);
        TopiaEntityAuthorizationDAO topiaEntityAuthorizationDAO = TopiaSecurityDAOHelper.getTopiaEntityAuthorizationDAO(childContext);
        TopiaExpressionLinkDAO linkDAO = TopiaSecurityDAOHelper.getTopiaExpressionLinkDAO(childContext);
        TopiaAssociationAuthorizationDAO topiaAssociationAuthorizationDAO = TopiaSecurityDAOHelper.getTopiaAssociationAuthorizationDAO(childContext);

        /* Création d'un admin */
        TopiaUser admin = topiaUserDAO.create();
        admin.setLogin("admin");
        admin.setPassword("azerty");
        topiaUserDAO.update(admin);
        childContext.commitTransaction();

        /* Création d'un utilisateur */
        TopiaUser thimel = topiaUserDAO.create();
        thimel.setLogin("thimel");
        thimel.setPassword("zou;bi@da");
        topiaUserDAO.update(thimel);
        childContext.commitTransaction();

        /* Création d'un groupe avec un utilisateur */
        TopiaUser ruchaud = topiaUserDAO.create();
        ruchaud.setLogin("ruchaud");
        ruchaud.setPassword("mdp");
        TopiaGroup groupRuchaud = topiaGroupDAO.create();
        groupRuchaud.setName("essai");

        groupRuchaud.setTopiaUser(new ArrayList<TopiaUser>());
        ruchaud.addTopiaGroup(groupRuchaud);

        topiaGroupDAO.update(groupRuchaud);
        topiaUserDAO.update(ruchaud);
        childContext.commitTransaction();

        /* Création des personnes */
        Person benjamin = personDAO.create();
        benjamin.setName("poussin");
        benjamin.setFirstname("benjamin");
        personDAO.update(benjamin);
        childContext.commitTransaction();

        Person jacques = personDAO.create();
        jacques.setName("poussin");
        jacques.setFirstname("jacques");
        personDAO.update(jacques);
        childContext.commitTransaction();

        Person mylene = personDAO.create();
        mylene.setName("poussin");
        mylene.setFirstname("mylene");
        personDAO.update(mylene);
        childContext.commitTransaction();

        /* Création des annimaux */
        Pet debux = petDAO.create();
        debux.setName("debux");
        debux.setType("chat");
        debux.setPerson(jacques);
        petDAO.update(debux);
        childContext.commitTransaction();

        Pet pluto = petDAO.create();
        pluto.setName("pluto");
        pluto.setType("chien");
        pluto.setPerson(jacques);
        petDAO.update(pluto);
        childContext.commitTransaction();

        Pet fliper = petDAO.create();
        fliper.setName("fliper");
        fliper.setType("dauphin");
        fliper.setPerson(mylene);
        petDAO.update(fliper);
        childContext.commitTransaction();

        /* Création des autorisations Entity */
        TopiaEntityAuthorization authorizationForAdmin = topiaEntityAuthorizationDAO.create();
        authorizationForAdmin.setExpression("*");
        authorizationForAdmin.setActions(15);
        authorizationForAdmin.setPrincipals(admin.getTopiaId());
        topiaEntityAuthorizationDAO.update(authorizationForAdmin);
        childContext.commitTransaction();

        TopiaEntityAuthorization authorizationForRuchaud = topiaEntityAuthorizationDAO.create();
        authorizationForRuchaud.setExpression(Person.class.getName() + "#*");
        authorizationForRuchaud.setActions(LOAD);
        authorizationForRuchaud.setPrincipals(groupRuchaud.getTopiaId());
        topiaEntityAuthorizationDAO.update(authorizationForRuchaud);
        childContext.commitTransaction();

        TopiaEntityAuthorization authorizationForThimel = topiaEntityAuthorizationDAO.create();
        authorizationForThimel.setExpression(jacques.getTopiaId());
        authorizationForThimel.setActions(LOAD);
        authorizationForThimel.setPrincipals(thimel.getTopiaId());
        topiaEntityAuthorizationDAO.update(authorizationForThimel);
        childContext.commitTransaction();

        /* Création d'une autorisation Link */
        TopiaExpressionLink link = linkDAO.create();
        link.setReplace(mylene.getTopiaId());
        link.setBy(jacques.getTopiaId());
        linkDAO.update(link);
        childContext.commitTransaction();

        /* Création d'une authorisation association */
        TopiaAssociationAuthorization associationAuthorization = topiaAssociationAuthorizationDAO.create();
        associationAuthorization.setIdBeginAssociation(jacques.getTopiaId());
        associationAuthorization.setNameAssociation("pet");
        associationAuthorization.setActions(LOAD);
        associationAuthorization.setPrincipals(ruchaud.getTopiaId());
        topiaAssociationAuthorizationDAO.update(associationAuthorization);
        childContext.commitTransaction();

        associationAuthorization = topiaAssociationAuthorizationDAO.create();
        associationAuthorization.setIdBeginAssociation(mylene.getTopiaId());
        associationAuthorization.setNameAssociation("pet");
        associationAuthorization.setActions(UPDATE);
        associationAuthorization.setPrincipals(ruchaud.getTopiaId());
        topiaAssociationAuthorizationDAO.update(associationAuthorization);
        childContext.commitTransaction();

        childContext.closeContext();
    }

    @Before
    public void setUp() throws TopiaException {
        dbPath = new File(tesDir, "topia-security").getAbsolutePath();
        if (!init) {

            initDatabase();
            init = true;
        }
        context = TopiaContextFactory.getContext(getProperties());
        securityManager = context.getService(TopiaSecurityService.class);
        factoryFilter = new TopiaSecurityFactoryFilter(securityManager);
    }

    @After
    public void tearDown() throws TopiaException {
        if (context != null) {
            context.closeContext();
        }
    }
    @Test
    public void testLoginThimel() throws Exception {
        /* Authentification de l'utilisateur Thimel */
        LoginContext loginContext = new LoginContext("topia", new TopiaCallbackHandler(
                "thimel", "zou;bi@da"));
        loginContext.login();
        Subject subject = loginContext.getSubject();

        /* Test */
        Assert.assertEquals(subject.getPrincipals().size(), 1);

        loginContext.logout();
    }

    @Test
    public void testLoginRuchaud() throws Exception {
        /* Authentification de l'utilisateur Ruchaud */
        LoginContext loginContext = new LoginContext("topia", new TopiaCallbackHandler(
                "ruchaud", "mdp"));
        loginContext.login();
        Subject subject = loginContext.getSubject();

        /* Test */
        Assert.assertEquals(subject.getPrincipals().size(), 2);

        loginContext.logout();
    }

    @Test
    public void testAuthorizationThimel() throws Exception {
        /* Authentification de l'utilisateur Thimel */
        LoginContext loginContext = new LoginContext("topia", new TopiaCallbackHandler(
                "thimel", "zou;bi@da"));
        loginContext.login();
        Subject subject = loginContext.getSubject();

        /* Tests */
        Subject.doAsPrivileged(subject, new PrivilegedExceptionAction<Object>() {

            @Override
            public Object run() throws Exception {
                TopiaContext childContext = context.beginTransaction();

                /* Personnes */
                PersonDAO personDAO = TopiaTestDAOHelper.getPersonDAO(childContext);
                List<Person> findAllPerson = personDAO.findAll();

                List<Person> personsLOAD = factoryFilter.filter(findAllPerson, LOAD);
                Assert.assertEquals(2, personsLOAD.size());

                List<Person> personsUPDATE = factoryFilter.filter(findAllPerson, UPDATE);
                Assert.assertEquals(0, personsUPDATE.size());

                /* Annimaux */
                PetDAO petDAO = TopiaTestDAOHelper.getPetDAO(childContext);
                List<Pet> findAllPet = petDAO.findAll();

                List<Pet> petLOAD = factoryFilter.filter(findAllPet, LOAD);
                Assert.assertEquals(0, petLOAD.size());

                List<Pet> petUPDATE = factoryFilter.filter(findAllPet, UPDATE);
                Assert.assertEquals(0, petUPDATE.size());
                return null;
            }
        }, null);

        loginContext.logout();
    }

    @Test
    public void testAuthorizationRuchaud() throws Exception {
        /* Authentification de l'utilisateur Admin */
        LoginContext loginContext = new LoginContext("topia", new TopiaCallbackHandler(
                "ruchaud", "mdp"));
        loginContext.login();
        Subject subject = loginContext.getSubject();

        /* Tests */
        Subject.doAsPrivileged(subject, new PrivilegedExceptionAction<Object>() {

            @Override
            public Object run() throws Exception {
                TopiaContext childContext = context.beginTransaction();

                /* Personnes */
                PersonDAO personDAO = TopiaTestDAOHelper.getPersonDAO(childContext);
                List<Person> findAllPerson = personDAO.findAll();

                List<Person> personsLOAD = factoryFilter.filter(findAllPerson, LOAD);
                Assert.assertEquals(3, personsLOAD.size());
//                Assert.assertEquals(12, personsLOAD.size());

                List<Person> personsUPDATE = factoryFilter.filter(findAllPerson, UPDATE);
                Assert.assertEquals(0, personsUPDATE.size());

                /* Annimaux */
                PetDAO petDAO = TopiaTestDAOHelper.getPetDAO(childContext);
                List<Pet> findAllPet = petDAO.findAll();

                List<Pet> petLOAD = factoryFilter.filter(findAllPet, LOAD);
                Assert.assertEquals(2, petLOAD.size());

                List<Pet> petUPDATE = factoryFilter.filter(findAllPet, UPDATE);
                Assert.assertEquals(1, petUPDATE.size());
                return null;
            }
        }, null);

        loginContext.logout();
    }

    @Test
    public void testAuthorizationAdmin() throws Exception {
        /* Authentification de l'utilisateur Ruchaud */
        LoginContext loginContext = new LoginContext("topia", new TopiaCallbackHandler(
                "admin", "azerty"));
        loginContext.login();
        Subject subject = loginContext.getSubject();

        /* Tests */
        Subject.doAsPrivileged(subject, new PrivilegedExceptionAction<Object>() {

            @Override
            public Object run() throws Exception {
                TopiaContext childContext = context.beginTransaction();

                /* Personnes */
                PersonDAO personDAO = TopiaTestDAOHelper.getPersonDAO(childContext);
                List<Person> findAllPerson = personDAO.findAll();

                List<Person> personsLOAD = factoryFilter.filter(findAllPerson, LOAD);
                Assert.assertEquals(3, personsLOAD.size());
//                Assert.assertEquals(15, personsLOAD.size());

                List<Person> personsUPDATE = factoryFilter.filter(findAllPerson, UPDATE);
                Assert.assertEquals(3, personsUPDATE.size());
//                Assert.assertEquals(15, personsUPDATE.size());

                /* Annimaux */
                PetDAO petDAO = TopiaTestDAOHelper.getPetDAO(childContext);
                List<Pet> findAllPet = petDAO.findAll();

                List<Pet> petLOAD = factoryFilter.filter(findAllPet, LOAD);
                Assert.assertEquals(3, petLOAD.size());
//                Assert.assertEquals(15, petLOAD.size());

                List<Pet> petUPDATE = factoryFilter.filter(findAllPet, UPDATE);
                Assert.assertEquals(3, petUPDATE.size());
//                Assert.assertEquals(15, petUPDATE.size());
                return null;
            }
        }, null);

        loginContext.logout();
    }
}
