/*
 * #%L
 * ToPIA :: Service Migration
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2015 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.topia.migration;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.service.ServiceRegistry;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaException;
import org.nuiton.topia.TopiaNotFoundException;
import org.nuiton.topia.TopiaRuntimeException;
import org.nuiton.topia.event.TopiaContextAdapter;
import org.nuiton.topia.event.TopiaContextEvent;
import org.nuiton.topia.event.TopiaContextListener;
import org.nuiton.topia.event.TopiaTransactionEvent;
import org.nuiton.topia.event.TopiaTransactionVetoable;
import org.nuiton.topia.framework.TopiaContextImplementor;
import org.nuiton.topia.framework.TopiaUtil;
import org.nuiton.topia.migration.mappings.TMSVersion;
import org.nuiton.topia.migration.mappings.TMSVersionDAO;
import org.nuiton.version.Version;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import static org.nuiton.i18n.I18n.t;

/**
 * Le moteur de migration proposé par topia. Il est basé sur un {@link AbstractTopiaMigrationCallback}
 * qui donne la version de l'application, les version de mises à jour disponibles.
 * 
 * Le call back offre aussi les commandes sql à passer pour chaque version de mise à jour.
 * 
 * FIXME Finir cette documentation
 *
 * @author tchemit
 * @version $Id$
 * @since 2.3.4
 */
public class TopiaMigrationEngine implements TopiaMigrationService {

    /** logger */
    private final static Log log = LogFactory.getLog(TopiaMigrationEngine.class);

//    /** Configuration hibernate ne mappant que l'entite version (initialise en pre-init) */
//    protected Configuration versionConfiguration;
//
//    /** Un drapeau pour savoir si la table version existe en base (initialise en pre-init) */
//    protected boolean versionTableExist;
//
//    /** Configuration hibernate ne mappant que l'entite version de l'ancien systeme de migration (initialise en pre-init) */
//    protected Configuration legacyVersionConfiguration;
//
//    /** Un drapeau pour savoir si la table version (de l'ancien service Manual) existe en base (initialise en pre-init) */
//    protected boolean legacyVersionTableExist;

    /** Pour enregistrer les version en base (initialise en pre-init) */
    protected TMSVersionPersister tmsVersionPersister;

    /** Version courante de la base (initialise en pre-init) */
    protected Version dbVersion;

    /** Drapeau pour savoir si la base est versionnée ou non */
    protected boolean dbNotVersioned;

    /**
     * A flag to know if none of the dealed entities tables exists in db.
     *
     * @since 2.5.3
     */
    protected boolean dbEmpty;

    /** Un drapeau pour effectuer la migration au demarrage (initialise en pre-init) */
    protected boolean migrateOnInit;

    /** CallbackHandler list (initialise en pre-init) */
    protected AbstractTopiaMigrationCallback callback;

    /** topia root context (initialise en pre-init) */
    protected TopiaContextImplementor rootContext;

    /** Un drapeau pour savoir si le service a bien ete initialise (i.e a bien fini la methode preInit) */
    protected boolean init;

    /**
     * A flag to check if version was detected in database.
     * 
     * This flag is set to {@code true} at the end of method {@link #detectDbVersion()}.
     */
    protected boolean versionDetected;

    /** Un drapeau pour afficher les requetes sql executees */
    protected boolean showSql;

    /** Un drapeau pour afficher la progression des requetes sql executees */
    protected boolean showProgression;

    /** delegate context listener. */
    protected final TopiaContextListener contextListener;

    /** delgate transaction listener */
    protected final TopiaTransactionVetoable transactionVetoable;

    public TopiaMigrationEngine() {

        transactionVetoable = new TopiaTransactionVetoable() {
            @Override
            public void beginTransaction(TopiaTransactionEvent event) {

                TopiaContextImplementor context =
                        (TopiaContextImplementor) event.getSource();

                // add topia context listener
                context.addTopiaContextListener(contextListener);

            }
        };
        contextListener = new TopiaContextAdapter() {

            @Override
            public void postCreateSchema(TopiaContextEvent event) {
                if (log.isDebugEnabled()) {
                    log.debug("postCreateSchema event called : will save version in database");
                }
                saveApplicationVersion();
            }

            @Override
            public void postUpdateSchema(TopiaContextEvent event) {
                if (log.isDebugEnabled()) {
                    log.debug("postUpdateSchema event called : will save version in database");
                }
                saveApplicationVersion();
            }

            @Override
            public void postRestoreSchema(TopiaContextEvent event) {
                if (log.isDebugEnabled()) {
                    log.debug("postRestoreSchema event detected, redo, schema migration");
                }
                if (migrateOnInit) {
                    // do automatic migration
                    if (log.isDebugEnabled()) {
                        log.debug("Starts Migrate from postRestoreSchema...");
                    }
                    try {
                        doMigrateSchema();
                    } catch (Exception e) {
                        if (log.isErrorEnabled()) {
                            log.error("postRestoreSchema schema migration failed for reason " + e.getMessage(), e);
                        }
                    }
                }
            }
        };
    }

    //--------------------------------------------------------------------------
    //-- TopiaService implementation
    //--------------------------------------------------------------------------

    @Override
    public Class<?>[] getPersistenceClasses() {
        return new Class<?>[]{TMSVersion.class};
    }

    @Override
    public String getServiceName() {
        return SERVICE_NAME;
    }

    @Override
    public boolean preInit(TopiaContextImplementor context) {
        rootContext = context;

        Properties config = context.getConfig();

        String callbackStr = getSafeParameter(config, MIGRATION_CALLBACK);
        if (log.isDebugEnabled()) {
            log.debug("Use callback            - " + callbackStr);
        }

        migrateOnInit = Boolean.valueOf(config.getProperty(MIGRATION_MIGRATE_ON_INIT, String.valueOf(Boolean.TRUE)));
        if (log.isDebugEnabled()) {
            log.debug("Migrate on init         - " + migrateOnInit);
        }

        showSql = Boolean.valueOf(config.getProperty(MIGRATION_SHOW_SQL, String.valueOf(Boolean.FALSE)));
        if (log.isDebugEnabled()) {
            log.debug("Show sql                - " + showSql);
        }

        showProgression = Boolean.valueOf(config.getProperty(MIGRATION_SHOW_PROGRESSION, String.valueOf(Boolean.FALSE)));
        if (log.isDebugEnabled()) {
            log.debug("Show progression        - " + showProgression);
        }
        // enregistrement du callback
        try {
            Class<?> clazz = Class.forName(callbackStr);
            callback = (AbstractTopiaMigrationCallback) clazz.newInstance();
        } catch (Exception e) {
            log.error("Could not instanciate CallbackHandler [" + callbackStr + "]", e);
        }

        // creation de la configuration hibernate ne concernant que l'entite Version
        // afin de pouvoir creer la table via un schemaExport si necessaire

        ServiceRegistry serviceRegistry = context.getServiceRegistry();
        MetadataSources versionSources = new MetadataSources(serviceRegistry);
        for (Class<?> aClass : getPersistenceClasses()) {
            String className = aClass.getName().replace('.', '/') + ".hbm.xml";
            versionSources.addResource(className);
        }
        Metadata versionMetadata = versionSources.getMetadataBuilder().build();

        // this other one
        MetadataSources legacySources = new MetadataSources(serviceRegistry);
        legacySources.addResource(TMSVersion.class.getName().replace('.', '/') + "Legacy.hbm.xml");
        Metadata legacyMetadata = legacySources.getMetadataBuilder().build();
        
        tmsVersionPersister = new TMSVersionPersister(context, versionMetadata, legacyMetadata);

        init = true;

        // add topia context listener
        context.addTopiaContextListener(contextListener);
        context.addTopiaTransactionVetoable(transactionVetoable);

        if (log.isDebugEnabled()) {
            log.debug("Service [" + this + "] is init.");
        }

        if (migrateOnInit) {
            // do automatic migration
            try {

                if (log.isDebugEnabled()) {
                    log.debug("Starts Migrate from preInit...");
                }
                doMigrateSchema();

            } catch (MigrationServiceException e) {
                throw new TopiaRuntimeException("Can't migrate schema for reason " + e.getMessage(), e);
            }
        } else {
            if (log.isDebugEnabled()) {
                log.debug("Service [" + this + "] skip migration on init as required");
            }
        }
        return true;
    }

    @Override
    public boolean postInit(TopiaContextImplementor context) {
        // nothing to do in post-init
        return true;
    }

    public void doMigrateSchema() throws MigrationServiceException {
        // migration
        boolean complete = migrateSchema();
        if (!complete) {
            if (log.isErrorEnabled()) {
                log.error(t("topia.migration.migration.incomplete"));
            }
            throw new TopiaRuntimeException(t("topia.migration.migration.incomplete"));
        }
    }

    //--------------------------------------------------------------------------
    //-- TopiaMigrationService implementation
    //--------------------------------------------------------------------------

    @Override
    public boolean migrateSchema() throws MigrationServiceException {

        checkInit();

        detectDbVersion();

        Version version = callback.getApplicationVersion();

        log.info(t("topia.migration.start.migration",
                   version.getVersion(),
                   dbVersion.getVersion())
        );

        boolean versionTableExist = tmsVersionPersister.isVersionTableExist();
        boolean legacyVersionTableExist = tmsVersionPersister.isLegacyVersionTableExist();

        if (log.isDebugEnabled()) {
            log.debug("is db not versionned ?    = " + dbNotVersioned);
            log.debug("is db empty ?             = " + dbEmpty);
            log.debug("TMSVersion exists ?       = " + versionTableExist);
            log.debug("LegacyTMSVersion exists ? = " + legacyVersionTableExist);
            log.debug("Update from version       = " + dbVersion);
            log.debug("Update to version         = " + version);
        }

        if (dbEmpty) {

            // db is empty (no table, no migration to apply)
            return true;
        }

        if (versionTableExist && dbVersion.equals(version)) {
            if (log.isInfoEnabled()) {
                log.info(t("topia.migration.skip.migration.db.is.up.to.date"));
            }
            // la base est a jour
            return true;
        }

        // Aucune version existante, la base de données est vierge
        if (versionTableExist && dbNotVersioned && migrateOnInit) {
            log.info(t("topia.migration.skip.migration.db.is.empty"));
            // la base est vierge, aucune migration nécessaire
            // mise à jour de la table tmsversion
            saveApplicationVersion();
            return true;
        }

        if (legacyVersionTableExist && dbVersion.equals(version)) {

            // on a trouvee une table depreciee tmsVersion avec la bonne version de base
            // il suffit donc d'enregister la version dans la nouvelle table
            if (log.isInfoEnabled()) {
                log.info(t("topia.migration.skip.migration.db.is.up.to.date"));
            }
            // la base est a jour mais il faut migrer la table
            saveApplicationVersion();
            return true;
        }

        SortedSet<Version> allVersions = new TreeSet<Version>();
        allVersions.addAll(Arrays.asList(callback.getAvailableVersions()));
        if (log.isInfoEnabled()) {
            log.info(t("topia.migration.available.versions", allVersions));
        }

        // tell if migration is needed
        boolean needToMigrate = false;

        if (dbVersion.before(version)) {

            // on filtre les versions a appliquer
            List<Version> versionsToApply = filterVersions(allVersions,
                                                           dbVersion,
                                                           version,
                                                           false,
                                                           true);

            if (versionsToApply.isEmpty()) {
                if (log.isInfoEnabled()) {
                    log.info(t("topia.migration.skip.migration.no.version.to.apply"));
                }
            } else {
                if (log.isInfoEnabled()) {
                    log.info(t("topia.migration.migrate.versions", versionsToApply));
                }

                callback.setTmsVersionPersister(tmsVersionPersister);

                // perform the migration
                needToMigrate = callback.doMigration(rootContext,
                                                     dbVersion,
                                                     showSql,
                                                     showProgression,
                                                     versionsToApply);

                if (log.isDebugEnabled()) {
                    log.debug("Handler choose : " + needToMigrate);
                }
                if (!needToMigrate) {
                    // l'utilisateur a annule la migration
                    return false;
                }
            }
        }

        // on sauvegarde la version si necessaire (base non versionnee ou migration realisee)
        if (!versionTableExist || needToMigrate) {

            if (log.isDebugEnabled()) {
                log.debug("Set application version in database to " + version);
            }

            // put version in database and create table if required
            saveApplicationVersion();
        }

        // return success flag
        // - no migration needed
        // - or migration needed and accepted
        return true;
    }

    //--------------------------------------------------------------------------
    //-- Internal methods
    //--------------------------------------------------------------------------

    /**
     * Enregistre la version donnee en base avec creation de la table
     * si elle n'existe pas.
     */
    protected void saveApplicationVersion() {

        checkInit();

        Version version = callback.getApplicationVersion();

        if (log.isDebugEnabled()) {
            log.debug("Save application version = " + version);
        }

        try {
            TopiaContext tx = rootContext.beginTransaction();
            try {

                // save version
                tmsVersionPersister.saveVersion(tx, version);

                // commit
                tx.commitTransaction();

            } finally {
                if (tx != null) {
                    tx.closeContext();
                }
            }
        } catch (TopiaException e) {
            throw new TopiaRuntimeException("Can't save application version for reason " + e.getMessage(), e);
        }

        dbVersion = version;
        
    }

    /**
     * Recupere depuis la base les états internes du service :
     * 
     * <ul>
     * <li>{@link #dbVersion}</li>
     * <li>{@link #dbEmpty}</li>
     * </ul>
     */
    protected void detectDbVersion() {

        if (versionDetected) {

            // this method was already invoked
            if (log.isDebugEnabled()) {
                log.debug("version was already detected : " + dbVersion);
            }
            return;
        }

        // compute dbempty field value
        dbEmpty = detectDbEmpty();

        if (log.isDebugEnabled()) {
            log.debug("Db is empty : " + dbEmpty);
        }


        Version v = null;
        try {

            boolean versionTableExist = tmsVersionPersister.isVersionTableExist();

            if (log.isDebugEnabled()) {
                log.debug("Table " + TMSVersionDAO.TABLE_NAME + " exist = " + versionTableExist);
            }

            if (versionTableExist) {

                // recuperation de la version de la base
                v = getVersion(true, TMSVersionDAO.TABLE_NAME);

                if (log.isWarnEnabled()) {
                    if (v == null) {
                        log.warn("Version not found on table " + TMSVersionDAO.TABLE_NAME);
                    }
                }
                return;
            }

            boolean legacyVersionTableExist = tmsVersionPersister.isLegacyVersionTableExist();

            if (legacyVersionTableExist) {

                if (log.isDebugEnabled()) {
                    log.debug("Legacy : detected " + TMSVersionDAO.LEGACY_TABLE_NAME + " table");
                }

                // recuperation de la version de la base
                v = getVersion(true, TMSVersionDAO.LEGACY_TABLE_NAME);

                if (v != null) {

                    if (log.isDebugEnabled()) {
                        log.debug("Legacy : " + t("topia.migration.detected.db.version", v));
                    }
                }
            }
        } finally {

            if (v == null) {
                // la base dans ce cas n'est pas versionee.
                // On dit que la version de la base est 0
                // et les schema de cette version 0 doivent
                // etre detenu en local
                v = Version.VZERO;
                dbNotVersioned = true;
                log.info(t("topia.migration.db.not.versionned"));
            } else {
                log.info(t("topia.migration.detected.db.version", v));
            }
            dbVersion = v;
            versionDetected = true;
        }
    }

    /**
     * Detects if there is some schema existing for at least one of the dealed
     * entity of the underlying db context.
     *
     * @return {@code true} if there is no schema for any of the dealed entities,
     * {@code false} otherwise.
     * @since 2.5.3
     */
    protected boolean detectDbEmpty() {


        try {
            boolean result;
            // get db real hibernate configuration
            //Configuration rootConfiguration =
            //        rootContext.getHibernateConfiguration();

            result = TopiaUtil.isSchemaEmpty(rootContext);
            return result;
        } catch (TopiaNotFoundException e) {
            throw new RuntimeException(e);
        }

    }

    protected Version getVersion(boolean versionTableExist, String tableName) {
        if (!versionTableExist) {

            // table does not exist, version is null
            return null;
        }
        try {
            TopiaContext tx = rootContext.beginTransaction();
            try {
                Version v = TMSVersionDAO.getVersion(tx, tableName);
                return v;
            } finally {
                if (tx != null) {
                    tx.closeContext();
                }
            }
        } catch (TopiaException e) {
            throw new TopiaRuntimeException("Can't obtain dbVersion for reason " + e.getMessage(), e);
        }
    }

    protected String getSafeParameter(Properties config, String key) {
        String value = config.getProperty(key, null);
        if (StringUtils.isEmpty(value)) {
            throw new IllegalStateException("'" + key + "' not set.");
        }
        return value;
    }

    protected void checkInit() {
        if (!init) {
            throw new IllegalStateException("le service n'est pas initialisé!");
        }
    }

    /**
     * Filter versions.
     *
     * @param versions   versions to filter
     * @param min        min version to accept
     * @param max        max version to accept
     * @param includeMin flag to include min version
     * @param includeMax flag to include max version
     * @return versions between min and max
     */
    protected List<Version> filterVersions(Set<Version> versions,
                                           Version min,
                                           Version max,
                                           boolean includeMin,
                                           boolean includeMax) {
        List<Version> toApply = new ArrayList<Version>();
        for (Version v : versions) {
            int t;
            if (min != null) {
                t = v.compareTo(min);
                if (t < 0 || t == 0 && !includeMin) {
                    // version trop ancienne
                    continue;
                }
            }
            if (max != null) {
                t = v.compareTo(max);
                if (t > 0 || t == 0 && !includeMax) {
                    // version trop recente
                    continue;
                }
            }
            toApply.add(v);
        }
        return toApply;
    }
}
