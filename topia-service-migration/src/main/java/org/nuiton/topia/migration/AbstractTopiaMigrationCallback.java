/*
 * #%L
 * ToPIA :: Service Migration
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.topia.migration;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.jdbc.Work;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaException;
import org.nuiton.topia.framework.TopiaContextImplementor;
import org.nuiton.util.StringUtil;
import org.nuiton.version.Version;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import static org.nuiton.i18n.I18n.t;

/**
 * Abstract migration callback.
 *
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @version $Id$
 * @since 2.5
 */
public abstract class AbstractTopiaMigrationCallback {

    /** Logger */
    static private Log log = LogFactory.getLog(AbstractTopiaMigrationCallback.class);

    private TMSVersionPersister tmsVersionPersister;

    /** @return the available versions from the call back */
    public abstract Version[] getAvailableVersions();

    /** @return the current application version (says the version to use) */
    public abstract Version getApplicationVersion();

    /**
     * Hook to ask user if migration can be performed.
     *
     * @param dbVersion the actual db version
     * @param versions  the versions to update
     * @return {@code false} if migration is canceled, {@code true} otherwise.
     */
    public abstract boolean askUser(Version dbVersion,
                                    List<Version> versions);

    protected abstract void migrateForVersion(Version version,
                                              TopiaContextImplementor tx,
                                              boolean showSql,
                                              boolean showProgression) throws Exception;

    /**
     * Tentative de migration depuis la version de la base version la version
     * souhaitee.
     * 
     * On applique toutes les migrations de version indiquee dans le parametre
     * <code>version</code>.
     * 
     * Pour chaque version, on cherche la methode migrateTo_XXX ou XXX est la
     * version transforme en identifiant java via la methode
     * {@link Version#getValidName()} et on l'execute.
     * 
     * Note: pour chaque version a appliquer, on ouvre une nouvelle transaction.
     *
     * @param ctxt            topia context de la transaction en cours
     * @param dbVersion       database version
     * @param showSql         drapeau pour afficher les requete sql
     * @param showProgression drapeau pour afficher la progression
     * @param versions        all versions knwon by service  @return migration a
     *                        ggrement
     * @return {@code true} si la migration est accepté, {@code false} autrement.
     */
    public boolean doMigration(TopiaContext ctxt,
                               Version dbVersion,
                               boolean showSql,
                               boolean showProgression,
                               List<Version> versions) throws MigrationServiceException {

        boolean doMigrate = askUser(dbVersion, versions);
        if (!doMigrate) {
            // l'utilisateur a refuse la migration
            return false;
        }
        TopiaContextImplementor tx;

        for (Version v : versions) {
            // ouverture d'une connexion direct JDBC sur la base
            try {

                tx = (TopiaContextImplementor) ctxt.beginTransaction();

                try {

                    log.info(t("topia.migration.start.migrate", v));

                    migrateForVersion(v, tx, showSql, showProgression);

                    tmsVersionPersister.saveVersion(tx, v);

                    tx.commitTransaction();

                } catch (Exception eee) {
                    // en cas d'erreur
                    log.error("Migration impossible de la base", eee);
                    // rollback du travail en cours
                    tx.rollbackTransaction();
                    // propagation de l'erreur
                    throw new MigrationServiceException(t("topia.migration.migration.failed", v), eee);
                } finally {
                    // close database connexion
                    if (tx != null) {
                        tx.closeContext();
                    }
                }

            } catch (MigrationServiceException eee) {
                throw eee;
            }catch (Exception eee) {
                log.error("Error lors de la tentative de migration", eee);
                doMigrate = false;
                // toute erreur arrête la mgration
                break;
            }
        }
        return doMigrate;
    }

    public void executeSQL(TopiaContextImplementor tx, String... sqls)
            throws TopiaException {
        executeSQL(tx, false, false, sqls);
    }

    /**
     * Executes the given {@code sqls} requests.
     *
     * @param tx              the session
     * @param showSql         flag to see sql requests
     * @param showProgression flag to see progession on console
     * @param sqls            requests to execute
     * @throws TopiaException if any pb
     * @since 2.3.0
     */
    public void executeSQL(TopiaContextImplementor tx,
                           final boolean showSql,
                           final boolean showProgression,
                           final String... sqls) throws TopiaException {


        final Set<String> sqlsWithoutComments = new LinkedHashSet<String>();
        for (String sql : sqls) {
            if (!sql.trim().startsWith("--")) {
                sqlsWithoutComments.add(sql);
            }
        }
        if (log.isInfoEnabled()) {
            log.info(t("topia.migration.start.sqls", sqlsWithoutComments.size()));
        }
        if (showSql) {
            StringBuilder buffer = new StringBuilder();
            for (String s : sqlsWithoutComments) {
                buffer.append(s).append("\n");
            }
            log.info("SQL TO EXECUTE :\n" +
                     "--------------------------------------------------------------------------------\n" +
                     "--------------------------------------------------------------------------------\n" +
                     buffer.toString() +
                     "--------------------------------------------------------------------------------\n" +
                     "--------------------------------------------------------------------------------\n"
            );
        }
        tx.getHibernate().doWork(new Work() {

            @Override
            public void execute(Connection connection) throws SQLException {
                int index = 0;
                int max = sqls.length;
                for (String sql : sqls) {
                    index++;
                    long t0 = System.nanoTime();
                    if (log.isInfoEnabled()) {
                        String message = "";

                        if (showProgression) {
                            message = t("topia.migration.start.sql", index, max);
                        }
                        if (showSql) {
                            message += "\n" + sql;
                        }
                        if (showProgression || showSql) {

                            log.info(message);
                        }
                    }
                    PreparedStatement sta = connection.prepareStatement(sql);
                    try {
                        sta.executeUpdate();
                    } finally {
                        sta.close();
                    }
                    if (log.isDebugEnabled()) {
                        String message;
                        message = t("topia.migration.end.sql", index, max, StringUtil.convertTime(System.nanoTime() - t0));
                        log.debug(message);
                    }
                }
            }
        });

    }

    public void setTmsVersionPersister(TMSVersionPersister tmsVersionPersister) {
        this.tmsVersionPersister = tmsVersionPersister;
    }

}
