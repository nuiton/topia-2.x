/*
 * #%L
 * ToPIA :: Service Migration
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.topia.migration;

/**
 * TopiaMigrationServiceException.java
 *
 * @author Chatellier Eric
 * @author Chevallereau Benjamin
 * @author Eon Sébastien
 * @author Trève Vincent
 * @version $Revision$
 *
 * Last update : $Date$
 */
public class MigrationServiceException extends Exception {

	/**
	 * Version
	 */
	private static final long serialVersionUID = -8900901171551405745L;
	
	/**
	 * Constructeur par defaut
	 */
	public MigrationServiceException() {
	}
	
	/**
	 * Constructeur avec message
	 * @param message Le message d'erreur
	 */
	public MigrationServiceException(String message) {
		super(message);
	}
	
	/**
	 * Constructeur avec message et exception
	 * @param message Le message d'erreur
	 * @param exception l'exception
	 */
	public MigrationServiceException(String message, Throwable exception) {
		super(message, exception);
	}

	/**
	 * Constructeur avec exception
	 * @param exception l'exception
	 */
	public MigrationServiceException(Throwable exception) {
		super(exception);
	}
}
