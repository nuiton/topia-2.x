/*
 * #%L
 * ToPIA :: Service Migration
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2015 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.topia.migration.mappings;

import org.apache.commons.lang3.StringUtils;
import org.nuiton.version.Version;
import org.nuiton.version.Versions;

import java.io.Serializable;

/**
 * TMSVersion.java
 *
 * @author Chatellier Eric
 * @author Chevallereau Benjamin
 * @author Eon Sébastien
 * @author Trève Vincent
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @since 1.0
 */
public class TMSVersion implements Serializable {

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_VERSION = "version";

    public static TMSVersion valueOf(Version version) {
        return new TMSVersion(version.toString());
    }

    public static TMSVersion valueOf(String version) {
        return new TMSVersion(version);
    }

    /** La version. */
    private String version;

    public TMSVersion() {
    }

    public TMSVersion(String version) {
        if (StringUtils.isEmpty(version)) {
            throw new IllegalArgumentException("version parameter can not be null nor empty.");
        }
        this.version = version;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Version toVersion() {
        return StringUtils.isEmpty(version) ? null : Versions.valueOf(version);
    }
}
