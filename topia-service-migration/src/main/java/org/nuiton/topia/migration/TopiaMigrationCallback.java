/*
 * #%L
 * ToPIA :: Service Migration
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.topia.migration;

import org.nuiton.topia.framework.TopiaContextImplementor;
import org.nuiton.version.Version;

import java.lang.reflect.Method;

/**
 * Default migration call back to use.
 * 
 * Replace deprecated implementation {@code ManualMigrationCallback}.
 *
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @version $Id$
 * @since 2.3.4
 * @deprecated since 2.5, use now the {@link TopiaMigrationCallbackByClass} or {@link TopiaMigrationCallbackByMethod}.
 *             <b>will not be replaced and remove before version {@code 2.6}</b>.
 */
@Deprecated
public abstract class TopiaMigrationCallback extends TopiaMigrationCallbackByMethod {

    @Deprecated
    protected Method getMigrationMethod(Version version) throws NoSuchMethodException {

        String methodName = "migrateTo_" + version.getValidName();

        Method m = getClass().getMethod(methodName,
                                        TopiaContextImplementor.class,
                                        boolean.class,
                                        boolean.class
        );
        return m;
    }
}
