package org.nuiton.topia.migration;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.boot.Metadata;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaException;
import org.nuiton.topia.framework.TopiaUtil;
import org.nuiton.topia.migration.mappings.TMSVersion;
import org.nuiton.topia.migration.mappings.TMSVersionDAO;
import org.nuiton.version.Version;
import org.nuiton.version.VersionBuilder;

import static org.nuiton.i18n.I18n.t;

/*
 * #%L
 * ToPIA :: Service Migration
 * %%
 * Copyright (C) 2004 - 2015 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/**
 * To persiste the version inside the {@link TMSVersion}.
 *
 * Created on 3/22/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.9.4
 */
public class TMSVersionPersister {

    /** Logger. */
    private static final Log log = LogFactory.getLog(TMSVersionPersister.class);

    /** Root context utilisé pour la migration. */
    protected TopiaContext context;

    /** Configuration hibernate ne mappant que l'entite version. */
    protected final Metadata versionMetadata;

    /** Configuration hibernate ne mappant que l'entite version de l'ancien systeme de migration. */
    protected final Metadata legacyVersionMetadata;

    /** Un drapeau pour savoir si la table version existe en base. */
    protected Boolean versionTableExist;

    /** Un drapeau pour savoir si la table version (de l'ancien service Manual) existe en base. */
    protected Boolean legacyVersionTableExist;

    public TMSVersionPersister(TopiaContext context, Metadata versionMetadata, Metadata legacyVersionMetadata) {
        this.context = context;
        this.versionMetadata = versionMetadata;
        this.legacyVersionMetadata = legacyVersionMetadata;
    }

    public boolean isLegacyVersionTableExist() {

        if (legacyVersionTableExist == null) {

            legacyVersionTableExist = TopiaUtil.isSchemaExist(context, legacyVersionMetadata, TMSVersion.class.getName());

        }

        return legacyVersionTableExist;

    }

    public boolean isVersionTableExist() {

        if (versionTableExist == null) {

            versionTableExist = TopiaUtil.isSchemaExist(context, versionMetadata, TMSVersion.class.getName());

        }

        return versionTableExist;

    }

    public void saveVersion(TopiaContext tx, Version version) throws TopiaException {

        if (log.isInfoEnabled()) {
            log.info("Table exists        = " + isVersionTableExist());
            log.info("Legacy table exists = " + isLegacyVersionTableExist());
        }

        if (isLegacyVersionTableExist()) {
            deleteLegacyTable();
        }

        Version dbVersion;

        if (isVersionTableExist()) {

            dbVersion = getVersion(tx);

        } else {

            dbVersion = null;
            createTableIfRequired();

        }

        if (log.isInfoEnabled()) {
            log.info("Db version          = " + dbVersion);
            log.info("Version to save     = " + version);
        }

        if (ObjectUtils.notEqual(dbVersion, version)) {

            persistVersion(tx, version);

        }

    }

    protected Version getVersion(TopiaContext tx) throws TopiaException {

        // delete all previous data in table
        TMSVersion tmsVersion = TMSVersionDAO.get(tx);
        Version version = tmsVersion == null ? null : VersionBuilder.create(tmsVersion.getVersion()).build();
        return version;

    }

    protected void persistVersion(TopiaContext tx, Version version) throws TopiaException {

        // delete all previous data in table
        TMSVersionDAO.deleteAll(tx);

        if (log.isInfoEnabled()) {
            log.info(t("topia.migration.saving.db.version", version));
        }

        // create new version and store it in table
        TMSVersion tmsVersion = TMSVersionDAO.create(tx, version.getVersion());
        if (log.isDebugEnabled()) {
            log.debug("Created version : " + tmsVersion.getVersion());
        }

    }

    protected void createTableIfRequired() {

        // si la base n'etait pas versionnee, la table version n'existe pas
        // creation
        if (log.isDebugEnabled()) {
            log.debug("Adding tms_version table");
        }

        // creer le schema en base
        // dans la configuration versionConfiguration, il n'y a que la table version
        TMSVersionDAO.createTable(versionMetadata);

        if (log.isDebugEnabled()) {
            log.debug("Table for " + TMSVersion.class.getSimpleName() + " created");
        }

        versionTableExist = true;

    }

    protected void deleteLegacyTable() {

        if (log.isDebugEnabled()) {
            log.debug("Will drop legacy tmsVersion table");
        }
        // on supprime l'ancienne table
        TMSVersionDAO.dropTable(legacyVersionMetadata);

        legacyVersionTableExist = false;

    }

}
