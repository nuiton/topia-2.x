/*
 * #%L
 * ToPIA :: Service Migration
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.topia.migration;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.framework.TopiaContextImplementor;
import org.nuiton.version.Version;

import java.lang.reflect.Method;

/**
 * Migration callback base on methods.
 * 
 * The callback defines for each version of {@link #getAvailableVersions()}
 * a method named {@code migrate_on_XXX} where {@code XXX} is the version with
 * all dots replaces by underscores.
 * 
 * Replace deprecated implementation {@code TopiaMigrationCallBack}.
 *
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @version $Id$
 * @since 2.5
 */
public abstract class TopiaMigrationCallbackByMethod extends AbstractTopiaMigrationCallback {

    /** Logger */
    static private Log log = LogFactory.getLog(TopiaMigrationCallbackByMethod.class);

    @Override
    protected void migrateForVersion(Version version,
                                     TopiaContextImplementor tx,
                                     boolean showSql,
                                     boolean showProgression) throws Exception {

        String methodName = "migrateTo_" + version.getValidName();

        Method m = getClass().getMethod(methodName,
                                        TopiaContextImplementor.class,
                                        boolean.class,
                                        boolean.class
        );

        m.setAccessible(true);

        if (log.isDebugEnabled()) {
            log.debug("launch method " + m.getName());
        }

        m.invoke(this, tx, showSql, showProgression);

    }


}
