/*
 * #%L
 * ToPIA :: Service Migration
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.topia.migration;

import org.nuiton.topia.framework.TopiaService;

/**
 * TopiaMigrationService.java
 *
 * @author Chatellier Eric
 * @author Chevallereau Benjamin
 * @author Eon Sébastien
 * @author Trève Vincent
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @version $Id$
 */
public interface TopiaMigrationService extends TopiaService {

    /** Nom du service */
    String SERVICE_NAME = "migration";

    /** Nom du service topia */
    String TOPIA_SERVICE_NAME = "topia.service.migration";

    /** Pour spécifier dans la configuration le callback a utiliser */
    String MIGRATION_CALLBACK = "topia.service.migration.callback";

    /** Un drapeau pour indiquer si on doit lancer le service au demarrage */
    String MIGRATION_MIGRATE_ON_INIT = "topia.service.migration.no.migrate.on.init";

    /** Pour afficher les requetes sql executees */
    String MIGRATION_SHOW_SQL = "topia.service.migration.showSql";

    /** Pour afficher la progression des requetes sql executees */
    String MIGRATION_SHOW_PROGRESSION = "topia.service.migration.showProgression";

    boolean migrateSchema() throws MigrationServiceException;
}
