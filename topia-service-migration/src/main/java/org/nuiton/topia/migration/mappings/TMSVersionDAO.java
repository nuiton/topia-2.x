/*
 * #%L
 * ToPIA :: Service Migration
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2015 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.topia.migration.mappings;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.EnumSet;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.boot.Metadata;
import org.hibernate.jdbc.Work;
import org.hibernate.tool.hbm2ddl.SchemaExport;
import org.hibernate.tool.schema.TargetType;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaException;
import org.nuiton.topia.TopiaRuntimeException;
import org.nuiton.topia.framework.TopiaContextImplementor;
import org.nuiton.topia.framework.TopiaUtil;
import org.nuiton.version.Version;
import org.nuiton.version.Versions;

/**
 * TMSVersion DAO helper.
 *
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @since 2.3.4
 */
public class TMSVersionDAO {

    /** logger */
    private final static Log log = LogFactory.getLog(TMSVersionDAO.class);

    public static final String LEGACY_TABLE_NAME = "tmsVersion";

    public static final String TABLE_NAME = "tms_version";

    public static TMSVersion get(TopiaContext tx) throws TopiaException {

        try {
            Session session = ((TopiaContextImplementor) tx).getHibernate();
            Criteria criteria = session.createCriteria(TMSVersion.class);
            List<?> list = criteria.list();
            TMSVersion result = list.isEmpty() ? null : (TMSVersion) list.get(0);
            return result;
        } catch (HibernateException e) {
            throw new TopiaException("Could not obtain version", e);
        }
    }

    public static void createTable(Metadata metatada) {
        // creer le schema en base
        // dans la configuration il n'y a que la table version
        EnumSet<TargetType> targetTypes = EnumSet.of(TargetType.DATABASE);
        if (log.isDebugEnabled()) {
            targetTypes = EnumSet.of(TargetType.DATABASE, TargetType.STDOUT);
        }
        new SchemaExport().createOnly(targetTypes, metatada);
    }

    public static void dropTable(Metadata metatada) {
        // supprimer le schema en base
        // dans la configuration il n'y a que la table version
        EnumSet<TargetType> targetTypes = EnumSet.of(TargetType.DATABASE);
        if (log.isDebugEnabled()) {
            targetTypes = EnumSet.of(TargetType.DATABASE, TargetType.STDOUT);
        }
        new SchemaExport().drop(targetTypes, metatada);
    }

    public static TMSVersion create(TopiaContext tx, String version) throws TopiaException {

        try {
            Session session = ((TopiaContextImplementor) tx).getHibernate();

            TMSVersion result = TMSVersion.valueOf(version);
            // save entity
            session.save(result);
            return result;
        } catch (HibernateException e) {
            throw new TopiaException("Could not create version " + version, e);
        }
    }

    public static void update(TopiaContext tx, TMSVersion version) throws TopiaException {
        try {
            Session session = ((TopiaContextImplementor) tx).getHibernate();
            session.saveOrUpdate(version);
            tx.commitTransaction();
        } catch (HibernateException e) {
            throw new TopiaException("Could not update version " + version, e);
        }
    }

    public static void deleteAll(TopiaContext tx) throws TopiaException {
        try {
            Session session = ((TopiaContextImplementor) tx).getHibernate();
            Criteria criteria = session.createCriteria(TMSVersion.class);
            List<?> list = criteria.list();
            for (Object o : list) {
                session.delete(o);
            }
        } catch (HibernateException e) {
            throw new TopiaException("Could not delete all versions", e);
        }
    }

    public static final String LEGACY_MAPPING =
            "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
            "<!DOCTYPE hibernate-mapping PUBLIC \"-//Hibernate/Hibernate Mapping DTD 3.0//EN\" \"classpath://org/hibernate/hibernate-mapping-3.0.dtd\">\n" +
            "<hibernate-mapping>\n" +
            "    <class name=\"" + TMSVersion.class.getName() + "\" table=\"" + LEGACY_TABLE_NAME + "\">\n" +
            "    <id column=\"" + TMSVersion.PROPERTY_VERSION + "\" name=\"" + TMSVersion.PROPERTY_VERSION + "\"/>\n" +
            "  </class>\n" +
            "</hibernate-mapping>";

    public static Version getVersion(TopiaContext tx, String tableName) {
        try {
            TopiaContextImplementor txImpl = (TopiaContextImplementor) tx;

            // Get schema name
            String schemaName = TopiaUtil.getSchemaName(txImpl);

            GetVersionWork work = new GetVersionWork(schemaName, tableName);
            txImpl.getHibernate().doWork(work);
            Version v = work.getVersion();
            return v;
        } catch (TopiaException e) {
            throw new TopiaRuntimeException("Can't obtain dbVersion for reason " + e.getMessage(), e);
        }
    }

    public static class GetVersionWork implements Work {

        protected Version version;

        private final String tableName;
        private final String schemaName;

        public GetVersionWork(String schemaName, String tableName) {
            this.tableName = tableName;
            this.schemaName = schemaName;
        }

        @Override
        public void execute(Connection connection) throws SQLException {

            String fullTableName = schemaName == null ?
                        tableName : schemaName + "." + tableName;


            PreparedStatement st = connection.prepareStatement("select " + TMSVersion.PROPERTY_VERSION + " from " + fullTableName + ";");
            try {
                ResultSet set = st.executeQuery();

                if (set.next()) {
                    version = Versions.valueOf(set.getString(1));
                }
            } catch (SQLException eee) {
                if (log.isErrorEnabled()) {
                    log.error("Could not find version", eee);
                }
                version = null;
            } finally {
                st.close();
            }
        }

        public Version getVersion() {
            return version;
        }
    }
}
