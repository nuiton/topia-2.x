<?xml version="1.0" encoding="UTF-8"?>
<!--
  #%L
  ToPIA
  %%
  Copyright (C) 2004 - 2022 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.

  You should have received a copy of the GNU General Lesser Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-3.0.html>.
  #L%
  -->
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <parent>
    <groupId>org.nuiton</groupId>
    <artifactId>nuitonpom</artifactId>
    <version>11.11</version>
  </parent>

  <artifactId>topia</artifactId>
  <version>2.17-SNAPSHOT</version>
  <packaging>pom</packaging>

  <name>ToPIA</name>
  <description>
    Tools for Portable and Independent Architecture :
    Framework de persistance et de distribution d'application.
  </description>
  <url>http://nuiton.page.nuiton.org/topia-2.x/</url>
  <inceptionYear>2004</inceptionYear>

  <developers>
    
    <developer>
      <name>Benjamin Poussin</name>
      <id>bpoussin</id>
      <email>poussin@codelutin.com</email>
      <organization>CodeLutin</organization>
      <organizationUrl>http://codelutin.com</organizationUrl>
      <timezone>Europe/Paris</timezone>
      <roles>
        <role>Développeur</role>
      </roles>
    </developer>

    <developer>
      <name>Arnaud Thimel</name>
      <id>athimel</id>
      <email>thimel@codelutin.com</email>
      <organization>CodeLutin</organization>
      <organizationUrl>http://codelutin.com</organizationUrl>
      <timezone>Europe/Paris</timezone>
      <roles>
        <role>Développeur</role>
      </roles>
    </developer>

    <developer>
      <name>Julien Ruchaud</name>
      <id>jruchaud</id>
      <email>ruchaud@codelutin.com</email>
      <organization>CodeLutin</organization>
      <organizationUrl>http://codelutin.com</organizationUrl>
      <timezone>Europe/Paris</timezone>
      <roles>
        <role>Développeur</role>
      </roles>
    </developer>

    <developer>
      <name>Eric Chatellier</name>
      <id>echatellier</id>
      <email>chatellier@codelutin.com</email>
      <organization>CodeLutin</organization>
      <organizationUrl>http://codelutin.com</organizationUrl>
      <timezone>Europe/Paris</timezone>
      <roles>
        <role>Développeur</role>
      </roles>
    </developer>
    
    <developer>
      <name>Brendan Le Ny</name>
      <id>bleny</id>
      <email>bleny@codelutin.com</email>
      <organization>CodeLutin</organization>
      <organizationUrl>http://codelutin.com</organizationUrl>
      <timezone>Europe/Paris</timezone>
      <roles>
        <role>Développeur</role>
      </roles>
    </developer>

    <developer>
      <name>Jean Couteau</name>
      <id>jcouteau</id>
      <email>couteau@codelutin.com</email>
      <organization>CodeLutin</organization>
      <organizationUrl>http://codelutin.com</organizationUrl>
      <timezone>Europe/Paris</timezone>
      <roles>
        <role>Documentation writer</role>
      </roles>
    </developer>
  </developers>
  <contributors>
    <contributor>
      <name>Florian Desbois</name>
      <roles>
        <role>Développeur</role>
      </roles>    
    </contributor>
    <contributor>
      <name>Sylvain Letellier</name>
      <timezone>Europe/Paris</timezone>
      <roles>
        <role>Développeur</role>
      </roles>
    </contributor>
    <contributor>
      <name>Nicolas Dupont</name>
      <email>ndupont@codelutin.com</email>
      <organization>CodeLutin</organization>
      <organizationUrl>http://codelutin.com</organizationUrl>
      <timezone>Europe/Paris</timezone>
    </contributor>
    <contributor>
      <name>Eduardo Ore</name>
      <email>eore@codelutin.com</email>
      <organization>CodeLutin</organization>
      <organizationUrl>http://codelutin.com</organizationUrl>
      <timezone>Europe/Paris</timezone>
    </contributor>
    <contributor>
      <name>Gabriel Landais</name>
      <email>glandais@codelutin.com</email>
      <organization>CodeLutin</organization>
      <organizationUrl>http://codelutin.com</organizationUrl>
      <timezone>Europe/Paris</timezone>
    </contributor>
    <contributor>
      <name>Stéphane Chorlet</name>
      <email>schorlet@codelutin.com</email>
      <organization>CodeLutin</organization>
      <organizationUrl>http://codelutin.com</organizationUrl>
      <timezone>Europe/Paris</timezone>
    </contributor>
    <contributor>
      <name>Jonathan pepin</name>
      <email>jpepin@codelutin.com</email>
      <organization>CodeLutin</organization>
      <organizationUrl>http://codelutin.com</organizationUrl>
      <timezone>Europe/Paris</timezone>
    </contributor>
    <contributor>
      <name>Nolwenn Ranou</name>
      <email>nrannou@codelutin.com</email>
      <organization>CodeLutin</organization>
      <organizationUrl>http://codelutin.com</organizationUrl>
      <timezone>Europe/Paris</timezone>
    </contributor>
    <contributor>
      <name>Tony Chemit</name>
      <timezone>Europe/Paris</timezone>
      <roles>
        <role>Développeur</role>
      </roles>
    </contributor>
  </contributors>

  <modules>
    <module>topia-persistence</module>
    <module>topia-service-replication</module>
    <module>topia-service-migration</module>
    <!--<module>topia-service-security</module>-->
  </modules>

  <scm>
    <connection>scm:git:https://gitlab.nuiton.org/nuiton/topia-2.x.git</connection>
    <developerConnection>scm:git:https://gitlab.nuiton.org/nuiton/topia-2.x.git</developerConnection>
    <url>https://gitlab.nuiton.org/nuiton/topia-2.x.git</url>
  </scm>

  <properties>
    <!-- pour un muli module on doit fixer le projectId -->
    <gitlabProjectName>topia</gitlabProjectName>

    <!-- libs version -->
    <eugeneVersion>3.0</eugeneVersion>
    <nuitonCsvVersion>3.1</nuitonCsvVersion>
    <nuitonDecoratorVersion>3.0</nuitonDecoratorVersion>
    <nuitonUtilsVersion>3.1</nuitonUtilsVersion>
    <processorPluginVersion>1.3</processorPluginVersion>
    <nuitonI18nVersion>4.0</nuitonI18nVersion>
    <xmlrpcVersion>3.1.2</xmlrpcVersion>
    <hibernateVersion>5.6.5.Final</hibernateVersion>
    <sl4jVersion>1.7.35</sl4jVersion>
    <h2Version>1.4.200</h2Version>
    <hamcrestVersion>2.2</hamcrestVersion>

    <!-- i18n configuration -->
    <i18n.bundles>fr_FR,en_GB,es_ES</i18n.bundles>
    
    <javaVersion>1.8</javaVersion>
    <signatureArtifactId>java18</signatureArtifactId>
    <signatureVersion>1.0</signatureVersion>

    <jrstPluginVersion>2.3</jrstPluginVersion>

    <!-- Topia déploie des modules avec le classifier 'test' donc on ne peut pas ignorer les tests en postRelease -->
    <gitflow.postReleaseGoals>deploy -DperformRelease</gitflow.postReleaseGoals>
    <!-- Pour garder la continuité des TAG Git on déclare explicitement le préfixe -->
    <gitflow.versionTagPrefix>topia-</gitflow.versionTagPrefix>
  </properties>

  <dependencyManagement>
    <dependencies>

      <dependency>
        <groupId>org.nuiton.eugene</groupId>
        <artifactId>eugene</artifactId>
        <version>${eugeneVersion}</version>
        <scope>provided</scope>
      </dependency>

      <dependency>
        <groupId>org.nuiton.eugene</groupId>
        <artifactId>eugene-java-templates</artifactId>
        <version>${eugeneVersion}</version>
        <scope>provided</scope>
      </dependency>

      <dependency>
        <groupId>org.nuiton</groupId>
        <artifactId>nuiton-utils</artifactId>
        <version>${nuitonUtilsVersion}</version>
      </dependency>

      <dependency>
        <groupId>org.nuiton</groupId>
        <artifactId>nuiton-version</artifactId>
        <version>1.0</version>
      </dependency>

      <dependency>
        <groupId>org.nuiton</groupId>
        <artifactId>nuiton-csv</artifactId>
        <version>${nuitonCsvVersion}</version>
      </dependency>

      <dependency>
        <groupId>org.nuiton</groupId>
        <artifactId>nuiton-decorator</artifactId>
        <version>${nuitonDecoratorVersion}</version>
      </dependency>

      <dependency>
        <groupId>org.nuiton.i18n</groupId>
        <artifactId>nuiton-i18n</artifactId>
        <version>${nuitonI18nVersion}</version>
      </dependency>

      <dependency>
        <groupId>org.hibernate</groupId>
        <artifactId>hibernate-core</artifactId>
        <version>${hibernateVersion}</version>
        <scope>compile</scope>
      </dependency>

      <dependency>
        <groupId>org.hibernate</groupId>
        <artifactId>hibernate-ehcache</artifactId>
        <version>${hibernateVersion}</version>
        <scope>runtime</scope>
      </dependency>

      <dependency>
        <groupId>javax.persistence</groupId>
        <artifactId>javax.persistence-api</artifactId>
        <version>2.2</version>
      </dependency>

      <dependency>
        <groupId>javax.transaction</groupId>
        <artifactId>jta</artifactId>
        <version>1.1</version>
        <scope>runtime</scope>
      </dependency>

      <!-- hibernate-core only include api, need implementation,
           binding with log4j will be used
      -->
      <dependency>
        <groupId>org.slf4j</groupId>
        <artifactId>slf4j-api</artifactId>
        <version>${sl4jVersion}</version>
      </dependency>
      <dependency>
        <groupId>org.slf4j</groupId>
        <artifactId>slf4j-log4j12</artifactId>
        <version>${sl4jVersion}</version>
        <scope>test</scope>
      </dependency>

      <!-- Guava -->
      <dependency>
        <groupId>com.google.guava</groupId>
        <artifactId>guava</artifactId>
        <version>31.0.1-jre</version>
      </dependency>

      <!-- Commons libs -->

      <dependency>
        <groupId>org.apache.commons</groupId>
        <artifactId>commons-collections4</artifactId>
        <version>4.4</version>
      </dependency>

      <dependency>
        <groupId>commons-beanutils</groupId>
        <artifactId>commons-beanutils</artifactId>
        <version>1.9.4</version>
      </dependency>

      <dependency>
        <groupId>commons-logging</groupId>
        <artifactId>commons-logging</artifactId>
        <version>1.2</version>
      </dependency>

      <dependency>
        <groupId>commons-io</groupId>
        <artifactId>commons-io</artifactId>
        <version>2.11.0</version>
      </dependency>

      <dependency>
        <groupId>org.apache.commons</groupId>
        <artifactId>commons-lang3</artifactId>
        <version>3.12.0</version>
      </dependency>

      <dependency>
        <groupId>org.codehaus.plexus</groupId>
        <artifactId>plexus-component-annotations</artifactId>
        <version>2.1.1</version>
      </dependency>

      <!-- BD H2 for testing -->
      <dependency>
        <groupId>com.h2database</groupId>
        <artifactId>h2</artifactId>
        <version>${h2Version}</version>
        <scope>test</scope>
      </dependency>

      <dependency>
        <groupId>org.hamcrest</groupId>
        <artifactId>hamcrest</artifactId>
        <version>${hamcrestVersion}</version>
        <scope>test</scope>
      </dependency>

      <dependency>
        <groupId>org.hamcrest</groupId>
        <artifactId>hamcrest-core</artifactId>
        <version>${hamcrestVersion}</version>
        <scope>test</scope>
      </dependency>

      <!-- Junit -->
      <dependency>
        <groupId>junit</groupId>
        <artifactId>junit</artifactId>
        <version>4.13.2</version>
        <scope>test</scope>
      </dependency>

      <!-- Log4J -->
      <dependency>
        <groupId>log4j</groupId>
        <artifactId>log4j</artifactId>
        <version>1.2.17</version>
        <scope>test</scope>
      </dependency>

    </dependencies>
  </dependencyManagement>

  <build>
    <pluginManagement>
      <plugins>
        <plugin>
          <groupId>org.nuiton.processor</groupId>
          <artifactId>processor-maven-plugin</artifactId>
          <version>${processorPluginVersion}</version>
          <executions>
            <execution>
              <phase>generate-sources</phase>
              <goals>
                <goal>process</goal>
              </goals>
            </execution>
          </executions>
          <configuration>
            <includes>**/*.java</includes>
            <filters>
              org.nuiton.processor.filters.GeneratorTemplatesFilter,
              org.nuiton.processor.filters.ActiveLogsCodeFilter
            </filters>
          </configuration>
        </plugin>

        <plugin>
          <groupId>org.nuiton.eugene</groupId>
          <artifactId>eugene-maven-plugin</artifactId>
          <version>${eugeneVersion}</version>
          <configuration>
            <inputs>zargo</inputs>
            <resolver>org.nuiton.util.FasterCachedResourceResolver</resolver>
          </configuration>
        </plugin>

        <plugin>
          <groupId>org.nuiton.i18n</groupId>
          <artifactId>i18n-maven-plugin</artifactId>
          <version>${nuitonI18nVersion}</version>
        </plugin>

        <plugin>
          <artifactId>maven-site-plugin</artifactId>
          <dependencies>
            <dependency>
              <groupId>org.nuiton.jrst</groupId>
              <artifactId>doxia-module-jrst</artifactId>
              <version>${jrstPluginVersion}</version>
            </dependency>
          </dependencies>
        </plugin>

        <plugin>
          <groupId>org.codehaus.mojo</groupId>
          <artifactId>license-maven-plugin</artifactId>
          <configuration>
            <extraExtensions>
              <objectmodel>xml</objectmodel>
              <xsl>xml</xsl>
              <xsd>xml</xsd>
            </extraExtensions>
          </configuration>
        </plugin>
        <plugin>
          <artifactId>maven-dependency-plugin</artifactId>
          <version>3.1.2</version> <!-- 3.2.0 buggued -->
        </plugin>
      </plugins>
    </pluginManagement>
  </build>

  <reporting>
    <!--TC-20100413 : by default do nothing except documentation -->
    <excludeDefaults>true</excludeDefaults>
  </reporting>

</project>
