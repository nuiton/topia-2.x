/*
 * #%L
 * ToPIA :: Service Replication
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.topia.replication.operation;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.TopiaException;
import org.nuiton.topia.framework.TopiaContextImplementor;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.util.EntityOperator;
import org.nuiton.topia.persistence.util.TopiaEntityHelper;
import org.nuiton.topia.replication.TopiaReplicationContext;
import org.nuiton.topia.replication.TopiaReplicationOperation;
import org.nuiton.topia.replication.model.ReplicationModel;
import org.nuiton.topia.replication.model.ReplicationNode;
import org.nuiton.topia.replication.model.ReplicationOperationDef;
import org.nuiton.topia.replication.model.ReplicationOperationPhase;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Pour attacher une association.
 * 
 * L'opération requière 2 ou 3 paramètres :
 * <ul>
 * <li>{@code parameters[0]} : le nom de l'association à traiter</li>
 * <li>{@code parameters[1]} : un drapeau pour savoir si on est sur le reverse
 * de l'association</li>
 * <li>{@code parameters[2]} : le noeud source de l'association
 * (uniquement utilisé si on est sur le reverse d'une association)</li>
 * </ul>
 * 
 * Deux cas peuvent se produire :
 * 
 * - le noeud de l'operation est la source de l'association, dans ce cas la
 * {@code entities} contient les entites sources de l'association et on
 * retrouve les entites associes a partir du type de l'association
 * 
 * Ce premier cas est verifie quand {@code reverse} (le second parametre) est
 * à {@code false}.
 * 
 * - le noeud de l'operation est la cible de l'association, dans ce cas la
 * {@code entities} contient les entities associées (cibles) de
 * l'assocation et on retrouve les entities a partir d'un troisieme parametre
 * qui donne le node source de l'association.
 * 
 * Note : cette operation est interne, et n'est pas creable par l'utilisateur
 * via la methode {@link #register(ReplicationModel, ReplicationNode,
 * ReplicationOperationPhase, Object...)}.
 *
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @since 2.2.0
 * @deprecated since 2.5.2, this operation will be removed in version 2.6 and
 * will not be replaced : prefer use the {@link AttachLink} instead.
 */
@Deprecated
public class AttachAssociation implements TopiaReplicationOperation {

    /** Logger */
    private static final Log log = LogFactory.getLog(AttachAssociation.class);

    @Override
    public void register(ReplicationModel model,
                         ReplicationNode ownerNode,
                         ReplicationOperationPhase phase,
                         Object... parameters) {

//        throw new UnsupportedOperationException(
//                _("topia.replication.error.operation.uncreatable", getClass()));
    }

    @Override
    public void run(TopiaReplicationContext replicationContext,
                    ReplicationOperationDef operationDef,
                    TopiaContextImplementor srcCtxt,
                    TopiaContextImplementor dstCtxt,
                    List<? extends TopiaEntity> entities) throws TopiaException {

        String name = (String) operationDef.getParameters()[0];
        Boolean reverse = (Boolean) operationDef.getParameters()[1];

        if (log.isDebugEnabled()) {
            log.debug("currentNode : " + operationDef.getNode() +
                      ", association name : " + name + " reverse ? " + reverse);
        }

        ReplicationNode ownerNode;
        ReplicationNode cibleNode;

        EntityOperator<? super TopiaEntity> ownerOperator;

        // contient la liste des ids d'association autorisees ici
        List<String> associationsId;

        // contient la liste des ids des entite source de l'association
        List<String> ownerIds;
        if (reverse) {

            // on est en mode reverse, i.e :
            // - le noeud courant est la cible
            // - le noeud cible est passe en parametre
            // - nodeEntities contient les entities sources

            ownerNode = (ReplicationNode) operationDef.getParameters()[2];
            ownerOperator = ownerNode.getOperator();

            cibleNode = operationDef.getNode();

            ownerIds = TopiaEntityHelper.getTopiaIdList(entities);
            associationsId = replicationContext.getEntityIds(cibleNode);

        } else {

            // on est en mode non reverse, i.e :
            // - le noeud courant est la source
            // - le noeud cible est deduit de l'association sur le noeud source
            // - nodeEntities contient les entities cibles

            ownerNode = operationDef.getNode();
            ownerOperator = ownerNode.getOperator();

            cibleNode = ownerNode.getAssociations().get(name);

            ownerIds = replicationContext.getEntityIds(cibleNode);
            associationsId = TopiaEntityHelper.getTopiaIdList(entities);
        }

        if (ownerIds == null || ownerIds.isEmpty()) {
            // pas de donnees a traiter
            log.info(t("topia.replication.attachAssociation.nothing.to.do",
                       ownerOperator));
            return;
        }

        // contient la liste des entites sources de l'association
        List<? extends TopiaEntity> ownerEntities;

        // on recharge obligatoirement les donnees sources car elles ont pu etre
        // modifiees (dettachement d'association ou autres)
        // ils nous faut les entites telles qu'elles sont en base source

        ownerEntities = TopiaEntityHelper.getEntitiesList(
                srcCtxt,
                ownerIds.toArray(new String[ownerIds.size()])
        );

        boolean shouldCommit = false;

        if (log.isInfoEnabled()) {
            log.info("ownerNode : " + ownerNode + " , targetNode : " +
                     cibleNode + ", association : " + name);
        }
        if (log.isDebugEnabled()) {
            log.debug("owner ids : " + ownerIds);
            log.debug("association ids : " + associationsId);
        }

        for (TopiaEntity src : ownerEntities) {

            // les association cibles connues pour l'entite sur la base source
            Collection<?> targetEntities =
                    (Collection<?>) ownerOperator.get(name, src);

            if (targetEntities == null || targetEntities.isEmpty()) {
                if (log.isDebugEnabled()) {
                    log.debug("no association '" + name + "' attached to  " +
                              src);
                }
                // pas de donnees dans l'association
                continue;
            }
            if (log.isDebugEnabled()) {
                log.debug("will try to attach " + targetEntities.size() +
                          " association(s) '" + name + "' to " + src);
            }

            // l'entite repliquee a laquelle on veut attacher l'association

            TopiaEntity dst = dstCtxt.findByTopiaId(src.getTopiaId());

            // les association cibles connues pour l'entite sur la base
            // destination
            Collection<?> dstTargetEntities = (Collection<?>)
                    ownerOperator.get(name, dst);

            // les ids des entities deja associees
            List<String> dstTargetAssociationsId =
                    dstTargetEntities == null ?
                    Collections.<String>emptyList() :
                    TopiaEntityHelper.getTopiaIdList(
                            (Collection<? extends TopiaEntity>) dstTargetEntities);

            boolean shouldUpdate = false;
            for (Object a : targetEntities) {

                TopiaEntity assosiationSrc = (TopiaEntity) a;

                // on verifie que l'association doit etre rattachee
                if (associationsId.contains(assosiationSrc.getTopiaId())) {
                    if (dstTargetAssociationsId.contains(
                            assosiationSrc.getTopiaId())) {
                        // deja attache
                        if (log.isDebugEnabled()) {
                            log.debug("already attached association '" + name +
                                      "' : " + assosiationSrc);
                        }
                        continue;

                    }

                    // la donnees doit etre attachee

                    TopiaEntity assosiationDst =
                            dstCtxt.findByTopiaId(assosiationSrc.getTopiaId());
                    ownerOperator.addChild(name, dst, assosiationDst);
                    if (log.isDebugEnabled()) {
                        log.debug("will attach association '" + name + "' : " +
                                  assosiationDst);
                    }
                    shouldUpdate = true;
                }

            }

            if (shouldUpdate) {
                if (log.isTraceEnabled()) {
                    log.trace("will update " + dst.getTopiaId());
                }
                //FIXME: on ne peut pas updater l'objet car l'objet peut rentre
                // en conflit dans la session hibernate
                // cela fonctionne sans faire d'update (heureusement...)
                //dst.update();
                shouldCommit = true;
            }
        }

        if (shouldCommit) {
            dstCtxt.commitTransaction();
        }
    }
}
