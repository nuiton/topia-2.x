/*
 * #%L
 * ToPIA :: Service Replication
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.topia.replication;

import org.nuiton.topia.framework.TopiaContextImplementor;
import org.nuiton.topia.replication.model.ReplicationOperationDef;

/**
 * Le contrat d'une operation {@link TopiaReplicationOperation} qui peut être
 * rollbacker lorsque la replication a échouée.
 * 
 *
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @since 2.4.3
 */
public interface TopiaReplicationOperationUndoable extends TopiaReplicationOperation {

    /**
     * Execute l'operation inverse avec le parametrage donnee
     * (pour annuler l'opération).
     * 
     * Note : le commit sur le context cible doit etre geree dans la methode.
     *
     * @param operationDef       la definition de l'operation a realiser
     * @param replicationContext le context de replication
     * @param dstCtxt            le context destination
     * @throws Exception pour toute erreur
     */
    void rollback(ReplicationOperationDef operationDef,
                  TopiaReplicationContext replicationContext,
                  TopiaContextImplementor dstCtxt
    ) throws Exception;
}
