/*
 * #%L
 * ToPIA :: Service Replication
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.topia.replication.operation;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.TopiaException;
import org.nuiton.topia.framework.TopiaContextImplementor;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.util.EntityOperator;
import org.nuiton.topia.replication.TopiaReplicationContext;
import org.nuiton.topia.replication.TopiaReplicationOperation;
import org.nuiton.topia.replication.model.ReplicationLink;
import org.nuiton.topia.replication.model.ReplicationModel;
import org.nuiton.topia.replication.model.ReplicationNode;
import org.nuiton.topia.replication.model.ReplicationOperationDef;
import org.nuiton.topia.replication.model.ReplicationOperationPhase;

import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Pour charger une association sur le noeud de l'operation.
 * 
 * Note : cette operation est interne, et n'est pas creable par
 * l'utilisateur via la methode
 * {@link #register(ReplicationModel, ReplicationNode, ReplicationOperationPhase, Object...)}.
 *
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @since 2.2.0
 */
public class LoadLink implements TopiaReplicationOperation {

    /** Logger */
    private static final Log log = LogFactory.getLog(LoadLink.class);

    @Override
    public void register(ReplicationModel model,
                         ReplicationNode ownerNode,
                         ReplicationOperationPhase phase,
                         Object... parameters) {

//        throw new UnsupportedOperationException(
//                _("topia.replication.error.operation.uncreatable", getClass()));
    }

    @Override
    public void run(TopiaReplicationContext replicationContext,
                    ReplicationOperationDef operationDef,
                    TopiaContextImplementor srcCtxt,
                    TopiaContextImplementor dstCtxt,
                    List<? extends TopiaEntity> nodeEntities
    ) throws TopiaException {

        ReplicationLink link = (ReplicationLink) operationDef.getParameters()[0];

        String name = link.getName();

        ReplicationNode ownerNode = link.getSource();

        if (!ownerNode.equals(operationDef.getNode())) {
            throw new IllegalStateException(
                    t("topia.replication.error.operation.loadLink.illegalSource",
                      operationDef.getNode(), ownerNode));
        }

        if (log.isDebugEnabled()) {
            log.debug("currentNode : " + operationDef.getNode() +
                      " , link to load : " + link);
        }

        EntityOperator<? super TopiaEntity> ownerOperator =
                ownerNode.getOperator();

        for (TopiaEntity src : nodeEntities) {

            // on se contente de charger l'association
            int size = ownerOperator.sizeChild(name, src);
            if (log.isDebugEnabled()) {
                log.debug("load association '" + name + "' on " + src +
                          " : " + size);
            }
        }
    }
}
