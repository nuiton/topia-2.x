/*
 * #%L
 * ToPIA :: Service Replication
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.topia.replication.model;

import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaEntityEnum;
import org.nuiton.topia.replication.TopiaReplicationOperation;

import java.util.Arrays;

/**
 * Definition of a concrete operation to execute.
 * 
 * A such operation involves :
 * <ul>
 * <li>the replication node</li>
 * <li>the replication phase</li>
 * <li>the replication operation type</li>
 * <li>the replication operation arguments</li>
 * </ul>
 * 
 * This definition is detected when building replication model.
 * 
 * Then when starting replication, based on this definition, we can instanciate
 * the operation to execute.
 * 
 * <b>Note:</b> Such objects can be comparable via their {@link #phase}.
 *
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @since 2.2.0
 */
public class ReplicationOperationDef implements Comparable<ReplicationOperationDef> {

    protected final ReplicationOperationPhase phase;

    protected final Class<? extends TopiaReplicationOperation> operationClass;

    protected final ReplicationNode node;

    protected final Object[] parameters;

    public ReplicationOperationDef(
            ReplicationOperationPhase phase,
            Class<? extends TopiaReplicationOperation> operation,
            ReplicationNode node,
            Object... parameters) {
        operationClass = operation;
        this.phase = phase;
        this.node = node;
        this.parameters = parameters;
    }

    public ReplicationNode getNode() {
        return node;
    }

    public Object[] getParameters() {
        return parameters;
    }

    public Class<? extends TopiaReplicationOperation> getOperationClass() {
        return operationClass;
    }

    public ReplicationOperationPhase getPhase() {
        return phase;
    }

    public TopiaEntityEnum getContract() {
        return node.getContract();
    }

    public Class<? extends TopiaEntity> getEntityType() {
        return node.getEntityType();
    }

    @Override
    public String toString() {
        return " <" + operationClass.getSimpleName() + " on " + node +
               (parameters.length == 0 ? "" :
                ", params:" + Arrays.toString(parameters)) + ">";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ReplicationOperationDef that = (ReplicationOperationDef) o;

        if (!node.equals(that.node)) return false;
        if (!operationClass.equals(that.operationClass)) return false;
        return phase == that.phase;
    }

    @Override
    public int hashCode() {
        int result = phase.hashCode();
        result = 31 * result + operationClass.hashCode();
        result = 31 * result + node.hashCode();
        return result;
    }

    @Override
    public int compareTo(ReplicationOperationDef o) {
        int result = getPhase().compareTo(o.getPhase());
        return result;
    }
}
