/*
 * #%L
 * ToPIA :: Service Replication
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.topia.replication;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.*;
import org.nuiton.topia.TestHelper;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaContextFactory;
import org.nuiton.topia.TopiaTestDAOHelper.TopiaTestEntityEnum;
import org.nuiton.topia.framework.TopiaContextImplementor;
import org.nuiton.topia.persistence.TopiaEntityEnum;
import org.nuiton.topia.replication.model.ReplicationOperationPhase;
import org.nuiton.topia.replication.operation.*;
import org.nuiton.topia.test.entities.*;

import java.io.File;
import java.io.IOException;
import java.util.Properties;

/**
 * TopiaReplicationServiceImplTest on model TopiaTest
 * 
 * Created: 07 jun. 09 17:14:22
 *
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @since 2.2.0
 */
public class TopiaReplicationOperationTest extends AbstractTopiaReplicationServiceTest {

    /** Logger */
    private static final Log log =
            LogFactory.getLog(TopiaReplicationOperationTest.class);

    protected static final TopiaEntityEnum[] contracts = {
            TopiaTestEntityEnum.Person,
            TopiaTestEntityEnum.Pet,
            TopiaTestEntityEnum.Race
    };

    protected static final String entitiesList =
            PersonImpl.class.getName() + "," +
            PetImpl.class.getName() + "," +
            RaceImpl.class.getName();

    static protected Person person, person2;

    static protected Pet pet, pet2;

    static protected Race race, race2;

    protected static File tesDir;

    @BeforeClass
    public static void beforeClass() throws IOException {
        tesDir = TestHelper.getTestBasedir(TopiaReplicationOperationTest.class);

    }

    @AfterClass
    public static void after() throws Exception {
        AbstractTopiaReplicationServiceTest.after();
    }

    @Before
    @Override
    public void setUp() throws Exception {

        super.setUp();

        person = update(person);
        person2 = update(person2);
        pet = update(pet);
        pet2 = update(pet2);
        race = update(race);
        race2 = update(race2);
    }

    @After
    @Override
    public void tearDown() throws Exception {
        super.tearDown();
        if (dstCtxt != null && !dstCtxt.isClosed()) {
            dstCtxt.closeContext();
        }
    }

    @Test(expected = NullPointerException.class)
    public void testGetOperation_nullOperationClass() throws Exception {
        getModelBuilder().getOperationProvider().getOperation((Class<? extends TopiaReplicationOperation>) null);
    }

    protected TopiaReplicationModelBuilder getModelBuilder() {
        return service.getModelBuilder();
    }

    @Test
    @Override
    public void testGetOperation() throws Exception {

        getOperation(UnregistredOperation.class, false);
        getOperation(UncreatableOperation.class, true);
        getOperation(FakeOperation.class, true);
        getOperation(Duplicate.class, true);
//        getOperation(AttachAssociation.class, true);
        getOperation(DettachAssociation.class, true);
    }

    @Test(expected = NullPointerException.class)
    public void testCreateOperation_nullModel() throws Exception {
        getModelBuilder().createOperation(null, null, null, null);
    }

    @Test(expected = NullPointerException.class)
    public void testCreateOperation_nullType() throws Exception {

        model = getModelBuilder().createModel(context, contracts, true);
        getModelBuilder().createOperation(model, null, null, null);
    }

    @Test(expected = NullPointerException.class)
    public void testCreateOperation_nullPhase() throws Exception {

        model = getModelBuilder().createModel(context, contracts, true);
        getModelBuilder().createOperation(model, TopiaTestEntityEnum.Pet, null, null);
    }

    @Test(expected = NullPointerException.class)
    public void testCreateOperation_nullOperationClass() throws Exception {

        model = getModelBuilder().createModel(context, contracts, true);
        getModelBuilder().createOperation(model, TopiaTestEntityEnum.Pet, ReplicationOperationPhase.before, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateOperation_noNode() throws Exception {

        model = getModelBuilder().createModel(context, contracts, true);
        // le noeud Pet n'existe pas
        getModelBuilder().addAfterOperation(model, TopiaTestEntityEnum.Pet, Duplicate.class);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateOperation_noOperation() throws Exception {

        model = getModelBuilder().createModel(context, contracts, true);
        // le noeud Pet n'existe pas
        getModelBuilder().addAfterOperation(model, TopiaTestEntityEnum.Pet, UnregistredOperation.class);
    }

    @Test
//    @Test(expected = UnsupportedOperationException.class)
    public void testCreateSupportedBeforeOperation_Duplicate() throws Exception {
        createSupportedBeforeOperation(TopiaTestEntityEnum.Person, person, Duplicate.class);
    }

//    @Test(expected = UnsupportedOperationException.class)
//    public void testCreateUnsupportedBeforeOperation_AttachAssociation() throws Exception {
//        createUnsupportedBeforeOperation(TopiaTestEntityEnum.Person, person, AttachAssociation.class);
//    }

    @Test
//    @Test(expected = UnsupportedOperationException.class)
    public void testCreateSupportedBeforeOperation_DetachAssociation() throws Exception {
        createSupportedBeforeOperation(TopiaTestEntityEnum.Person, person, DettachAssociation.class);
    }

    @Test
//    @Test(expected = UnsupportedOperationException.class)
    public void testCreateSupportedAfterOperation_Duplicate() throws Exception {
        createSupportedAfterOperation(TopiaTestEntityEnum.Person, person, Duplicate.class);
    }

//    @Test(expected = UnsupportedOperationException.class)
//    public void testCreateSupportedAfterOperation_AttachAssociation() throws Exception {
//        createUnsupportedAfterOperation(TopiaTestEntityEnum.Person, person, AttachAssociation.class);
//    }

    @Test
//    @Test(expected = UnsupportedOperationException.class)
    public void testCreateSupportedAfterOperation_DetachAssociation() throws Exception {
        createSupportedAfterOperation(TopiaTestEntityEnum.Person, person, DettachAssociation.class);
    }

//    @Test(expected = UnsupportedOperationException.class)
//    public void testCreateUnsupportedBeforeOperation_UncreatableOperation() throws Exception {
//        createUnsupportedBeforeOperation(TopiaTestEntityEnum.Person, person, UncreatableOperation.class);
//    }

//    @Test(expected = UnsupportedOperationException.class)
//    public void testCreateUnsupportedAfterOperation_UncreatableOperation() throws Exception {
//        createUnsupportedAfterOperation(TopiaTestEntityEnum.Person, person, UncreatableOperation.class);
//    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateOperation_wrongParameterNumber() throws Exception {

        model = getModelBuilder().createModel(context, contracts, true, pet.getTopiaId());
        getModelBuilder().addBeforeOperation(model, TopiaTestEntityEnum.Pet, FakeOperation.class);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateOperation_wrongParameterNumber2() throws Exception {

        model = getModelBuilder().createModel(context, contracts, true, pet.getTopiaId());
        getModelBuilder().addBeforeOperation(model, TopiaTestEntityEnum.Pet, FakeOperation.class, String.class, String.class);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateOperation_wrongParameterType() throws Exception {

        model = getModelBuilder().createModel(context, contracts, true, pet.getTopiaId());
        getModelBuilder().addBeforeOperation(model, TopiaTestEntityEnum.Pet, FakeOperation.class, Integer.class);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateOperation_nullParameter() throws Exception {

        model = getModelBuilder().createModel(context, contracts, true, pet.getTopiaId());
        getModelBuilder().addBeforeOperation(model, TopiaTestEntityEnum.Pet, FakeOperation.class, (Object) null);
    }

    @Test
    public void testCreateOperation() throws Exception {

        model = getModelBuilder().createModel(context, contracts, true, pet.getTopiaId());
        getModelBuilder().addBeforeOperation(model, TopiaTestEntityEnum.Pet, FakeOperation.class, "before");
        getModelBuilder().addAfterOperation(model, TopiaTestEntityEnum.Race, FakeOperation.class, "after");
    }

    @Test(expected = NullPointerException.class)
    public void testDoReplicate_nullModel() throws Exception {

        service.doReplicate(null, null);
    }

    @Test(expected = NullPointerException.class)
    public void testDoReplicate_nullDstCtxt() throws Exception {

        model = getModelBuilder().createModel(context, contracts, true);
        service.doReplicate(model, null);
    }

    @Override
    protected TopiaContext createDb(String name) throws Exception {

//        File localDB = new File(getTestDir(getClass()), "db_" + name);

        Properties config = getH2Properties(name);

        context = TopiaContextFactory.getContext(config);

        TopiaContextImplementor tx =
                (TopiaContextImplementor) context.beginTransaction();

        person = tx.getDAO(Person.class).create(Person.PROPERTY_NAME, "pudding master");
        race = tx.getDAO(Race.class).create(Race.PROPERTY_NAME, "race I");
        pet = tx.getDAO(Pet.class).create(Pet.PROPERTY_NAME, "pudding", Pet.PROPERTY_PERSON, person, Pet.PROPERTY_RACE, race);

        person2 = tx.getDAO(Person.class).create(Person.PROPERTY_NAME, "pudding II master");
        pet2 = tx.getDAO(Pet.class).create(Pet.PROPERTY_NAME, "pudding II");
        race2 = tx.getDAO(Race.class).create(Race.PROPERTY_NAME, "race II");

        tx.commitTransaction();
        tx.closeContext();
        return context;
    }

    @Override
    protected TopiaEntityEnum[] getContracts() {
        return contracts;
    }

    @Override
    protected Log getLog() {
        return log;
    }

    protected Properties getH2Properties(String dbName) throws IOException {

        Properties config =  TestHelper.initTopiaContextConfiguration(tesDir, dbName);

//        config.setProperty("hibernate.show_sql", "false");
//        config.setProperty("hibernate.hbm2ddl.auto", "create");

        config.setProperty("topia.persistence.classes", entitiesList);
//        config.setProperty("hibernate.dialect", "org.hibernate.dialect.H2Dialect");
//        config.setProperty("hibernate.connection.driver_class", "org.h2.Driver");
//        config.setProperty("hibernate.connection.url", "jdbc:h2:file:" + f.getAbsolutePath() + ";create=true");
//        config.setProperty("hibernate.connection.username", "sa");
//        config.setProperty("hibernate.connection.password", "");

        config.setProperty("topia.service.replication", TopiaReplicationServiceImpl.class.getName());

        return config;
    }

    @Override
    protected TopiaContext createDb2(String name) throws Exception {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void testDetectTypes() throws Exception {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void testDetectAssociations() throws Exception {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void testDetectDirectDependencies() throws Exception {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void testDetectShell() throws Exception {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void testDetectDependencies() throws Exception {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void testDetectObjectsToDettach() throws Exception {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void testDetectOperations() throws Exception {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void testDoReplicate() throws Exception {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}


