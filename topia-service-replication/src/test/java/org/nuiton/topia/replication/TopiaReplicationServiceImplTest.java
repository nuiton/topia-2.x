/*
 * #%L
 * ToPIA :: Service Replication
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.topia.replication;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.nuiton.topia.TestHelper;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaContextFactory;
import org.nuiton.topia.TopiaException;
import org.nuiton.topia.TopiaTestDAOHelper.TopiaTestEntityEnum;
import org.nuiton.topia.framework.TopiaContextImplementor;
import org.nuiton.topia.persistence.TopiaEntityEnum;
import org.nuiton.topia.persistence.util.TopiaEntityIdsMap;
import org.nuiton.topia.replication.model.ReplicationModel;
import org.nuiton.topia.replication.operation.AttachAssociation;
import org.nuiton.topia.replication.operation.DettachAssociation;
import org.nuiton.topia.replication.operation.Duplicate;
import org.nuiton.topia.replication.operation.FakeOperation;
import org.nuiton.topia.replication.operation.UncreatableOperation;
import org.nuiton.topia.replication.operation.UnregistredOperation;
import org.nuiton.topia.test.entities.Person;
import org.nuiton.topia.test.entities.PersonImpl;
import org.nuiton.topia.test.entities.Pet;
import org.nuiton.topia.test.entities.PetImpl;
import org.nuiton.topia.test.entities.Race;
import org.nuiton.topia.test.entities.RaceImpl;

import java.io.File;
import java.io.IOException;
import java.util.Properties;

/**
 * TopiaReplicationServiceImplTest on model TopiaTest
 * 
 * Created: 07 jun. 09 17:14:22
 *
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @since 2.2.0
 */
public class TopiaReplicationServiceImplTest extends AbstractTopiaReplicationServiceTest {

    /** Logger */
    private static final Log log =
            LogFactory.getLog(TopiaReplicationServiceImplTest.class);

    protected static final TopiaEntityEnum[] contracts = {TopiaTestEntityEnum.Person, TopiaTestEntityEnum.Pet, TopiaTestEntityEnum.Race};

    protected static final String entitiesList = PersonImpl.class.getName() + "," + PetImpl.class.getName() + "," + RaceImpl.class.getName();

    static protected Person person, person2;

    static protected Pet pet, pet2, pet3;

    static protected Race race, race2, race3;

    protected static File tesDir;

    @BeforeClass
    public static void beforeClass() throws IOException {
        tesDir = TestHelper.getTestBasedir(
                TopiaReplicationServiceImplTest.class);
    }

    @AfterClass
    public static void after() throws Exception {
        AbstractTopiaReplicationServiceTest.after();
    }

    @Before
    @Override
    public void setUp() throws Exception {

        super.setUp();

        person = update(person);
        person2 = update(person2);
        pet = update(pet);
        pet2 = update(pet2);
        race = update(race);
        race2 = update(race2);
        race3 = update(race3);
    }

    @After
    @Override
    public void tearDown() throws Exception {
        super.tearDown();
        if (dstCtxt != null && !dstCtxt.isClosed()) {
            dstCtxt.closeContext();
        }
    }

    @Test
    @Override
    public void testDetectTypes() throws Exception {

        detectTypes(race, Race.class);
        detectTypes(pet, Pet.class, Person.class, Race.class);
        detectTypes(person, Pet.class, Person.class, Race.class);

        detectTypes(pet2, Pet.class);
        detectTypes(person2, Person.class);
        detectTypes(race2, Race.class);

        detectTypes(race3, Race.class);
        detectTypes(pet3, Pet.class, Race.class);
    }

    @Test
    @Override
    public void testGetOperation() throws Exception {

        getOperation(UnregistredOperation.class, false);
        getOperation(UncreatableOperation.class, true);
        getOperation(FakeOperation.class, true);
        getOperation(Duplicate.class, true);
        getOperation(AttachAssociation.class, true);
        getOperation(DettachAssociation.class, true);
    }

    @Test
    @Override
    public void testDetectAssociations() throws Exception {

        detectAssociations(person, TopiaTestEntityEnum.Person, Person.PROPERTY_PET);
        detectAssociations(race);
        detectAssociations(pet);

        detectAssociations(person2);
        detectAssociations(race2);
        detectAssociations(pet2);
    }

    @Test
    @Override
    public void testDetectDirectDependencies() throws Exception {

        detectDirectDependencies(person);
        detectDirectDependencies(race);
        detectDirectDependencies(pet, TopiaTestEntityEnum.Pet, Pet.PROPERTY_PERSON, TopiaTestEntityEnum.Pet, Pet.PROPERTY_RACE);

        detectDirectDependencies(person2);
        detectDirectDependencies(race2);
        detectDirectDependencies(pet2);
    }

    @Test
    @Override
    public void testDetectShell() throws Exception {

        detectShell(person, TopiaTestEntityEnum.Pet, TopiaTestEntityEnum.Race);
        detectShell(race);
        detectShell(pet, TopiaTestEntityEnum.Person, TopiaTestEntityEnum.Race);
        detectShell(person2);
        detectShell(race2);
        detectShell(pet2);
    }

    @Test
    @Override
    public void testDetectDependencies() throws Exception {

        detectDependencies(person, new TopiaTestEntityEnum[]{TopiaTestEntityEnum.Race}, new TopiaTestEntityEnum[]{TopiaTestEntityEnum.Person}, new TopiaTestEntityEnum[]{TopiaTestEntityEnum.Pet});
        detectDependencies(race, new TopiaTestEntityEnum[]{TopiaTestEntityEnum.Race});
        detectDependencies(pet, new TopiaTestEntityEnum[]{TopiaTestEntityEnum.Race}, new TopiaTestEntityEnum[]{TopiaTestEntityEnum.Person}, new TopiaTestEntityEnum[]{TopiaTestEntityEnum.Pet});

        detectDependencies(person2, new TopiaTestEntityEnum[]{TopiaTestEntityEnum.Person});
        detectDependencies(race2, new TopiaTestEntityEnum[]{TopiaTestEntityEnum.Race});
        detectDependencies(pet2, new TopiaTestEntityEnum[]{TopiaTestEntityEnum.Pet});
    }

    @Test
    @Override
    public void testDetectObjectsToDettach() throws Exception {

        detectObjectsToDettach(person, TopiaTestEntityEnum.Person, new String[]{Person.PROPERTY_PET});
        detectObjectsToDettach(race);
        detectObjectsToDettach(pet, TopiaTestEntityEnum.Person, new String[]{Person.PROPERTY_PET});

        detectObjectsToDettach(person2);
        detectObjectsToDettach(race2);
        detectObjectsToDettach(pet2);

        detectObjectsToDettach(race3);
        detectObjectsToDettach(pet3);
    }

    @Test
    @Override
    public void testDetectOperations() throws Exception {

        //TODO Make some real test on detected operations...

        detectOperations(person);
        detectOperations(pet);
        detectOperations(race);

        detectOperations(person2);
        detectOperations(pet2);
        detectOperations(race2);

        detectOperations(race3);
        detectOperations(pet3);
    }

    @Test
    @Override
    public void testDoReplicate() throws Exception {

        doReplicate(TopiaTestEntityEnum.Person, person);
        doReplicate(TopiaTestEntityEnum.Person, person2);
        doReplicate(TopiaTestEntityEnum.Person, person, person2);

        doReplicate(TopiaTestEntityEnum.Pet, pet);
        doReplicate(TopiaTestEntityEnum.Pet, pet2);
        doReplicate(TopiaTestEntityEnum.Pet, pet, pet2, pet3);
        doReplicate(TopiaTestEntityEnum.Pet, person2, pet3);

        doReplicate(TopiaTestEntityEnum.Race, race);
        doReplicate(TopiaTestEntityEnum.Race, race2);
        doReplicate(TopiaTestEntityEnum.Race, race, race2);

    }

    /**
     * Cette methode montre pourquoi la simple replication ne peut pas
     * fonctionne :)
     * 
     * Le replicateur ne deplique pas dans le bon ordre et on a donc des
     * violations de clef etrangeres...
     *
     * @throws Exception pour toute erreur
     */
    @Test(expected = TopiaException.class)
    public void testSimpleReplicateFailed() throws Exception {

        TopiaContext dstRootCtxt = createDb2("testSimpleReplicateFailed");

        //model = service.prepare(contracts, pet.getTopiaId());

        TopiaContext srcCtxt = ctxt.beginTransaction();
        dstCtxt = (TopiaContextImplementor) dstRootCtxt.beginTransaction();

        try {

            srcCtxt.replicateEntity(dstCtxt, pet);

            dstCtxt.commitTransaction();

        } finally {
            srcCtxt.rollbackTransaction();
            srcCtxt.closeContext();
            dstCtxt.closeContext();
        }
    }

    /**
     * Cette methode montre comment manuellement on peut effectuer la
     * replication (en dettachant les dependances qui forment des cycles)
     * 
     * La methode utilisee ici peut ne pas fonctionner : si une clef metier est
     * posee sur une dependance alors cela ne fonctionne pas.
     *
     * @throws Exception pour toute erreur
     */
    @Test
    public void testSimpleReplicateNotSure() throws Exception {

        TopiaContext dstRootCtxt = createDb2("testSimpleReplicateNotSure");

        //model = service.prepare(contracts, pet.getTopiaId());

        TopiaContext srcCtxt = ctxt;
        dstCtxt = (TopiaContextImplementor) dstRootCtxt.beginTransaction();

        try {


            srcCtxt.replicateEntity(dstCtxt, race);

            // on dettache l'entite qui pose probleme

            pet.setPerson(null);
            srcCtxt.replicateEntity(dstCtxt, pet);
            srcCtxt.rollbackTransaction();

            srcCtxt.replicateEntity(dstCtxt, person);

            dstCtxt.commitTransaction();
            ((Pet) dstCtxt.findByTopiaId(pet.getTopiaId())).setPerson((Person) dstCtxt.findByTopiaId(person.getTopiaId()));
            dstCtxt.commitTransaction();

            srcCtxt.rollbackTransaction();
            person = update(person);

            assertEntityEquals(person, dstCtxt.findByTopiaId(person.getTopiaId()), null);
        } finally {
            srcCtxt.rollbackTransaction();
            //srcCtxt.closeContext();
            dstCtxt.closeContext();
        }
    }

    /**
     * Cette methode montre comment manuellement on peut effectuer la
     * replication (en dettachant les associations qui forment des cycles)
     * 
     * La methode utilisee ici fonctionne mieux que la precedante : il parrait
     * dificille de pose une une clef metier sur une association :).
     * 
     * On remarque que l'on dettache l'assocation qui forme un cycle et que l'on
     * est pas obligee de la reattachee car elle est bi-directionnelle.
     * 
     * On doit optimiser l'algorithme dans la methode {@link
     * ReplicationModel#adjustOperations(TopiaEntityIdsMap)}.
     *
     * @throws Exception pour toute erreur
     */
    @Test
    public void testSimpleReplicateSure() throws Exception {

        TopiaContext dstRootCtxt = createDb2("testSimpleReplicateSure");

        //model = service.prepare(contracts, pet.getTopiaId());

        TopiaContext srcCtxt = ctxt;
        dstCtxt = (TopiaContextImplementor) dstRootCtxt.beginTransaction();

        try {

            srcCtxt.replicateEntity(dstCtxt, race);
            // on dettache l'association qui pose probleme
            person.setPet(null);
            srcCtxt.replicateEntity(dstCtxt, person);

            srcCtxt.replicateEntity(dstCtxt, pet);
            srcCtxt.rollbackTransaction();
            dstCtxt.commitTransaction();

            //((Person) dstCtxt.findByTopiaId(person.getTopiaId())).addPet(((Pet) dstCtxt.findByTopiaId(pet.getTopiaId())));
            //dstCtxt.commitTransaction();

            srcCtxt.rollbackTransaction();

            srcCtxt.closeContext();
            dstCtxt.closeContext();

            ctxt = context.beginTransaction();
            dstCtxt = (TopiaContextImplementor) dstRootCtxt.beginTransaction();

            person = update(person);

            assertEntityEquals(person, dstCtxt.findByTopiaId(person.getTopiaId()), null);

        } finally {
            dstCtxt.closeContext();
        }
    }

    @Override
    protected TopiaContext createDb(String name) throws Exception {

//        File localDB = new File(getTestDir(getClass()), "db_" + name);

        Properties config = getH2Properties(name);

        context = TopiaContextFactory.getContext(config);

        TopiaContextImplementor tx = (TopiaContextImplementor) context.beginTransaction();

        person = tx.getDAO(Person.class).create(Person.PROPERTY_NAME, "pudding master");
        race = tx.getDAO(Race.class).create(Race.PROPERTY_NAME, "race I");
        pet = tx.getDAO(Pet.class).create(Pet.PROPERTY_NAME, "pudding", Pet.PROPERTY_PERSON, person, Pet.PROPERTY_RACE, race);

        person2 = tx.getDAO(Person.class).create(Person.PROPERTY_NAME, "pudding II master");
        pet2 = tx.getDAO(Pet.class).create(Pet.PROPERTY_NAME, "pudding II");
        race2 = tx.getDAO(Race.class).create(Race.PROPERTY_NAME, "race II");

        race3 = tx.getDAO(Race.class).create(Race.PROPERTY_NAME, "race III");
        pet3 = tx.getDAO(Pet.class).create(Pet.PROPERTY_NAME, "pudding III", Pet.PROPERTY_RACE, race3);

        tx.commitTransaction();
        tx.closeContext();
        return context;
    }

    @Override
    protected TopiaContext createDb2(String name) throws Exception {

//        File localDB = new File(getTestDir(getClass()), "db_" + name);
//
//        log.info("db dir :\n" + localDB.getAbsolutePath());

        Properties config = getH2Properties(name);

        return TopiaContextFactory.getContext(config);
    }

    @Override
    protected TopiaEntityEnum[] getContracts() {
        return contracts;
    }

    @Override
    protected Log getLog() {
        return log;
    }

    protected Properties getH2Properties(String dbName) throws IOException {


        Properties config = TestHelper.initTopiaContextConfiguration(tesDir, dbName);

//        config.setProperty("hibernate.show_sql", "false");
//        config.setProperty("hibernate.hbm2ddl.auto", "create");

        config.setProperty("topia.persistence.classes", entitiesList);
//        config.setProperty("hibernate.dialect", "org.hibernate.dialect.H2Dialect");
//        config.setProperty("hibernate.connection.driver_class", "org.h2.Driver");
//        config.setProperty("hibernate.connection.url", "jdbc:h2:file:" + f.getAbsolutePath() + "/db;create=true");
//        config.setProperty("hibernate.connection.username", "sa");
//        config.setProperty("hibernate.connection.password", "");

        config.setProperty(TopiaReplicationServiceImpl.TOPIA_SERVICE_NAME, TopiaReplicationServiceImpl.class.getName());

        return config;
    }
}


