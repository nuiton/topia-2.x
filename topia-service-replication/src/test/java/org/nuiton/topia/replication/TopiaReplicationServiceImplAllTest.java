/*
 * #%L
 * ToPIA :: Service Replication
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.topia.replication;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.nuiton.topia.TestHelper;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaContextFactory;
import org.nuiton.topia.TopiaException;
import org.nuiton.topia.TopiaTestDAOHelper.TopiaTestEntityEnum;
import org.nuiton.topia.framework.TopiaContextImplementor;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaEntityEnum;
import org.nuiton.topia.test.entities.Person;
import org.nuiton.topia.test.entities.PersonImpl;
import org.nuiton.topia.test.entities.Pet;
import org.nuiton.topia.test.entities.PetImpl;
import org.nuiton.topia.test.entities.Race;
import org.nuiton.topia.test.entities.RaceImpl;

import java.io.File;
import java.io.IOException;
import java.util.Properties;

/**
 * TopiaReplicationServiceImplTest on model TopiaTest
 * 
 * Created: 07 jun. 09 17:14:22
 *
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @since 2.2.0
 */
public class TopiaReplicationServiceImplAllTest extends AbstractTopiaReplicationServiceTest {

    /** Logger */
    private static final Log log = LogFactory.getLog(TopiaReplicationServiceImplTest.class);

    protected static final TopiaEntityEnum[] contracts = {
            TopiaTestEntityEnum.Person,
            TopiaTestEntityEnum.Pet,
            TopiaTestEntityEnum.Race
    };

    protected static final String entitiesList =
            PersonImpl.class.getName() + "," +
            PetImpl.class.getName() + "," +
            RaceImpl.class.getName();

    static protected Person person, person2;

    static protected Pet pet, pet2, pet3;

    static protected Race race, race2, race3;

    protected static File tesDir;

    @BeforeClass
    public static void beforeClass() throws IOException {
        tesDir = TestHelper.getTestBasedir(TopiaReplicationServiceImplAllTest.class);
    }
    
    @AfterClass
    public static void after() throws Exception {
        AbstractTopiaReplicationServiceTest.after();
    }

    @Before
    @Override
    public void setUp() throws Exception {

        super.setUp();

        person = update(person);
        person2 = update(person2);
        pet = update(pet);
        pet2 = update(pet2);
        race = update(race);
        race2 = update(race2);
        race3 = update(race3);
    }

    @After
    @Override
    public void tearDown() throws Exception {
        super.tearDown();
        if (dstCtxt != null && !dstCtxt.isClosed()) {
            dstCtxt.closeContext();
        }
    }

//    @Ignore

    @Test
    @Override
    public void testDetectTypes() throws Exception {

        detectTypes(race, Race.class);
        detectTypes(pet, Pet.class, Person.class, Race.class);
        detectTypes(person, Pet.class, Person.class, Race.class);

        detectTypes(pet2, Pet.class);
        detectTypes(person2, Person.class);
        detectTypes(race2, Race.class);

        detectTypes(race3, Race.class);
        detectTypes(pet3, Pet.class, Race.class);
    }

//    @Ignore

    @Test
    @Override
    public void testGetOperation() throws Exception {
    }

//    @Ignore

    @Test
    @Override
    public void testDetectAssociations() throws Exception {

        detectAssociations(person, TopiaTestEntityEnum.Person, Person.PROPERTY_PET);
        detectAssociations(race);
        detectAssociations(pet);

        detectAssociations(person2);
        detectAssociations(race2);
        detectAssociations(pet2);

    }

//    @Ignore

    @Test
    @Override
    public void testDetectDirectDependencies() throws Exception {

        detectDirectDependencies(person);
        detectDirectDependencies(race);
        detectDirectDependencies(pet, TopiaTestEntityEnum.Pet, Pet.PROPERTY_PERSON, TopiaTestEntityEnum.Pet, Pet.PROPERTY_RACE);

        detectDirectDependencies(person2);
        detectDirectDependencies(race2);
        detectDirectDependencies(pet2);
    }

//    @Ignore

    @Test
    @Override
    public void testDetectShell() throws Exception {

        detectShell(person, TopiaTestEntityEnum.Pet, TopiaTestEntityEnum.Race);
        detectShell(race);
        detectShell(pet, TopiaTestEntityEnum.Person, TopiaTestEntityEnum.Race);
        detectShell(person2, TopiaTestEntityEnum.Pet, TopiaTestEntityEnum.Race);
        detectShell(race2);
        detectShell(pet2, TopiaTestEntityEnum.Person, TopiaTestEntityEnum.Race);
    }

//    @Ignore

    @Test
    @Override
    public void testDetectDependencies() throws Exception {

        detectDependencies(null,
                           new TopiaTestEntityEnum[]{TopiaTestEntityEnum.Race}, new TopiaTestEntityEnum[]{TopiaTestEntityEnum.Person}, new TopiaTestEntityEnum[]{TopiaTestEntityEnum.Pet});
    }

//    @Ignore

    @Test
    @Override
    public void testDetectObjectsToDettach() throws Exception {

        detectObjectsToDettach(null, TopiaTestEntityEnum.Person, new String[]{Person.PROPERTY_PET});
    }

//    @Ignore

    @Test
    @Override
    public void testDetectOperations() throws Exception {

        detectOperations(null);
    }

//    @Ignore

    @Test
    @Override
    public void testDoReplicate() throws Exception {

        doReplicateAll();

    }

    @Override
    protected TopiaContext createDb(String name) throws Exception {

//        File localDB = new File(getTestDir(getClass()), "db_" + name);

        Properties config = getH2Properties(name);

        context = TopiaContextFactory.getContext(config);

        TopiaContextImplementor tx = (TopiaContextImplementor) context.beginTransaction();

        person = tx.getDAO(Person.class).create(Person.PROPERTY_NAME, "pudding master");
        race = tx.getDAO(Race.class).create(Race.PROPERTY_NAME, "race I");
        pet = tx.getDAO(Pet.class).create(Pet.PROPERTY_NAME, "pudding", Pet.PROPERTY_PERSON, person, Pet.PROPERTY_RACE, race);

        person2 = tx.getDAO(Person.class).create(Person.PROPERTY_NAME, "pudding II master");
        pet2 = tx.getDAO(Pet.class).create(Pet.PROPERTY_NAME, "pudding II");
        race2 = tx.getDAO(Race.class).create(Race.PROPERTY_NAME, "race II");

        race3 = tx.getDAO(Race.class).create(Race.PROPERTY_NAME, "race III");
        pet3 = tx.getDAO(Pet.class).create(Pet.PROPERTY_NAME, "pudding III", Pet.PROPERTY_RACE, race3);

        tx.commitTransaction();
        tx.closeContext();
        return context;
    }

    @Override
    protected TopiaContext createDb2(String name) throws Exception {

//        File localDB = new File(getTestDir(getClass()), "db_" + name);

        Properties config = getH2Properties(name);

        return TopiaContextFactory.getContext(config);
    }

    @Override
    protected TopiaEntityEnum[] getContracts() {
        return contracts;
    }

    @Override
    protected Log getLog() {
        return log;
    }

    protected Properties getH2Properties(String dbName) throws IOException {

        Properties config =  TestHelper.initTopiaContextConfiguration(tesDir, dbName);

//        config.setProperty("hibernate.show_sql", "false");
//        config.setProperty("hibernate.hbm2ddl.auto", "create");

        config.setProperty("topia.persistence.classes", entitiesList);
//        config.setProperty("hibernate.dialect", "org.hibernate.dialect.H2Dialect");
//        config.setProperty("hibernate.connection.driver_class", "org.h2.Driver");
//        config.setProperty("hibernate.connection.url", "jdbc:h2:file:" + f.getAbsolutePath() + ";create=true");
//        config.setProperty("hibernate.connection.username", "sa");
//        config.setProperty("hibernate.connection.password", "");

        config.setProperty(TopiaReplicationServiceImpl.TOPIA_SERVICE_NAME, TopiaReplicationServiceImpl.class.getName());

        return config;
    }


    @Override
    protected void createModel(TopiaEntity entity) throws TopiaException {
        model = getModelBuilder().createModelForAll(getContracts());
    }

    @Override
    protected void prepareModel(String... ids) throws TopiaException {
        model = service.prepareForAll(getContracts());
    }

}


